//
//  FBRecommendationViewController.h
//

#import <UIKit/UIKit.h>

@interface FBRecommendationViewController : UIViewController
@property (nonatomic, retain) NSArray *pois;
@property (nonatomic, retain) NSString *poisSourceStringEnum;
@end
