//
//  OXReportButtonPresentationViewController.m
//  Ocean
//
//  Created by Pavan Kataria on 20/04/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXReportButtonPresentationViewController.h"

@interface OXReportButtonPresentationViewController ()

@end

@implementation OXReportButtonPresentationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.reportButton.layer setCornerRadius:self.reportButton.frame.size.height/2];
    [self.reportButton.layer setBorderColor:[UIColor colorWithRed:0.89 green:0.89 blue:0.89 alpha:1].CGColor];
    [self.reportButton.layer setBorderWidth:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)reportButtonPressed:(id)sender{
    NSLog(@"report button pressed");
    if([self.delegate respondsToSelector:@selector(reportButtonPressed)]){
        [self.delegate reportButtonPressed];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
