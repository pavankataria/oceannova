//
//  OXSignupPageTableViewController.h
//  Ocean
//
//  Created by Pavan Kataria on 06/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXSignUpTableViewCell.h"

@interface OXSignupPageTableViewController : UITableViewController<OXSignUpTableViewCellProtocol>
@property (nonatomic, retain) NSDictionary *facebookUserProfileDictionary;

@property (nonatomic, assign) OXSignUpVCTableViewType displayType;
@end
