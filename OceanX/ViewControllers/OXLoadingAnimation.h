//
//  OXLoadingAnimation.h
//  Testing Animation
//
//  Created by admin on 06/03/2015.
//  Copyright (c) 2015 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXLoadingAnimation : UIView


-(id)init; //runs with the default image of Ocean logo. You need to set frame manually

-(id)initWithImage:(UIImage *)image; //pick your own image!

-(id)initWithFrame:(CGRect)frame andImage:(UIImage *)image; //pick your own frame and image! if image is nil, sets default image

-(void)centerAnimation; //center animation after frame change

-(void)beginAnimation;
-(void)hideAnimation;
-(void)showAnimation;
@end
