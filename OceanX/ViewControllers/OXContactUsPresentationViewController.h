//
//  OXContactUsPresentationViewController.h
//  Ocean
//
//  Created by Pavan Kataria on 17/04/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXDetailPOIProtocol.h"

@interface OXContactUsPresentationViewController : UIViewController

@property (nonatomic, weak) id <OXDetailPOIProtocol> delegate;
@property (nonatomic, retain) OXPOIModel *poiObject;

@property (nonatomic, retain) UILabel *contactUsLabel;

@property (nonatomic, retain) UIButton *telephoneButton;
@property (nonatomic, retain) UIButton *emailButton;
@property (nonatomic, retain) UIButton *facebookButton;
@property (nonatomic, retain) UIButton *twitterButton;

@end