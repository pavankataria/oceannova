//
//  OXReportButtonPresentationViewController.h
//  Ocean
//
//  Created by Pavan Kataria on 20/04/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXDetailPOIProtocol.h"

@interface OXReportButtonPresentationViewController : UIViewController
@property (nonatomic, weak) IBOutlet UIButton *reportButton;
@property (nonatomic, retain) OXPOIModel *poiObject;
@property (nonatomic, weak) id <OXDetailPOIProtocol> delegate;

-(IBAction)reportButtonPressed:(id)sender;

@end
