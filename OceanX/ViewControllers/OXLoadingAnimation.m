//
//  OXLoadingAnimation.m
//  Testing Animation
//
//  Created by admin on 06/03/2015.
//  Copyright (c) 2015 admin. All rights reserved.
//

#import "OXLoadingAnimation.h"

const CGFloat shortDuration = 0.4;
const CGFloat longDuration = 0.6;
const CGFloat largeSize = 2.5;
const CGFloat smallSize = 1.4;

@interface OXLoadingAnimation ()

@property (nonatomic, assign) BOOL startedLongContraction;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImageView *animatedView;


@end



@implementation OXLoadingAnimation


-(id)init
{
    self = [super init];
    
    if(self){
        
        self.image = [UIImage imageNamed:@"oceanIconLoading"];
        
    }
    
    return self;
}


-(id)initWithFrame:(CGRect)frame andImage:(UIImage *)image
{
    self = [super init];
    
    if(self){
        
        self.frame = frame;
        
        if (image) {
            self.image = image;
        }else{
            
            self.image = [UIImage imageNamed:@"oceanIconLoading"];
        }
        
    }
    
    return self;
    
}

-(id)initWithImage:(UIImage *)image
{
    self = [super init];
    
    if(self){
        
        self.frame = self.superview.frame;
        self.image = image;
        
    }
    
    return self;
}

-(void)centerAnimation
{
    self.animatedView.center = self.center;
}

-(void)beginAnimation
{
    
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    
    self.animatedView = [[UIImageView alloc] initWithImage:self.image];
    self.animatedView.center = self.center;
    [self addSubview:self.animatedView];
    
    self.startedLongContraction = FALSE;
    
    [self pulse:self.animatedView toSize:largeSize withDuration:longDuration];
    
}

-(void)pulse:(UIView*)view toSize: (float)size withDuration:(float) duration
{
    
//    NSLog(@"%@", self.startedLongContraction  == YES ? @"startedLongContraction: YES" : @"startedLongContraction: NO" );
    CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulseAnimation.duration = duration;
    pulseAnimation.toValue = [NSNumber numberWithFloat:size];
    pulseAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    pulseAnimation.autoreverses = YES;
    pulseAnimation.repeatCount = 1;
    pulseAnimation.removedOnCompletion = NO;
    
    self.startedLongContraction = !self.startedLongContraction;
    
    if (self.startedLongContraction) {
        
        duration = shortDuration;
        size = smallSize;
        
    }else{
        
        duration = longDuration;
        size = largeSize;
    }
    
    

//    [CATransaction setCompletionBlock:^{
    
        //[self performSelector:@selector(startPulse) withObject:self afterDelay:0.5];
//        NSLog(@"animation ended!");
//        [self pulse:view toSize:size withDuration:duration];
        
//    }];
    [view.layer addAnimation:pulseAnimation forKey:nil];

    
    
    
}

-(void)showAnimation
{
    if (self.hidden == TRUE) {
        self.hidden = !self.hidden;
    }
}

-(void)hideAnimation
{
    if (self.hidden == FALSE) {
        self.hidden = !self.hidden;
    }
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
