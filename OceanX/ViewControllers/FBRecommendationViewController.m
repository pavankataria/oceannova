//
//  FBRecommendationViewController.,
//

#import "FBRecommendationViewController.h"
#import "ZLSwipeableView.h"
#import "UIColor+FlatColors.h"
#import "CardView.h"
#import "OXDetailPOIViewController.h"

@interface FBRecommendationViewController () <ZLSwipeableViewDataSource,
ZLSwipeableViewDelegate, UIActionSheetDelegate>{
    UIStoryboard * poiDetailSB;
}

@property (nonatomic, weak) IBOutlet ZLSwipeableView *swipeableView;


@property (nonatomic) NSUInteger colorIndex;

@property (nonatomic) BOOL loadCardFromXib;
@property (nonatomic, strong) NSMutableArray *poisMutable;

@end

@implementation FBRecommendationViewController
//-(void)setPois:(NSArray *)pois{
//    _pois = [pois mutableCopy];
//    _poisMutable = [[NSMutableArray alloc] init];
//    
////    for(OXPOISearchByListModel *poi in pois){
////        NSLog(@"poi pictures: %@", ((OXProcessedPicture*)[poi.pictures firstObject]).pictureURL);
////        Person *person = [[Person alloc] initWithName:[NSString stringWithFormat:@"%@", poi.poiName]
////                                       imageURLString:((OXProcessedPicture*)[poi.pictures firstObject]).pictureURL];
//
////        [_poisMutable addObject:person];
////    }
//    _poisMutable = [[OXPOISearchByListModel setupSearchResultsArrayWithResponse:pois] mutableCopy];
//}


-(void)setPois:(NSArray *)pois{
    
    _pois = [pois mutableCopy];
    _poisMutable = [[NSMutableArray alloc] init];
    
//    for(OXPOISearchByListModel *poi in pois){
//        NSLog(@"poi picfacetures: %@", ((OXProcessedPicture*)[poi.pictures firstObject]).pictureURL);
//        Person *person = [[Person alloc] initWithName:[NSString stringWithFormat:@"%@", poi.poiName]
//                                       imageURLString:((OXProcessedPicture*)[poi.pictures firstObject]).pictureURL];
//        [_poisMutable addObject:person];
//    }
    _poisMutable = [pois mutableCopy];
    
    for(OXPOISearchByListModel *poi in self.poisMutable){
        NSLog(@"poi name: %@", poi.poiName);
    }

    NSLog(@"pois facebook recommendation: %@", self.poisMutable);

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    poiDetailSB = [UIStoryboard storyboardWithName:@"OXPOIDetailSB" bundle:nil];

    // Do any additional setup after loading the view, typically from a nib.
    self.colorIndex = 0;
    
    // Optional Delegate

    self.loadCardFromXib = 1;

}

- (void)viewDidLayoutSubviews {
    // Required Data Source
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.swipeableView.delegate = self;
    self.swipeableView.dataSource = self;


}
#pragma mark - Action

-(void)swipeableView:(ZLSwipeableView *)swipeableView didTapSwipingView:(UIView *)view atLocation:(CGPoint)location{
    NSLog(@"swipeable view tapped: subviews: %@", [view subviews]);
    CardView *card = (CardView*)[[view subviews] firstObject];
    OXPOISearchByListModel *poi = card.poi;
    OXDetailPOIViewController *destinationVC = [poiDetailSB instantiateInitialViewController];
    destinationVC.poiID = poi.poiId;
    destinationVC.parentPOIObject = poi;
    
//    NSLog(@"trying to remove back button");
//    self.title = @"";
//    
//    
    [self.navigationController pushViewController:destinationVC animated:YES];
}
- (IBAction)swipeLeftButtonAction:(UIButton *)sender {
    [self.swipeableView swipeTopViewToLeft];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];

}
-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [super viewWillDisappear:animated];
    
}


- (IBAction)swipeRightButtonAction:(UIButton *)sender {
    [self.swipeableView swipeTopViewToRight];
}
- (IBAction)swipeUpButtonAction:(UIButton *)sender {
    [self.swipeableView swipeTopViewToUp];
}
- (IBAction)swipeDownButtonAction:(UIButton *)sender {
    [self.swipeableView swipeTopViewToDown];
}

- (IBAction)reloadButtonAction:(UIButton *)sender {
    self.colorIndex = 0;
    
    [self.swipeableView discardAllSwipeableViews];
    [self.swipeableView loadNextSwipeableViewsIfNeeded];
    
    /*
     UIActionSheet *actionSheet = [[UIActionSheet alloc]
     initWithTitle:@"Load Cards"
     delegate:self
     cancelButtonTitle:@"Cancel"
     destructiveButtonTitle:nil
     otherButtonTitles:@"Programmatically", @"From Xib", nil];
     [actionSheet showInView:self.view];
     */
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet
clickedButtonAtIndex:(NSInteger)buttonIndex {
    self.loadCardFromXib = buttonIndex == 1;
    
    self.colorIndex = 0;
    
    [self.swipeableView discardAllSwipeableViews];
    [self.swipeableView loadNextSwipeableViewsIfNeeded];
}

#pragma mark - ZLSwipeableViewDelegate

- (void)swipeableView:(ZLSwipeableView *)swipeableView
         didSwipeView:(UIView *)view
          inDirection:(ZLSwipeableViewDirection)direction {
    NSLog(@"did swipe in direction: %zd", direction);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
       didCancelSwipe:(UIView *)view {
    NSLog(@"did cancel swipe");
}


#pragma mark - ZLSwipeableViewDataSource

- (UIView *)nextViewForSwipeableView:(ZLSwipeableView *)swipeableView {
    NSLog(@"NEXT SWIPEABLE VIEW CALLED");

    if (self.colorIndex < self.pois.count) {
        NSLog(@"NEXT SWIPEABLE VIEW CALLED");

        CardView *view = [[CardView alloc] initWithFrame:swipeableView.bounds];
        CardView *contentView =
        [[[NSBundle mainBundle] loadNibNamed:@"CardContentView"
                                       owner:self
                                     options:nil] objectAtIndex:0];
        contentView.translatesAutoresizingMaskIntoConstraints = NO;
        [view addSubview:contentView];
//        [contentView performSelector:@selector(setValues:) withObject:self.pois[self.colorIndex]];
        [contentView setPoi:self.pois[self.colorIndex]];
        contentView.poisSourceStringEnum = self.poisSourceStringEnum;
        
        // This is important:
        // https://github.com/zhxnlai/ZLSwipeableView/issues/9
        NSDictionary *metrics = @{
                                  @"height" : @(view.bounds.size.height),
                                  @"width" : @(view.bounds.size.width)
                                  };
        NSDictionary *views = NSDictionaryOfVariableBindings(contentView);
        [view addConstraints:
         [NSLayoutConstraint
          constraintsWithVisualFormat:@"H:|[contentView(width)]"
          options:0
          metrics:metrics
          views:views]];
        [view addConstraints:[NSLayoutConstraint
                              constraintsWithVisualFormat:
                              @"V:|[contentView(height)]"
                              options:0
                              metrics:metrics
                              views:views]];
        
        self.colorIndex++;

        return view;
    }
    return nil;
}
-(void)swipeableViewNoMoreViewsToSwipe{
    NSLog(@"show no more");
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - ()

- (UIColor *)colorForName:(NSString *)name {
    NSString *sanitizedName =
    [name stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *selectorString =
    [NSString stringWithFormat:@"flat%@Color", sanitizedName];
    Class colorClass = [UIColor class];
    return [colorClass performSelector:NSSelectorFromString(selectorString)];
}
@end
