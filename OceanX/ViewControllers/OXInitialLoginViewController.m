//
//  OXInitialLoginViewController.m
//  Ocean
//
//  Created by Pavan Kataria on 04/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXInitialLoginViewController.h"
#import "UIImage+ImageEffects.h"
#import "PKUserSession.h"
#import "OXSignupPageTableViewController.h"
#import "AppDelegate.h"



@interface OXInitialLoginViewController (){
    NSDictionary *facebookUserProfileDictionary;
}

@end

@implementation OXInitialLoginViewController
- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillDisappear:animated];
}
-(void)lifeTestSegmentControlValueDidChange:(UISegmentedControl*)segmentControl{
    NSLog(@"value changed: %@", segmentControl);
}
- (void)viewDidLoad {
    [super viewDidLoad];
#if DEBUG
    [self.liveOrTestSegmentControl addTarget:self action:@selector(lifeTestSegmentControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
#endif
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(facebookStateChanged:) name:FBSessionStateChangedNotification object:nil];

    if ([OXLoginManager isLoggedIn]) {
        
        //        [[OCNMetadata sharedInstance] refresh];
        //        [self performSegueWithIdentifier:@"LoginSuccessful" sender:nil];
        NSLog(@"LOGIN SUCCESSFUL");
        
        //        [OXHTTPClient getMetaDataWithCompletionBlock]
        [[OXCookieManager sharedManager] assignedSavedSessionCookies];
        
        [[AppDelegate getAppDelegateReference] showMainScreen];
    }

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"InitialLoginBackground"]]];
}
-(void)facebookStateChanged:(NSNotification*)notification{
    NSLog(@"INITIAL LOGIN FACEBOOK STATE CHANGED: %@", notification.object);
    FBSession *session = notification.object;
    FBSessionState state = session.state;
    [self sessionStateChanged:session state:state];
}
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state{
    // If the session was opened successfully
    if (state == FBSessionStateOpen){
        NSLog(@"Session open: %@", session.accessTokenData.accessToken);
        // Show the user the logged-in UI
        //        [self userLoggedIn];
        //        [self showMainScreen];
        [self loginWithFacebook];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        NSLog(@"Session closed");
        // Show the user the logged-out UI
        //        [self userLoggedOut];
    }
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self setupButtons];
}
-(void)setupButtons{
    [self.facebookLoginButton setBackgroundColor:[PKUIAppearanceController facebookLoginButtonColour]];
    [self.facebookLoginButton.layer setCornerRadius:4];
    [self.signupWithEmailButton setBackgroundColor:[UIColor whiteColor]];
    [self.signupWithEmailButton.layer setCornerRadius:4];
    [self addShadowToThreeSidesToView:self.facebookLoginButton];
    [self addShadowToThreeSidesToView:self.signupWithEmailButton];

    [self addShadowAllAroundView:self.loginButton];
    [self addShadowAllAroundView:self.informationLabel];
    
    //Left align the button image
    [self.facebookLoginButton setImage:[[UIImage imageNamed:@"facebookLoginIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.signupWithEmailButton setImage:[[UIImage imageNamed:@"emailLoginIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];

    [self.facebookLoginButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [self.signupWithEmailButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    self.facebookLoginButton.tintColor = [UIColor whiteColor];
    self.signupWithEmailButton.tintColor = [UIColor lightGrayColor];
    
    self.facebookLoginButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.signupWithEmailButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    CGFloat padding = 20;
    self.facebookLoginButton.imageEdgeInsets = UIEdgeInsetsMake(0, padding, 0, 0);
    self.facebookLoginButton.titleEdgeInsets = UIEdgeInsetsMake(0, padding*2, 0, padding);
    
    self.signupWithEmailButton.imageEdgeInsets = UIEdgeInsetsMake(0, padding, 0, 0);
    self.signupWithEmailButton.titleEdgeInsets = UIEdgeInsetsMake(0, padding*2, 0, padding);
}

-(void)addShadowAllAroundView:(UIView*)view{
    view.layer.shadowOffset = CGSizeMake(1, 1);
    view.layer.shadowRadius = 2.0;
    view.layer.shadowOpacity = 1.0;

}

-(void)addShadowToThreeSidesToView:(UIView*)inputView{
    //This method will create a shadow on three sides by pulling the fourth side's edge into the center causing it not to be seen
    CGFloat shadowOffset = 5;
    inputView.layer.masksToBounds = NO;
    inputView.layer.shadowOffset = CGSizeMake(0, shadowOffset);
    inputView.layer.shadowRadius = 0.5;
    inputView.layer.shadowOpacity = 1;
    inputView.layer.shadowRadius = 4;
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    // Start at the Top Left Corner
    [path moveToPoint:CGPointMake(0.0, 0.0)];

    // This is the extra point in the middle :) Its the secret sauce.
    [path addLineToPoint:CGPointMake(CGRectGetWidth(inputView.frame) / 2.0, CGRectGetHeight(inputView.frame) / 2.0)];

    // Move to the Top Right Corner
    [path addLineToPoint:CGPointMake(CGRectGetWidth(inputView.frame), 0.0)];
    
    // Move to the Bottom Right Corner
    [path addLineToPoint:CGPointMake(CGRectGetWidth(inputView.frame), CGRectGetHeight(inputView.frame) - shadowOffset)];
    
    // Move to the Bottom Left Corner
    [path addLineToPoint:CGPointMake(0.0, CGRectGetHeight(inputView.frame) - shadowOffset)];
    
    // Move to the Close the Path
    [path closePath];
    inputView.layer.shadowPath = path.CGPath;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - view controller other - Methods
-(IBAction)faceboolLoginButtonPressed:(id)sender{
    [self loginWithFacebook];
}

-(IBAction)signupWithEmailButton:(id)sender{
    [self signupWithEmail];
}
-(void)loginWithFacebook{
    
    [SVProgressHUD showWithStatus:@"Logging in" maskType:SVProgressHUDMaskTypeBlack];
    
    [[OXLoginManager sharedManager] loginWithFacebookWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
//        NSLog(@"success: %@", response);
        [self processNeedInfoWithFacebookResponse:response];
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
//        NSLog(@"failed: %@", error);
                [SVProgressHUD dismiss];
    }];
}

-(void)signupWithEmail{
    facebookUserProfileDictionary = nil;
    [self performSegueWithIdentifier:kSegueIdentifierShowSignupPage sender:self];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)processNeedInfoWithFacebookResponse:(NSDictionary*)dictionary{
    NSLog(@"process facebook login dictionary: %@", dictionary);
    if([[dictionary objectForKey:@"newUser:"] boolValue] == TRUE || [[dictionary objectForKey:@"newUser"] boolValue] == TRUE){
        [self grabFacebookUserProfileDetails];
        [Flurry logEvent:kFlurryFBSignup];
    }
    else{
        NSLog(@"DOESNT NEED TO SIGN UP BECAUSE THIS PERSON IS ALREADY A USER. Hopefully there is a jsession key: %@", dictionary);
        [[AppDelegate getAppDelegateReference] showMainScreen];
        [SVProgressHUD dismiss];
        [Flurry logEvent:kFlurryFBLogin];
    }
}

-(void)grabFacebookUserProfileDetails{
    /* make the API call */
    [FBRequestConnection startWithGraphPath:@"/me"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              if(error){
                                  [FBErrorUtility userMessageForError:error];
                              }
                              else{
                                  [self setupSignupPageViewControllerWithFacebookResponseDictionary:result];
                              }
                              [SVProgressHUD dismiss];

                          }];
}
-(void)setupSignupPageViewControllerWithFacebookResponseDictionary:(NSDictionary*)facebookDictionaryResponse{
    facebookUserProfileDictionary = facebookDictionaryResponse;
    NSLog(@"going to perform segue for signup page");
    [self performSegueWithIdentifier:kSegueIdentifierShowSignupPage sender:self];
    NSLog(@"called perform segue for signup page");

}

#pragma mark - Navigation - Methods
-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
    NSLog(@"just came from :%@", self.restorationIdentifier);
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:kSegueIdentifierShowSignupPage]){
        OXSignupPageTableViewController *signupPage = segue.destinationViewController;
        if([[facebookUserProfileDictionary allKeys] count] > 0){
            signupPage.facebookUserProfileDictionary = facebookUserProfileDictionary;
            signupPage.displayType = OXSignUpVCTableViewTypeFacebook;
        }
        else{
            signupPage.displayType = OXSignUpVCTableViewTypeEmail;
        }
        NSLog(@"set facebook dictionary for signup page");
    }
}

@end
