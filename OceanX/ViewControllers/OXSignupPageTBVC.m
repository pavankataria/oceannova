//
//  OXSignupPageTBVC.m
//  Ocean
//
//  Created by Pavan Kataria on 17/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSignupPageTBVC.h"
#import "OXWebViewViewController.h"
#import "AppDelegate.h"
#import "TSMessage.h"

const CGFloat kMinimumPasswordLength = 5;
const CGFloat kYearOfBornLabelTag = 1;

@interface OXSignupPageTBVC ()<UITextViewDelegate, UIPickerViewDelegate>
@property (nonatomic, retain) UIPickerView *genderPicker;
@property (nonatomic, retain) UIPickerView *yearOfBirthPicker;

@property (nonatomic, retain) NSArray *genderOptionsArray;
@property (nonatomic, retain) NSArray *yearsOptionsArray;
@property (strong, nonatomic) OXUserModel *user;


@end

@implementation OXSignupPageTBVC
#pragma mark - lazy loading - Methods
-(OXUserModel *)user{
    if(!_user){
        _user = [[OXUserModel alloc] init];
    }
    return _user;
}
-(UIPickerView *)genderPicker{
    if(!_genderPicker){
        _genderPicker = [[UIPickerView alloc] init];
        _genderPicker.delegate = self;
    }
    return _genderPicker;
}
-(UIPickerView *)yearOfBirthPicker{
    if(!_yearOfBirthPicker){
        _yearOfBirthPicker = [[UIPickerView alloc] init];
        _yearOfBirthPicker.delegate = self;
    }
    return _yearOfBirthPicker;
}

-(NSArray *)genderOptionsArray{
    if(!_genderOptionsArray){
        _genderOptionsArray = [@[@"Select gender", @"Male", @"Female"] mutableCopy];
    }
    return _genderOptionsArray;
}
-(NSArray *)yearsOptionsArray{
    if(!_yearsOptionsArray){
        //Create Years Array from 1960 to This year
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy"];
        NSMutableArray *years = [[NSMutableArray alloc] init];
        int currentYear  = [[formatter stringFromDate:[NSDate date]] intValue] - (int)kMinimumAgeRequirementToSignUp;
        for (NSUInteger initialYear = kInitialYearOfBornToShow; initialYear <= currentYear; initialYear++) {
            [years addObject:[NSString stringWithFormat:@"%lu", (unsigned long)initialYear]];
        }
        _yearsOptionsArray = years;
    }
    return _yearsOptionsArray;
}

#pragma mark - view controller - Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupDefaultState];
}

-(void)setupDefaultState{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hudTapped:) name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
    [self.view setBackgroundColor:[PKUIAppearanceController grayColor]];
    [self setupTextFields];
    [self setupTableView];
}
-(void)setupTableView{
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.tableView addGestureRecognizer:gestureRecognizer];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXPrivacyAndTermsTableViewHeaderFooterView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"sectionFooterView"];
    
    [self.yearOfBirthPicker selectRow:90 inComponent:0 animated:NO];
}
- (void)hudTapped:(NSNotification *)notification{
    NSLog(@"They tapped the HUD");
    [SVProgressHUD dismiss];
}
-(void)doneButtonFromPickerToolBarAction{
    if([self.yobTextField isFirstResponder]){
        [self pickerView:self.yearOfBirthPicker didSelectRow:[self.yearOfBirthPicker selectedRowInComponent:0] inComponent:0];
        [self setTintColorToNormalForTextField:self.yobTextField];
    }
    
    [self hideKeyboard];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField == self.yobTextField){
        if(self.yobTextField.text.length > 0){
            NSUInteger indexOfObject = [self.yearsOptionsArray indexOfObject:self.yobTextField.text];
            [self.yearOfBirthPicker selectRow:indexOfObject inComponent:0 animated:NO];
        }
    }
    return YES;
}
- (void)hideKeyboard {
    [self.view endEditing:YES];;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupNavigationBar];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupNavigationBar{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.title = @"Register";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);

}
-(void)backButtonTapped:(id)sender{
    [self performSegueWithIdentifier:@"unwindSegueToProfilePage" sender:self];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    OXPrivacyAndTermsTableViewHeaderFooterView *sectionFooterView = (OXPrivacyAndTermsTableViewHeaderFooterView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"sectionFooterView"];
    sectionFooterView.delegate = self;
    return sectionFooterView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 100;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - OXSectionFooterPrivacyAndTermsProtocol - Methods

-(void)showWebView:(NSString*)urlString{
    UIStoryboard * settingScreenStoryBoard = [UIStoryboard storyboardWithName:@"SettingsScreenSB" bundle:nil];
    UINavigationController *navigationController = [settingScreenStoryBoard instantiateViewControllerWithIdentifier:@"WebView"];
    OXWebViewViewController *webViewViewController = [[navigationController childViewControllers] lastObject];
    webViewViewController.URL = urlString;
    [self presentViewController:navigationController animated:YES completion:nil];
}

-(void)privacyScreenLinkPressed{
    [self showWebView:@"http://oceanapp.com/privacy/"];
}
-(void)termsAndConditionsLinkPressed{
    [self showWebView:@"http://oceanapp.com/eula/"];
}


#pragma mark - picker delegate - Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if(pickerView == self.genderPicker){
        return 1;
    }
    else if(pickerView == self.yearOfBirthPicker){
        return 1;
    }
    return 0;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView == self.genderPicker){
        return self.genderOptionsArray.count;
    }
    else if(pickerView == self.yearOfBirthPicker){
        return self.yearsOptionsArray.count;
    }
    return 0;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *titleForRow;
    if(pickerView == self.genderPicker){
        titleForRow = self.genderOptionsArray[row];
    }
    else if(pickerView == self.yearOfBirthPicker){
        titleForRow = self.yearsOptionsArray[row];
    }
    return titleForRow;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView == self.genderPicker){
        [self setTintColorToNormalForImageView:self.genderImageView];
        if(row == 0){
            self.genderTextField.text = @"";
        }
        else{
            self.genderTextField.text = self.genderOptionsArray[row];
        }
    }
    else if(pickerView == self.yearOfBirthPicker){
        self.yobTextField.text = self.yearsOptionsArray[row];
    }
}


#pragma mark - TextField Delegate - Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.tableView scrollRectToVisible:self.tableView.tableFooterView.frame animated:YES
     ];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    NSInteger nextTag = textField.tag + 1;
//    UIView *nextResponder = [self.view viewWithTag:nextTag];
//    [textField resignFirstResponder];
//    
//    if (nextResponder) {
//        [nextResponder becomeFirstResponder];
//    }
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [self setTintColorToNormalForTextField:textField];
    if ( self.emailTextField ||  self.usernameTextField || self.passwordTextField){
        NSCharacterSet* numbers = [NSCharacterSet characterSetWithCharactersInString:@" "];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([numbers characterIsMember:c]) {
                return NO;
            }
        }
    }
    return YES;
}

#pragma mark - sign up page - Methods

-(void)setupTextFields{
    self.genderTextField.inputView = self.genderPicker;
    self.yobTextField.inputView = self.yearOfBirthPicker;
    
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:(CGRect){{0,-44}, {CGRectGetWidth(_genderPicker.frame), 44}}];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonFromPickerToolBarAction)];
    
    UIBarButtonItem *flexibleSpaceBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                            target:nil
                                                                                            action:nil];
    
    toolBar.items = [[NSArray alloc] initWithObjects:flexibleSpaceBarButton, barButtonDone,nil];
    [toolBar setBackgroundColor:[_genderPicker backgroundColor]];
    self.genderTextField.inputAccessoryView = toolBar;
    self.yobTextField.inputAccessoryView = toolBar;
}


- (BOOL)validationPassed{
    NSString *username = [self.usernameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *password = [self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *email = [self.emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    BOOL error = NO;
    if (![PKQuickMethods validateEmailWithString:email]){
        [self setTintColorToRedForImageView:self.emailImageView];
        [TSMessage showNotificationWithTitle:@"Email Invalid. Please enter a valid email address." type:TSMessageNotificationTypeError];
//        [SVProgressHUD showErrorWithStatus:@"Email Invalid. Please enter a valid email address."];
//        error = YES;
        return NO;
    }
    if( ([username length] < kValidateUsernameMinimumLength) || ([username length] > kValidateUsernameMaximumLength)){
        [self setTintColorToRedForImageView:self.usernameImageView];
        
        [TSMessage showNotificationWithTitle:@"Username must be between 3-30 characters long." type:TSMessageNotificationTypeError];
//        [SVProgressHUD showErrorWithStatus:@"Username must be between 3-30 characters long."];
//        error = YES;
        return NO;
    }
    if ([password length] < kMinimumPasswordLength){
        [self setTintColorToRedForImageView:self.passwordImageView];
        [TSMessage showNotificationWithTitle:@"Password should be at least 5 characters long." type:TSMessageNotificationTypeError];
//        [SVProgressHUD showErrorWithStatus:@"Password should be at least 5 characters long."];
//        error = YES;
        return NO;
    }
/*
    if(self.genderTextField.text.length <= 0){
        [self setTintColorToRedForImageView:self.genderImageView];
        [TSMessage showNotificationWithTitle:@"Please enter gender." type:TSMessageNotificationTypeError];
//        [SVProgressHUD showErrorWithStatus:@"Please enter a gender."];
        error = YES;
        return NO;
    }
    if(self.yobTextField.text.length <= 0){
        [self setTintColorToRedForImageView:self.yobImageView];
        [TSMessage showNotificationWithTitle:@"Please enter year of birth." type:TSMessageNotificationTypeError];
        //        [SVProgressHUD showErrorWithStatus:@"Please enter a gender."];
        error = YES;
        return NO;
    }
 */
    if(error){
        return NO;
    }
    return YES;
}

-(void)setTintColorToNormalForTextField:(UITextField*)textField{
    if(textField == self.emailTextField){
        [self setTintColorToNormalForImageView:self.emailImageView];
    }
    
    if(textField == self.usernameTextField){
        [self setTintColorToNormalForImageView:self.usernameImageView];
    }
    
    if(textField == self.passwordTextField){
        [self setTintColorToNormalForImageView:self.passwordImageView];
    }
    
    if(textField == self.genderTextField){
        [self setTintColorToNormalForImageView:self.genderImageView];
    }
}
-(void)setTintColorToNormalForImageView:(UIImageView*)imageViewIcon{
    [imageViewIcon setTintColor:[PKUIAppearanceController pkLightGrayColor]];
    //set up transition
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.4;
    [imageViewIcon.layer addAnimation:transition forKey:nil];
}
-(void)setTintColorToRedForImageView:(UIImageView*)imageViewIcon{
    [imageViewIcon setTintColor:rgb(198, 8, 8)];
    //set up transition
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.4;
    [imageViewIcon.layer addAnimation:transition forKey:nil];
}
-(IBAction)signupActionButton:(id)sender{
    [self performSignup];
}
- (void)setValuesInUser{
    self.user.email = self.emailTextField.text;
    self.user.username = self.usernameTextField.text;
    self.user.password = self.passwordTextField.text;
    if([self.yobTextField.text length]){
        self.user.yearBorn = [self.yobTextField.text integerValue];
    }
    if([self.genderTextField.text length]){
        if([self.genderTextField.text isEqualToString:self.genderOptionsArray[1]]){
            self.user.gender = 1;
        }
        else if([self.genderTextField.text isEqualToString:self.genderOptionsArray[2]]){
            self.user.gender = 2;
        }
        else{
            
        }
    }
}

- (void)performSignup{
    if ([self validationPassed]){
        [self setValuesInUser];
        
        [SVProgressHUD showWithStatus:@"Signing up" maskType:SVProgressHUDMaskTypeBlack];
        
        NSLog(@"USER BEFORE SENDING  %@", [self.user retrieveCreateUserParamsForSignupWithEmail]);
        
        [OXRestAPICient signUpWithUser:self.user successBlock:^(AFHTTPRequestOperation *operation, id response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showSuccessWithStatus:@"Success" maskType:SVProgressHUDMaskTypeBlack];
                [[AppDelegate getAppDelegateReference] showMainScreen];
                [[StatusHUDSBViewController sharedManager] displaySuccessStatusWithTitle:kCreateNewUserSuccessTitle message:kCreateNewUserSuccessMessage];
                [Flurry logEvent:kFlurryManualSignUp];
            });
            
        } failBlock:^(AFHTTPRequestOperation *operation, id error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"response: %@", error);
                [SVProgressHUD showErrorWithStatus:@"Signup failed" maskType:SVProgressHUDMaskTypeBlack];

                NSDictionary * dictionary = (NSDictionary*) error;
                if ([dictionary isKindOfClass:[NSDictionary class]]) {
//                    self.errorLabel.text = [dictionary getValueForKey:@"message"];
                    
                    [SVProgressHUD showErrorWithStatus:[dictionary getValueForKey:@"message"] maskType:SVProgressHUDMaskTypeBlack];
                }
                NSLog(@"Problem : %@", error);
                
            });
            
        }];
    }
}

@end

