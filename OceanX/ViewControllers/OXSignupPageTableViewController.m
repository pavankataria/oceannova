//
//  OXSignupPageTableViewController.m
//  Ocean
//
//  Created by Pavan Kataria on 06/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSignupPageTableViewController.h"
#import "SignupPageTableViewProtocol.h"
#import "OXLoginButtonTableViewCell.h"
#import "OXGenderSelectionTableViewCell.h"
#import "AppDelegate.h"
#import "LoginTCAndPrivacyPolicyTableViewCell.h"
#import "OXWebViewViewController.h"

NSString * const kSignupPageFacebookEmail = @"email";
NSString * const kSignupPageFacebookGender = @"gender";
NSString * const kSignupPageFacebookDOB = @"birthday";

//static const CGFloat kMinimumPasswordLength2 = 5;

@interface SignupPageTableViewCellModel : NSObject<SignupPageTableViewConfiguration>
@property (nonatomic, assign) NSUInteger sectionNumber;
@property (nonatomic, retain) NSString *propertyName;
@property (nonatomic, retain) NSString *propertyValue;
@end

@implementation SignupPageTableViewCellModel;
-(instancetype)initWithSectionNumber:(kSignupPageTableViewControllerFieldSectionType)sectionType{
    self = [super init];
    if(self) {
        self.sectionNumber = sectionType;
        self.propertyName = [SignupPageTableViewCellModel jsonFieldTypeForOXSignupPageTableViewControllerFieldSectionType:sectionType];
    }
    return self;
}
+(NSString *)jsonFieldTypeForOXSignupPageTableViewControllerFieldSectionType:(kSignupPageTableViewControllerFieldSectionType)sectionType{
    
    switch (sectionType) {
        case kSignupPageTableViewControllerFieldSectionTypeEmail:
            return @"email";
            break;
        case kSignupPageTableViewControllerFieldSectionTypeUsername:
            return @"username";
            break;
        case kSignupPageTableViewControllerFieldSectionTypePassword:
            return @"password";
            break;
        case kSignupPageTableViewControllerFieldSectionTypeYearOfBirth:
            return @"dob";
            break;
        case kSignupPageTableViewControllerFieldSectionTypeGender:
            return @"gender";
            break;
        default:
            break;
    }
}


@end


#pragma mark - END OF SIGN UP PAGE TABLE VIEW CONTROLLER

@interface OXSignupPageTableViewController ()<LoginTCAndPrivacyPolicyTableViewCellProtocol>{
    NSTimer *timer;
}
@property (nonatomic, retain) NSMutableArray *sectionArrays;

@end
@implementation OXSignupPageTableViewController

-(NSMutableArray *)sectionArrays{
    if(!_sectionArrays){
        _sectionArrays = [[NSMutableArray alloc] init];
    }
    return _sectionArrays;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    [self setupNavigationBar];
    
    if(self.displayType == OXSignUpVCTableViewTypeFacebook){
        NSLog(@"VIEW DID LOAD SIGNUP PAGE: %@", self.facebookUserProfileDictionary);
        NSLog(@"facebook dictionary %@", _facebookUserProfileDictionary);
    }
    else if(self.displayType == OXSignUpVCTableViewTypeEmail){
        NSLog(@"logging in via email");
    }
    [self setupTableViewControllerFromFacebookDictionary];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    OXSignUpTableViewCell *cell = (OXSignUpTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell.propertyInputTextField becomeFirstResponder];
}
-(void)setupTableViewControllerFromFacebookDictionary{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXSignUpTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellReuseIdentifierSignupTableViewCell];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXLoginButtonTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellReuseIdentifierLoginButtonTableViewCell];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LoginTCAndPrivacyPolicyTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellReuseIdentifierTCAndPrivacyTableViewCell];
    
    
    [self.tableView setBackgroundColor:[PKUIAppearanceController grayColor]];
    
    //Properties to display in priority order
    if(self.displayType == OXSignUpVCTableViewTypeFacebook){
        //Email
        if(![self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookEmail]){
            [self.sectionArrays addObject:[[SignupPageTableViewCellModel alloc] initWithSectionNumber:kSignupPageTableViewControllerFieldSectionTypeEmail]];
        }
        
        //Username
        [self.sectionArrays addObject:[[SignupPageTableViewCellModel alloc] initWithSectionNumber:kSignupPageTableViewControllerFieldSectionTypeUsername]];
        
        //Password
        [self.sectionArrays addObject:[[SignupPageTableViewCellModel alloc] initWithSectionNumber:kSignupPageTableViewControllerFieldSectionTypePassword]];
        
        //DOB
        if(![self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookDOB]){
            [self.sectionArrays addObject:[[SignupPageTableViewCellModel alloc] initWithSectionNumber:kSignupPageTableViewControllerFieldSectionTypeYearOfBirth]];
        }
        
        //Gender
        if(![self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookGender]){
            [self.sectionArrays addObject:[[SignupPageTableViewCellModel alloc] initWithSectionNumber:kSignupPageTableViewControllerFieldSectionTypeGender]];
        }
    }
    else if(self.displayType == OXSignUpVCTableViewTypeEmail){
        //Email
        [self.sectionArrays addObject:[[SignupPageTableViewCellModel alloc] initWithSectionNumber:kSignupPageTableViewControllerFieldSectionTypeEmail]];
        //Username
        [self.sectionArrays addObject:[[SignupPageTableViewCellModel alloc] initWithSectionNumber:kSignupPageTableViewControllerFieldSectionTypeUsername]];
        
        //Password
        [self.sectionArrays addObject:[[SignupPageTableViewCellModel alloc] initWithSectionNumber:kSignupPageTableViewControllerFieldSectionTypePassword]];
        
        //DOB
        [self.sectionArrays addObject:[[SignupPageTableViewCellModel alloc] initWithSectionNumber:kSignupPageTableViewControllerFieldSectionTypeYearOfBirth]];
        
        //Gender
        [self.sectionArrays addObject:[[SignupPageTableViewCellModel alloc] initWithSectionNumber:kSignupPageTableViewControllerFieldSectionTypeGender]];
    }
    [self.tableView reloadData];
}

-(void)setupNavigationBar{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.title = @"Register";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);

}
-(void)backButtonTapped:(id)sender{
    [self performSegueWithIdentifier:@"unwindSegueToProfilePage" sender:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sectionArrays.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SignupPageTableViewCellModel *object;
    
    if(indexPath.row < self.sectionArrays.count){
        object = [self.sectionArrays objectAtIndex:indexPath.row];
    }
    if(object.sectionNumber == kSignupPageTableViewControllerFieldSectionTypeGender){
        OXGenderSelectionTableViewCell *genderCell = [tableView dequeueReusableCellWithIdentifier:@"genderCell"];
        if(!genderCell){
            genderCell = [[OXGenderSelectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"genderCell"];
            [genderCell.genderSelectionControl setTintColor:[PKUIAppearanceController darkBlueColor]];
        }
        return genderCell;
    }
    else{
        if(indexPath.row >= self.sectionArrays.count){
            LoginTCAndPrivacyPolicyTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierTCAndPrivacyTableViewCell];
            cell.delegate = self;
            return cell;
        }
        else{
            OXSignUpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierSignupTableViewCell];
            cell.delegate = self;

            [cell setupWithObject:object withIndexPath:indexPath];
            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width, 0, cell.bounds.size.width);
            return cell;
        }
    }
}
-(void)loginButton:(id)sender{
    [self createSignupDictionaryAndRegister];
}
-(void)createSignupDictionaryAndRegister{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    for(SignupPageTableViewCellModel *object in self.sectionArrays){
        NSString *string = object.propertyValue;
        if([string length] > 0){
            [dictionary setObject:object.propertyValue forKey:object.propertyName];
        }
        else{
            NSLog(@"property name: %@", object.propertyName);
            if(self.displayType == OXSignUpVCTableViewTypeFacebook){
                if([object.propertyName isEqualToString:@"email"]){
                    [SVProgressHUD showErrorWithStatus:@"Please enter your email" maskType:SVProgressHUDMaskTypeBlack];
                    return;
                }
                if([object.propertyName isEqualToString:@"username"]){
                    [SVProgressHUD showErrorWithStatus:@"Please create a username" maskType:SVProgressHUDMaskTypeBlack];
                    return;
                }
                if([object.propertyName isEqualToString:@"password"]){
                    [SVProgressHUD showErrorWithStatus:@"Please create a password" maskType:SVProgressHUDMaskTypeBlack];
                    return;
                }
                if([object.propertyName isEqualToString:@"gender"]){
                    [SVProgressHUD showErrorWithStatus:@"Please enter your gender" maskType:SVProgressHUDMaskTypeBlack];
                    return;
                }
                if([object.propertyName isEqualToString:@"dob"]){
                    [SVProgressHUD showErrorWithStatus:@"Please enter year of birth" maskType:SVProgressHUDMaskTypeBlack];
                    return;
                }
                
            }
        }
//        else{
////            [self validateDictionaryToDisplayCorrectMessage:dictionary];
//            //VALIDATE ALL TEXT FIELDS
//            return;
//        }
    }
    if(self.displayType == OXSignUpVCTableViewTypeFacebook){
        NSLog(@"prepared dictionary: %@", dictionary);

        [self validateAndFixSignUpDictionaryForFacebook:dictionary];
//        LoginTCAndPrivacyPolicyTableViewCell *cell = (LoginTCAndPrivacyPolicyTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.sectionArrays.count inSection:0]];
        
        
    }
    else if (self.displayType == OXSignUpVCTableViewTypeEmail){
        [self validateAndFixSignUpDictionaryForEmail:dictionary];
    }
    [self signUpWithDictionary:dictionary];
}
-(void)validateAndFixSignUpDictionaryForEmail:(NSDictionary*)dictionary{
//    if(dictionary objectForKey:<#(id)#>)
//- (BOOL)performValidation{
    
    
    for(NSString *key in dictionary){
        NSLog(@"key: %@ ", key);
    }
    
//    NSString *emailKey = [SignupPageTableViewCellModel jsonFieldTypeForOXSignupPageTableViewControllerFieldSectionType:kSignupPageTableViewControllerFieldSectionTypeEmail];

//    if(![dictionary objectForKey:emailKey]){
//        [SVProgressHUD showErrorWithStatus:@"Email Invalid. Please enter a valid email address."];
//    }
//        if (![PKQuickMethods validateEmailWithString:email]){
//        return NO;
//    }

//    NSString *username = [self.userNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    NSString *password = [self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    NSString *email = [self.emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    
//    if (![PKQuickMethods validateEmailWithString:email]){
//        [SVProgressHUD showErrorWithStatus:@"Email Invalid. Please enter a valid email address."];
//        return NO;
//    }
//    else if( ([username length] < kValidateUsernameMinimumLength) || ([username length] > kValidateUsernameMaximumLength)){
//        [SVProgressHUD showErrorWithStatus:@"Invalid username.Please enter Username."];
//        return NO;
//    }
//    else if ([password length] < kMinimumPasswordLength2){
//        [SVProgressHUD showErrorWithStatus:@"Password too short. Password should be at least 5 characters long."];
//        return NO;
//    }
//    else if(self.user.yearBorn == 0)
//    {
//        [SVProgressHUD showErrorWithStatus:kCreateNewUserInvalidBirthYearMessage];
//        return NO;
//    }
//    else if(!self.checkmarkButton.selected){
//        [SVProgressHUD showErrorWithStatus:@"Please select Terms & Conditions."];
//        return NO;
//    }
//    return YES;
}

-(void)validateAndFixSignUpDictionaryForFacebook:(NSMutableDictionary*)dictionary{
    NSString *emailKey = [SignupPageTableViewCellModel jsonFieldTypeForOXSignupPageTableViewControllerFieldSectionType:kSignupPageTableViewControllerFieldSectionTypeEmail];
    
    NSString *yearOfBirthKey = [SignupPageTableViewCellModel jsonFieldTypeForOXSignupPageTableViewControllerFieldSectionType:kSignupPageTableViewControllerFieldSectionTypeYearOfBirth];
    
    NSString *genderKey = [SignupPageTableViewCellModel jsonFieldTypeForOXSignupPageTableViewControllerFieldSectionType:kSignupPageTableViewControllerFieldSectionTypeGender];
    
    
    //Email
    
    
    if([self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookEmail]){
        if ([[self.facebookUserProfileDictionary valueForKey:kSignupPageFacebookEmail] isKindOfClass:[NSNull class]]){
            [dictionary setObject:[self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookEmail] forKey:emailKey];
        }
    }
    
    //DOB
    
    if([self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookDOB]){
        if ([[self.facebookUserProfileDictionary valueForKey:kSignupPageFacebookDOB] isKindOfClass:[NSNull class]]){
            NSInteger yearUserWasBorn = [self getAppropriateYearOfBirthForStringDate:[self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookDOB]];
            [dictionary setObject:[NSNumber numberWithInteger:yearUserWasBorn] forKey:yearOfBirthKey];
        }
    }
    
    //Gender
    if([self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookGender]){
        if ([[self.facebookUserProfileDictionary valueForKey:kSignupPageFacebookGender] isKindOfClass:[NSNull class]]){
            [dictionary setObject:[self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookGender] forKey:genderKey];
        }
    }
    NSLog(@"final dictionary: %@", dictionary);
}
-(void)validateAndFixSignUpDictionary:(NSMutableDictionary*)dictionary{
    NSString *emailKey = [SignupPageTableViewCellModel jsonFieldTypeForOXSignupPageTableViewControllerFieldSectionType:kSignupPageTableViewControllerFieldSectionTypeEmail];
    
    NSString *yearOfBirthKey = [SignupPageTableViewCellModel jsonFieldTypeForOXSignupPageTableViewControllerFieldSectionType:kSignupPageTableViewControllerFieldSectionTypeYearOfBirth];
    
    NSString *genderKey = [SignupPageTableViewCellModel jsonFieldTypeForOXSignupPageTableViewControllerFieldSectionType:kSignupPageTableViewControllerFieldSectionTypeGender];
    
    
    //Email
    if(![dictionary objectForKey:emailKey]){
        [dictionary setObject:[self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookEmail] forKey:emailKey];
    }

    //DOB
    if(![dictionary objectForKey:yearOfBirthKey]){
        NSInteger yearUserWasBorn = [self getAppropriateYearOfBirthForStringDate:[self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookDOB]];
        [dictionary setObject:[NSNumber numberWithInteger:yearUserWasBorn] forKey:yearOfBirthKey];
//        NSLog(@"checking year of birth: %lu", yearUserWasBorn);
    }
    else{
//        NSLog(@"key year of birth exists");
    }

    //Gender
    if(![dictionary objectForKey:genderKey]){
        [dictionary setObject:[self.facebookUserProfileDictionary objectForKey:kSignupPageFacebookGender] forKey:genderKey];
    }
//    NSLog(@"validate and final dictionary: %@", dictionary);
}
-(NSUInteger)getAppropriateYearOfBirthForStringDate:(NSString*)stringDate{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy"];
    NSDate *userBirthday = [df dateFromString:stringDate];

    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear fromDate:userBirthday];
    
    NSInteger yearInteger = [components year];
//    NSLog(@"year user was born %lu", (long)yearInteger);
    return yearInteger;
}

-(void)signUpWithDictionary:(NSDictionary*)signUpDictionary{
    
    if(self.displayType == OXSignUpVCTableViewTypeFacebook){
        NSString *accessToken = FBSession.activeSession.accessTokenData.accessToken;
        [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeBlack];
        [OXRestAPICient postLoginWithFacebookWithAccessTokenString:accessToken
                                              withParamsDictionary:signUpDictionary
                                                      successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                                          if([operation.response statusCode] == 202){
//                                                              [[AppDelegate getAppDelegateReference] showMainScreen];
//                                                              [SVProgressHUD dismiss];
                                                              NSDictionary * dictionary = (NSDictionary*) response;
                                                              if ([dictionary isKindOfClass:[NSDictionary class]]) {
                                                                  //                    self.errorLabel.text = [dictionary getValueForKey:@"message"];
                                                                  NSLog(@"DICTIONARY RESPONSE: %@", response);
                                                                  [SVProgressHUD showErrorWithStatus:[dictionary getValueForKey:@"errorMessage"] maskType:SVProgressHUDMaskTypeBlack];
                                                              }
                                                          }
                                                          else if([operation.response statusCode] == 200){
                                                              
                                                              [self runFacebookRecommendationForNewSignedUpFacebookUser];

                                                          }
                                                          
                                                      }
                                                         failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                                             [SVProgressHUD dismiss];
                                                         }];
    }
    else if(self.displayType == OXSignUpVCTableViewTypeEmail){
        NSLog(@"REGISTER VIA EMAIL SIGN UP API");
    }
}

-(void)runFacebookRecommendationForNewSignedUpFacebookUser{
    timer = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(callGetRequestManually:) userInfo:nil repeats:NO];

    [SVProgressHUD showWithStatus:@"Analysing facebook likes" maskType:SVProgressHUDMaskTypeNone];
    NSString *accessToken = FBSession.activeSession.accessTokenData.accessToken;

    MKCoordinateRegion currentRegion = [[OXLocationManager sharedTracker] currentMapRegion];
    OCNPlaneModel * plane = [OCNPlaneModel planeModelFromMapRegion:currentRegion];


    
    [OXRestAPICient postFacebookRecommendationsWithAccessTokenString:accessToken
                                                   andPlaneRestModel:plane successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                                       NSInteger responseCode = [operation.response statusCode];
                                                       [timer invalidate];
                                                       if(responseCode == 200){
                                                           //triggered_no_notification
                                                           /*
                                                            output in case no recommendation : server returns null for the iOS to know the recommendation wasn't triggered (not enough data)
                                                            */
                                                           [self showHomePageWithInstantRecommendationWithResponse:response];
                                                       }
                                                       else if(responseCode == 202){
                                                           NSString *responseMessage = [response objectForKey:@"message"];
                                                           [SVProgressHUD showWithStatus:responseMessage maskType:SVProgressHUDMaskTypeBlack];
                                                       }
                                                    

                                                   }
                                                           failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                                               [self showHomePage];

                                                               NSLog(@"signup page error:%@",error);
                                                   }];
}
-(void)callGetRequestManually:(NSTimer *)t{
    [t invalidate];
    [OXRestAPICient getFacebookRecommendationsWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"facebook GET recommendations RESPONSE: %@", response);
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPushNameFBRecommendation object:nil userInfo:response];
        
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        NSLog(@"failed: facebook GET recommendations ERROR: %@", error);
        [self showHomePage];
    }];
}
-(void)showHomePageWithInstantRecommendationWithResponse:(id)response{
    [[AppDelegate getAppDelegateReference] setupTabBarAndThenShowInstantRecommendationWithResponse:response];
    [SVProgressHUD dismiss];

}
-(void)showHomePage{
    [[AppDelegate getAppDelegateReference] showMainScreen];
    [SVProgressHUD dismiss];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    OXLoginButtonTableViewCell *loginButtonCell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierLoginButtonTableViewCell];
    [loginButtonCell.loginButton addTarget:self action:@selector(loginButton:) forControlEvents:UIControlEventTouchUpInside];
    loginButtonCell.separatorInset = UIEdgeInsetsMake(0,0, 0, 0);
    return loginButtonCell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 80;
}

- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
        return 20.0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    SignupPageTableViewCellModel *object = [self.sectionArrays objectAtIndex:indexPath.row];
//    if(object.sectionNumber == kSignupPageTableViewControllerFieldSectionTypeGender){
//        return 100;
//    }
    return 50;
}

#pragma mark - convenience method - Methods

#pragma mark - OXSignUpTableViewCell - Methods
-(void)textFieldDidChangeForIndexPath:(NSIndexPath *)indexPath withTextField:(UITextField *)textField{
    SignupPageTableViewCellModel *object = [self.sectionArrays objectAtIndex:indexPath.row];
    object.propertyValue = textField.text;
    NSLog(@"property %@ value: %@", object.propertyName, object.propertyValue);
}



#pragma mark - LoginTCAndPrivacyPolicyTableViewCellProtocol - Methods

-(void)showWebView:(NSString*)urlString{
    UIStoryboard * settingScreenStoryBoard = [UIStoryboard storyboardWithName:@"SettingsScreenSB" bundle:nil];
    UINavigationController *navigationController = [settingScreenStoryBoard instantiateViewControllerWithIdentifier:@"WebView"];
    OXWebViewViewController *webViewViewController = [[navigationController childViewControllers] lastObject];
    webViewViewController.URL = urlString;
    [self presentViewController:navigationController animated:YES completion:nil];
}
- (void)termsAndConditionsButtonPressed{
    [self showWebView:@"http://oceanapp.com/eula/"];
}

- (void)privacyPolicyButtonPressed{
    [self showWebView:@"http://oceanapp.com/privacy/"];
}


@end
