//
//  OXInitialLoginViewController.h
//  Ocean
//
//  Created by Pavan Kataria on 04/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXInitialLoginViewController : UIViewController
@property (nonatomic, weak) IBOutlet UIButton *facebookLoginButton;
@property (nonatomic, weak) IBOutlet UIButton *signupWithEmailButton;
@property (nonatomic, weak) IBOutlet UIButton *loginButton;
@property (nonatomic, weak) IBOutlet UILabel *informationLabel;

@property (nonatomic, weak) IBOutlet UISegmentedControl *liveOrTestSegmentControl;

-(IBAction)prepareForUnwind;
@end
