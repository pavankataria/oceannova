//
//  OXContactUsPresentationViewController.m
//  Ocean
//
//  Created by Pavan Kataria on 17/04/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXContactUsPresentationViewController.h"

@interface OXContactUsPresentationViewController ()
@property (nonatomic, assign) CGFloat labelPadding;

@end

@implementation OXContactUsPresentationViewController
- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - Other methods - Methods
-(void)setPoiObject:(OXPOIModel *)poiObject{
    _poiObject = poiObject;
    self.labelPadding = 20;

    [self.view layoutIfNeeded];
    CGRect viewFrame = self.view.frame;
    self.view.frame = viewFrame;
    
    [self setupContactUsButtons];

//    [self.delegate updateDetailPOIScrollViewContentSize];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    
}

//-(void)setupAddressLabel{
//    if(!self.addressLabel){
//        self.addressLabel = [[UILabel alloc] init];
//        [self.mapView addSubview: self.addressLabel];
//        
//        [self.mapView insertSubview:self.addressLabel aboveSubview:self.labelBackgroundView];
//    }
//    self.addressLabel.frame = CGRectMake(self.labelPadding, self.mapView.frame.size.width - self.labelHeight, self.mapView.frame.size.width - 2*self.labelPadding, self.labelHeight);
//    self.addressLabel.backgroundColor = [UIColor clearColor];
//    self.addressLabel.textColor = [UIColor whiteColor];
//    self.addressLabel.textAlignment = NSTextAlignmentCenter;
//    self.addressLabel.adjustsFontSizeToFitWidth = YES;
//    self.addressLabel.font = [UIFont fontWithName:@"AvenirNext-Medium" size:18.0];
//    [self.addressLabel setText:self.poiObject.poiAddress];
//    self.addressLabel.numberOfLines = 0;
//    [self.addressLabel sizeToFit];
//    [self.addressLabel setTextAlignment:NSTextAlignmentLeft];
//    
//    CGRect temp = self.addressLabel.frame;
//    temp.origin.y = CGRectGetMaxY(self.mapView.frame)-CGRectGetHeight(self.addressLabel.frame)-self.labelPadding;
//    self.addressLabel.frame = temp;
//}

-(void)setupContactUsButtons{
    
    if([self.poiObject.socialContact allKeys].count > 0){
        
        CGFloat padding;
        CGFloat buttonHeight;
        if(IS_IPHONE_4_OR_LESS){
            padding = 15;
            buttonHeight = self.view.frame.size.height * 0.5;
        }
        else if(IS_IPHONE_5){
            padding = 10;
            buttonHeight = self.view.frame.size.height * 0.45;
        }
        else{
            padding = self.labelPadding;
            buttonHeight = self.view.frame.size.height * 0.45;
        }
        
        CGFloat buttonWidth = buttonHeight;
        CGRect  buttonSize = CGRectMake(0, 0, buttonWidth, buttonHeight);
        buttonSize.origin.x = padding;
        buttonSize.origin.y = self.view.frame.size.height/2-buttonSize.size.height/2;
        
        
        int x = CGRectGetWidth(self.view.frame) - padding;
        if([self.poiObject.socialContact objectForKey:kExtraInfoTableViewCellIndexPathRowLabelNameTwitter]){
            self.twitterButton  = [[UIButton alloc] initWithFrame:buttonSize];
            [self.twitterButton setBackgroundImage:[UIImage imageNamed:@"twitter_unselected"] forState:UIControlStateNormal];
            [self.twitterButton setBackgroundImage:[UIImage imageNamed:@"twitter"] forState:UIControlStateHighlighted];
            [self.twitterButton setBackgroundImage:[UIImage imageNamed:@"twitter"] forState:UIControlStateSelected];
            [self.twitterButton addTarget:self action:@selector(twitterButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            CGRect button = self.twitterButton.frame;
            x -= CGRectGetWidth(self.twitterButton.frame);
            button.origin.x = x;
            self.twitterButton.frame = button;
            x -= padding;
            [self.view addSubview:self.twitterButton];
        }
        
        if([self.poiObject.socialContact objectForKey:kExtraInfoTableViewCellIndexPathRowLabelNameFacebook]){
            self.facebookButton = [[UIButton alloc] initWithFrame:buttonSize];
            [self.facebookButton setBackgroundImage:[UIImage imageNamed:@"facebook_unselected"] forState:UIControlStateNormal];
            [self.facebookButton setBackgroundImage:[UIImage imageNamed:@"facebook"] forState:UIControlStateHighlighted];
            [self.facebookButton setBackgroundImage:[UIImage imageNamed:@"facebook"] forState:UIControlStateSelected];
            [self.facebookButton addTarget:self action:@selector(facebookButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            CGRect button = self.facebookButton.frame;
            x -= CGRectGetWidth(self.facebookButton.frame);
            button.origin.x = x;
            self.facebookButton.frame = button;
            x -= padding;
            [self.view addSubview:self.facebookButton];
        }
        
        if([self.poiObject.socialContact objectForKey:kExtraInfoTableViewCellIndexPathRowLabelNameWebsite]){
            self.emailButton = [[UIButton alloc] initWithFrame:buttonSize];
            [self.emailButton setBackgroundImage:[UIImage imageNamed:@"mail_unselected"] forState:UIControlStateNormal];
            [self.emailButton setBackgroundImage:[UIImage imageNamed:@"mail"] forState:UIControlStateHighlighted];
            [self.emailButton setBackgroundImage:[UIImage imageNamed:@"mail"] forState:UIControlStateSelected];
            [self.emailButton addTarget:self action:@selector(emailButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            CGRect button = self.emailButton.frame;
            x -= CGRectGetWidth(self.emailButton.frame);
            button.origin.x = x;
            self.emailButton.frame = button;
            x -= padding;
            [self.view addSubview:self.emailButton];
        }
        
        
        if([self.poiObject.socialContact objectForKey:kExtraInfoTableViewCellIndexPathRowLabelNamePhone]){
            self.telephoneButton = [[UIButton alloc] initWithFrame:buttonSize];
            [self.telephoneButton setBackgroundImage:[UIImage imageNamed:@"call_unselected"] forState:UIControlStateNormal];
            [self.telephoneButton setBackgroundImage:[UIImage imageNamed:@"call"] forState:UIControlStateHighlighted];
            [self.telephoneButton setBackgroundImage:[UIImage imageNamed:@"call"] forState:UIControlStateSelected];
            [self.telephoneButton addTarget:self action:@selector(telephoneButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            CGRect button = self.telephoneButton.frame;
            x -= CGRectGetWidth(self.telephoneButton.frame);
            button.origin.x = x;
            self.telephoneButton.frame = button;
            x -= padding;
            [self.view addSubview:self.telephoneButton];
        }
        
        
        
        self.contactUsLabel = [[UILabel alloc] init];
        
        //    :CGRectMake(self.labelPadding, 5, self.view.frame.size.width - 2 * self.labelPadding, 40)];
        [self.contactUsLabel setText:@"Contact Us"];
        self.contactUsLabel.textColor = rgb(90, 90, 90);
        self.contactUsLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
        [self.contactUsLabel setTextAlignment:NSTextAlignmentLeft];
        
        [self.contactUsLabel sizeToFit];
        
        CGRect rect = self.contactUsLabel.frame;
        rect.origin.x = self.labelPadding;
        rect.origin.y = self.view.bounds.size.height/2-rect.size.height/2;
        self.contactUsLabel.frame = rect;
        
        NSLog(@"contactUsLabel.frame %@", NSStringFromCGRect(self.contactUsLabel.frame));
        [self.view addSubview:self.contactUsLabel];
        
        
        [self.view layoutIfNeeded];
    }
    else{
        CGRect viewFrame = self.view.frame;
        viewFrame.size.height = 0;
        self.view.frame = viewFrame;
        [self.view layoutIfNeeded];
    }
}
-(void)facebookButtonTapped:(id)sender{
    NSString *objectValue = [[NSMutableString alloc] initWithString:[self.poiObject.socialContact objectForKey:kExtraInfoTableViewCellIndexPathRowLabelNameFacebook]];

    NSString *facebookProfileIDString =[NSString stringWithFormat:@"fb://profile/%@", objectValue];
    NSURL *facebookProfileURL = [NSURL URLWithString:facebookProfileIDString];
    
    if([[UIApplication sharedApplication] canOpenURL:facebookProfileURL]){
        [[UIApplication sharedApplication] openURL:facebookProfileURL];
    }
    else{
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Couldn't open the facebook page for %@", self.poiObject.poiName] maskType:SVProgressHUDMaskTypeBlack];
    }

}
-(void)emailButtonTapped:(id)sender{
    NSMutableString *objectValue = [[NSMutableString alloc] initWithString:[self.poiObject.socialContact objectForKey:kExtraInfoTableViewCellIndexPathRowLabelNameWebsite]];
    NSURL *url;

    if([objectValue hasPrefix:@"http://"]){
        url = [NSURL URLWithString:objectValue];
    }
    else{
        NSString *strurl = [NSString stringWithFormat:@"http://%@",objectValue];
        url = [NSURL URLWithString:strurl];
    }
    
    if (![[UIApplication sharedApplication] openURL:url]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Couldn't open url: %@", objectValue] maskType:SVProgressHUDMaskTypeBlack];
    }
}
-(void)twitterButtonTapped:(id)sender{
    
    NSString *objectValue = [self.poiObject.socialContact objectForKey:kExtraInfoTableViewCellIndexPathRowLabelNameTwitter];

    NSString *twitterUsername = [objectValue stringByReplacingOccurrencesOfString:@"@" withString:@""];
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]]){
        
        NSString *twitterLink = [NSString stringWithFormat:@"twitter://user?screen_name=%@", twitterUsername] ;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterLink]];
    }
    else{
        NSString *twitterLink = [NSString stringWithFormat:@"https://www.twitter.com/%@", twitterUsername];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: twitterLink]];
    }
    
    [Flurry logEvent:kFlurryPlaceTwitterHandle withParameters:[self getFlurryParameters]];

}
-(void)telephoneButtonTapped:(id)sender{
    NSString *objectValue = [self.poiObject.socialContact objectForKey:kExtraInfoTableViewCellIndexPathRowLabelNamePhone];

    NSLog(@"phone number: %@",objectValue);
    NSString *strippedPhoneNumber;
    NSMutableCharacterSet *allowedCharacters = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
    [allowedCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@"+"]];
    NSCharacterSet *doNotWant = [allowedCharacters invertedSet];
    
    strippedPhoneNumber = [[objectValue componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
    NSLog(@"stripped: %@", strippedPhoneNumber); // => foobarbazfoo
    NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", strippedPhoneNumber]];
    NSLog(@"phone url: %@", phoneURL);
    if([[UIApplication sharedApplication] canOpenURL:phoneURL]){
        NSLog(@"phone url can open");
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    else{
    }

}
//-(void)likeButtonTapped:(id)sender{
//    NSLog(@"like button tapped");
//    [self.likeButton setSelected:![self.likeButton isSelected]];
//    [self.dislikeButton setSelected:NO];
//    [self.maybeButton setSelected:NO];
//    
//}
//-(void)dislikeButtonTapped:(id)sender{
//    NSLog(@"dislike button tapped");
//    [self.dislikeButton setSelected:![self.dislikeButton isSelected]];
//    [self.likeButton setSelected:NO];
//    [self.maybeButton setSelected:NO];
//    
//}
//-(void)maybeButtonTapped:(id)sender{
//    NSLog(@"maybe button tapped");
//    [self.maybeButton setSelected:![self.maybeButton isSelected]];
//    [self.likeButton setSelected:NO];
//    [self.dislikeButton setSelected:NO];
//}

//-(void)setPoiObject:(OXPOIModel *)poiObject{
//    _poiObject = poiObject;
////    [self.likeButton setSelected:poiObject.userLiked];
////    [self.dislikeButton setSelected:poiObject.userDisliked];
////    [self.maybeButton setSelected:poiObject.userConfused];
//}
- (NSDictionary *)getFlurryParameters
{
    NSDictionary *params =
    [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld", (long)self.poiObject.poiID],
     kBasePOIPoiIdKey, // Parameter Name
     nil];
    
    return params;
}

@end
