//
//  OXCoordinateModel.h
//  OceanX
//
//  Created by Pavan Kataria on 23/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OXSearchModelOperationProtocol.h"


@interface OXCoordinateModel : NSObject<OXSearchModelOperationProtocol>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue;

@property (nonatomic, retain) NSString *placeMarkTitleAndSubtitle;
-(NSDictionary*)params;

@end
