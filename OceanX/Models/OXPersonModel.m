//
//  OXPersonModel.m
//  OceanX
//
//  Created by Pavan Kataria on 15/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXPersonModel.h"

@implementation OXPersonModel
//+ (NSDictionary *)JSONKeyPathsByPropertyKey{
//    return @{@"personId":@"userId",
//             @"username":@"username",
//             @"personImageURL":@"imageURL",};
//}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue{
//    NSLog(@"Full PERSON model Dictionary : %@", dictionaryValue);
    self = [super init];
    if (self == nil) return nil;
    
    self.personId = [[dictionaryValue objectForKey:@"userId"] integerValue];
    self.username = [dictionaryValue objectForKey:@"username"];
    
    if([dictionaryValue objectForKey:@"imageURL"]){
        self.personImageURL = [NSURL URLWithString:[[dictionaryValue objectForKey:@"imageURL"] stringValue]];
    }
    return self;
}

-(NSString*)name{
    return self.username;
}

@end