//
//  OXProcessedPicture.h
//  OceanX
//
//  Created by Pavan Kataria on 03/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OXProcessedPictureElement : NSObject

@property (nonatomic, retain) NSString *pictureURL;
@property (nonatomic, assign) CGSize pictureSize;
//#warning todo: figure out how to do caching with SDWebImage.
@property (nonatomic, retain) UIImage *pictureImage;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;
+(NSArray*)pictureElementArrayFromJSONResponse:(NSArray*)array;

@end

@interface OXProcessedPicture : NSObject
@property (nonatomic, retain) NSString *pictureURL;
@property (nonatomic, assign) NSUInteger pictureId;
@property (nonatomic, assign, getter=isMainPicture) BOOL mainPicture;
@property (nonatomic, retain) NSArray *processedPictures;

@property (nonatomic, retain) UIImage *mainImageToBeUsedTemporarilyUntilProcessedImageURLSAreComingThrough;
-(void)loadImages;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;
+(NSArray*)getPicturesArrayFromJSONResponse:(NSArray*)array;

@end
