//
//  NewsFeedDataModel.m
//  OceanX
//
//  Created by Pavan Kataria on 09/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "NewsFeedDataModel.h"
@implementation OXSectionData : NSObject
-(instancetype)initWithSectionAtIndexSection:(NSUInteger)indexSection{
    self = [super init];
    if(self){
        self.indexSection = indexSection;
    }
    return self;
}
@end

@implementation OXSections : NSObject
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
//        NSLog(@"section: %@", dictionary );
        self.pois = [[NSMutableArray alloc] init];
        for(NSDictionary *poi in [dictionary objectForKey:@"pois"]){
            [self.pois addObject:[[OXPOISearchByListModel alloc] initWithDictionary:poi]];
        }
        self.sectionTitle = [dictionary objectForKey:@"sectionTitle"];
        
        if([dictionary objectForKey:@"sectionCaption"]){
            self.sectionCaption = [dictionary objectForKey:@"sectionCaption"];
        }
//        NSLog(@"section title: %@\n", self.sectionTitle);
//        NSLog(@"%lu poi objects", self.pois.count);
        
//        for(OXPOIModel *poi in self.pois){
//            NSLog(@"    poi name: %@", poi.poiName);
//        }
    }
    return self;
}
@end

@interface NewsFeedDataModel ()
@end
@implementation NewsFeedDataModel
-(NSMutableArray *)sectionData{
    if(!_sectionData){
        _sectionData = [[NSMutableArray alloc] init];
    }
    return _sectionData;
}
-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    if(self){
        NSArray *sections = [dictionary objectForKey:@"sections"];
        
        self.sections = [[NSMutableArray alloc] init];
        for(NSDictionary *section in sections){
            [self.sections addObject:[[OXSections alloc] initWithDictionary:section]];
        }
        self.totalNumberOfSectionsToDisplay = [self getTotalNumberOfSectionsForNewsFeedDataObject:self];
    }
    return self;
}
-(NSUInteger)getTotalNumberOfSectionsForNewsFeedDataObject:(NewsFeedDataModel*)newsFeedDataObject{
    NSUInteger totalNumberOfSections = 0;
    for(OXSections *section in newsFeedDataObject.sections){
        [self.sectionData addObject:[[OXSectionData alloc] initWithSectionAtIndexSection:totalNumberOfSections]];
        totalNumberOfSections += 1; //for the title
        totalNumberOfSections += section.pois.count;
    }
    return totalNumberOfSections;
}
-(BOOL)shouldDisplaySectionHeaderForSection:(NSUInteger)section{
    for(OXSectionData *sectionData in self.sectionData){
        if(sectionData.indexSection == section){
            return YES;
        }
    }
    return NO;
}

-(NSUInteger)getNormalisedSectionIndexForTableViewTrickSectionIndex:(NSUInteger)section{
    for(int i = 0; i < self.sectionData.count; i++){
        OXSectionData *sectionData = self.sectionData[i];
        if(sectionData.indexSection == section){
            return i;
        }
    }
    [NSException raise:@"shouldnt arrive here at all" format:@"couldnt find normalised section title for section %lu. Trick section might be a normal cell index instead of a trick section index", (unsigned long)section];
    return 0;
}

-(BOOL)areThereAnyPOISForTrickSection:(NSUInteger)section{
    NSUInteger normalisedSection = [self getNormalisedSectionIndexForTableViewTrickSectionIndex:section];
    
    return ([[self.sections[normalisedSection] pois] count] > 0);
    
}

-(NSString*)appropriateTitleForPOITrickSection:(NSUInteger)section{
//    for (int i = 0; i < self.sectionData.count; i++) {
//        NSLog(@"trick index section: %lu and indexPath.section: %lu", [self.sectionData[i] indexSection], section);
//        if([self.sectionData[i] indexSection] >= section){
//            return [self.sections[i] sectionTitle];
//        }
//    }
//    [NSException raise:@"shouldnt arrive here" format:@"Trick section title at section %lu not found", section];
//    return @"N/A";
    
    
    NSInteger poiIsInRow = 0;
    
    for (NSInteger i = self.sectionData.count-1; i >= 0; i--) {
        if(section >= [self.sectionData[i] indexSection]){
//            poiIsInRow = section - [self.sectionData[i] indexSection] - 1;
//            return [[self.sections[i] pois] objectAtIndex:poiIsInRow];
            return [self.sections[i] sectionTitle];
        }
    }
    [NSException raise:@"should never reach here" format:@"Section: %lu poi not retrieved correctly", (unsigned long)section];
    return nil;

}
-(NSString*)newsFeedTitleAtSection:(NSUInteger)section{
    for (int i = 0; i < self.sectionData.count; i++) {
        
        if([self.sectionData[i] indexSection] == section){
            return [self.sections[i] sectionTitle];
        }
    }
    [NSException raise:@"shouldnt arrive here" format:@"Section title at section %lu not found", (unsigned long)section];
    return @"N/A";
}


-(OXPOISearchByListModel*)poiForSection:(NSUInteger)section{ //example: 2
    NSInteger poiIsInRow = 0;

    for (NSInteger i = self.sectionData.count-1; i >= 0; i--) {
        if(section > [self.sectionData[i] indexSection]){
            poiIsInRow = section - [self.sectionData[i] indexSection] - 1;
//            NSLog(@"poi is in row: %d", poiIsInRow);
            if(poiIsInRow < [self.sections[i] pois].count){
                return [[self.sections[i] pois] objectAtIndex:poiIsInRow];
            }
        }
    }
    return nil;
//    [NSException raise:@"should never reach here" format:@"Section: %lu poi not retrieved correctly", section];
    return nil;
    
/*    for (int i = 1; i < self.sectionData.count; i++) {
        if(section < [self.sectionData[i] indexSection]){
            poiIsInRow = section - [self.sectionData[i-1] indexSection] - 1;
            return [[self.sections[i-1] pois] objectAtIndex:poiIsInRow];
        }
    }
    poiIsInRow = section - [self.sectionData[self.sectionData.count-1] indexSection] - 1;
    return [[self.sections[self.sectionData.count-1] pois] objectAtIndex:poiIsInRow];
 */
}

@end
