//
//  OXImageStackItem.m
//  OceanX
//
//  Created by Pavan Kataria on 26/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXImageStackItem.h"
#import "UIImage+Resize.h"
#import "UIImage+SPImageFunctions.h"
#import "UIImage+Crop.h"

@implementation OXImageStackItem


-(instancetype)initWithImage:(UIImage *)image{
    self = [super init];
    if(!self) return nil;
    NSLog(@"ORIGINAL SIZE OF ORIGINAL IMAGE: %@", NSStringFromCGSize(image.size));
    UIImage *largeImage = [image scaledCopyOfWidth:640];

    _originalImage = largeImage;
    _thumbnailImage = [_originalImage resizedImage:CGSizeMake(200, 200) interpolationQuality:kCGInterpolationMedium];
    
    return self;
}
-(instancetype)initWithImage:(UIImage *)image withSideLength:(CGFloat)sideLength{
    self = [super init];
    if(!self) return nil;
    NSLog(@"ORIGINAL SIZE OF ORIGINAL IMAGE: %@ WITH SIDE LENGTH: %f", NSStringFromCGSize(image.size), sideLength);
    //    UIImage *largeImage = [image scaledCopyOfWidth:640];
    
    UIImage *largeImage = [image squareCropImageToSideLength:image withSideLengthFloat:sideLength];
    _originalImage = largeImage;
//    _thumbnailImage = [_originalImage resizedImage:CGSizeMake(100, 100) interpolationQuality:kCGInterpolationMedium];
    _thumbnailImage = [_originalImage resizedImage:CGSizeMake(200, 200) interpolationQuality:kCGInterpolationHigh];

    
    return self;
}
@end
