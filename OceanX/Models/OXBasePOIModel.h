//
//  OXBasePOIModel.h
//  
//
//  Created by Systango on 2/3/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

NSString * const kPOIModelPoiNameKey;
NSString * const kBasePOIPoiIdKey;
NSString * const kBasePOICoordinatesKey;
NSString * const kBasePOIPicturesKey;
NSString * const kBasePOIPoiScoreKey;

#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>


@interface OXBasePOIModel : NSObject

@property (nonatomic, strong) NSString * poiName;
@property (nonatomic, strong) NSArray * pictures;
@property (nonatomic, assign) NSInteger poiID;
@property (nonatomic, strong) OXCoordinateModel * coordinates;
@property (nonatomic, strong) NSNumber* poiScore;
@property (nonatomic, strong) NSURL * thumbnailUrl;

- (instancetype)initWithId:(NSInteger)poiID;
- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue;

//TODO : Make algo to fetch proper image as per required size
// currently we are picking |lastObject| from array
- (NSURL *)getPictureFromData:(NSDictionary *)dictionaryValue;

@end
