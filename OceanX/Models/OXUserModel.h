//
//  OXUserModel.h
//  OceanX
//
//  Created by Pavan Kataria on 15/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXPersonModel.h"

typedef NS_ENUM(NSUInteger, Gender) {
    male = 1,
    female
};

@interface OXUserModel : OXPersonModel

@property (nonatomic, strong) NSString * nationality;
@property (nonatomic, strong) NSString * countryName;
@property (nonatomic, assign) Gender gender;
@property (nonatomic, assign) NSInteger yearBorn;
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSArray * tags;
@property (nonatomic, assign) BOOL profileActivated;
@property (nonatomic, assign) double totalFacetCompletion;
@property (nonatomic, strong) NSArray * facetScore;
@property (nonatomic, strong) NSString * password;
@property (nonatomic, assign) BOOL isProfileInComplete;
@property (nonatomic, assign) NSInteger profileIncompletionPercentage;

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue;

- (instancetype)initWithEmail:(NSString*) email username:(NSString*) username password:(NSString*) password gender:(Gender) gender yearborn:(NSInteger) yearBorn;

- (instancetype)initWithUsername:(NSString *)username;

- (instancetype)initWithUsername:(NSString *)username password:(NSString *)password;

- (NSDictionary*)retrieveLoginParams;

- (NSDictionary*)retrieveCreateUserParams;
- (NSDictionary*)retrieveCreateUserParamsForSignupWithEmail;

- (NSDictionary*)retrieveResetPasswordParams;

- (void)saveUserCredentials;
+ (void)saveUsernameWithUsername:(NSString*)usernameString;
@end
