//
//  OXBusinessHoursModel.m
//  OceanX
//
//  Created by Pavan Kataria on 15/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXBusinessHoursModel.h"

@implementation OXBusinessHoursModel
-(instancetype)init{
    self = [super init];
    return self;
}


//-(NSString *)daysRepresentation{
//    if(!_startDay){
//        return @"Select day"; //OPEN ON // OPEN FROM TILL
//    }
//    else{
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"EEE"];
//   
//        if(_startDay != nil && _endDay != nil){
//            return [NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:_startDay], [formatter stringFromDate:_endDay]];
//        }
//        else{
//            return [formatter stringFromDate:_startDay];
//        }
//    }
//}

-(NSString *)daysRepresentation{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE"];
    return [[dateFormatter stringFromDate:_weekday] uppercaseString];
}
-(NSUInteger)standAloneLocalDayOfWeekRepresentation{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"c"];
    return [[dateFormatter stringFromDate:_weekday] integerValue];
}

-(NSString *)hoursRepresentation{
    if(!_startHour || !_endHour){
        return @"Closed";
    }
    else{
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:[OXBusinessHoursModel getFormatForTimeWith24HourSystem:NO]];
        
        
        return [NSString stringWithFormat:@"%@%@%@", [timeFormatter stringFromDate:self.startHour], kPrettifiedBusinessOpeningTimeSeparatorTime, [timeFormatter stringFromDate:self.endHour]];
    }
}


+(NSString *)getJSONHoursRepresentationDate:(NSDate*)date{

    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:[OXBusinessHoursModel getJSONHoursTimeFormat]];
    return [NSString stringWithFormat:@"%@", [timeFormatter stringFromDate:date]];
}


+(NSMutableArray*)defaultMondayToFriday9to5WithoutSiestas{
    NSMutableArray *defaultArray = [[NSMutableArray alloc] init];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:[OXBusinessHoursModel getFormatForTimeWith24HourSystem:NO]];

    for(int i = 0; i < 7; i++){
        OXBusinessHoursModel *currentDay = [[OXBusinessHoursModel alloc] init];
        NSString *dateString = [NSString stringWithFormat:@"0%d-01-0001", i+1];
        NSString *startTime;
        NSString *endTime;
        
        currentDay.dayIndex = i;
        currentDay.weekday = [dateFormatter dateFromString:dateString];

        if (i < 5) {
            startTime = [NSString stringWithFormat:@"9:00am"];
            endTime = [NSString stringWithFormat:@"5:30pm"];
            currentDay.businessOpen = YES;
        }
        else{
            currentDay.businessOpen = NO;
        }
        currentDay.startHour = [timeFormatter dateFromString:startTime];
        currentDay.endHour = [timeFormatter dateFromString:endTime];
        
        NSLog(@"opening times = %@ - %@", [timeFormatter stringFromDate:currentDay.startHour], [timeFormatter stringFromDate:currentDay.endHour]);
        [defaultArray addObject:currentDay];
    }
    return defaultArray;
}

-(void)setBusinessOpen:(BOOL)businessOpen{
    _businessOpen = businessOpen;
    if(businessOpen == FALSE){
        self.startHour = nil;
        self.endHour = nil;
    }
}
+(NSUInteger)getHourPositionForDate:(NSDate*)date use24HourSystem:(BOOL)use24HourSystem{
    NSUInteger hour = [[self hourAndMinuteComponentsForDate:date use24HourSystem:use24HourSystem] hour];
    NSLog(@"hour retrieved: %lu", (unsigned long)hour);
    return hour;
}

+(NSUInteger)getMinutePositionForDate:(NSDate*)date use24HourSystem:(BOOL)use24HourSystem{
    NSUInteger minute = [[self hourAndMinuteComponentsForDate:date use24HourSystem:use24HourSystem] minute]/5;
    NSLog(@"minute retrieved: %lu", (unsigned long)minute);
    return minute;
}
+(NSUInteger)getMeridiemPositionForDate:(NSDate*)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[OXBusinessHoursModel getMeridiemTimeFormat]];
    NSUInteger meridiemPosition = ([[[dateFormatter stringFromDate:date] lowercaseString] isEqualToString:@"am"] ? 0 : 1);
    NSLog(@"meridiem retrieved: %lu", (unsigned long)meridiemPosition);

    return meridiemPosition;
}
+(NSDateComponents*)hourAndMinuteComponentsForDate:(NSDate*)date use24HourSystem:(BOOL)use24Hours{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[OXBusinessHoursModel getFormatForTimeWith24HourSystem:use24Hours]];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar components:(NSCalendarUnitMinute | NSCalendarUnitHour) fromDate:date];
}


+(NSString *)getJSONHoursTimeFormat{
    return @"HH:mm:ss";
}
+(NSString*)getFormatForTimeWith24HourSystem:(BOOL)use24Hours{
    if(use24Hours){
        return @"kk:mma";

    }
    else{
        return @"h:mma";
    }
}

+(NSString*)getMeridiemTimeFormat{
    return @"a";
}

-(NSDictionary *)params{
    return @{
     @"day": [NSNumber numberWithInteger:self.dayIndex],
     @"openTime": [OXBusinessHoursModel getJSONHoursRepresentationDate:self.startHour],
     @"closeTime": [OXBusinessHoursModel getJSONHoursRepresentationDate:self.endHour],
     @"isOpen": [NSNumber numberWithBool:self.isBusinessOpen]
     };
    
//    return @{@"day": [NSNumber numberWithUnsignedInteger:[self standAloneLocalDayOfWeekRepresentation]],
//             @"openTime": [self openingHourForDate:self.startHour],
//             @"closeTime" : [self openingHourForDate:self.endHour],
//             @"open" : [NSNumber numberWithBool:self.isBusinessOpen]
//             };
}

-(NSString*)openingHourForDate:(NSDate*)date{
    if(!date){
        return @"Closed";
    }
    else{
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:[OXBusinessHoursModel getFormatForTimeWith24HourSystem:NO]];
        return [NSString stringWithFormat:@"%@", [timeFormatter stringFromDate:date]];
    }
}


-(BOOL)isEqualToBusinessOpeningHourModel:(OXBusinessHoursModel*)openingHourObject{
    //if a siesta feature is added in the future then ensure that the time is in the same object but split afterwards
    //just so that its easier for checking.
    //That means when submitting the business opening hours to the server, the opening hour objects that contain siestas would need to be split
    if([self isBusinessOpen] == [openingHourObject isBusinessOpen]){
        if([[self hoursRepresentation] isEqualToString:[openingHourObject hoursRepresentation]]){
//            NSLog(@"currentHoursRepresentation: %@ == weekday: %lu @ hours: %@", [self hoursRepresentation], [openingHourObject dayIndex], [openingHourObject hoursRepresentation]);
            return YES;
        }
    }
    return NO;
}


-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    if(self){
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:[OXBusinessHoursModel getJSONHoursTimeFormat]];

        if([[dictionary objectForKey:@"isOpen"] boolValue] == YES){
            NSString *startTime = [dictionary objectForKey:@"openTime"];
            NSString *endTime = [dictionary objectForKey:@"closeTime"];
            self.startHour = [timeFormatter dateFromString:startTime];
            self.endHour = [timeFormatter dateFromString:endTime];
            self.businessOpen = YES;
        }
        else{
            self.endHour = nil;
            self.startHour = nil;
            self.businessOpen = NO;

        }
        self.dayIndex = [[dictionary objectForKey:@"day"] integerValue];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        
        NSString *dateString = [NSString stringWithFormat:@"0%lu-01-0001", (long)(self.dayIndex + 1)];
        self.weekday = [dateFormatter dateFromString:dateString];

//        NSLog(@"POI day: %lu opening times = %@ - %@", self.dayIndex, [timeFormatter stringFromDate:self.startHour], [timeFormatter stringFromDate:self.endHour]);
    }
    return self;
}

+(NSArray*)businessHoursFromServerBusinessHoursDictionary:(NSDictionary*)dictionary{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSDictionary *businessHourItem in dictionary) {
        OXBusinessHoursModel *businessHour = [[OXBusinessHoursModel alloc] initWithDictionary:businessHourItem];
        [tempArray addObject:businessHour];
    }
    NSSortDescriptor *mySortDescriptor = [NSSortDescriptor sortDescriptorWithKey: @"dayIndex.integerValue" ascending:YES];
    return [tempArray sortedArrayUsingDescriptors: @[mySortDescriptor]];
    
}
@end

