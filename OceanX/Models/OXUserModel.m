//
//  OXUserModel.m
//  OceanX
//
//  Created by Pavan Kataria on 15/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

NSString * const kUserModelUserIDKey = @"userId";
NSString * const kUserModelUserNameKey = @"username";
NSString * const kUserModelImageURLKey = @"imageURL";
NSString * const kUserModelTotalFacetCompletionKey = @"totalFacetCompletion";
NSString * const kUserModelYearBornKey = @"yearBorn";
NSString * const kUserModelTagsKey = @"tags";
NSString * const kUserModelProfileActivatedKey = @"profileActivated";
NSString * const kUserModelNationalityKey = @"nationality";
NSString * const kUserModelGenderKey = @"gender";

NSString * const kUserModelEmailKey = @"email";
NSString * const kUserModelCountryNameKey = @"countryName";
NSInteger const kFacetScoreLimit = 30;

#import "OXUserModel.h"
#import "OXFacetModel.h"

@implementation OXUserModel

#pragma mark - Init Methods

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue{
    NSLog(@"Full USER model Dictionary : %@", dictionaryValue);
    self = [super init];
    if (self) {
        self.personId = [[dictionaryValue getValueForKey:kUserModelUserIDKey] integerValue];
        self.username = [dictionaryValue objectForKey:kUserModelUserNameKey];
        NSLog(@"USERNAme: %@", self.username);
        self.personImageURL = [NSURL URLWithString:[[dictionaryValue getValueForKey:kUserModelImageURLKey] stringValue]];
        _email = [dictionaryValue getValueForKey:kUserModelEmailKey];
        _nationality = [dictionaryValue getValueForKey:kUserModelNationalityKey];
        _facetScore = [self getFacetsFromDictionary:dictionaryValue];
        _countryName = [dictionaryValue getValueForKey:kUserModelCountryNameKey];
        _gender = [[dictionaryValue getValueForKey:kUserModelGenderKey] integerValue];
        _profileActivated = [[dictionaryValue getValueForKey:kUserModelProfileActivatedKey] boolValue];
        _tags = [self getTagsFromDictionary:dictionaryValue];
        _yearBorn = [[dictionaryValue getValueForKey:kUserModelYearBornKey] integerValue];
        _totalFacetCompletion = [[dictionaryValue getValueForKey:kUserModelTotalFacetCompletionKey] doubleValue];
        
    }
    return self;
}

-(instancetype)initWithEmail:(NSString*) email username:(NSString*) username password:(NSString*) password gender:(Gender) gender yearborn:(NSInteger) yearBorn{
    
    self = [super init];
    
    if (self) {
        
        _email = email;
        _gender = gender;
        _yearBorn = yearBorn;
        _password = password;
        self.username = username;
    }
    return self;
}

-(instancetype)initWithUsername:(NSString *)username {
    
    return [self initWithEmail:nil username:username password:nil gender:male yearborn:0];
}

-(instancetype)initWithUsername:(NSString *)username password:(NSString *)password{
    
    return [self initWithEmail:@"" username:username password:password gender:male yearborn:0];
}

#pragma mark - Public Methods
// TODO: add constatnts
-(NSDictionary*)retrieveLoginParams
{
    return
    @{
      @"username": self.username,
      @"password": self.password,
      };
}

-(NSDictionary*)retrieveCreateUserParams
{
    return
    @{ @"email": self.email,
       @"username": self.username,
       @"password": self.password,
       @"gender": (self.gender == male) ? kCreateNewUserGenderCellMaleString : kCreateNewUserGenderCellFemaleString,
       @"dob":@(self.yearBorn),
       @"eula":@1
       };
}
//Because of a design on the server that I disagree with
//this url expects a different parameter name for the same year of birth data object
//It should be consistent across all api calls if its the same parameter object which in this case
//is the year of birth of the user
//Instead ONLY ONE METHOD SHOULD BE USED TO RETRIEVE THE PARAMS FOR THIS USER OBJECT
-(NSDictionary*)retrieveCreateUserParamsForSignupWithEmail
{
    //Because of a design on the server that I disagree with
    //this url expects a different parameter name for the same year of birth data object
    //It should be consistent across all api calls if its the same parameter object which in this case
    //is the year of birth of the user
    
    if(self.gender == male || self.gender == female){
        return
        @{ @"email": self.email,
           @"username": self.username,
           @"password": self.password,
           @"gender": (self.gender == male) ? kCreateNewUserGenderCellMaleString : kCreateNewUserGenderCellFemaleString,
           @"yearBorn":@(self.yearBorn),
           @"eula":@1
           };
    }
    else {
        return @{ @"email": self.email,
           @"username": self.username,
           @"password": self.password,
           @"yearBorn":@(self.yearBorn),
           @"eula":@1
           };
    }
}

-(NSDictionary*)retrieveResetPasswordParams{
    
    // The API call takes username as parameter, but it works for email. so sending email as parameter temporarily
    return
    @{
      @"username":self.username
      };
}

-(BOOL) isProfileInComplete
{
    return ([self.facetScore count] < kFacetScoreLimit);
}
+ (void)saveUsernameWithUsername:(NSString*)usernameString{
    NSLog(@"SAVING USERNAME: %@", usernameString);
    [[NSUserDefaults standardUserDefaults]setObject:usernameString forKey:kSavedUserNameKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)saveUserCredentials{
    [[NSUserDefaults standardUserDefaults] setObject:self.username forKey:kSavedUserNameKey];
    [[NSUserDefaults standardUserDefaults] setObject:self.password forKey:kSavedPasswordKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

#pragma mark - Private Methods

-(NSArray * )getTagsFromDictionary:(NSDictionary *)dictionaryValue
{
    NSMutableArray * tags = [NSMutableArray array];
    for (NSDictionary * tagData in [dictionaryValue getValueForKey:kUserModelTagsKey]) {
        [tags addObject:[[OXTagModel alloc] initWithDictionary:tagData]];
    }
    return  tags;
}

-(NSArray * )getFacetsFromDictionary:(NSDictionary *)dictionaryValue
{
    NSMutableArray * facets = [NSMutableArray array];
    for (NSDictionary * facetData in [dictionaryValue getValueForKey:kUserModelFacetScoreKey]) {
        [facets addObject:[[OXFacetModel alloc] initWithDictionary:facetData]];
    }
    return  facets;
}


@end

