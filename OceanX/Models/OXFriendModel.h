//
//  OXFriendModel.h
//  OceanX
//
//  Created by Pavan Kataria on 13/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OXPersonModel.h"

@interface OXFriendModel : OXPersonModel<GenericDataModelForCellConfiguration>
@property (nonatomic, assign, getter=isFriendSelected) BOOL friendSelected;
+(NSArray*)createRandomFriendsArrayForTestingPurposesWithCount:(NSUInteger)totalNumberOfFriendsToMake;
+(NSMutableArray*)getFriendsArrayFromJSONResponse:(NSDictionary*)response;


@end
