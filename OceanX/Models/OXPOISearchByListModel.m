//
//  OXPOISearchByListModel.m
//  OceanX
//
//  Created by Pavan Kataria on 03/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//
NSString * const kPOISearchAddedByKey = @"addedBy";
NSString * const kPOISearchTags = @"tags";
NSString * const kPOISearchCaption = @"caption";
NSString * const kPOISearchPoiNameKey = @"poiName";
NSString * const kPOISearchAddressKey = @"poiAddress";
NSString * const kPOISearchPoiId = @"poiId";
NSString * const kPOISearchPicturesKey= @"pictures";
NSString * const kPOISearchPoiScoreKey = @"poiScore";

NSString * const kPOISearchUserLikeOrDislikeKey = @"poiPreference";
//NSString * const kPOISearchUserDislikedKey = @"userDisliked";



NSString * const kPOISearchCoordinatesKey = @"coordinates";
NSString * const kPOISearchCoordinateXKey = @"coordinateX";
NSString * const kPOISearchCoordinateYKey = @"coordinateY";

#import "OXPOISearchByListModel.h"
#import "OXProcessedPicture.h"


@implementation OXPOISearchByListModel

-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    if(self){
//        NSLog(@"dictionary: %@", dictionary);
        self.addedBy = [dictionary objectForKey:kPOISearchAddedByKey];
        self.tags = [OXTagModel getTagsFromDictionary:[dictionary objectForKey:kPOISearchTags]] ;
        NSString *caption = [[dictionary objectForKey:kPOISearchCaption] isKindOfClass:[NSNull class]] ? @"" : [dictionary objectForKey:kPOISearchCaption];
        self.caption = caption ;
//        NSLog(@"PKPK search CAPTION: %@", self.caption);
        NSDictionary *coordinates = [dictionary objectForKey:kPOISearchCoordinatesKey];
        self.coordinate = CLLocationCoordinate2DMake([[coordinates objectForKey:kPOISearchCoordinateYKey] doubleValue], [[coordinates objectForKey:kPOISearchCoordinateXKey] doubleValue]);

        self.pictures = [OXProcessedPicture getPicturesArrayFromJSONResponse:[dictionary objectForKey:kPOISearchPicturesKey]];
        self.poiAddress = [dictionary objectForKey:kPOISearchAddressKey];
        self.poiId = [[dictionary objectForKey:kPOISearchPoiId] integerValue];
        
        self.poiName = [dictionary objectForKey:kPOISearchPoiNameKey];
    	self.poiScore = [[dictionary objectForKey:kPOISearchPoiScoreKey] floatValue];
//        NSLog(@"userLike: %@, userDisliked %@", [dictionary objectForKey:kPOISearchUserLikedKey], [dictionary objectForKey:kPOISearchUserDislikedKey]);
        
//        self.userLiked = [[dictionary objectForKey:kPOISearchUserLikedKey] integerValue];
        
        if([[dictionary objectForKey:kPOISearchUserLikeOrDislikeKey] isKindOfClass:[NSNumber class]]){
            
        }
        else{
    //#warning todo: create an enum for LIKE, DISLIKE and NEUTRAL for strings
            NSString *stringLikeOrDislike = [dictionary objectForKey:kPOISearchUserLikeOrDislikeKey];
            if([stringLikeOrDislike isEqualToString:@"LIKE"]){
                self.userLiked = TRUE;
                self.userDisLiked = FALSE;
            }
            else if([stringLikeOrDislike isEqualToString:@"DISLIKE"]){
                self.userLiked = FALSE;
                self.userDisLiked = TRUE;
            }
            if([stringLikeOrDislike isEqualToString:@"NEUTRAL"]){
                self.userLiked = FALSE;
                self.userDisLiked = FALSE;
            }
        }
    }
    return self;
}
#pragma mark - OXSearchPOIForSectionViewOperationProtocol methods

-(NSString*)OXSearchPOIForSectionViewOperationPoiName{
    return self.poiName;
}
-(NSString *)OXSearchPOIForSectionViewOperationPoiAddedBy{
    return self.addedBy;
}
-(CLLocationCoordinate2D)OXSearchPOIForSectionViewOperationPoiCoordinate{
    return self.coordinate;
}
-(UIImage*)OXSearchPOIForSectionViewOperationPoiScoreImage{
        UIImage *poiScoreImage;
    
    if(self.poiScore < 0.2){
        poiScoreImage = [UIImage imageNamed:@"veryUnamused"];
    }
    else if(self.poiScore < 0.4){
        poiScoreImage = [UIImage imageNamed:@"unamused"];
    }
    else if(self.poiScore < 0.6){
        poiScoreImage = [UIImage imageNamed:@"neutral"];
    }
    else if(self.poiScore < 0.8){
        poiScoreImage = [UIImage imageNamed:@"happy"];
    }
    else {
        poiScoreImage = [UIImage imageNamed:@"veryhappy"];
    }
    
    return poiScoreImage;
}




#pragma mark end -
+(NSArray *)setupSearchResultsArrayWithResponse:(NSArray *)array{
    NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
    
    for(NSDictionary *dictionary in array){
        OXPOISearchByListModel *currentPOI  = [[OXPOISearchByListModel alloc] initWithDictionary:dictionary];
        [mutableArray addObject:currentPOI];
    }
    return mutableArray;
}
+(void)searchByListWithPOISearchObject:(OXPOISearchModel*)object
                      withSuccessBlock:(SuccessCompletionBlock) successBlock
                             failBlock:(FailureCompletionBlock) failBlock{
    
    [OXRestAPICient searchByListWithPOISearchObject:object
                                   withSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
                                       if(successBlock){
                                           successBlock(operation, [self setupSearchResultsArrayWithResponse:response]);
                                       }
                                   } failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                       if(failBlock){
                                           failBlock(operation, error);
                                       }
                                    }];
}

/*

{
    addedBy = mrsjoanna;
    bestTags =         (
                        charcuterie,
                        "farmer & butcher",
                        "home made"
                        );
    caption = "<null>";
    coordinates =         {
        coordinateX = "-0.1522830426692963";
        coordinateY = "51.51987457275391";
    };
    pictures =         (
                        {
                            isMainPicture = 1;
                            pictureId = "<null>";
                            pictureUrl = "https://s3.amazonaws.com/ocean-bucket-test/ocean/picture/poi/751/2011_big.jpg";
                            processedPictures =                 (
                            );
                        }
                        );
    poiAddress = "4 Moxon Street, London, W1U 4EW";
    poiId = 751;
    poiName = "Ginger Pig";
    poiScore = "0.625";
    userDisliked = 0;
    userLiked = 0;
}

*/
-(NSString *)getDistanceAwayFromUser{
    return [self getDistanceWithCoordinate:self.coordinate];
}
-(NSString*)getDistanceWithCoordinate:(CLLocationCoordinate2D)placeCoordinate{
    CLLocationCoordinate2D userLocationCoordinate = [[OXLocationManager sharedTracker] currentLocation];
    CLLocation * placelocation = [[CLLocation alloc] initWithLatitude:placeCoordinate.latitude longitude:placeCoordinate.longitude];
    
    CLLocation * currentLocation = [[CLLocation alloc] initWithLatitude:userLocationCoordinate.latitude longitude:userLocationCoordinate.longitude];
    
    NSNumber * distance =  [NSNumber numberWithDouble:[placelocation distanceFromLocation:currentLocation]];
    
    
    if([distance integerValue] < 100){
        return [NSString stringWithFormat:@"Less than 100 metres away"];
    }
    else if ([distance integerValue] < 1000) {
        return [NSString stringWithFormat:@"%ld metres away", (long) (50 * floor((distance.floatValue/50)+0.5))];
    }
    else{
        return [NSString stringWithFormat:@"%.01fkm away",  distance.floatValue/1000];
    }
}

-(UIImage*)getPOIScoreImage{
    UIImage *poiScoreImage;
    
    if(self.poiScore < 0.2){
        poiScoreImage = [UIImage imageNamed:@"veryUnamused"];
    }
    else if(self.poiScore < 0.4){
        poiScoreImage = [UIImage imageNamed:@"unamused"];
    }
    else if(self.poiScore < 0.6){
        poiScoreImage = [UIImage imageNamed:@"neutral"];
    }
    else if(self.poiScore < 0.8){
        poiScoreImage = [UIImage imageNamed:@"happy"];
    }
    else {
        poiScoreImage = [UIImage imageNamed:@"veryhappy"];
    }
    
    return poiScoreImage;
}


@end
