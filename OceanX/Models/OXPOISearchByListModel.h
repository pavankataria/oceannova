//
//  OXPOISearchByListModel.h
//  OceanX
//
//  Created by Pavan Kataria on 03/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OXPOISearchModel.h"

@interface OXPOISearchByListModel : NSObject<OXSearchPOIForSectionViewOperationProtocol>
@property (nonatomic, retain) NSString *addedBy;
@property (nonatomic, retain) NSArray <OXTagsProtocolConfiguration>*tags;
@property (nonatomic, retain) NSString *caption;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) NSArray *pictures;
@property (nonatomic, retain) NSString *poiAddress;
@property (nonatomic, assign) NSInteger poiId;
@property (nonatomic, retain) NSString *poiName;
@property (nonatomic, assign) CGFloat poiScore;
@property (nonatomic, assign) BOOL userLiked;
@property (nonatomic, assign) BOOL userDisLiked;


-(instancetype)initWithDictionary:(NSDictionary*)dictionary;
-(NSString *)getDistanceAwayFromUser;

+(NSArray*)setupSearchResultsArrayWithResponse:(NSArray*)array;


+(void)searchByListWithPOISearchObject:(OXPOISearchModel*)object
                      withSuccessBlock:(SuccessCompletionBlock) successBlock
                             failBlock:(FailureCompletionBlock) failBlock;

-(UIImage*)getPOIScoreImage;

@end
/*
 {
 addedBy = mrsjoanna;
 bestTags =         (
 charcuterie,
 "farmer & butcher",
 "home made"
 );
 caption = "<null>";
 coordinates =         {
 coordinateX = "-0.1522830426692963";
 coordinateY = "51.51987457275391";
 };
 pictures =         (
 {
 isMainPicture = 1;
 pictureId = "<null>";
 pictureUrl = "https://s3.amazonaws.com/ocean-bucket-test/ocean/picture/poi/751/2011_big.jpg";
 processedPictures =                 (
 );
 }
 );
 poiAddress = "4 Moxon Street, London, W1U 4EW";
 poiId = 751;
 poiName = "Ginger Pig";
 poiScore = "0.625";
 userDisliked = 0;
 userLiked = 0;
 }
 
 */
