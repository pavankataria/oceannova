//
//  OXImageStackItem.h
//  OceanX
//
//  Created by Pavan Kataria on 26/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OXImagesStackItemProtocol <NSObject>
@optional
-(UIImage*)thumbnailImage;
-(UIImage*)originalImage;

@end
@interface OXImageStackItem : NSObject

@property (nonatomic, retain) UIImage *thumbnailImage;
@property (nonatomic, retain) UIImage *originalImage;
-(instancetype)initWithImage:(UIImage *)image;
-(instancetype)initWithImage:(UIImage *)image withSideLength:(CGFloat)sideLength;
@end



