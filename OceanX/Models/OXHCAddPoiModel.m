//
//  OXHCAddPoiModel.m
//  OceanX
//
//  Created by Pavan Kataria on 23/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXHCAddPoiModel.h"
#import "OXTagModel.h"

#import "OXImageStackItem.h"
#import "OXBusinessHoursModel.h"
#import "OXTagPlaceCountModel.h"

@implementation OXHCAddPoiPicturesModel


+(NSArray*)getArrayOfJSON64PicturesWithCapturedImagesArray:(NSArray*)capturedImages{
    NSMutableArray *json64Pictures = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < capturedImages.count; i++){
        OXImageStackItem *currentPicture = capturedImages[i];
        OXHCAddPoiPicturesModel *addPoiPictureObject = [[OXHCAddPoiPicturesModel alloc] init];
        
        NSData   * largeimageData = UIImageJPEGRepresentation(currentPicture.originalImage, 0.8);
        NSString * largeBase64    = [largeimageData base64EncodedStringWithOptions:0];
        
        addPoiPictureObject.base64Image =  largeBase64;
        if(i == 0)
            addPoiPictureObject.mainPicture = YES;
        addPoiPictureObject.mainPicture = NO;
        [json64Pictures addObject:addPoiPictureObject.base64Image];
    }
    return json64Pictures;
}
@end
@implementation OXHCAddPoiModel


-(void)setTags:(NSArray *)tags{
    NSMutableArray *tagsArray = [[NSMutableArray alloc] init];
    for(OXTagPlaceCountModel *tag in tags){
        [tagsArray addObject:[tag tagName]];
    }
    _tags = tagsArray;
}


-(NSMutableArray *)json64Pictures{
    if(!_json64Pictures){
        _json64Pictures = [[NSMutableArray alloc] init];
    }
    return _json64Pictures;
}
-(void)setPictures:(NSArray *)pictures{
    NSLog(@"started SETTING IMAGES");

    for(int i = 0; i < pictures.count; i++){
        OXImageStackItem *currentPicture = pictures[i];
        OXHCAddPoiPicturesModel *addPoiPictureObject = [[OXHCAddPoiPicturesModel alloc] init];
        
        NSData   * largeimageData = UIImageJPEGRepresentation(currentPicture.originalImage, 0.8);
        NSString * largeBase64    = [largeimageData base64EncodedStringWithOptions:0];

        addPoiPictureObject.base64Image =  largeBase64;//[UIImagePNGRepresentation(currentPicture.originalImage) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        if(i == 0)
        addPoiPictureObject.mainPicture = YES;
        addPoiPictureObject.mainPicture = NO;
//        NSLog(@"setting image %d, 64bit: %@", i, addPoiPictureObject.base64Image);
        [self.json64Pictures addObject:addPoiPictureObject.base64Image];
    }
    NSLog(@"DONE SETTING IMAGES");
}
//
//-(NSArray *)get64RepresentationOfPictures{
//    NSMutableArray *pictures64StringArray = [[NSMutableArray alloc] init];
//    for(OXImageStackItem *currentPicture in self.pictures){
//        [pictures64StringArray addObject:[self base64StringForImage:currentPicture.originalImage]];
//    }
//    return pictures64StringArray;
//}

-(NSDictionary*)params{
    OXCoordinateModel *coordinate = [[OXCoordinateModel alloc] init];
//    coordinate = CLLocationCoordinate2DMake(0.0, 0.0);
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    coordinate.coordinate = [[OXLocationManager sharedTracker] currentLocation];
    
    NSDictionary *picturesDictionary;
    
    if(self.json64Pictures.count > 0){
        picturesDictionary = @{@"pictures" : [self json64Pictures], @"mainPictureIndex": [NSNumber numberWithInteger:0]};
        [dictionary setObject:picturesDictionary forKey:@"pictures"];

    }
    NSLog(@"self previous wrong coordinate sent to the server: {%f, %f}", coordinate.coordinate.longitude, coordinate.coordinate.latitude);
    if(self.tags.count > 0){
        [dictionary setObject:self.tags forKey:@"tags"];
    }
    [dictionary setObject:self.twitter forKey:@"twitter"];
    [dictionary setObject:self.telephone forKey:@"telephone"];
    [dictionary setObject:self.website forKey:@"website"];
    [dictionary setObject:self.address forKey:@"address"];
    [dictionary setObject:[coordinate params] forKey:@"coordinate"];
    [dictionary setObject:[self businessHoursForJSON] forKey:@"businessHours"];
    [dictionary setObject:self.poiName forKey:@"poiName"];
    return dictionary;
}

//-(NSArray *)tags{
//    if([_tags count] > 0){
//        return _tags;
//    }
//    else{
//        return [NSArray array];  
//    }
//}
-(NSArray*)businessHoursForJSON{
    if([_businessHours count] > 0){
        NSMutableArray *businessHoursJSONArray = [[NSMutableArray alloc] init];
        for(int i = 0; i < self.businessHours.count; i++){
            [businessHoursJSONArray addObject:[self.businessHours[i] params]];
        }
        return businessHoursJSONArray;
    }
    else{
        return [NSArray array];
    }
}

-(NSString *)address{
    if([_address length] > 0){
        return _address;
    }
    return kJSONNullString;
}
-(NSString *)telephone{
    if([_telephone length] > 0){
        return _telephone;
    }
    return kJSONNullString;
}

-(NSString *)poiName{
    if([_poiName length] > 0){
        return _poiName;
    }
    return kJSONNullString;
}
-(NSString *)website{
    if([_website length] > 0){
        return _website;
    }
    return kJSONNullString;
}

-(NSString *)twitter{
    if([_twitter length] > 0){
        return _twitter;
    }
    return kJSONNullString;
}




//#warning todo: put this method in an image category
- (NSString *)base64StringForImage:(UIImage*)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}
/*
{
    "pictures": {
        "pictures": [
                     "Array"
                     ],
        "mainPictureIndex": 0
    },
    "poiName": "",
    "website": "",
    "businessHours": [
                      {
                          "day": 0,
                          "openTime": "",
                          "closeTime": "",
                          "open": false
                      }
                      ],
    "telephone": "",
    "twitter": "",
    "coordinate": {
        "coordinateX": 0,
        "coordinateY": 0,
        "positionTime": ""
    },
    "tags": [
             ""
             ],
    "address": ""
}
*/


-(BOOL)isPoiValidAtBusinessDetailsPage{
    if([self.poiName length] > 0){
        return YES;
    }
    return NO;
}
@end
