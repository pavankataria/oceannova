//
//  OXFacetModel.h
//  OceanX
//
//  Created by Systango on 2/6/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OXDimensionModel.h"

@interface OXFacetModel : NSObject

@property (nonatomic, strong) NSNumber * facetID;
@property (nonatomic, strong) NSString * facetDescription;
@property (nonatomic, strong) NSString * facetText;
@property (nonatomic, strong) OXDimensionModel * dimension;
@property (nonatomic, strong) NSArray * questions;
@property (nonatomic, assign) double facetScore;

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue;
+(NSString*) hiddenFacetDescription;

@end
