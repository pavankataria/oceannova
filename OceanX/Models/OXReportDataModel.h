//
//  OXReportDataModel.h
//  OceanX
//
//  Created by Systango on 16/02/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

typedef NS_ENUM(NSUInteger, ReportScreenType){
    ReportScreenTypeSpam = 0,
    ReportScreenTypeInnapropriate,
    ReportScreenTypeTextReport
};

#import <Foundation/Foundation.h>

@interface OXReportDataModel : NSObject

@property (nonatomic, assign) NSInteger reportID;
@property (nonatomic, strong) NSString *reportText;
@property (nonatomic, assign) ReportScreenType reportType;
@property (nonatomic, assign) ReportSource reportSource;

- (instancetype)initWithId:(NSInteger)reportID andSource:(ReportSource)reportSource;

- (NSDictionary*)params;

@end
