//
//  OXQuestionModel.h
//  OceanX
//
//  Created by Systango on 2/6/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OXQuestionModel : NSObject

@property (nonatomic, strong) NSString * questionText;
@property (nonatomic) NSInteger questionId;
@property (nonatomic) NSInteger answer;


- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue;

+(NSArray*)JSONArrayFromQuestionsArray:(NSArray*)questionsArray;

@end
