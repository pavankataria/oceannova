//
//  OXBasePOIModel.m
//  
//
//  Created by Systango on 2/3/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

NSString * const kPOIModelPoiNameKey = @"poiName";
NSString * const kBasePOIPoiIdKey = @"poiId";
NSString * const kBasePOICoordinatesKey = @"coordinates";
NSString * const kBasePOIPicturesKey = @"pictures";
NSString * const kBasePOIPoiScoreKey = @"poiScore";
NSString * const kThumbnailUrlKey = @"thumbnailUrl";
NSString * const kBasePOIProcessedPicturesKey = @"processedPictures";

#import "OXBasePOIModel.h"

@implementation OXBasePOIModel

#pragma mark - Init methods

- (instancetype)initWithId:(NSInteger)poiID
{
    self = [super init];
    if (self) {
        _poiID = poiID;
    }
    return self;
    
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue
{
    self = [super init];
    if (self)
    {
        self.poiName = [dictionaryValue getValueForKey:kPOIModelPoiNameKey];
        self.pictures = [dictionaryValue getValueForKey:kBasePOIPicturesKey];
        self.poiID = [[dictionaryValue getValueForKey:kBasePOIPoiIdKey] integerValue];
        self.coordinates = [[OXCoordinateModel alloc] initWithDictionary:[dictionaryValue getValueForKey:kBasePOICoordinatesKey]];
        self.poiScore = [dictionaryValue objectForKey:kBasePOIPoiScoreKey];
        self.thumbnailUrl = [self getPictureFromData:dictionaryValue];
    }
    return self;
}

#pragma mark - Private methods

//TODO : Make algo to fetch proper image as per required size
// currently we are picking |lastObject| from array

- (NSURL *)getPictureFromData:(NSDictionary *)dictionaryValue {
    NSArray *pictureList = [dictionaryValue objectForKey:kBasePOIPicturesKey];
    for (NSDictionary *pictureDictionary in pictureList) {
        NSArray *processedPicturedictionaries = [pictureDictionary objectForKey:kBasePOIProcessedPicturesKey];
        
        NSMutableArray *processedPictures = [NSMutableArray array];
        for (NSDictionary *processedPictureDictionary in processedPicturedictionaries) {
            [processedPictures addObject:[processedPictureDictionary objectForKey:@"url"]];
        }
        return [NSURL URLWithString:[processedPictures firstObject]];
    }
    return nil;
}

#pragma mark - Override IsEqual

-(BOOL) isEqual:(id)object{
    
    if ([object isKindOfClass:OXBasePOIModel.class]) {
        
        return ((OXBasePOIModel *)object).poiID == self.poiID;
    }
    
    return NO;
    
}

@end
