//
//  OXNewFacetDataModel.m
//  OceanX
//
//  Created by Systango on 2/9/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//


#import "OXNewFacetDataModel.h"

@implementation OXNewFacetDataModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue{
    self = [super init];
    if (self)
    {
        _facetText = [dictionaryValue getValueForKey:kFacetModelFacetTextKey];
        _facetID = [dictionaryValue getValueForKey:kFacetModelFacetIdKey];
        _facetDescription = [dictionaryValue getValueForKey:kFacetModelFacetDescriptionKey];
        _facetScore = FACET_NULL_VALUE;
        _dimension = [self getDimensionFromDictionary:dictionaryValue];
        _questions = [self getQuestionsFromDictionary:dictionaryValue];
    }
    return self;
}

#pragma mark - Public Methods

+(NSArray*)facetsArrayWithFacetsArray:(NSArray*)facetsArray{
    NSMutableArray * facets = [NSMutableArray array];
    
    for (NSDictionary * facetData in facetsArray) {
        [facets addObject:[[OXNewFacetDataModel alloc] initWithDictionary:facetData]];
    }
    return facets;
}

#pragma mark - Private Methods

-(NSArray *)getQuestionsFromDictionary:(NSDictionary *)dictionaryValue
{
    NSMutableArray * questions = [NSMutableArray array];
    for (NSDictionary * questionsData in [dictionaryValue getValueForKey:kFacetModelQuestionsKey]) {
        [questions addObject:[[OXQuestionModel alloc] initWithDictionary:questionsData]];
    }
    return  questions;
}

-(OXDimensionModel *)getDimensionFromDictionary:(NSDictionary *)dictionaryValue{
    
    return [[OXDimensionModel alloc] initWithDictionary:[dictionaryValue getValueForKey:kFacetModelDimensionDetailsKey]];
}



@end
