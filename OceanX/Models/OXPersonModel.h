//
//  OXPersonModel.h
//  OceanX
//
//  Created by Pavan Kataria on 15/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>

@interface OXPersonModel : NSObject//MTLModel <MTLJSONSerializing>

@property (nonatomic, assign) NSInteger personId;
@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSURL *personImageURL;


//+ (NSDictionary *)JSONKeyPathsByPropertyKey;
- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue;
-(NSString*)name;

@end

