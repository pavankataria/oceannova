//
//  OXDimensionModel.h
//  OceanX
//
//  Created by Systango on 2/6/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OXDimensionModel : NSObject

@property (nonatomic, strong) NSNumber * dimensionId;
@property (nonatomic, strong) NSString * dimensionText;
@property (nonatomic, strong) NSString * dimensionCode;

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue;

@end
