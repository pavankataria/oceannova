//
//  OXHCAddPoiModel.h
//  OceanX
//
//  Created by Pavan Kataria on 23/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OXCoordinateModel.h"
#import "OXHCModelDelegate.h"
#import "OXBusinessHoursModel.h"

@interface OXHCAddPoiPicturesModel : NSObject
@property (nonatomic, retain) NSString *base64Image;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, assign, getter=isMainPicture) BOOL mainPicture;
+(NSArray*)getArrayOfJSON64PicturesWithCapturedImagesArray:(NSArray*)capturedImages;
@end



@interface OXHCAddPoiModel : NSObject<OXHCModelDelegate>
@property (nonatomic, retain) NSArray *pictures;
@property (nonatomic, retain) NSMutableArray *json64Pictures;

@property (nonatomic, retain) NSString *poiName;
@property (nonatomic, retain) NSString *website;
@property (nonatomic, retain) NSString *telephone;
@property (nonatomic, retain) NSString *twitter;
@property (nonatomic, retain) NSString *address;

@property (nonatomic, retain) OXCoordinateModel *coordinate;
@property (nonatomic, retain) NSArray *businessHours;
@property (nonatomic, retain) NSMutableArray *tags;

-(BOOL)isPoiValidAtBusinessDetailsPage;
/*
{
    "pictures": {
        "pictures": [
                     "Array"
                     ],
        "mainPictureIndex": 0
    },
    "poiName": "",
    "website": "",
    "businessHours": [
                      {
                          "day": 0,
                          "openTime": "",
                          "closeTime": "",
                          "open": false
                      }
                      ],
    "telephone": "",
    "twitter": "",
    "coordinate": {
        "coordinateX": 0,
        "coordinateY": 0,
        "positionTime": ""
    },
    "tags": [
             ""
             ],
    "address": ""
}
*/
@end
