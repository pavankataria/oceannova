//
//  OXProcessedPicture.m
//  OceanX
//
//  Created by Pavan Kataria on 03/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//
NSString * const kProcessedPicturePictureURL = @"url";
NSString * const kProcessedPictureWidthKey = @"width";
NSString * const kProcessedPictureHeightKey = @"height";

NSString * const kProcessedPicturePicturesKey = @"pictures";

NSString * const kProcessedPicturePictureIdKey = @"Id";
NSString * const kProcessedPictureMainPictureKey = @"isMainPicture";
NSString * const kProcessedPictureProcessedPicturesArrayKey = @"processedPictures";


#import "OXProcessedPicture.h"
@implementation OXProcessedPictureElement
-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    if(self){
//        NSLog(@"processed picture element: %@", dictionary);
        self.pictureURL = [dictionary objectForKey:kProcessedPicturePictureURL];
        self.pictureSize = (CGSize){[[dictionary objectForKey:kProcessedPictureWidthKey] floatValue], [[dictionary objectForKey:kProcessedPictureHeightKey] floatValue]};
    }
    return self;
}
+(NSArray*)pictureElementArrayFromJSONResponse:(NSArray*)array{
    NSMutableArray *pictureProcessedArray = [[NSMutableArray alloc] init];
    for (NSDictionary *pictureElementDictionary in array) {
        OXProcessedPictureElement *pictureProcessedElement = [[OXProcessedPictureElement alloc] initWithDictionary:pictureElementDictionary];
        [pictureProcessedArray addObject:pictureProcessedElement];
    }
    return pictureProcessedArray;
}
@end
@implementation OXProcessedPicture
-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    if(self){
//        NSLog(@"processed picture:%@", dictionary);
        self.pictureURL = [dictionary objectForKey:kProcessedPicturePictureURL];

        if([dictionary objectForKey:kProcessedPicturePictureIdKey] != [NSNull null]){
            self.pictureId = [[dictionary objectForKey:kProcessedPicturePictureIdKey] integerValue];
        }
        else{
            self.pictureId = -1;
        }
        
        self.mainPicture = [[dictionary objectForKey:kProcessedPictureMainPictureKey] boolValue];
        self.processedPictures = [OXProcessedPictureElement pictureElementArrayFromJSONResponse:[dictionary objectForKey:kProcessedPictureProcessedPicturesArrayKey]];

    }
    return self;
}

+(NSArray *)getPicturesArrayFromJSONResponse:(NSArray *)array{
    NSMutableArray *arrayOfPictureElements = [[NSMutableArray alloc] init];
    for(NSDictionary *pictureDictionary in array){
        OXProcessedPicture *pictureElement = [[OXProcessedPicture alloc] initWithDictionary:pictureDictionary];
        [arrayOfPictureElements addObject:pictureElement];
    }
    
//    NSLog(@"pictures: %@", arrayOfPictureElements);
    return arrayOfPictureElements;
}
-(void)loadImages{
   /* if(!processedPicture.mainImageToBeUsedTemporarilyUntilProcessedImageURLSAreComingThrough){
        NSString *imageURL = [processedPicture.pictureURL stringByReplacingOccurrencesOfString:@"ocean-bucket-test" withString:@"ocean-bucket"];
        //            NSLog(@"loading image url: %@", imageURL);
        NSURL *url = [NSURL URLWithString:imageURL];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        __weak OXPlaceDisplayImageTableViewCell *weakCell = self;
        __weak OXProcessedPicture *weakProcessedImage = processedPicture;
        
        [weakCell.placeImageView setImageWithURLRequest:request
                                       placeholderImage:nil
                                                success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                    //                                                       NSLog(@"successfully downloaded image");
                                                    
                                                    [UIView transitionWithView:weakCell.placeImageView
                                                                      duration:0.1f
                                                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                                                    animations:^{
                                                                        //                                                                           NSLog(@"doing animation");
                                                                        weakCell.placeImageView.image = image;
                                                                        weakProcessedImage.mainImageToBeUsedTemporarilyUntilProcessedImageURLSAreComingThrough = image;
                                                                        
                                                                    } completion:nil];
                                                    [weakCell setNeedsLayout];
                                                    
                                                } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                                    NSLog(@"failed to load image: %@", error);
                                                }];
    }
    else{
        self.placeImageView.image = processedPicture.mainImageToBeUsedTemporarilyUntilProcessedImageURLSAreComingThrough;
    }
    */
}
@end


