//
//  OXTagPlaceCountModel.h
//  OceanX
//
//  Created by Pavan Kataria on 19/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXTagModel.h"
#import "OXProtocolsHeader.h"

@interface OXTagPlaceCountModel : OXTagModel<GenericDataModelForCellConfiguration, OXTagModelOperationProtocol>
@property (nonatomic, assign) NSUInteger totalPlacesCount;
@property (nonatomic, retain) NSString *totalPlacesCountString;
@property (nonatomic, assign, getter=shouldCreateNewTag) BOOL createNewTag;

-(instancetype)initWithTagName:(NSString*)tagName andTotalPlacesCount:(NSUInteger)totalPlacesCount;
-(instancetype)initWithTagName:(NSString*)tagName andTotalPlacesCount:(NSUInteger)totalPlacesCount andCreateNewTag:(BOOL)createNewTag;



+(NSArray*)createRandomTagPlaceCountsArrayForTestingPurposesWithCount:(NSUInteger)totalNumberOfTagPlaceCountsToMake;
+(NSMutableArray *)createAutoSuggestionArrayFromResponse:(NSArray *)response filteringOutStoredTags:(NSArray*)storedTagsArray;
- (NSComparisonResult)compare:(OXTagPlaceCountModel *)otherObject;
//+(NSArray*)handleAutoTagsArrayResponse:(NSArray*)response;

@end
