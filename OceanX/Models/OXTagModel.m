//
//  OXTagModel.m
//  OceanX
//
//  Created by Pavan Kataria on 17/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

NSString * const kTagModelTagNameKey = @"tagName";
NSString * const kTagModelTagAffirmedKey = @"hasAffirmed";
NSString * const kTagModelTagCountKey = @"tagCount";
NSString * const kTagModelTagIdKey = @"tagId";


#import "OXTagModel.h"

@implementation OXTagModel
/*
 @"ID": @"idTag",
 @"name": @"tagName",
 @"tagCount":@"tagCount"
 
 */

-(NSString*)name{
    NSLog(@"tag name: %@", _tagName);
    return self.tagName;
}

-(void)setName:(NSString *)name{
    _tagName = name;
}

+(NSArray*)createRandomTagsArrayForTestingPurposesWithCount:(NSUInteger)totalNumberOfTagsToMake{
    NSMutableArray *randomFriendsArray = [[NSMutableArray alloc] init];
    for(NSUInteger i = 0; i < totalNumberOfTagsToMake; i++){
        OXTagModel *tag = [[self alloc] init];
        tag.tagName = @"PavanKataria";
        [randomFriendsArray addObject:tag];
    }
    return [randomFriendsArray mutableCopy];
}

-(instancetype)initWithDictionary:(NSDictionary*) dictionaryValue{
    
    self = [super init];
    if (self) {
        _tagName = [dictionaryValue getValueForKey:kTagModelTagNameKey];
        
        if([dictionaryValue objectForKey:kTagModelTagIdKey]){
            _tagId = [[dictionaryValue getValueForKey:kTagModelTagIdKey] integerValue];
        }
        if([dictionaryValue getValueForKey:kTagModelTagCountKey]){
            _tagCount = [[dictionaryValue getValueForKey:kTagModelTagCountKey] integerValue]-1;
        }
        if([dictionaryValue getValueForKey:kTagModelTagAffirmedKey]){
            _tagHasAffirmed = [[dictionaryValue getValueForKey:kTagModelTagAffirmedKey] boolValue];
        }
    }
    return self;
    
}


+(NSArray <OXTagsProtocolConfiguration>*)getTagsFromDictionary:(NSDictionary *) dictionaryValue{
    NSMutableArray <OXTagsProtocolConfiguration>* tags = (NSMutableArray<OXTagsProtocolConfiguration>*)[NSMutableArray array];
    for (NSDictionary * tagData in dictionaryValue) {
//        NSLog(@"TAG DATA: %@", tagData);
        [tags addObject:[[OXTagModel alloc] initWithDictionary:tagData]];
    }
    return [self sortTagsArray:tags ascendingOrder:NO];
}

+(NSArray<OXTagsProtocolConfiguration>*)sortTagsArray:(NSMutableArray<OXTagsProtocolConfiguration>*)array ascendingOrder:(BOOL)ascendingOrder{
    NSSortDescriptor *sortByTagCount = [[NSSortDescriptor alloc] initWithKey:@"tagCount" ascending:ascendingOrder];
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"tagName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    
    NSArray <OXTagsProtocolConfiguration>*tags = (NSArray<OXTagsProtocolConfiguration>*)[array sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortByTagCount, sortByName, nil]];
    return tags;
}
-(NSString*)totalPlacesCountString{
    return [NSString stringWithFormat:@"%lu", (long)self.tagCount];
}


+(NSDictionary*)paramsForAddTagsForPOIIDWithTagsArray:(NSArray*)array andPOIID:(NSUInteger)poiID{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:[NSNumber numberWithInteger:poiID] forKey:@"poiId"];
    
    NSMutableArray *tagsStringArray = [[NSMutableArray alloc] init];
    [dictionary setObject:tagsStringArray forKey:@"tagNames"];
    
    for (id<OXTagsProtocolConfiguration> tag in array) {
        [tagsStringArray addObject:[tag tagName]];
    }
    NSLog(@"dictionary add tags for poi result: %@", dictionary);
    return dictionary;
}
-(void)setAffirm:(BOOL)affirm withPOIID:(NSUInteger)poiID{
    if(affirm){
        [OXRestAPICient affirmTagID:self.tagId withPOIID:poiID
                       successBlock:^(AFHTTPRequestOperation *operation, id response) {
                           NSLog(@"successfulled affirm tag id");
                       } failBlock:^(AFHTTPRequestOperation *operation, id error) {
                           NSLog(@"failed update tag id: %@", operation);
                       }];
    }
    else{
        [OXRestAPICient disaffirmTagID:self.tagId withPOIID:poiID
                          successBlock:^(AFHTTPRequestOperation *operation, id response) {
                              NSLog(@"successfulled disaffirm tag id");
                          } failBlock:^(AFHTTPRequestOperation *operation, id error) {
                              NSLog(@"failed update tag id: %@", operation);
                          }];
    }
}

@end
