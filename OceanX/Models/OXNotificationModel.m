//
//  OXNotificationModel.m
//  Ocean
//
//  Created by Sam Payne on 01/07/2014.
//  Copyright (c) 2014 Ocean. All rights reserved.
//

#import "OXNotificationModel.h"

@implementation OXNotificationModel

+(NSDictionary*) JSONKeyPathsByPropertyKey{
    
    return @{};
}

- (id)initWithReceiverId:(NSInteger)recieverId recieverName:(NSString *)recieverName
{
    self = [super init];
    if(self)
    {
        _recieverId = recieverId;
        _recieverName = recieverName;
    }
    
    return self;
}

- (id)initWithSenderId:(NSInteger)senderId senderName:(NSString *)senderName
{
    self = [super init];
    if(self)
    {
        _senderId = senderId;
        _senderName = senderName;
    }
    
    return self;
}

+(NSMutableArray*) arrayOfNotificationsFromJSONDictionary:(NSDictionary*) dictionary{

  NSMutableArray * returnArray = [NSMutableArray new];

  for (NSDictionary * notificationAsJSON in dictionary) {
    OXNotificationModel * notification = [MTLJSONAdapter modelOfClass:OXNotificationModel.class fromJSONDictionary:notificationAsJSON error:nil];
    [returnArray addObject:notification];
  }

  return returnArray;
  
}

-(BOOL) isEqual:(id)object{
    
    if ([object isKindOfClass:OXNotificationModel.class]) {
        
        OXNotificationModel * notification = (OXNotificationModel*) object;
        
        return ((self.senderId!=0 && notification.senderId == self.senderId)
                || (self.recieverId !=0 && notification.recieverId == self.recieverId));
        
    }
    
    return NO;
    
}

@end
