//
//  OXPrettifiedBusinessOpeningTimeModel.h
//  OceanX
//
//  Created by Pavan Kataria on 25/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OXBusinessHoursModel.h"

@interface OXPrettifiedBusinessOpeningTimeModel : NSObject

//#warning todo: Make a parent class for OXPrettifiedBusinessOpeningTimeModel and OXBusinessOpeningHourModel for some shared variables and methods
@property (nonatomic, retain) NSDate *startingWeekDay;
@property (nonatomic, retain) NSDate *endingWeekDay;
@property (nonatomic, retain) NSDate *startHour;
@property (nonatomic, retain) NSDate *endHour;

@property (nonatomic, retain) NSString *daysString;
@property (nonatomic, retain) NSString *hoursString;
-(NSString*)daysRepresentation;
-(NSString*)hoursRepresentation;


-(instancetype)initWithBusinessHoursObject:(OXBusinessHoursModel*)businessHoursObject;

+(NSMutableArray*)prettifyBusinessOpeningHours:(NSArray*)businessOpeningHours;
@end
