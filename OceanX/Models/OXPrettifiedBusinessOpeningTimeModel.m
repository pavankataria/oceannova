//
//  OXPrettifiedBusinessOpeningTimeModel.m
//  OceanX
//
//  Created by Pavan Kataria on 25/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXPrettifiedBusinessOpeningTimeModel.h"

@implementation OXPrettifiedBusinessOpeningTimeModel

-(instancetype)initWithBusinessHoursObject:(OXBusinessHoursModel*)businessHoursObject{
    self = [super init];
    if(self){
        self.startingWeekDay = businessHoursObject.weekday;
        self.startHour = businessHoursObject.startHour;
        self.endHour = businessHoursObject.endHour;
    }
    return self;
}
-(NSString *)daysRepresentation{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE"];
    
    NSString *daysRepresentation;
    
    if(self.endingWeekDay){
        daysRepresentation = [NSString stringWithFormat:@"%@%@%@", [dateFormatter stringFromDate:self.startingWeekDay], kPrettifiedBusinessOpeningTimeSeparatorDays, [dateFormatter stringFromDate:self.endingWeekDay]];
    }
    else{
        daysRepresentation = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:self.startingWeekDay]];
    }
    return daysRepresentation;
}
-(NSString *)hoursRepresentation{
    if(!self.startHour || !self.endHour){
        return @"Closed";
    }
    else{
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:[OXBusinessHoursModel getFormatForTimeWith24HourSystem:NO]];
        return [NSString stringWithFormat:@"%@%@%@", [timeFormatter stringFromDate:self.startHour], kPrettifiedBusinessOpeningTimeSeparatorTime, [timeFormatter stringFromDate:self.endHour]];
    }
}

+(NSMutableArray*)prettifyBusinessOpeningHours:(NSArray*)businessOpeningHours{
    NSMutableArray *prettifiedBusinessOpeningHours = [[NSMutableArray alloc] init];
    
    for(int kStartingSearch = 0; kStartingSearch < businessOpeningHours.count; kStartingSearch++){
        OXBusinessHoursModel *currentOH = businessOpeningHours[kStartingSearch];
        
        OXPrettifiedBusinessOpeningTimeModel *prettifiedOHObject = [[OXPrettifiedBusinessOpeningTimeModel alloc] initWithBusinessHoursObject:currentOH];
        //        NSLog(@"i = %d, starting saved hour: %@", kStartingSearch, [currentOH hoursRepresentation]);
        
        for(int j = kStartingSearch+1; j < businessOpeningHours.count; j++){
            
            OXBusinessHoursModel *nextOH = businessOpeningHours[j];
            //            NSLog(@"j = %d, ending saved hour: %@", j, [nextOH hoursRepresentation]);
            //
            //
//            NSLog(@"currently comparing: current day: %@ &time: %@ with jDay: %@ &time: %@", [currentOH daysRepresentation], [currentOH hoursRepresentation], [nextOH daysRepresentation], [nextOH hoursRepresentation]);
            if([currentOH isEqualToBusinessOpeningHourModel:nextOH]){
                [prettifiedOHObject setEndingWeekDay:nextOH.weekday];
                kStartingSearch = j;
                NSLog(@"prettifiedObject: %@ - %@", prettifiedOHObject.startingWeekDay, prettifiedOHObject.endingWeekDay);

            }
            else{
//                NSLog(@"prettifiedObject: %@ - %@", prettifiedOHObject.startingWeekDay, prettifiedOHObject.endingWeekDay);
                break;
            }
        }
        [prettifiedBusinessOpeningHours addObject:prettifiedOHObject];
    }
    return prettifiedBusinessOpeningHours;
}


@end
