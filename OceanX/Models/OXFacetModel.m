//
//  OXFacetModel.m
//  OceanX
//
//  Created by Systango on 2/6/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXFacetModel.h"

@implementation OXFacetModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue{
    self = [super init];
    if (self)
    {
        _facetText = [dictionaryValue getValueForKey:kFacetModelFacetTextKey];
        _facetID = [dictionaryValue getValueForKey:kFacetModelFacetIdKey];
        _facetDescription = [dictionaryValue getValueForKey:kFacetModelFacetDescriptionKey];
//        self.facetScore = FACET_NULL_VALUE;
        _facetScore = [[dictionaryValue getValueForKey:kUserModelFacetScoreKey] doubleValue];
        _dimension = [self getDimensionFromDictionary:dictionaryValue];
        _questions = [self getQuestionsFromDictionary:dictionaryValue];
    }
    return self;
}

+(NSString*) hiddenFacetDescription{
    
    return @"";
}
#pragma mark Private Methods

-(OXDimensionModel *)getDimensionFromDictionary:(NSDictionary *)dictionaryValue{
    if ([dictionaryValue hasValueForKey:kFacetModelDimensionDetailsKey]) {
        return
        [[OXDimensionModel alloc] initWithDictionary:[dictionaryValue getValueForKey:kFacetModelDimensionDetailsKey]];
    }
    return [[OXDimensionModel alloc] initWithDictionary:dictionaryValue];
}



-(NSArray *)getQuestionsFromDictionary:(NSDictionary *)dictionaryValue
{
    NSMutableArray * questions = [NSMutableArray array];
    for (NSDictionary * questionsData in [dictionaryValue getValueForKey:kFacetModelQuestionsKey]) {
        [questions addObject:[[OXQuestionModel alloc] initWithDictionary:questionsData]];
    }
    return  questions;
}



@end
