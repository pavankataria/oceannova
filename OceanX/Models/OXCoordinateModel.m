//
//  OXCoordinateModel.m
//  OceanX
//
//  Created by Pavan Kataria on 23/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

NSString * const kCoordinateModelCoordinateXKey = @"coordinateX";
NSString * const kCoordinateModelCoordinateYKey = @"coordinateY";

#import "OXCoordinateModel.h"

/*"coordinate": {
    "coordinateX": 0,
    "coordinateY": 0,
    "positionTime": ""
},
*/
@implementation OXCoordinateModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue{
    self = [super init];
    if (self) {
        _coordinate = CLLocationCoordinate2DMake([[dictionaryValue getValueForKey:kCoordinateModelCoordinateYKey] floatValue], [[dictionaryValue getValueForKey:kCoordinateModelCoordinateXKey] floatValue]);
    }
    return self;
}

-(NSDictionary*)params{
    return @{kCoordinateModelCoordinateXKey : [NSNumber numberWithDouble:self.coordinate.longitude],
             kCoordinateModelCoordinateYKey : [NSNumber numberWithDouble:self.coordinate.latitude]
             };
}

#pragma mark - OXSearchModelOperationProtocol - Methods

-(NSString *)searchResultTitle{
    return self.placeMarkTitleAndSubtitle;
}

@end
