//
//  OXHCModelDelegate.h
//  OceanX
//
//  Created by Pavan Kataria on 23/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol OXHCModelDelegate <NSObject>
@required
-(NSDictionary*)params;
@end

