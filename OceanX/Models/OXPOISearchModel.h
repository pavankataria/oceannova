//
//  OXPOISearchModel.h
//  OceanX
//
//  Created by Pavan Kataria on 03/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXCoordinateModel.h"
@interface OXPOISearchModel : NSObject

@property (nonatomic, retain) OCNPlaneModel *planeRestModel;
@property (nonatomic, retain) NSMutableArray *friendsIds;
@property (nonatomic, retain) NSMutableArray *tags;
@property (nonatomic, retain) NSString *textSearchString;
@property (nonatomic, retain) OXCoordinateModel *coordinate;

-(NSString*)howManyJoining;
-(NSString*)searchingByText;
-(NSString*)currentLocationTitle;

-(NSDictionary*)params;
-(void)setWithCoordinateModel:(OXCoordinateModel*)object;

-(void)removeAllTagsFromSearch;
@end
