//
//  OXTagModel.h
//  OceanX
//
//  Created by Pavan Kataria on 17/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OXHCModelDelegate.h"

#import "OXProtocolsHeader.h"


@interface OXTagModel : NSObject
@property (nonatomic, assign) NSInteger tagId;
@property (nonatomic, retain) NSString *tagName;
@property (nonatomic, assign) NSInteger tagCount;
@property (nonatomic, assign) BOOL tagHasAffirmed;



//@property (assign) NSInteger tagCount;
//@property (assign) double userTagScore;
//@property (assign) NSInteger affirmedByUser;

-(NSString*)name;
-(void)setName:(NSString *)name;
+(NSArray*)createRandomTagsArrayForTestingPurposesWithCount:(NSUInteger)totalNumberOfTagsToMake;
-(instancetype)initWithDictionary:(NSDictionary*) dictionaryValue;
+(NSArray*)getTagsFromDictionary:(NSDictionary *)dictionaryValue;


-(void)setAffirm:(BOOL)affirm withPOIID:(NSUInteger)poiID;

+(NSDictionary*)paramsForAddTagsForPOIIDWithTagsArray:(NSArray*)array andPOIID:(NSUInteger)poiID;
@end
