//
//  OXReportDataModel.m
//  OceanX
//
//  Created by Systango on 16/02/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXReportDataModel.h"

@implementation OXReportDataModel

- (instancetype)initWithId:(NSInteger)reportID  andSource:(ReportSource)reportSource
{
    self = [super init];
    if (self) {
        _reportID = reportID;
        _reportSource = reportSource;
    }
    return self;
    
}

- (NSDictionary*)params {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:[self getStringForReportType] forKey:@"reportType"];
    if(self.reportType == ReportScreenTypeTextReport){
        [dictionary setObject:self.reportText forKey:@"reportMessage"];
    }
    return dictionary;
}


- (NSString *)getStringForReportType {
    switch (self.reportType) {
        case ReportScreenTypeSpam:
            return @"SPAM";
            break;
        case ReportScreenTypeInnapropriate:
            return @"INAPPROPRIATE";
            break;
        case ReportScreenTypeTextReport:
            return @"MESSAGE";
            break;
    }
    return nil;
}

@end
