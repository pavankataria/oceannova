//
//  OXPOISearchModel.m
//  OceanX
//
//  Created by Pavan Kataria on 03/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXPOISearchModel.h"

@interface OXPOISearchModel ()
@property (nonatomic, retain) NSMutableArray *stringTags;

@end

@implementation OXPOISearchModel

-(void)setTextSearchString:(NSString *)textSearchString{
    _textSearchString = textSearchString;
    if(textSearchString){
        [self removeAllTagsFromSearch];
    }
}
-(NSMutableArray *)stringTags{
    if(!_stringTags){
        _stringTags = [[NSMutableArray alloc] init];
    }
    return _stringTags;
}
-(void)setTags:(NSArray *)tags{
    [self.stringTags removeAllObjects];
    if(tags.count > 0){
        for(OXTagPlaceCountModel *tag in tags){
            [self.stringTags addObject:[tag tagName]];
            NSLog(@"self.string tags: %@", [tag tagName]);
        }
        _tags = [tags mutableCopy];
        self.textSearchString = nil;
        
        NSLog(@"SELF TAGS: %@", self.stringTags);
        NSLog(@"_tags: %@", _tags);
    }
}
-(OCNPlaneModel *)planeRestModel{
    if(!_planeRestModel){
        MKCoordinateRegion currentRegion = [[OXLocationManager sharedTracker] currentMapRegion];
        OCNPlaneModel * plane = [OCNPlaneModel planeModelFromMapRegion:currentRegion];
        _planeRestModel = plane;
    }
    return _planeRestModel;
}
-(NSDictionary*)params{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setObject:[MTLJSONAdapter JSONDictionaryFromModel:self.planeRestModel] forKey:@"planeRestModel"];
    
    if([self.friendsIds count] > 0){
        [dictionary setObject:self.friendsIds forKey:@"friendIds"];
    }
    
    NSLog(@"SELF TAGS: %@", self.stringTags);
    if([self.stringTags count] > 0){
        [dictionary setObject:self.stringTags forKey:@"tags"];
    }
    if(self.textSearchString){
        [dictionary setObject:self.textSearchString forKey:@"text"];
    }
    return dictionary;
}

-(void)setWithCoordinateModel:(OXCoordinateModel *)object{
    NSLog(@"SET WITH COORDINATE MODEL");
    MKCoordinateRegion currentRegion = [[OXLocationManager sharedTracker] regionWithLocation:object.coordinate];
    
    
    OCNPlaneModel * plane = [OCNPlaneModel planeModelFromMapRegion:currentRegion];
    self.coordinate = object;
    self.planeRestModel = plane;
}

-(NSString*)howManyJoining{
    if(self.friendsIds.count == 0){
        return @"None joining";
    }
    else{
        return [NSString stringWithFormat:@"%lu", (unsigned long)self.friendsIds.count];
    }
}
-(NSString*)searchingByText{
    if(self.textSearchString.length == 0){
        return @"";
    }
    else{
        return [NSString stringWithFormat:@"%@", [self textSearchString]];
    }
}

-(NSString*)currentLocationTitle{
    if(!self.coordinate){
        return @"Current Location";
    }
    else{
        return [self.coordinate placeMarkTitleAndSubtitle];
    }
}
-(void)removeAllTagsFromSearch{
    [self.tags removeAllObjects];
    [self.stringTags removeAllObjects];
}
@end
