//
//  NewsFeedDataModel.h
//  OceanX
//
//  Created by Pavan Kataria on 09/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OXPOIModel.h"
@interface OXSectionData : NSObject
@property (nonatomic, assign) NSUInteger indexSection;
-(instancetype)initWithSectionAtIndexSection:(NSUInteger)indexSection;
@end

@interface OXSections : NSObject
@property (nonatomic, retain) NSMutableArray *pois;
@property (nonatomic, retain) NSString *sectionTitle;
@property (nonatomic, retain) NSString *sectionCaption;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
@interface NewsFeedDataModel : NSObject
@property (nonatomic, retain) NSMutableArray *sections;
@property (nonatomic, assign) NSUInteger totalNumberOfSectionsToDisplay;
@property (nonatomic, retain) NSMutableArray *sectionData;



-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

-(BOOL)shouldDisplaySectionHeaderForSection:(NSUInteger)section;

-(NSString*)newsFeedTitleAtSection:(NSUInteger)section;
-(NSString*)appropriateTitleForPOITrickSection:(NSUInteger)section;
-(OXPOISearchByListModel*)poiForSection:(NSUInteger)section;
-(BOOL)areThereAnyPOISForTrickSection:(NSUInteger)section;
@end
