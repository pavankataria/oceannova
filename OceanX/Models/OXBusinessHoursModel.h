//
//  OXBusinessHoursModel.h
//  OceanX
//
//  Created by Pavan Kataria on 15/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OXHCModelDelegate.h"
//@interface OXDaySelectionModel : NSObject
////typedef NS_ENUM(NSUInteger, OXDaySelectionOpeningHoursType) {
////    ODaySelectionOpeningHoursSelectionNone,
////    ODaySelectionOpeningHoursSelectionSingleOrFrom,
////    ODaySelectionOpeningHoursSelectionPassing,
////    ODaySelectionOpeningHoursSelectionTill
////};
//
//@property (nonatomic, assign) NSUInteger dayIndex;
////@property (nonatomic, assign) OXDaySelectionOpeningHoursType dayPosition;
//-(NSString*)dayName;
//
//@end


@protocol OXBusinessHoursProtocol <NSObject>

-(NSString*)daysRepresentation;
-(NSString*)hoursRepresentation;

@end

@interface OXBusinessHoursModel : NSObject<OXHCModelDelegate>
@property (nonatomic, assign) NSUInteger dayIndex;
@property (nonatomic, retain) NSDate *weekday;

@property (nonatomic, retain) NSDate *startHour;

@property (nonatomic, retain) NSDate *endHour;
@property (nonatomic, assign, getter=isBusinessOpen) BOOL businessOpen;
-(instancetype)init;
-(instancetype)initWithDictionary:(NSDictionary*)dictionary;
-(NSString*)daysRepresentation;
-(NSString*)hoursRepresentation;

+(NSMutableArray*)defaultMondayToFriday9to5WithoutSiestas;


+(NSUInteger)getHourPositionForDate:(NSDate*)date use24HourSystem:(BOOL)use24HourSystem;
+(NSUInteger)getMinutePositionForDate:(NSDate*)date use24HourSystem:(BOOL)use24HourSystem;

+(NSUInteger)getMeridiemPositionForDate:(NSDate*)date;
+(NSString *)getJSONHoursRepresentationDate:(NSDate*)date;

+(NSString*)getFormatForTimeWith24HourSystem:(BOOL)use24Hours;

-(BOOL)isEqualToBusinessOpeningHourModel:(OXBusinessHoursModel*)openingHourObject;


+(NSArray*)businessHoursFromServerBusinessHoursDictionary:(NSDictionary*)dictionary;
@end
