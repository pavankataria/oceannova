//
//  OXNotificationModel.h
//  Ocean
//
//  Created by Sam Payne on 01/07/2014.
//  Copyright (c) 2014 Ocean. All rights reserved.
//

@interface OXNotificationModel : MTLModel <MTLJSONSerializing>


@property (nonatomic) NSInteger notificationId;
@property (nonatomic) NSInteger senderId;
@property (nonatomic,strong) NSString * senderName;
@property (nonatomic) NSInteger recieverId;
@property (nonatomic,strong) NSString * recieverName;
@property (nonatomic) NSInteger isNew;

+(NSMutableArray*) arrayOfNotificationsFromJSONDictionary:(NSDictionary*) dictionary;

- (id)initWithSenderId:(NSInteger)senderId senderName:(NSString *)senderName;
- (id)initWithReceiverId:(NSInteger)recieverId recieverName:(NSString *)recieverName;

@end
