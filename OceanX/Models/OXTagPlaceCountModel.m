//
//  OXTagPlaceCountModel.m
//  OceanX
//
//  Created by Pavan Kataria on 19/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXTagPlaceCountModel.h"

@implementation OXTagPlaceCountModel
-(instancetype)initWithTagName:(NSString*)tagName andTotalPlacesCount:(NSUInteger)totalPlacesCount{
    static BOOL defaultShouldCreateNewTag = NO;
    return [self initWithTagName:tagName andTotalPlacesCount:totalPlacesCount andCreateNewTag:defaultShouldCreateNewTag];
}

-(instancetype)initWithTagName:(NSString*)tagName andTotalPlacesCount:(NSUInteger)totalPlacesCount andCreateNewTag:(BOOL)createNewTag{
    self = [super init];
    if(self){
        self.createNewTag = createNewTag;
        self.tagName = tagName;
        self.totalPlacesCount = totalPlacesCount;
    }
    return self;
}
-(NSString*)getTotalPlacesCountStringRepresentation{
    NSString *display = @"";
    if(_totalPlacesCount > 0){
         display = [NSString stringWithFormat:@"%@ places", [NSNumberFormatter localizedStringFromNumber:@(_totalPlacesCount) numberStyle:NSNumberFormatterDecimalStyle]];
    }
    return display;
}
-(NSString *)totalPlacesCountString{
    return _totalPlacesCountString;
}

-(void)setTotalPlacesCount:(NSUInteger)totalPlacesCount{
//    NSLog(@"set total plces count: %lu", (NSUInteger)totalPlacesCount);
    _totalPlacesCount = totalPlacesCount;
    _totalPlacesCountString = [self getTotalPlacesCountStringRepresentation];
}
+(NSArray*)createRandomTagPlaceCountsArrayForTestingPurposesWithCount:(NSUInteger)totalNumberOfTagPlaceCountsToMake{
    NSMutableArray *randomTagsArray = [[NSMutableArray alloc] init];
    for(NSUInteger i = 0; i < totalNumberOfTagPlaceCountsToMake; i++){
        OXTagPlaceCountModel *tag = [[self alloc] initWithTagName:[NSString stringWithFormat:@"Tag %lu", (unsigned long)i] andTotalPlacesCount:arc4random()%10000];
        [randomTagsArray addObject:tag];
    }
    NSSortDescriptor* tagPlacesCountDescriptor = [[NSSortDescriptor alloc] initWithKey:@"totalPlacesCount" ascending:NO];
    NSArray *sortedArray = [randomTagsArray sortedArrayUsingDescriptors:@[tagPlacesCountDescriptor]];
    return sortedArray;
}

#pragma mark - GenericDataModelForCellConfiguration Protocol

-(NSString *)cellIdentifier{
    return kTableViewCellTagIdentifier;
}



#pragma mark - Class methods
+(NSMutableArray*)createArrayFromResponse:(NSArray*)response{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for(NSDictionary *dictionary in response){
        NSString *tagName = [dictionary objectForKey:kAddTagViewControllerTagPlaceCountModelTagName];
        NSUInteger placesCount = [[dictionary objectForKey:kAddTagViewControllerTagPlaceCountModelTagPlaceCount] integerValue];
        OXTagPlaceCountModel *currentTag = [[self alloc] initWithTagName:tagName andTotalPlacesCount:placesCount];
        [result addObject:currentTag];
    }
    return result;
}
+(NSArray*)filterUsingTagNamePropertyAndRemoveObjectsThatAreInArray:(NSArray*)needleArray fromArray:(NSMutableArray*)sourceArray{
    for(int i = 0; i < [needleArray count]; i++){
        OXTagPlaceCountModel *currentObjectFromNeedleArray = needleArray[i];
        for(int j = 0; j < [sourceArray count]; j++){
            OXTagPlaceCountModel *currentObjectFromSourceArray = sourceArray[j];
            if([currentObjectFromNeedleArray.tagName isEqualToString:currentObjectFromSourceArray.tagName]){
                [sourceArray removeObjectAtIndex:j];
                break;
            }
        }
    }
    
    //order by places count
    NSArray *sortedArray;
    sortedArray = [sourceArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        OXTagPlaceCountModel *first = a;
        OXTagPlaceCountModel *second = b;
        return [first compare:second];
    }];

    return sortedArray;
}

- (NSComparisonResult)compare:(OXTagPlaceCountModel *)otherObject{
    if (self.totalPlacesCount < otherObject.totalPlacesCount){
        return NSOrderedDescending;
    }
    else if (self.totalPlacesCount > otherObject.totalPlacesCount){
        return NSOrderedAscending;
    }
    else{
        return NSOrderedSame;
    }
}

+(NSMutableArray *)createAutoSuggestionArrayFromResponse:(NSArray *)response filteringOutStoredTags:(NSArray*)storedTagsArray {
    NSMutableArray *arrayFromResponse = [OXTagPlaceCountModel createArrayFromResponse:response];
    NSMutableArray *filteredAutoSuggestionArray = [[OXTagPlaceCountModel filterUsingTagNamePropertyAndRemoveObjectsThatAreInArray:storedTagsArray fromArray:arrayFromResponse] mutableCopy];
    
    return filteredAutoSuggestionArray;
}

#pragma mark - OXTagModelOperationProtocol methods
-(NSString *)OXTagModelOperationTagNameString{
    return [self tagName];
}
-(NSString *)OXTagModelOperationPlaceCountString{
    return [self getTotalPlacesCountStringRepresentation];
}
-(BOOL)OXTagModelOperationShouldCreateNewTag{
    return self.shouldCreateNewTag;
}




#pragma mark end -
@end
