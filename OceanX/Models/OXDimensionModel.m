//
//  OXDimensionModel.m
//  OceanX
//
//  Created by Systango on 2/6/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

NSString * const kDimensionModelDimensionIdKey = @"dimensionId";
NSString * const kDimensionModelDimensionTextKey = @"dimensionText";
NSString * const kDimensionModelDimensionCodeKey = @"dimensionCode";

#import "OXDimensionModel.h"

@implementation OXDimensionModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue{
    self = [super init];
    if (self)
    {
        _dimensionCode = [dictionaryValue getValueForKey:kDimensionModelDimensionCodeKey];
        _dimensionText = [dictionaryValue getValueForKey:kDimensionModelDimensionTextKey];
        _dimensionId = [dictionaryValue getValueForKey:kDimensionModelDimensionIdKey];
    }
    return self;
}

@end
