//
//  OXQuestionModel.m
//  OceanX
//
//  Created by Systango on 2/6/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//


NSString * const kQuestionModelQuestionIDKey = @"questionId";
NSString * const kQuestionModelQuestionTextKey = @"questionText";
NSString * const kQuestionModelQuestionScoreKey = @"questionScore";

#import "OXQuestionModel.h"

@implementation OXQuestionModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue{
    self = [super init];
    if (self)
    {
        _questionId = [[dictionaryValue getValueForKey:kQuestionModelQuestionIDKey] integerValue];
        _questionText = [dictionaryValue getValueForKey:kQuestionModelQuestionTextKey];
        _answer = [[dictionaryValue getValueForKey:kQuestionModelQuestionScoreKey] integerValue];
    }
    return self;
}

# pragma mark Public Methods
+(NSArray*)JSONArrayFromQuestionsArray:(NSArray*)questionsArray{
    NSMutableArray *array = [NSMutableArray array];
    
    for(OXQuestionModel *question in questionsArray){
        [array addObject:[question dictionaryForKeys]];
    }
    return array;
}

# pragma mark Private Methods
-(NSDictionary*)dictionaryForKeys
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:[NSNumber numberWithInteger:self.questionId] forKey:kQuestionModelQuestionIDKey ];
    [dictionary setObject:[NSNumber numberWithInteger:self.answer] forKey:kQuestionModelQuestionScoreKey];
    return dictionary;
}


@end
