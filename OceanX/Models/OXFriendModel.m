//
//  OXFriendModel.m
//  OceanX
//
//  Created by Pavan Kataria on 13/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXFriendModel.h"

@implementation OXFriendModel

+(NSArray*)createRandomFriendsArrayForTestingPurposesWithCount:(NSUInteger)totalNumberOfFriendsToMake{
    NSMutableArray *randomFriendsArray = [[NSMutableArray alloc] init];
    for(NSUInteger i = 0; i < totalNumberOfFriendsToMake; i++){
        OXFriendModel *friend = [[self alloc] init];
        
        friend.username = @"PavanKataria";
        [randomFriendsArray addObject:friend];
    }
    return [randomFriendsArray mutableCopy];
}

#pragma mark - GenericDataModelForCellConfiguration Protocol

-(NSString *)cellIdentifier{
    return kTableViewCellFriendIdentifier;
}
+(NSMutableArray*)getFriendsArrayFromJSONResponse:(NSDictionary*)response{
    NSMutableArray *friendsArray = [[NSMutableArray alloc] init];
    for(NSDictionary *friend in response){
        OXFriendModel *friendObject = [[OXFriendModel alloc] initWithDictionary:friend];
        [friendsArray addObject:friendObject];
    }
    return friendsArray;
}
@end
