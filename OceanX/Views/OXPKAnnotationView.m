//
//  OXPKAnnotationView.m
//  OceanX
//
//  Created by Pavan Kataria on 24/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXPKAnnotationView.h"

@implementation OXPKAnnotationView

-(id) initWithTitle:(NSString *) title AndCoordinate:(CLLocationCoordinate2D)coordinate{
    self =  [super init];
    if(self){
        _title = title;
        _coordinate = coordinate;
    }
    return self;
}
@end
