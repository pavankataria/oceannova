//
//  MapListTableViewCell.h
//  OceanX
//
//  Created by admin on 13/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol MapListCellProtocol <NSObject>

@required

// the cell adjusts it's size automatically within reasonable constraints


//-(void)setPlaceImage:(UIImage *)placeImage;
//-(void)setPlaceName:(NSString *)placeName;

@end

@interface MapListTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *placeImageView;

-(void)setPlaceName:(NSString *)placeName;
-(void)setAddedBy:(NSString *)addedBy;
-(void)setPlaceImage:(UIImage *)placeImage;
-(void)setPlaceScoreImage:(UIImage *)placeScoreImage;
-(void)setTagBannerTags:(NSArray<OXTagsProtocolConfiguration> *)tagsArray;



-(void)setPlaceImageWithURLString:(NSString *)urlString;






@end
