//
//  LoginInfoTableViewCell.h
//  Ocean
//
//  Created by Pavan Kataria on 06/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginInfoTableViewCell : UITableViewCell
@property (nonatomic, retain) IBOutlet UITextField *loginInformationTextField;

@end
