//
//  OXAddTagTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 20/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXAddTagTableViewCell.h"

@implementation OXAddTagTableViewCell

- (void)awakeFromNib {
    self.backgroundColor = [PKUIAppearanceController darkBlueColor];
    [self.addTagTextField setTextColor:[UIColor whiteColor]];
//?    self.addTagTextField
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
