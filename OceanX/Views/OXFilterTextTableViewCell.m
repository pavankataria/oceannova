//
//  OXFilterTextTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 08/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXFilterTextTableViewCell.h"

@implementation OXFilterTextTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellWithSearchPOIObject:(OXPOISearchModel*)object{
    if([object searchingByText].length > 0){
        self.textSearchTextField.text = [object searchingByText];//@"Search by text";
    }
}

@end
