//
//  OXThumbnailsViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 24/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXThumbnailsViewController.h"
#import "OXBusinessDetailsTableViewController.h"
@interface OXThumbnailsViewController ()

@end
@implementation OXThumbnailsViewController
-(void)awakeFromNib{
    [self initViews];
}
-(void)initViews{
    self.mainSliderView = [[PKPreviewSliderView alloc] init];
    [self.view addSubview:self.mainSliderView];
    self.thumbnailSliderView = [[PKThumbnailSliderView alloc] init];
    [self.view addSubview:self.thumbnailSliderView];
    
    self.mainSliderView.delegate = self.thumbnailSliderView;
    self.thumbnailSliderView.delegate = self.mainSliderView;
    self.thumbnailSliderView.parentDelegate = self;
    
}
-(void)setCapturedImages:(NSMutableArray *)capturedImages{
    _capturedImages = capturedImages;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"view did load method called in thumbnails view controller");
}
-(void)processPictures:(id)sender{
    [self.delegate processPicturesAndSendToServer];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);
    
    if(self.comingFromVC == kAddPlaceComingFromVCTypeDetailPOIPage){
        NSLog(@"kAddPlaceComingFromVCTypeDetailPOIPage");
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add pictures" style:UIBarButtonItemStyleDone target:self action:@selector(processPictures:)];
    }
    
}

- (void)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    /*
     CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
     CGFloat navigationBarHeight = self.navigationController.navigationBar.frame.size.height;
     CGFloat height = CGRectGetHeight(self.view.frame)-navigationBarHeight-statusBarHeight;
     CGFloat minimumBottomBorder = 150;
     //        CGFloat minimumTopBorder = 100;
     
     self.smallestEdgeLength = fmin(CGRectGetWidth(self.view.bounds), height);
     */
    
    [self.mainSliderView setFrame:CGRectMake(0, 0,CGRectGetWidth(self.view.frame), CGRectGetWidth(self.view.frame))];
    CGFloat remainingHeight = CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.mainSliderView.frame);
    [self.thumbnailSliderView setFrame:CGRectMake(0, CGRectGetMaxY(self.mainSliderView.frame), CGRectGetWidth(self.view.frame), remainingHeight)];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self setViewsWithImages];
}
-(void)setViewsWithImages{
    [self.mainSliderView setAlreadyLoadedImages:self.capturedImages];
    [self.thumbnailSliderView setAlreadyLoadedImages:self.capturedImages];
}

#pragma mark - Navigation

//  In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:kSegueIdentifierBusinessInfoViewController]){
        OXBusinessDetailsTableViewController * businessVC = segue.destinationViewController;
        businessVC.addPoiObject = self.addPoiObject;
        businessVC.businessImages = self.capturedImages;
        
    }
}


#pragma mark - PKThumbnailViewSliderDelegate - Methods
-(void)thumbnailSlider:(PKThumbnailSliderView *)thumbnailSlider didDeleteThumbnailPreviewAtIndexPosition:(NSUInteger)index{
    NSLog(@"deleting object at index: %lu", (unsigned long)index   );
    [self.capturedImages removeObjectAtIndex:index];
    [self.delegate updateViewAfterUpdateCapturedImagesArray:self.capturedImages];
    NSLog(@"did delete: captured images count: %lu", (unsigned long)self.capturedImages.count);
    if(self.capturedImages.count > 0){
        [self setViewsWithImages];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end