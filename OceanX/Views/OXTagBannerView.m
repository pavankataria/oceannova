//
//  OXTagBannerView.m
//  OceanX
//
//  Created by Pavan Kataria on 11/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXTagBannerView.h"
@interface OXTagBannerView ()
@property (nonatomic, retain) NSMutableArray *viewsArray;
@property (nonatomic, retain) NSMutableArray *tagsStringArray;

@end
@implementation OXTagBannerView
-(NSMutableArray *)viewsArray{
    if(!_viewsArray){
        _viewsArray = [[NSMutableArray alloc] init];
    }
    return _viewsArray;
}
-(NSMutableArray *)tagsStringArray{
    if(!_tagsStringArray){
        _tagsStringArray = [[NSMutableArray alloc] init];
    }
    return _tagsStringArray;
}


-(void)setTagsArray:(NSArray<OXTagsProtocolConfiguration> *)tagsArray{
    [self.tagsStringArray removeAllObjects];
    for(id <OXTagsProtocolConfiguration>tag in tagsArray){
        [self.tagsStringArray addObject:[tag tagName]];
    }
    [self createLabels];
}
-(void)awakeFromNib{
    [self setBackgroundColor:[PKUIAppearanceController darkBlueColor]];
    [self createLabels];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self createLabels];
}

-(void)createLabels{
    [self createLabelsfrom:self.tagsStringArray withXOrigin:0 withYOrigin:kTagBannerViewYOrigin inWidth:CGRectGetWidth(self.bounds)];
}
-(void)createLabelsfrom:(NSArray *)tagsArray withXOrigin:(CGFloat)xOrigin withYOrigin:(CGFloat)yOrigin inWidth:(CGFloat)availableWidth{
    BOOL isFirstLabel = TRUE;
    
    if([tagsArray count] > 0){
        for(UIView *subview in self.viewsArray) {
            [subview removeFromSuperview];
        }
        [self.viewsArray removeAllObjects];
        
        for (NSString *tag in tagsArray){
            if (isFirstLabel){
                // creates a single label
                UILabel *label = [self createLabelFromString:tag withXOrigin:xOrigin withYOrigin:yOrigin];
                xOrigin = xOrigin + label.frame.size.width;
                [self.viewsArray addObject:label];
                isFirstLabel = FALSE;
            }
            else{ //creates a dividing view and a single label
                UIView *dividingView = [self createDividingViewWithXOrigin:xOrigin withYOrigin:0.0/*yOrigin*/];
                
                
                UILabel *label = [self createLabelFromString:tag withXOrigin:xOrigin + dividingView.frame.size.width withYOrigin:yOrigin];
                //checking if the views fit
                CGFloat viewsWidth = dividingView.frame.size.width + label.frame.size.width + xOrigin;
                if ( availableWidth - viewsWidth > 0 ){
                    [self.viewsArray addObject:dividingView];
                    [self.viewsArray addObject:label];
                    xOrigin = label.frame.origin.x + label.frame.size.width;
                }else{
                    break;
                }
            }
        }
        [self centerViews:self.viewsArray inWidth:availableWidth];
        [self centerViewsVertically:self.viewsArray];
    }
    else{
        NSLog(@"tag array is empty");
    }
}

-(void)centerViewsVertically:(NSMutableArray *)views
{
    
    
    for (UIView *view in views){
        
        CGFloat originY = (self.frame.size.height - view.frame.size.height)/2;
        
        
        view.frame = CGRectMake(view.frame.origin.x, originY, view.frame.size.width, view.frame.size.height);
        
        
    }
    
}

-(void)centerViews:(NSMutableArray *)views inWidth:(CGFloat)availableWidth{
    CGFloat viewsWidth = 0.0; //sum of the widths of all the views on the list; inludes labels and dividers
    CGFloat spaceLeft = 0.0; //space left after subtracting viewsWidth from availableWidth, which is the width of the screen
    CGFloat bufferSize = 0.0; // Space left to be divided by two
    
    for (UIView *view in views){
        viewsWidth += view.frame.size.width;
    }
    spaceLeft = availableWidth - viewsWidth;
    bufferSize = spaceLeft/2;
    for (UIView *view in views) {
        view.frame = CGRectMake(view.frame.origin.x + bufferSize, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
        
        [self addSubview:view];
    }
}

-(UIView *)createDividingViewWithXOrigin:(CGFloat)xOrigin withYOrigin:(CGFloat)yOrigin{
    //put anything here that you would want to work as the dividing view
    //adjust as necessary
    CGFloat viewWidth = 40;
    CGFloat viewHeight = self.frame.size.height;
    
    UIView *dividingView = [[UIView alloc] initWithFrame:CGRectMake(xOrigin, yOrigin, viewWidth, viewHeight)];
    dividingView.backgroundColor = [UIColor clearColor];
    
    
    UIView *dotView = [[UIView alloc] init];
    [dotView setBackgroundColor:[UIColor whiteColor]];
    CGFloat radius = 2.5;
    CGRect circularFrame = CGRectMake(CGRectGetWidth(dividingView.frame)/2-radius, CGRectGetHeight(dividingView.frame)/2-radius, radius*2, radius*2);
    [dotView.layer setCornerRadius:radius];
    [dividingView addSubview:dotView];
    [dotView setFrame:circularFrame];
    
    return dividingView;
}

-(UILabel *)createLabelFromString:(NSString *)tag withXOrigin:(CGFloat)xOrigin withYOrigin:(CGFloat)yOrigin{
    UILabel *label = [[UILabel alloc] init];
    label.text = [tag capitalizedString];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0]];
    [label setTextColor:[UIColor whiteColor]];
    [label sizeToFit];
    label.frame = CGRectMake(xOrigin, yOrigin, label.frame.size.width, label.frame.size.height);
    return label;
}@end
