//
//  OXGenderSelectionTableViewCell.m
//  OceanX
//
//  Created by admin on 25/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXGenderSelectionTableViewCell.h"

@interface OXGenderSelectionTableViewCell ()

@property (strong, nonatomic) UILabel *label;
@property (assign, nonatomic) CGFloat buffer;

@end


@implementation OXGenderSelectionTableViewCell

- (void)awakeFromNib {
    // Initialization code
    NSLog(@"awake from nib");
    [self initialise];
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initialise];
    }
    return self;
}
-(void)initialise{
    self.genderSelectionControl = [[UISegmentedControl alloc] initWithItems:@[@"Male", @"Female"]];

    self.genderSelectionControl.tintColor = [UIColor whiteColor];
    self.label = [[UILabel alloc] init];
    self.label.text = @"Gender";
    [self.label sizeToFit];
    self.label.textColor = [UIColor whiteColor];

    self.buffer = 10;
    
    [self addSubview:self.genderSelectionControl];
//    [self addSubview:self.label];
}

-(void)genderSelected:(UISegmentedControl *)segment
{
    if(segment.selectedSegmentIndex == 0)
    {
        self.selectedGender = male;
        
    }else if (segment.selectedSegmentIndex == 1)
    {
        self.selectedGender = female;
        
    }
    
    NSLog(@"selected gender segment (OXGenderSelectionTableViewCell) %lu",(unsigned long)self.selectedGender);
    
    
}


-(void)layoutSubviews
{
    
    [super layoutSubviews];
    
    [self.genderSelectionControl addTarget:self action:@selector(genderSelected:) forControlEvents: UIControlEventValueChanged];

    CGFloat xOriginGenderSelection = (CGRectGetWidth(self.frame) - CGRectGetWidth(self.genderSelectionControl.frame))/2;
    
    CGFloat xOriginLabel = (CGRectGetWidth(self.frame) - CGRectGetWidth(self.label.frame))/2;
    
    
    self.label.frame = CGRectMake(xOriginLabel, CGRectGetMinY(self.label.frame), CGRectGetWidth(self.label.frame), CGRectGetHeight(self.label.frame));
    
    self.genderSelectionControl.frame = CGRectMake(xOriginGenderSelection, CGRectGetMaxY(self.label.frame) + self.buffer, CGRectGetWidth(self.genderSelectionControl.frame), CGRectGetHeight(self.genderSelectionControl.frame));
    self.genderSelectionControl.center = self.contentView.center;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end