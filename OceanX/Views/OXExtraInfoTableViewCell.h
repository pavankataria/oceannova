//
//  OXExtraInfoTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 12/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXPrettifiedBusinessOpeningTimeModel.h"

@interface OXExtraInfoTableViewCell : UITableViewCell<UITextFieldDelegate, OXBusinessDetailCellConfiguration>

@property (nonatomic, weak) IBOutlet UILabel *headerlabel;
@property (nonatomic, weak) IBOutlet UITextField *infoTextField;

@property (nonatomic, weak) id <OXBusinessDetailCellDelegate> delegate;

-(instancetype)getPreparedCellWithIndexPath:(NSIndexPath*)indexPath;
-(void)updateCellForPretifyOpeningHour:(OXPrettifiedBusinessOpeningTimeModel*)pretifyBusinessOHObject;
@end
