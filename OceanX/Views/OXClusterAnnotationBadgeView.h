//
//  TBClusterAnnotationView.h
//  TBAnnotationClustering
//
//  Created by Theodore Calmes on 10/4/13.
//  Copyright (c) 2013 Theodore Calmes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXClusterAnnotationBadgeView : UIView

@property (assign, nonatomic) NSUInteger count;

@end
