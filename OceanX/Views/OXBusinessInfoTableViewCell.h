//
//  OXBusinessInfoTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 12/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXBusinessInfoTableViewCell : UITableViewCell<UITextFieldDelegate, OXBusinessDetailCellConfiguration>

@property (nonatomic, weak) IBOutlet UIImageView *businessImageView;
@property (nonatomic, weak) IBOutlet UITextField *businessNameTextField;
@property (nonatomic, retain) NSIndexPath *indexPath;

@property (nonatomic, weak) id <OXBusinessDetailCellDelegate> delegate;

@end
