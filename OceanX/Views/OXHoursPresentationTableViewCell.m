//
//  OXHoursPresentationTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 19/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXHoursPresentationTableViewCell.h"

@implementation OXHoursPresentationTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)updateCellForPretifyOpeningHour:(OXPrettifiedBusinessOpeningTimeModel*)pretifyBusinessOHObject{
    self.headerlabel.text = [pretifyBusinessOHObject daysRepresentation];
    self.headerlabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
    self.headerlabel.textAlignment = NSTextAlignmentRight;
    self.infoTextField.text = [pretifyBusinessOHObject hoursRepresentation];
    [self.infoTextField setEnabled:NO];
}


@end
