//
//  OXSectionFooterSignUpTableViewCell.h
//  Ocean
//
//  Created by Pavan Kataria on 17/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol OXSectionFooterPrivacyAndTermsProtocol <NSObject>

-(void)privacyScreenLinkPressed;
-(void)termsAndConditionsLinkPressed;
@end

@interface OXSectionFooterSignUpTableViewCell : UITableViewCell<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *privacyConditionsTextView;
@property (nonatomic, weak) id <OXSectionFooterPrivacyAndTermsProtocol> delegate;

@end
