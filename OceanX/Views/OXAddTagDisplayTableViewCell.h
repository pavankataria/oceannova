//
//  OXAddTagDisplayTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 20/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXAddTagDisplayTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *tagName;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;
@end
