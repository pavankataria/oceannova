//
//  OXTextfieldTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 19/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXTextfieldTableViewCell.h"

@implementation OXTextfieldTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.textField.layer setCornerRadius:self.textField.frame.size.height/2];
    [self.textField.layer setBorderWidth:0];
    [self.textField setBackgroundColor:[PKUIAppearanceController darkBlueColor]];
    self.textField.textColor = [UIColor whiteColor];
    
    UIColor *color = [UIColor whiteColor];
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[kOXAddTagsTextFieldPlaceHolderText uppercaseString] attributes:@{NSForegroundColorAttributeName:color}];
    self.textField.textAlignment = NSTextAlignmentCenter; // Pre-iOS6 SDK: UITextAlignmentCenter

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
