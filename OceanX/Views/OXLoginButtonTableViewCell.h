//
//  OXLoginButtonTableViewCell.h
//  Ocean
//
//  Created by Pavan Kataria on 13/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXLoginButtonTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIButton *loginButton;
@end
