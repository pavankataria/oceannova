//
//  WouldYouLikeView.m
//  Ocean
//
//  Created by Pavan Kataria on 16/04/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "WouldYouLikeView.h"
@interface WouldYouLikeView()
@property (nonatomic, assign) BOOL subviewsLayedOut;
@end
@implementation WouldYouLikeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithPOI:(OXPOISearchByListModel*)poi{
    self = [super init];
    if(self){
        _poi = poi;
    }
    return self;
}

-(NSAttributedString*)attributedTextForQuestionLabel{
    
    //shortcut
    //three templates
    //and see which one fits.
    NSAttributedString *finalRenderedAttributeString;
    NSString *prefixString;
    if(self.poi.poiScore >= 0.6){
        prefixString = @"You like ";
    }
    else if(self.poi.poiScore >= 0.4){
        prefixString = @"You may like ";
    }
    else if(self.poi.poiScore >= 2){
        prefixString = @"You may dislike ";
    }
    else{
        prefixString = @"You dislike ";
    }
    NSMutableString *youLikeString = [[NSMutableString alloc] initWithString:prefixString];
    NSInteger numberOfSentencesToTry = 2;
    
    
    for(int sentenceCounter = 0; sentenceCounter < numberOfSentencesToTry; sentenceCounter++){
        NSAttributedString *renderedString;
        NSMutableString *trialSentence = [[NSMutableString alloc] initWithString:youLikeString];
        NSString *nextStringElement = @"";
        //Sentence basic sentence
        if(sentenceCounter == 0){
            nextStringElement = [NSString stringWithFormat:@"%@.", [[self.poi.tags firstObject] tagName]];
            [trialSentence appendString:nextStringElement];
//            NSLog(@"trialSentence: %@", trialSentence);
            
            youLikeString = trialSentence;
            renderedString = [self formAttributesStringFromYouLikeString:youLikeString];
            finalRenderedAttributeString = renderedString;
        }
        else{
            for (int i = 0; i < self.poi.tags.count; i++) {
                NSMutableString *trialSentence2 = [[NSMutableString alloc] initWithString:prefixString];
                if(i == 0){
                    nextStringElement = [NSString stringWithFormat:@"%@", [self.poi.tags[i] tagName]];
                    [trialSentence2 appendString:nextStringElement];
                    
                    //See if second word is accessible
                    if (1 < self.poi.tags.count) {
                        i = 1;
                        [trialSentence2 appendString:[NSString stringWithFormat:@" and %@.", [self.poi.tags[i] tagName]]];
//                        NSLog(@"sentence so far: %@", trialSentence2);
                        youLikeString = trialSentence2;
                    }
                    else{
                        //Add full stop to end of first word since second word wasn't available
                        [trialSentence2 appendString:@"."];
                        youLikeString = trialSentence2;
                    }
                    NSAttributedString *renderedString = [self questionLabelCanFitYouLikeString:youLikeString];
                    if(renderedString.length){
                        finalRenderedAttributeString = renderedString;
                    }
                }
                else{
                    static NSString *endString = @"and";
                    static NSString *seperatorString = @",";
                    static NSString *fullStopString = @".";
                    NSString *firstTagName = [self.poi.tags[0] tagName];
                    NSString *secondTagName = [self.poi.tags[1] tagName];
                    
                    for(int j = 2; ((j < i+1) && (j < self.poi.tags.count)); j++){
                        
                        NSMutableString *stringSoFar = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@, %@", prefixString, firstTagName, secondTagName]];
                        //If last word
                        NSString *nextProcessingWord = [self.poi.tags[j] tagName];
                        
                        if(j < i){
                            [stringSoFar appendString:[NSString stringWithFormat:@"%@ %@", seperatorString, nextProcessingWord]];
                        }
                        else if(j == i){
                            [stringSoFar appendString:[NSString stringWithFormat:@"%@ %@ %@%@", seperatorString, endString, nextProcessingWord, fullStopString]];
                            youLikeString = stringSoFar;
                        }
                    }
                    NSAttributedString *renderedString = [self questionLabelCanFitYouLikeString:youLikeString];
                    if(renderedString.length){
                        finalRenderedAttributeString = renderedString;
                    }
                }
            }
        }
    }
    return finalRenderedAttributeString;
}
-(NSAttributedString*)questionLabelCanFitYouLikeString:(NSString*)youLikeString{
    NSAttributedString * renderedString = [self formAttributesStringFromYouLikeString:youLikeString];
    CGRect rect  = [renderedString boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                                options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                context:nil];
    rect = CGRectIntegral(rect);
//    NSLog(@"string: %@ size: %@", renderedString.string, NSStringFromCGRect(rect));
    
    CGFloat widthOfRenderedString = rect.size.width;
    
    if(widthOfRenderedString < self.tagsLabel.bounds.size.width-10){
//        NSLog(@"finalRenderedAttributeString: %@ size: %@", renderedString.string, NSStringFromCGRect(rect));
        return renderedString;
    }
    else{
        return [[NSAttributedString alloc] initWithString:@""];
    }
}
-(NSAttributedString *)formAttributesStringFromYouLikeString:(NSString*)youLikeString{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:youLikeString];
    
    //Overall text
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    UIFont *tagFont = [UIFont fontWithName:@"Nabila" size:20];
    UIColor *grayColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:font
                             range:NSMakeRange(0,[attributedString length])];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:grayColor
                             range:NSMakeRange(0, [attributedString length])];
    
    for(int i = 0; i < self.poi.tags.count; i++){
        NSString *tagName = [self.poi.tags[i] tagName];
        
        NSArray *rangeResults = [self rangesOfString:tagName inString:attributedString.string];
        
        for(int j = 0; j < rangeResults.count; j++){
            [attributedString addAttribute:NSFontAttributeName
                                     value:tagFont
                                     range:[rangeResults[j] rangeValue]];
            
            [attributedString addAttribute:NSForegroundColorAttributeName
                                     value:[PKUIAppearanceController darkBlueColor]
                                     range:[rangeResults[j] rangeValue]];
            }
        
        
    }
    
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentCenter;
    style.firstLineHeadIndent = 10.0f;
    style.headIndent = 5;
    style.tailIndent = -5.0f;
    [style setLineBreakMode:NSLineBreakByTruncatingTail];
    
    
    [attributedString addAttribute:NSParagraphStyleAttributeName
                             value:style
                             range:NSMakeRange(0, [attributedString length])];
    
    return attributedString;
}


- (NSArray *)rangesOfString:(NSString *)searchString inString:(NSString *)str {
    NSMutableArray *results = [NSMutableArray array];
    NSRange searchRange = NSMakeRange(0, [str length]);
    NSRange range;
    while ((range = [str rangeOfString:searchString options:0 range:searchRange]).location != NSNotFound) {
        [results addObject:[NSValue valueWithRange:range]];
        searchRange = NSMakeRange(NSMaxRange(range), [str length] - NSMaxRange(range));
    }
    return results;
}
-(void)setText{
    if(self.subviewsLayedOut){
        CATransition *animation = [CATransition animation];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.type = kCATransitionFade;
        animation.duration = 0.5;
        [self.tagsLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        NSMutableAttributedString *attributedText = [[self attributedTextForQuestionLabel] mutableCopy];
        [attributedText appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", kPOIContentViewWouldYouVisitQuestionString]]];
        self.tagsLabel.attributedText = attributedText;
//        NSLog(@"rect of question label: %@", NSStringFromCGRect(self.tagsLabel.frame));
    }
}
-(void)setPoi:(OXPOISearchByListModel *)poi{
    _poi = poi;
    [self setText];
}
-(void)layoutSubviews{
    [super layoutSubviews];
    self.subviewsLayedOut = TRUE;

    [self performSelector:@selector(setText) withObject:self afterDelay:0.1];
}
-(void)setNeedsDisplay{
    [super setNeedsDisplay];
    [self setText];
}
@end
