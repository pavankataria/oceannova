//
//  WouldYouLikeView.h
//  Ocean
//
//  Created by Pavan Kataria on 16/04/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WouldYouLikeView : UIView
@property (nonatomic, retain) IBOutlet UILabel *tagsLabel;


@property (nonatomic, retain) OXPOISearchByListModel *poi;
-(instancetype)initWithPOI:(OXPOISearchByListModel*)poi;
-(void)setupValues;
-(void)setText;
@end
