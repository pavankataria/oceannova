//
//  OXSignUpTableViewCell.h
//  Ocean
//
//  Created by Pavan Kataria on 12/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupPageTableViewProtocol.h"

@protocol OXSignUpTableViewCellProtocol <NSObject>

-(void)textFieldDidChangeForIndexPath:(NSIndexPath*)indexPath withTextField:(UITextField*)textField;

@end
@interface OXSignUpTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *propertyInputTextField;
@property (nonatomic, weak) id <OXSignUpTableViewCellProtocol> delegate;

-(void)setupWithObject:(id<SignupPageTableViewConfiguration>)object withIndexPath:(NSIndexPath*)indexPath;


@end
