//
//  OXHoursTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 16/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXHoursTableViewCell.h"

@implementation OXHoursTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateCellWithObject:(OXBusinessHoursModel*)businessHourModel{
    self.daysLabel.text = [businessHourModel daysRepresentation];
    self.hoursLabel.text = [businessHourModel hoursRepresentation];
    
    NSLog(@"self.daysLabel.text: %@ hoursLabel.text: %@", self.daysLabel.text, self.hoursLabel.text);
    
}
@end
