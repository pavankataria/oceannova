//
//  CardView.m
//
//  Created by Pavan Kataria on 07/04/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "CardView.h"
#import "OXProcessedPicture.h"

#import "UIImageView+WebCache.h"

#define PASOutletAssert(outlet) NSAssert(outlet, @"IBOutlet not set for " @#outlet)
@interface CardView (){
}
@property (nonatomic, weak) IBOutlet UIView *picLabelContainerView;

@property (nonatomic, weak) IBOutlet UIImageView *poiScoreImageView;
@property (nonatomic, weak) IBOutlet UILabel *poiNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *poiAddedByLabel;
@property (nonatomic, weak) IBOutlet UILabel *poiDistanceLabel;
@property (nonatomic, weak) IBOutlet UIImageView *poiImageImageView;




//Extra
@property (nonatomic, retain) NSMutableArray *poiTags;
@property (nonatomic, weak) IBOutlet UILabel *textQuestionLabel;

@end
@implementation CardView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame withPOI:(OXPOISearchByListModel*)poi{
    self = [super initWithFrame:frame];
    if(self){
        [self setup];
        _poi = poi;
    }
    return self;
}
-(void)setValues{
    self.poiNameLabel.text = self.poi.poiName;
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    [self setup];
    self.textQuestionLabel.text = @"";
    
    
}
-(void)setupValues{
//    PASOutletAssert(self.poiNameLabel);
//    PASOutletAssert(self.poiAddedByLabel);
//    PASOutletAssert(self.poiDistanceLabel);
//    PASOutletAssert(self.poiScoreImageView);
//    NSLog(@"self.poiName: %@", self.poi.poiAddress);
    
    self.poiNameLabel.text = self.poi.poiName;
    self.poiAddedByLabel.text = [NSString stringWithFormat:@"By %@", self.poi.addedBy];
    self.poiDistanceLabel.text = [self.poi getDistanceAwayFromUser];
    self.poiScoreImageView.image = [self.poi getPOIScoreImage];
    OXProcessedPicture *processedPicture = [self.poi.pictures firstObject];
    
    NSURL *url = [NSURL URLWithString:processedPicture.pictureURL];
    [self.poiImageImageView sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image)
        {
            self.poiImageImageView.alpha = 0.0;
            [UIView animateWithDuration:0.5
                             animations:^{
                                 self.poiImageImageView.alpha = 1.0;
                             }];
        }

    }];
}


-(NSAttributedString*)attributedTextForQuestionLabel{
    //shortcut
    //three templates
    //and see which one fits.
    NSAttributedString *finalRenderedAttributeString;
    NSString *prefixString;
    if([self.poisSourceStringEnum isEqualToString:@"GENERIC"]){
        if(self.poi.poiScore >= 0.4){
            prefixString = @"Others like ";
        }
        else{
            prefixString = @"Others dislike ";
        }
    }
    else{
        if(self.poi.poiScore >= 0.6){
            prefixString = @"You like ";
        }
        else if(self.poi.poiScore >= 0.4){
            prefixString = @"You may like ";
        }
        else if(self.poi.poiScore >= 2){
            prefixString = @"You may dislike ";
        }
        else{
            prefixString = @"You dislike ";
        }
    }
    NSMutableString *youLikeString = [[NSMutableString alloc] initWithString:prefixString];
    NSInteger numberOfSentencesToTry = 2;
    
    
    for(int sentenceCounter = 0; sentenceCounter < numberOfSentencesToTry; sentenceCounter++){
        NSAttributedString *renderedString;
        NSMutableString *trialSentence = [[NSMutableString alloc] initWithString:youLikeString];
        NSString *nextStringElement = @"";
        //Sentence basic sentence
        if(sentenceCounter == 0){
            nextStringElement = [NSString stringWithFormat:@"%@.", [[self.poi.tags firstObject] tagName]];
            [trialSentence appendString:nextStringElement];
            NSLog(@"trialSentence: %@", trialSentence);
            
            youLikeString = trialSentence;
            renderedString = [self formAttributesStringFromYouLikeString:youLikeString];
            finalRenderedAttributeString = renderedString;
        }
        else{
            
//            for (int i = 0; i < self.poi.tags.count; i++) {
//                NSString *nextStringElement = @"";
//                if(i == 0){
//                    nextStringElement = [NSString stringWithFormat:@"%@", [self.poi.tags[i] tagName]];
//                    [textQuestionString appendString:nextStringElement];
//                    continue;
//                }
//                if(i < self.poi.tags.count-1){
//                    nextStringElement = [NSString stringWithFormat:@", %@,", [self.poi.tags[i] tagName]];
//                    [textQuestionString appendString:nextStringElement];
//                }
//                else{
//                    nextStringElement = [NSString stringWithFormat:@" and %@", [self.poi.tags[i] tagName]];
//                    [textQuestionString appendString:nextStringElement];
//                }
//            }

            for (int i = 0; i < self.poi.tags.count; i++) {
                NSMutableString *trialSentence2 = [[NSMutableString alloc] initWithString:prefixString];
                if(i == 0){
                    nextStringElement = [NSString stringWithFormat:@"%@", [self.poi.tags[i] tagName]];
                    [trialSentence2 appendString:nextStringElement];
                    
                    //See if second word is accessible
                    if (1 < self.poi.tags.count) {
                        i = 1;
                        [trialSentence2 appendString:[NSString stringWithFormat:@" and %@.", [self.poi.tags[i] tagName]]];
                        NSLog(@"sentence so far: %@", trialSentence2);
                        youLikeString = trialSentence2;
                    }
                    else{
                        //Add full stop to end of first word since second word wasn't available
                        [trialSentence2 appendString:@"."];
                        youLikeString = trialSentence2;
                    }
                    NSAttributedString *renderedString = [self questionLabelCanFitYouLikeString:youLikeString];
                    if(renderedString.length){
                        finalRenderedAttributeString = renderedString;
                    }
                }
                else{
                    static NSString *endString = @"and";
                    static NSString *seperatorString = @",";
                    static NSString *fullStopString = @".";
                    NSString *firstTagName = [self.poi.tags[0] tagName];
                    NSString *secondTagName = [self.poi.tags[1] tagName];

                    
                    for(int j = 2; ((j < i+1) && (j < self.poi.tags.count)); j++){
                        
                        NSMutableString *stringSoFar = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@%@, %@", prefixString, firstTagName, secondTagName]];
                        //If last word
                        NSString *nextProcessingWord = [self.poi.tags[j] tagName];

                        if(j < i){
                            [stringSoFar appendString:[NSString stringWithFormat:@"%@ %@", seperatorString, nextProcessingWord]];
                        }
                        else if(j == i){
                            [stringSoFar appendString:[NSString stringWithFormat:@"%@ %@ %@%@", seperatorString, endString, nextProcessingWord, fullStopString]];
                            youLikeString = stringSoFar;
                        }
                    }
                    NSAttributedString *renderedString = [self questionLabelCanFitYouLikeString:youLikeString];
                    if(renderedString.length){
                        finalRenderedAttributeString = renderedString;
                    }
                    
                }
                
            }


        }

//        else if(sentenceCounter == 1){
//            for(int i = 1; i < self.poi.tags.count; i++){
//                NSString *tagName = [self.poi.tags[i] tagName];
//                NSString *subString = @"";
//                if(i == 1){
//                    subString = [NSString stringWithFormat:@" and %@", tagName];
//                    
//                }
//                else{
//                    NSString * stringSoFar = ;
//                    NSString *tagName = [self.poi.tags[i] tagName];
//                    nextStringElement = [NSString stringWithFormat:@", %@,", [self.poi.tags[sentenceCounter] tagName]];
//                }
//                [trialSentence appendString:subString];
//
//            }
//        }
    }

    
    
    return finalRenderedAttributeString;
}
-(NSAttributedString*)questionLabelCanFitYouLikeString:(NSString*)youLikeString{
    NSAttributedString * renderedString = [self formAttributesStringFromYouLikeString:youLikeString];
    
    
    CGRect rect  = [renderedString boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                                options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                context:nil];
    rect = CGRectIntegral(rect);
    NSLog(@"string: %@ size: %@", renderedString.string, NSStringFromCGRect(rect));
    
    CGFloat widthOfRenderedString = rect.size.width;
    
    if(widthOfRenderedString < self.textQuestionLabel.bounds.size.width-10){
        NSLog(@"finalRenderedAttributeString: %@ size: %@", renderedString.string, NSStringFromCGRect(rect));
        return renderedString;
    }
    else{
        return [[NSAttributedString alloc] initWithString:@""];
    }
}
-(NSAttributedString *)formAttributesStringFromYouLikeString:(NSString*)youLikeString{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:youLikeString];
    
    //Overall text
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    UIFont *tagFont = [UIFont fontWithName:@"Nabila" size:20];
    UIColor *grayColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:font
                             range:NSMakeRange(0,[attributedString length])];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:grayColor
                             range:NSMakeRange(0, [attributedString length])];
    
    
    
    
    // 2) label under image for iphone 5 and 4 even though it should be on top.
    
    
    //    3) aspect ration changes for iphone 4
    //
    //        https://99designs.co.uk/mobile-app-design/contests/guaranteed-prize-create-simple-selection-screen-place-recommendation-475124/entries/55
    
    
    
    
    
    
    for(int i = 0; i < self.poi.tags.count; i++){
        [attributedString addAttribute:NSFontAttributeName
                                 value:tagFont
                                 range:[attributedString.string rangeOfString:[self.poi.tags[i] tagName]]];
        
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[PKUIAppearanceController darkBlueColor]
                                 range:[attributedString.string rangeOfString:[self.poi.tags[i] tagName]]];
        
        
        
    }
    
    
    
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentCenter;
    style.firstLineHeadIndent = 10.0f;
    style.headIndent = 5;
    style.tailIndent = -5.0f;
    [style setLineBreakMode:NSLineBreakByTruncatingTail];

    
    [attributedString addAttribute:NSParagraphStyleAttributeName
                             value:style
                             range:NSMakeRange(0, [attributedString length])];

    return attributedString;
}
- (void)setup {
    [self setupInnerViews];

    // Shadow

    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.33;
    self.layer.shadowOffset = CGSizeMake(0, 1.5);
    self.layer.shadowRadius = 4.0;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [[UIScreen mainScreen] scale];

    // Corner Radius
    self.layer.cornerRadius = 10.0;
    
    
}

-(void)setPoi:(OXPOISearchByListModel *)poi{
    _poi = poi;
    [self setupValues];
}

-(void)setupInnerViews{
//    self.picLabelContainerView.layer.cornerRadius = 10;
//    [self.picLabelContainerView setClipsToBounds:YES];
//    [self.picLabelContainerView.layer setBorderWidth:2];
//    [self.picLabelContainerView.layer setBorderColor:[UIColor colorWithRed:0.89 green:0.89 blue:0.89 alpha:1].CGColor];
//    self.picLabelContainerView.layer.backgroundColor = [UIColor cyanColor].CGColor;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self performSelector:@selector(setText) withObject:self afterDelay:0.1];
}
-(void)setText{
    CATransition *animation = [CATransition animation];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATransitionFade;
    animation.duration = 0.5;
    [self.textQuestionLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];

    NSMutableAttributedString *attributedText = [[self attributedTextForQuestionLabel] mutableCopy];
    
    [attributedText appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", kPOIContentViewWouldYouVisitQuestionString]]];
    self.textQuestionLabel.attributedText = attributedText;
    
    
    NSLog(@"rect of question label: %@", NSStringFromCGRect(self.textQuestionLabel.frame));
}
@end
