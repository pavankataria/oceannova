//
//  OXPlaceDisplayImageTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 03/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WouldYouLikeView.h"

@interface OXPlaceDisplayImageTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *placeImageView;
@property (nonatomic, weak) IBOutlet UIImageView *placePOIScoreImageView;

@property (nonatomic, weak) IBOutlet UIView *picContainerView;


@property (nonatomic, weak) IBOutlet WouldYouLikeView *wouldYouLikeView;

-(void)setupCellWithObject:(OXPOISearchByListModel*)object;
-(void)setupCellWithPOIModelObject:(OXPOIModel*)object;
@end
