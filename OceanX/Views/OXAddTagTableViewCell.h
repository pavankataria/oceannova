//
//  OXAddTagTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 20/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXAddTagTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UITextField *addTagTextField;
@property (nonatomic, weak) IBOutlet UIButton *addButton;
-(IBAction)addTagFromCacheList;

@end
