//
//  OXHoursTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 16/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXBusinessHoursModel.h"

@interface OXHoursTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *daysLabel;
@property (nonatomic, weak) IBOutlet UILabel *hoursLabel;
-(void)updateCellWithObject:(OXBusinessHoursModel*)businessHourModel;
@end
