//
//  MapListTableViewCell.m
//  OceanX
//
//  Created by admin on 13/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "MapListTableViewCell.h"
#import "OXTagBannerView.h"


@interface MapListTableViewCell ()

@property (nonatomic, strong) UILabel *placeNameLabel;
@property (nonatomic, strong) UIImage *placeImage;

@property (nonatomic, strong) UILabel *addedByLabel;

@property (nonatomic, strong) UIImageView *placeScoreImageView;


@property (nonatomic, assign) CGFloat placeNameLabelHeight;
@property (nonatomic, assign) CGFloat placeNameLabelBuffer;
@property (nonatomic, assign) CGFloat tagBannerHeight;
@property (nonatomic, assign) CGFloat placeScoreViewHeight; // it's a square, so it's also the width
@property (nonatomic, assign) CGFloat tagBannerBuffer;
@property (nonatomic, assign) CGFloat placeScreImageBuffer;
@property (nonatomic, strong) OXTagBannerView *tagBanner;


@end


@implementation MapListTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        //various element sizes
        self.placeNameLabelHeight = 30;
        self.placeNameLabelBuffer = 10;
        self.tagBannerHeight = 20;
        self.placeScoreViewHeight = 30;
        self.tagBannerBuffer = 10;
        self.placeScreImageBuffer = 5;
        
        
        
        
        
        //set up place name label
        self.placeNameLabel = [[UILabel alloc] init];
        [self addSubview:self.placeNameLabel];
        
        //set up added by label
        self.addedByLabel = [[UILabel alloc] init];
        [self addSubview:self.addedByLabel];
        
        //set up placeImageView
        self.placeImageView = [[UIImageView alloc] init];
        [self addSubview: self.placeImageView];
        
        //set up placeScoreImageView
        self.placeScoreImageView = [[UIImageView alloc] init];
        [self addSubview:self.placeScoreImageView];
        
        
        //set up tag banner
        self.tagBanner = [[OXTagBannerView alloc] init];
        self.tagBanner.backgroundColor = [PKUIAppearanceController darkBlueColor];
        [self addSubview:self.tagBanner];
        
        
        
        
        
        /*self.placeNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 25, 300, 30)];
         self.placeImageView = [[UIImageView alloc] init];
         [self addSubview:self.placeNameLabel];
         [self addSubview: self.placeImageView];*/
    }
    return self;
}


-(void)setTagBannerTags:(NSArray<OXTagsProtocolConfiguration> *)tagsArray
{
    [self.tagBanner setTagsArray:tagsArray];
    
}

-(void)setPlaceName:(NSString *)placeName{
    self.placeNameLabel.text = placeName;
    
}

-(void)setAddedBy:(NSString *)addedBy
{
    self.addedByLabel.text = addedBy;
    [self.addedByLabel sizeToFit];
    
    //self.addedByLabel.frame = CGRectMake(self.addedByLabel.frame.origin.x, self.addedByLabel.frame.origin.y, 100, 30);
    
    NSLog(@"originX %f", self.addedByLabel.frame.origin.x);
    NSLog(@"originY %f", self.addedByLabel.frame.origin.y);
    
    NSLog(@"width %f", self.addedByLabel.frame.size.width);
    NSLog(@"height %f", self.addedByLabel.frame.size.height);
    
    
    NSLog(@"addedByLabel %@", self.addedByLabel.text);
    
}

-(void)setPlaceImage:(UIImage *)placeImage{
    //to make sure it's square and takes entire height and width of the view
    self.placeImageView.image = placeImage;
}

-(void)setPlaceScoreImage:(UIImage *)placeScoreImage
{
    self.placeScoreImageView.image = placeScoreImage;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    
    //place image view
    self.placeImageView.frame = CGRectMake(0, 0, CGRectGetHeight(self.frame), CGRectGetHeight(self.frame));
    
    //place name label
    CGFloat placeNameLabelWidth = CGRectGetWidth(self.frame) - CGRectGetWidth(self.placeImageView.frame) - self.placeScoreViewHeight - self.placeNameLabelBuffer;
    
    self.placeNameLabel.frame = CGRectMake(CGRectGetMaxX(self.placeImageView.frame) + self.placeNameLabelBuffer, CGRectGetMinY(self.placeNameLabel.frame), placeNameLabelWidth, self.placeNameLabelHeight);
    
    //added by label
    
    self.addedByLabel.frame = CGRectMake(CGRectGetMinX(self.placeNameLabel.frame), CGRectGetMaxY(self.placeNameLabel.frame), 20, 20);
    
    self.addedByLabel.font = [self.addedByLabel.font fontWithSize:12];
    self.addedByLabel.textColor = [UIColor grayColor];
    
    [self.addedByLabel sizeToFit];
    
    
    //place score image view
    CGFloat placeScoreImageViewxOrign = CGRectGetWidth(self.frame) - self.placeScoreViewHeight -self.placeScreImageBuffer; //height is equal to width
    self.placeScoreImageView.frame = CGRectMake(placeScoreImageViewxOrign, self.placeScreImageBuffer, self.placeScoreViewHeight, self.placeScoreViewHeight);
    
    //tag banner
    CGFloat tagBannerYOrigin = CGRectGetHeight(self.frame) - self.tagBannerHeight - self.tagBannerBuffer;
    
    CGFloat bannerWidth = CGRectGetWidth(self.frame) - CGRectGetMaxX(self.placeImageView.frame);
    
    self.tagBanner.frame = CGRectMake(CGRectGetMaxX(self.placeImageView.frame), tagBannerYOrigin, bannerWidth, self.tagBannerHeight);
    
    /*self.placeImageView.frame = CGRectMake(0, 0, self.frame.size.height, self.frame.size.height);
     [self.placeNameLabel setFrame:CGRectMake(self.frame.size.height + 10, 20, 300, 30)];*/
    
}

@end

