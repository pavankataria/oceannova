//
//  OXSearchBarView.h
//  OceanX
//
//  Created by Pavan Kataria on 31/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OXSearchBarView;
@protocol OXSearchBarDelegate <NSObject>
@required
-(void)searchBarView:(OXSearchBarView*)searchBarView searchBarDidBeginEditing:(UITextField*)textField;

@optional
-(void)searchBarTextFieldShouldReturn:(UITextField*)textField;
-(void)filterScreenTextFieldDidChange:(UITextField *)textField;
-(BOOL)filterScreenTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
@end

@protocol OXSearchBarDataSource <NSObject>

@optional
-(UIColor*)searchBarViewBackgroundColorForSearchBarTextField;
-(UITextBorderStyle)searchBarViewTextFieldStyleForSearchBarTextField;
-(NSString*)searchBarViewPlaceHolderString;
@end
@interface OXSearchBarView : UIView<UITextFieldDelegate>

@property (nonatomic, retain) IBOutlet UITextField *searchBarTextField;
@property (nonatomic, weak) id <OXSearchBarDelegate> delegate;
@property (nonatomic, weak) id <OXSearchBarDataSource> dataSource;


@property (nonatomic, retain) NSString *storyboardName;
-(void)reloadViews;
@end
