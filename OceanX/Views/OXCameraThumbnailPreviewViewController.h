//
//  OXCameraThumbnailPreviewViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 26/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXAddPOIViewControllerDelegate.h"
#import "OXImageStackItem.h"

@interface OXCameraThumbnailPreviewViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UIButton *showEditImagesVCButton;
@property (nonatomic, weak) id <OXAddPOIViewControllerDelegate> delegate;
@property (nonatomic, assign) kAddPlaceComingFromVCType comingFromVC;
-(void)addToThumbnailStackWithImage:(OXImageStackItem*)image;
-(void)updateThumbnailStackWithCapturedImages:(NSMutableArray*)capturedImages;


@end
