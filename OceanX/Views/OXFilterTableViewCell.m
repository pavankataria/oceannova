//
//  OXFilterTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXFilterTableViewCell.h"

@implementation OXFilterTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellWithSearchPOIObject:(OXPOISearchModel*)object andWithSection:(NSUInteger)section{
    switch (section) {
        case OXFilterTableSectionTypeLocation:
            self.headerLabel.text = [object currentLocationTitle];
//            self.infoLabel.text = [object currentLocationTitle];
            break;
        case OXFilterTableSectionTypeTags:
            self.headerLabel.text = [NSString stringWithFormat:@"%lu tags",(unsigned long)object.tags.count];
//            self.infoLabel.text = ;
            break;
            
        case OXFilterTableSectionTypeFriends:
            self.headerLabel.text = [object howManyJoining];
//            self.infoLabel.text = [object howManyJoining];
            break;
            
        case OXFilterTableSectionTypeText:
            self.headerLabel.text = [object searchingByText];//@"Search by text";
//            self.infoLabel.text = [object searchingByText];
            break;
            
        default:
            self.headerLabel.text = @"should never come here";
            break;
    }
    
    
    
}
@end
