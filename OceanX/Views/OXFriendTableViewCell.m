//
//  OXFriendTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 18/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXFriendTableViewCell.h"


@interface OXFriendTableViewCell ()

@property (nonatomic, assign) FriendStatus friendStatus;

@end

@implementation OXFriendTableViewCell

-(IBAction)addFriendButtonTapped:(id)sender{
    
    
}

-(IBAction)followUnfollow:(id)sender{
    
    [self.delegate userTappedFollowForFriendCell:self atIndexPath :self.indexPath];
    
}

-(void) updateWithFriend:(OXFriendModel *) friend withDelegate: (id <FriendCellDelegate>) delegate atIndexPath:(NSIndexPath*) path showButton:(BOOL) show{
    
    [self updateWithFriend:friend withDelegate:delegate atIndexPath:path friendStatus:show?StatusUnFriend:StatusAddFriend];
}

-(void) updateWithFriend:(OXFriendModel *) friend withDelegate: (id <FriendCellDelegate>) delegate atIndexPath:(NSIndexPath*) path friendStatus:(FriendStatus) friendStatus{
    
//    self.tintColor = [UIColor defaultColor];
    self.usernameLabel.text = friend.username;
    self.followUnfollow.hidden = (friendStatus == StatusPending);
    self.friendRequestPendingButton.hidden = (friendStatus != StatusPending);
    switch (friendStatus) {
        case StatusAddFriend:
            [self.followUnfollow setTitle:@"Add Friend" forState:UIControlStateNormal];
            break;
            
        case StatusUnFriend:
            [self.followUnfollow setTitle:@"Unfriend" forState:UIControlStateNormal];
            break;
            
        case StatusPending:
            break;
            
        default:
            break;
    }
    
    self.delegate = delegate;
    self.indexPath = path;
}

-(void) updateWithPlace:(OXFriendModel *) place withDelegate: (id <FriendCellDelegate>) delegate atIndexPath:(NSIndexPath*) path {
    
//    self.tintColor = [UIColor defaultColor];
    self.usernameLabel.text = place.name;
    
    [self.followUnfollow setHidden:YES];
    
    self.delegate = delegate;
    self.indexPath = path;
}

-(void)updateForModelObject:(OXFriendModel*)friend{

//    TODO : Remove if not needed
//    self.addFriendButton.selected =  friend.isFriendSelected;
    
//    if(self.addFriendButton.isSelected)
//    {
//        [self.addFriendButton setBackgroundColor:[UIColor colorWithRed:25/255.0 green:209/255.0 blue:224/255.0 alpha:1.0]];
//    }
//    else
//    {
//        [self.addFriendButton setBackgroundColor:[UIColor whiteColor]];
//    }
//    self.addFriendButton.hidden = YES;
    
    [self updateWithFriend:friend withDelegate:nil atIndexPath:nil friendStatus:StatusAddFriend];
    self.userInteractionEnabled = YES;
    
    //Leave alone
    self.usernameLabel.text = friend.username;
    //    self.friendImageView.image = [UIImage imageNamed:@"image"];

}
@end
