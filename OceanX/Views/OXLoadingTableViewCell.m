//
//  OXLoadingTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXLoadingTableViewCell.h"

@implementation OXLoadingTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.separatorInset = UIEdgeInsetsMake(0.f, self.bounds.size.width, 0.f, 0.f);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)startAnimatingActivityIndicatorView{
    [self.activityIndicator startAnimating];
}
@end
