//
//  OXLikeDislikeBarUITableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 09/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXLikeDislikeBarUITableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIButton *likeButton;
@property (nonatomic, weak) IBOutlet UIButton *maybeButton;
@property (nonatomic, weak) IBOutlet UIButton *dislikeButton;



@property (nonatomic, weak) IBOutlet UILabel *poiCaption;


@end
