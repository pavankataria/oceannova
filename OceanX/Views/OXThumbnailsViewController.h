//
//  OXThumbnailsViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 24/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKThumbnailSliderView.h"
#import "PKPreviewSliderView.h"

@protocol OXThumbnailsViewControllerDelegate <NSObject>

-(void)updateViewAfterUpdateCapturedImagesArray:(NSMutableArray*)capturedImages;
-(void)processPicturesAndSendToServer;

@end
@interface OXThumbnailsViewController : UIViewController<PKThumbnailSliderViewDelegate>
@property (nonatomic, retain) PKPreviewSliderView *mainSliderView;
@property (nonatomic, retain) PKThumbnailSliderView *thumbnailSliderView;

@property (nonatomic, retain) NSMutableArray *capturedImages;

@property (nonatomic, weak) id <OXThumbnailsViewControllerDelegate> delegate;

@property (nonatomic, retain) OXHCAddPoiModel *addPoiObject;
@property (nonatomic, assign) kAddPlaceComingFromVCType comingFromVC;

@end
