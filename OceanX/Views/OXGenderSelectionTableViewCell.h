//
//  OXGenderSelectionTableViewCell.h
//  OceanX
//
//  Created by admin on 25/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXGenderSelectionTableViewCell : UITableViewCell

@property (assign, nonatomic) Gender selectedGender;
@property (strong, nonatomic) UISegmentedControl *genderSelectionControl;

@end
