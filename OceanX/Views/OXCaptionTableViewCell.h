//
//  OXCaptionTableViewCell.h
//  Ocean
//
//  Created by Pavan Kataria on 02/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXCaptionTableViewCell : UITableViewCell
@property(nonatomic, weak) IBOutlet UILabel *captionLabel;
@end
