//
//  OXMapView.m
//  Ocean
//
//  Created by Sam Payne on 29/07/2014.
//  Copyright (c) 2014 Ocean Technologies. All rights reserved.
//

const NSInteger kMaxAnnotationCount = 20;
#define METERS_PER_MILE 1609.344

#import "OXMapView.h"

@interface OXMapView() <MKMapViewDelegate>

@property (nonatomic, strong) NSArray *allPlaces;
@property (nonatomic, strong) NSMutableArray *placeAnnotations;
@property (nonatomic, strong) TBCoordinateQuadTree *coordinateQuadTree;

@end


@implementation OXMapView

#pragma mark - Init methods



-(void) awakeFromNib {
    [super awakeFromNib];
    self.delegate = self;
    self.coordinateQuadTree = [[TBCoordinateQuadTree alloc] init];
    self.coordinateQuadTree.mapView = self;
}

#pragma mark - MKMapViewDelegate methods


-(void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    
    [self.customDelegate regionWillChange];
    
    
    
}




-(MKAnnotationView*) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    if([annotation isKindOfClass:[OXAnnotation class]]){
        
        OXAnnotation *oxAnnotation = (OXAnnotation *)annotation;
        
        MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:kOXAnnotationIdentifier];
        
        annotationView = oxAnnotation.annotationView;
        
        return annotationView;
        
        //TODO: Reusable annotation is generating issues in custom annotation, it displays wrong badgecount
        //        if(!annotationView)
        //        {
        //            annotationView = oxAnnotation.annotationView;
        //        }
        //        else
        //        {
        //            annotationView.annotation = annotation;
        //        }
    }
    
    // Currently below code is not in used, but it will be require for displaying another type of annotation
    else if ([annotation isKindOfClass:[MKUserLocation class]]) {
        
        MKAnnotationView * view = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Current"];
        view.image = [UIImage imageNamed:@"nav_current_location.png"];
        
        return view;
        
    }else if([self.selectedAnnotations containsObject:annotation]) {
        
        MKAnnotationView * view = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Location"];
        [OCNAppConfig mapSetPinPointHighlightedImageAndOffsetForMKAnnotationView:view];
        
        return view;
        
    }else if([self.annotations containsObject:annotation]){
    }
    
    return nil;
    
}

-(void) mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    if (![self isUpdatedRectInsideLastRect]) {
        [self.customDelegate regionChanged];
    }else{
        [self updateWithNewPlaces:self.allPlaces];
    }
    
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    for (UIView *view in views) {
        [self addBounceAnnimationToView:view];
    }
}

-(void)selectAnnotation:(id<MKAnnotation>)annotation animated:(BOOL)animated {
    [super selectAnnotation:annotation animated:animated];
}


-(void) mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    OXAnnotation *annotation = view.annotation;
    
    // for cluster- it willl break cluster into separate annotations, for kSingleAnnotationCount- it perform |selectedAnnotationAtIndex| delegate
    if (annotation.clusterAnnotations.count > kSingleAnnotationCount) {
        
        if (annotation.clusterAnnotations.count > kMaxAnnotationCount) {
            NSArray *clusterAnnotations = annotation.clusterAnnotations;
            MKMapRect zoomRect = MKMapRectNull;
            for (id <MKAnnotation> annotation in clusterAnnotations)
            {
                MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
                MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
                zoomRect = MKMapRectUnion(zoomRect, pointRect);
            }
            [mapView setVisibleMapRect:zoomRect animated:YES];
        } else {
            if ([self.customDelegate respondsToSelector:@selector(selectedCluster:)]) {
                [self.customDelegate selectedCluster:annotation.clusterAnnotations];
            }
        }
    }
    else if ([view.reuseIdentifier isEqualToString:kOXAnnotationIdentifier]) {
        
        OXAnnotation *selectedAnnotation = view.annotation;
        if ([self.customDelegate respondsToSelector:@selector(selectedAnnotationId:)]) {
            [self.customDelegate selectedAnnotationId:selectedAnnotation.annotationID];
        }
    }
}

#pragma mark - Public methods

// remove old places and add new places on map
-(void) refreshWithPlaces:(NSMutableArray*) places {
    
    
    self.allPlaces = [NSArray arrayWithArray:places];
    [self updateWithNewPlaces:self.allPlaces];
}

-(void) setCustomDelegate:(id<OXMapViewDelegate>)customDelegate {
    
    _customDelegate = customDelegate;
    [self setRegion:[[OCNLocationTracker tracker] currentMapRegion] animated:YES];
    
}


#pragma mark - Private methods

- (BOOL)isUpdatedRectInsideLastRect {
    
    if (MKMapRectContainsRect(self.lastUpdatedMapRect, self.visibleMapRect)) {
        return YES;
    }
    self.lastUpdatedMapRect = self.visibleMapRect;
    return NO;
}


-(void) clear {
    if(self.placeAnnotations.count) {
        [self removeAnnotations:self.placeAnnotations];
    }
}

// remove old places and add new places on map
-(void)updateWithNewPlaces:(NSArray*) newPlaces{
    
    NSLog(@"update with new places");
    
    if (newPlaces) {
        [self.coordinateQuadTree buildTreeWithElements:newPlaces];
        [[NSOperationQueue new] addOperationWithBlock:^{
            double scale = self.bounds.size.width / self.visibleMapRect.size.width;
            NSArray *annotations = [self.coordinateQuadTree clusteredAnnotationsWithinMapRect:self.visibleMapRect withZoomScale:scale];
            self.placeAnnotations = [NSMutableArray arrayWithArray:annotations];
            [self updateMapViewAnnotationsWithAnnotations:annotations];
        }];
    }
}

- (void)deselectAllAnnotation
{
    NSArray *selectedAnnotations = self.selectedAnnotations;
    for (OXAnnotation *annotationView in selectedAnnotations) {
        [self deselectAnnotation:annotationView animated:NO];
    }
}

//TODO: have used this functions from ClusterMap library, need to refactor
- (void)addBounceAnnimationToView:(UIView *)view {
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    bounceAnimation.values = @[@(0.05), @(1.1), @(0.9), @(1)];
    
    bounceAnimation.duration = 0.6;
    NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounceAnimation.values.count];
    for (NSUInteger i = 0; i < bounceAnimation.values.count; i++) {
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    }
    [bounceAnimation setTimingFunctions:timingFunctions.copy];
    bounceAnimation.removedOnCompletion = NO;
    
    [view.layer addAnimation:bounceAnimation forKey:@"bounce"];
}

- (void)updateMapViewAnnotationsWithAnnotations:(NSArray *)annotations {
    NSMutableSet *before = [NSMutableSet setWithArray:self.annotations];
    [before removeObject:[self userLocation]];
    NSSet *after = [NSSet setWithArray:annotations];
    
    NSMutableSet *toKeep = [NSMutableSet setWithSet:before];
    [toKeep intersectSet:after];
    
    NSMutableSet *toAdd = [NSMutableSet setWithSet:after];
    [toAdd minusSet:toKeep];
    
    NSMutableSet *toRemove = [NSMutableSet setWithSet:before];
    [toRemove minusSet:after];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self addAnnotations:[toAdd allObjects]];
        [self removeAnnotations:[toRemove allObjects]];
    }];
}


#pragma mark - Not in use methods

// it provide current visible map's radius
- (CLLocationDistance)getRadius {
    // init center location from center coordinate
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:self.centerCoordinate.latitude
                                                            longitude:self.centerCoordinate.longitude];
    double topCenterLat = centerLocation.coordinate.latitude-self.region.span.latitudeDelta/2.;
    CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterLat
                                                               longitude:centerLocation.coordinate.longitude];
    
    CLLocationDistance distance = [centerLocation distanceFromLocation:topCenterLocation];
    return distance;
}

// check whether particular coordinate is inside Map or not
-(BOOL)isLocationInsideMapView:(CLLocationCoordinate2D)coordinate {
    MKMapPoint userPoint = MKMapPointForCoordinate(coordinate);
    MKMapRect mapRect = self.visibleMapRect;
    return MKMapRectContainsPoint(mapRect, userPoint);
}


@end


