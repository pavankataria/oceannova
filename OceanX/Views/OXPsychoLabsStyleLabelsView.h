//
//  OXPsychoLabsStyleLabelsView.h
//  OceanX
//
//  Created by Pavan Kataria on 30/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol OXTagsProtocolConfiguration;
@interface OXPsychoLabsStyleLabelsView : UIView


//-(void)initWithTagsLabels:(NSArray<OXTagsProtocolConfiguration>*)tagArray;
-(void)initWithTagsLabels:(NSArray*)tagArray;

@end
