//
//  OXTagTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 19/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXTagPlaceCountModel.h"
#import "OXProtocolsHeader.h"
@interface OXTagTableViewCell : UITableViewCell<GenericTableViewCellConfiguration, OXSearchTableViewCellOperationProtocol>

@property (nonatomic, weak) IBOutlet UILabel *tagNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *tagPlacesCountLabel;


-(void)setTableCellForUIMessageCellForIndexPath:(NSIndexPath*)indexPath;
-(void)setSearchTableCellForUIMessageCellForIndexPath:(NSIndexPath*)indexPath;
-(void)setCellForCreateEmptyCellWithTagName:(NSString*)tagName;
-(void)setCellWithObjectFollowingProtocol:(id<OXTagModelOperationProtocol>)object;
-(void)setTableCellForUIMessageCellForDetailPOIVCForIndexPath:(NSIndexPath*)indexPath;

@end
