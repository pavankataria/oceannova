//
//  OXCameraThumbnailPreviewViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 26/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXCameraThumbnailPreviewViewController.h"

@interface OXCameraThumbnailPreviewViewController (){
    
}
@property (nonatomic, retain) NSMutableArray *thumbnailStack;

@end



@implementation OXCameraThumbnailPreviewViewController
-(NSMutableArray *)thumbnailStack{
    if(!_thumbnailStack){
        _thumbnailStack = [[NSMutableArray alloc] init];
    }
    return _thumbnailStack;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}


-(void)addToThumbnailStackWithImage:(OXImageStackItem*)imageStackItem{
    [self.thumbnailStack addObject:imageStackItem];
    [self.thumbnailImageView setImage:imageStackItem.thumbnailImage];
}

-(IBAction)showEditImagesVCButtonAction:(id)sender{
    [self.delegate addPOIShowEditImagesWithViewController:self.thumbnailStack];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(void)updateThumbnailStackWithCapturedImages:(NSMutableArray*)capturedImages{
    [self.thumbnailStack removeAllObjects];
    self.thumbnailStack = [[NSMutableArray alloc] initWithArray:capturedImages];
    self.thumbnailStack = capturedImages;
    [self.thumbnailImageView setImage:[[capturedImages lastObject] thumbnailImage]];
}
@end
