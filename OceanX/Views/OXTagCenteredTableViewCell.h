//
//  OXTagCenteredTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 18/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXTagCenteredTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *centeredTagDisplayLabel;
@end
