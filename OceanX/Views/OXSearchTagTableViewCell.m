//
//  OXSearchTagTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 08/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSearchTagTableViewCell.h"

@implementation OXSearchTagTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSString *)OXTagModelOperationTagNameString{
    return self.tagNameLabel.text;
}

-(NSString *)OXTagModelOperationPlaceCountString{
    return self.tagPlacesCountLabel.text;
}
-(void)setCellForSearchWithObjectFollowingProtocol:(id<OXTagModelOperationProtocol>)object{
    self.tagNameLabel.text =  [object OXTagModelOperationTagNameString];
    self.tagPlacesCountLabel.text = [object OXTagModelOperationPlaceCountString];
    self.userInteractionEnabled = YES;
    [self.tagDeleteButton setHidden:YES];

    
}
-(void)setCellWithObjectFollowingProtocol:(id<OXTagModelOperationProtocol>)object{
    
    self.tagNameLabel.text =  [object OXTagModelOperationTagNameString];
    self.tagPlacesCountLabel.text = [object OXTagModelOperationPlaceCountString];
    self.tagNameLabel.textColor = [UIColor blackColor];
    self.userInteractionEnabled = YES;
    [self.tagDeleteButton setHidden:NO];

}
-(void)setSearchTableCellForUIMessageCellForIndexPath:(NSIndexPath*)indexPath{
    self.tagNameLabel.textColor = [UIColor lightGrayColor];
    self.userInteractionEnabled = NO;
    [self.tagDeleteButton setHidden:YES];
    
    NSString *stringText;
    switch (indexPath.row) {
        case 0:
            stringText = @"Tags describe places. Type..";
            break;
        default:
            stringText = @"";
            break;
    }
    self.tagNameLabel.text = stringText;
    self.tagPlacesCountLabel.text = @"";
}


@end
