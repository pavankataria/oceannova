//
//  OXCaptionView.m
//  Ocean
//
//  Created by Pavan Kataria on 19/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXCaptionView.h"

@implementation OXCaptionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self layoutIfNeeded];
    }
    return self;
}
@end
