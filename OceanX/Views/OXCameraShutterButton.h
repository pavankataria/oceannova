//
//  OXCameraShutterButton.h
//  OceanX
//
//  Created by Pavan Kataria on 25/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE

@interface OXCameraShutterButton : UIButton

@property (nonatomic, assign) BOOL processingPicture;
@end
