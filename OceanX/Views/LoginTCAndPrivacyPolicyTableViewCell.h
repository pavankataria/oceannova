//
//  LoginTCAndPrivacyPolicyTableViewCell.h
//  Ocean
//
//  Created by Pavan Kataria on 13/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginTCAndPrivacyPolicyTableViewCellProtocol <NSObject>

-(void)termsAndConditionsButtonPressed;
-(void)privacyPolicyButtonPressed;
@end

@interface LoginTCAndPrivacyPolicyTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIButton *termsAndConditionsButton;
@property (nonatomic, weak) IBOutlet UIButton *privacyPolicyButton;
@property (nonatomic, weak) IBOutlet UIButton *checkMarkButton;

@property (nonatomic, weak) id <LoginTCAndPrivacyPolicyTableViewCellProtocol> delegate;
 @end
