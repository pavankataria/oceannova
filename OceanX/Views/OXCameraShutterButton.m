//
//  OXCameraShutterButton.m
//  OceanX
//
//  Created by Pavan Kataria on 25/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXCameraShutterButton.h"

@implementation OXCameraShutterButton


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
/*- (void)drawRect:(CGRect)rect {
    CGRect frame = rect;
    NSLog(@"shutter frame: %@", NSStringFromCGRect(frame));

    
//#warning todo - create whatsapp like shutter button
    CGRect outerCircleFrame = frame;
    int outerCircleDiameter = CGRectGetWidth(outerCircleFrame)/2;
    CGFloat gapBetweenOuterAndInnerCircle = 2.0;
    CGFloat innerCircleDiameter = outerCircleDiameter - gapBetweenOuterAndInnerCircle;
    CGRect innerCircleFrame = CGRectMake(0, 0, innerCircleDiameter, innerCircleDiameter);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(ctx, 85.0/255.0, 143.0/255.0, 220.0/255.0, 1.0);
    CGContextAddEllipseInRect(ctx, outerCircleFrame);
    CGContextAddEllipseInRect(ctx, innerCircleFrame);

    CGContextSetFillColor(ctx, CGColorGetComponents([[UIColor redColor] CGColor]));
    CGContextFillPath(ctx);
}
*/
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self styliseButton];
    }
    return self;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [self styliseButton];
}

-(void)styliseButton{
    [PKQuickMethods setCornerRadiusForView:self withCornerRadius:self.frame.size.height/2];
    [self setProcessingPicture:NO];
}

-(void)setProcessingPicture:(BOOL)processingPicture{
    _processingPicture = processingPicture;
    if(_processingPicture){
        self.backgroundColor = [UIColor darkGrayColor];
        self.enabled = FALSE;
    }
    else{
        self.backgroundColor = [UIColor whiteColor];
        self.enabled = TRUE;
    }
}
@end
