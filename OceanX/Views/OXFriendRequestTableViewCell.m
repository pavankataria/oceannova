//
//  OCNFriendRequestTableViewCell.m
//  Ocean
//
//  Created by Sam Payne on 09/07/2014.
//  Copyright (c) 2014 Ocean. All rights reserved.
//

#import "OXFriendRequestTableViewCell.h"

@implementation OXFriendRequestTableViewCell

-(IBAction)accept:(id)sender{
    
    [self.delegate userDidTapAccept:self.notification];
    
}

-(IBAction)reject:(id)sender{
    
    [self.delegate userDidTapReject:self.notification];
    
}

-(void) updateWithNotification:(OXNotificationModel *)notification forIndexPath:(NSIndexPath*) path withDelegate:(id<FriendRequestCellDelegate>) delegate{
    
    self.notification = notification;
    self.delegate = delegate;
    self.indexPath = path;
    self.usernameLabel.text = notification.senderName;
}

@end
