//
//  UICellTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 20/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "UICellTableViewCell.h"

@implementation UICellTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
