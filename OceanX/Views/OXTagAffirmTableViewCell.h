//
//  OXTagAffirmTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 18/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXTagAffirmTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *tagNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *tagCount;
@property (nonatomic, weak) IBOutlet UIButton *upArrowButton;
-(void)upArrowButtonSetSelected:(BOOL)selected;
@end
