//
//  OXLikeDislikeBarUITableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 09/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXLikeDislikeBarUITableViewCell.h"
#import "NSLayoutConstraint+EvenDistribution.h"

@interface OXLikeDislikeBarUITableViewCell()
@property (nonatomic, retain) UIView *bottomSeparatorView;
@end

@implementation OXLikeDislikeBarUITableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.bottomSeparatorView = [[UIView alloc] init];
    [self.bottomSeparatorView setBackgroundColor:[PKUIAppearanceController grayColor]];

    [self addSubview:self.bottomSeparatorView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)likeButtonTapped{
    [self.likeButton setSelected:![self.likeButton isSelected]];
//    [self.dislikeButton setSelected:![self.likeButton isSelected]];
    [self.dislikeButton setSelected:NO];
    [self.maybeButton setSelected:NO];
}
-(IBAction)dislikeButtonTapped{
    [self.dislikeButton setSelected:![self.dislikeButton isSelected]];
    [self.likeButton setSelected:NO];
    [self.maybeButton setSelected:NO];
}
-(IBAction)maybeButtonTapped:(id)sender{
    [self.maybeButton setSelected:![self.maybeButton isSelected]];
    [self.likeButton setSelected:NO];
    [self.dislikeButton setSelected:NO];
}

//- (IBAction)likeButtonTapped:(id)sender
//{
//    
//}
//    UIButton *theButton = (UIButton *)sender;
//    if ([theButton currentImage] == self.imageOne) {
//        [theButton setImage:self.imageTwo forState:UIControlStateNormal];
//    }
//    else {
//        [theButton setImage:self.imageOne forState:UIControlStateNormal];
//    }
//    
//    // (remaining action code)...
//}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.bottomSeparatorView.frame = CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1);
    
//    [self.likeButton setTranslatesAutoresizingMaskIntoConstraints:NO];
//    [self.dislikeButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    /*
     NSArray *constraints = [NSLayoutConstraint constraintsForEvenDistributionOfItems:@[self.dislikeButton, self.likeButton]
                                                              relativeToCenterOfItem:self
                                                                          vertically:NO];

    [self addConstraints:constraints];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.dislikeButton
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0f
                                                      constant:0.0f]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.likeButton
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0f
                                                      constant:0.0f]];
     */
}
@end
