//
//  PicContainerView.m
//  Ocean
//
//  Created by Pavan Kataria on 14/04/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PicContainerView.h"
#import <QuartzCore/QuartzCore.h>
@implementation PicContainerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib{
    NSLog(@"woken up from nib a long long time ago");
//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
//    
//    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//    maskLayer.frame = self.bounds;
//    maskLayer.path  = maskPath.CGPath;
//    self.layer.mask = maskLayer;
//    [self.layer setMasksToBounds:YES];
    
//    UIBezierPath *maskPath;
//    maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
//                                     byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
//                                           cornerRadii:CGSizeMake(10.0, 10.0)];
//    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//    maskLayer.frame = self.bounds;
//    maskLayer.path = maskPath.CGPath;
//    self.layer.mask = maskLayer;
    [self setMaskTo:self byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) withCornerRadii:CGSizeMake(10, 10)];
}

- (void)setMaskTo:(id)sender byRoundingCorners:(UIRectCorner)corners withCornerRadii:(CGSize)radii
{
    // UIButton requires this
    [sender layer].cornerRadius = 10;
    
    UIBezierPath *shapePath = [UIBezierPath bezierPathWithRoundedRect:[sender bounds]
                                                    byRoundingCorners:corners
                                                          cornerRadii:radii];
    
    CAShapeLayer *newCornerLayer = [CAShapeLayer layer];
    newCornerLayer.frame = [sender bounds];
    newCornerLayer.path = shapePath.CGPath;
    [sender layer].mask = newCornerLayer;
}


@end
