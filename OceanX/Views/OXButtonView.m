//
//  OXButtonView.m
//  OceanX
//
//  Created by Pavan Kataria on 18/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXButtonView.h"

@implementation OXButtonView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self styliseButton];
    }
    return self;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [self styliseButton];
}

-(void)styliseButton{
    [PKQuickMethods setCornerRadiusForView:self withCornerRadius:4];
}

@end
