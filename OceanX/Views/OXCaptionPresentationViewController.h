//
//  OXCaptionPresentationViewController.h
//  Ocean
//
//  Created by Pavan Kataria on 19/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXDetailPOIProtocol.h"

@interface OXCaptionPresentationViewController : UIViewController
@property (nonatomic, weak) id <OXDetailPOIProtocol> delegate;
@property (nonatomic, retain) OXPOIModel *poiObject;

@end
