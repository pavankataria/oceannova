//
//  LoginTCAndPrivacyPolicyTableViewCell.m
//  Ocean
//
//  Created by Pavan Kataria on 13/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "LoginTCAndPrivacyPolicyTableViewCell.h"
#import "OXWebViewViewController.h"

@implementation LoginTCAndPrivacyPolicyTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)checkMarkButtonTapped:(id)sender {
    self.checkMarkButton.selected = !self.checkMarkButton.selected;
}
- (IBAction)termsAndConditionsButtonTapped:(id)sender {
    [self.delegate termsAndConditionsButtonPressed];
}

- (IBAction)privacyPolicyButtonTapped:(id)sender {
    [self.delegate privacyPolicyButtonPressed];
}


@end
