//
//  OXMapView.h
//  Ocean
//
//  Created by Sam Payne on 29/07/2014.
//  Copyright (c) 2014 Ocean Technologies. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "TBCoordinateQuadTree.h"
#import "OXClusterAnnotationBadgeView.h"

@protocol OXMapViewDelegate <NSObject>

- (void)selectedAnnotationId:(NSInteger) annotationId;
- (void)selectedCluster:(NSArray *)annotations;
- (void)deselectedAnnotation;
- (void)regionChanged;
-(void)regionWillChange;

@end

@interface OXMapView : MKMapView <MKMapViewDelegate>
@property (nonatomic, weak) id <OXMapViewDelegate> customDelegate;
@property (nonatomic, assign) MKMapRect lastUpdatedMapRect;

- (void)refreshWithPlaces:(NSMutableArray*) places;
- (void)deselectAllAnnotation;

@end
