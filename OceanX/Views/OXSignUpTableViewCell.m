//
//  OXSignUpTableViewCell.m
//  Ocean
//
//  Created by Pavan Kataria on 12/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSignUpTableViewCell.h"
@interface OXSignUpTableViewCell()<UIPickerViewDelegate>{
    NSIndexPath *_indexPath;
}
@property (nonatomic, retain) UIPickerView *yearOfBirthPicker;
@property (nonatomic, retain) NSMutableArray *yearsOptionsArray;
@property (nonatomic, retain) UITextField *yobTextField;
@end

@implementation OXSignUpTableViewCell

-(UIPickerView *)yearOfBirthPicker{
    if(!_yearOfBirthPicker){
        _yearOfBirthPicker = [[UIPickerView alloc] init];
        _yearOfBirthPicker.delegate = self;
    }
    return _yearOfBirthPicker;
}

-(NSArray *)yearsOptionsArray{
    if(!_yearsOptionsArray){
        //Create Years Array from 1960 to This year
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy"];
        NSMutableArray *years = [[NSMutableArray alloc] init];
        int currentYear  = [[formatter stringFromDate:[NSDate date]] intValue] - (int)kMinimumAgeRequirementToSignUp;
        for (NSUInteger initialYear = kInitialYearOfBornToShow; initialYear <= currentYear; initialYear++) {
            [years addObject:[NSString stringWithFormat:@"%lu", (unsigned long)initialYear]];
        }
        _yearsOptionsArray = years;
    }
    return _yearsOptionsArray;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupWithObject:(id<SignupPageTableViewConfiguration>)object withIndexPath:(NSIndexPath*)indexPath{
    self.propertyInputTextField.placeholder = [object.propertyName capitalizedString];
    if([[object propertyName] isEqualToString:@"password"]){
        [self.propertyInputTextField setSecureTextEntry:YES];
    }
    else if([[object propertyName] isEqualToString:@"dob"]){
        self.yobTextField = self.propertyInputTextField;
        [self.yobTextField setPlaceholder:@"Enter year of birth"];
        [self.yearOfBirthPicker selectRow:90 inComponent:0 animated:NO];
        [self setupTextFields];
    }
    self.propertyInputTextField.delegate = self;
    [self.propertyInputTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _indexPath = indexPath;
}

-(void)textFieldDidChange:(UITextField*)textField{
    if([self.delegate respondsToSelector:@selector(textFieldDidChangeForIndexPath:withTextField:)]){
        [self.delegate textFieldDidChangeForIndexPath:_indexPath withTextField:textField];
    }
}

#pragma mark - UIPickerView - Methods

-(void)doneButtonFromPickerToolBarAction{
    if([self.yobTextField isFirstResponder]){
        [self pickerView:self.yearOfBirthPicker didSelectRow:[self.yearOfBirthPicker selectedRowInComponent:0] inComponent:0];
        [self.yobTextField resignFirstResponder];
        if([self.delegate respondsToSelector:@selector(textFieldDidChangeForIndexPath:withTextField:)]){
            [self.delegate textFieldDidChangeForIndexPath:_indexPath withTextField:self.yobTextField];
        }

    }
    
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField == self.yobTextField){
        if(self.yobTextField.text.length > 0){
            NSUInteger indexOfObject = [self.yearsOptionsArray indexOfObject:self.yobTextField.text];
            [self.yearOfBirthPicker selectRow:indexOfObject inComponent:0 animated:NO];
        }
    }
    return YES;
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if(pickerView == self.yearOfBirthPicker){
        return 1;
    }
    return 0;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView == self.yearOfBirthPicker){
        return self.yearsOptionsArray.count;
    }
    return 0;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *titleForRow;
    if(pickerView == self.yearOfBirthPicker){
        titleForRow = self.yearsOptionsArray[row];
    }
    return titleForRow;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView == self.yearOfBirthPicker){
        self.yobTextField.text = self.yearsOptionsArray[row];
        if([self.delegate respondsToSelector:@selector(textFieldDidChangeForIndexPath:withTextField:)]){
            [self.delegate textFieldDidChangeForIndexPath:_indexPath withTextField:self.yobTextField];
        }
    }
}



-(void)setupTextFields{
    self.yobTextField.inputView = self.yearOfBirthPicker;
    
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:(CGRect){{0,-44}, {CGRectGetWidth(_yearOfBirthPicker.frame), 44}}];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonFromPickerToolBarAction)];
    
    UIBarButtonItem *flexibleSpaceBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                            target:nil
                                                                                            action:nil];
    
    toolBar.items = [[NSArray alloc] initWithObjects:flexibleSpaceBarButton, barButtonDone,nil];
    [toolBar setBackgroundColor:[_yearOfBirthPicker backgroundColor]];
    self.yobTextField.inputAccessoryView = toolBar;
}



@end
