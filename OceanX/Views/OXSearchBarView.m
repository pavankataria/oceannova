//
//  OXSearchBarView.m
//  OceanX
//
//  Created by Pavan Kataria on 31/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSearchBarView.h"
@interface OXSearchBarView(){
    UIStoryboard *main;
    UIViewController *searchFilterScreen;
    NSCharacterSet *blockedCharacters;

}
@property (nonatomic, retain) UIView *searchResultsContainer;

@end
@implementation OXSearchBarView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    [self setupSearchBarView];
}

-(void)setupSearchBarView{
//    NSLog(@"setup search bar view");
    [self setBackgroundColor:[PKUIAppearanceController darkBlueColor]];
    [self setupTextField];
}

-(void)setupTextField{
    self.searchBarTextField.delegate = self;
    [self.searchBarTextField addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    
    NSMutableCharacterSet *allowedCharacters = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
    [allowedCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@"_ "]];
    blockedCharacters = [allowedCharacters invertedSet];

    [self reloadViews];
//<<<< HEAD
//    [self.searchBarTextField setBackgroundColor:[UIColor grayColor]];
//    [self.searchBarTextField addTarget:self action:@selector(addTagTextFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
//    CGRect frame = self.searchBarTextField.frame;
//    NSLog(@"frame: %@", NSStringFromCGRect(self.searchBarTextField.frame));
//    frame.origin.y = CGRectGetHeight(self.frame)-CGRectGetHeight(frame);
//    self.searchBarTextField.frame = frame;
//    NSLog(@"frame: %@", NSStringFromCGRect(self.searchBarTextField.frame));

//====
//    [self.searchBarTextField addTarget:self action:@selector(addTagTextFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
//>>>> master
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        
    }
    return self;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textfield begin editing");
    [self.delegate searchBarView:self searchBarDidBeginEditing:textField];
}


-(void)setStoryboardName:(NSString *)storyboardName{
    _storyboardName = storyboardName;
}
-(void)showHiddenView{
//    NSAssert([self.storyboardName length], @"No storyboard name set for OXSearchBarView. Set string property `storyboardName`");

}


-(void)reloadViews{
    [self setupTextFieldColors];
    [self setupTextFieldStyle];

}
-(void)setupTextFieldStyle{
    if([self.dataSource respondsToSelector:@selector(searchBarViewTextFieldStyleForSearchBarTextField)]){
        self.searchBarTextField.borderStyle = [self.dataSource searchBarViewTextFieldStyleForSearchBarTextField];
    }
    else{
        self.searchBarTextField.borderStyle = UITextBorderStyleNone;
    }
}

-(void)setupTextFieldColors{
    UIColor *placeholderColor;
    UIColor *textColor;
    UIColor *backgroundTextFieldColor;

    if([self.dataSource respondsToSelector:@selector(searchBarViewBackgroundColorForSearchBarTextField)]){
        backgroundTextFieldColor = [self.dataSource searchBarViewBackgroundColorForSearchBarTextField];
        placeholderColor = [PKUIAppearanceController darkBlueColor];
        textColor = [UIColor darkGrayColor];
    }
    else{
        backgroundTextFieldColor = self.searchBarTextField.backgroundColor = [UIColor clearColor];
        placeholderColor = [UIColor whiteColor];
        textColor = [UIColor whiteColor];
    }
    self.searchBarTextField.backgroundColor = backgroundTextFieldColor;
    self.searchBarTextField.textColor = textColor;
    
    
    if([self.dataSource respondsToSelector:@selector(searchBarViewPlaceHolderString)]){
        self.searchBarTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[self.dataSource searchBarViewPlaceHolderString] attributes:@{NSForegroundColorAttributeName:placeholderColor}];
        if([self.searchBarTextField.placeholder isEqualToString:kOXSearchTextFieldAddressPlaceHolderText]){
            self.searchBarTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        }
    }
    
    self.searchBarTextField.returnKeyType = UIReturnKeySearch;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"text field should return method called");
    if([self.delegate respondsToSelector:@selector(searchBarTextFieldShouldReturn:)]){
        NSLog(@"responds to selector: textfieldshould return");
        [self.delegate performSelector:@selector(searchBarTextFieldShouldReturn:) withObject:textField];
    }
    return NO;
}

-(void)textFieldDidChange:(UITextField *)textField{
    if([self.delegate respondsToSelector:@selector(filterScreenTextFieldDidChange:)]){
        [self.delegate filterScreenTextFieldDidChange:textField];
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if([self.delegate respondsToSelector:@selector(filterScreenTextField:shouldChangeCharactersInRange:replacementString:)]){
        return [self.delegate filterScreenTextField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    return YES;
}

@end
