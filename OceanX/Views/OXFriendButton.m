//
//  OXFriendButton.m
//  
//
//  Created by Pavan Kataria on 18/12/2014.
//
//

#import "OXFriendButton.h"

@implementation OXFriendButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib{
    [super awakeFromNib];
    [self styliseFriendButton];
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self styliseFriendButton];
    }
    return self;
}

-(void)styliseFriendButton{
    self.layer.borderColor = [PKUIAppearanceController lightBlueColor].CGColor;
    self.layer.borderWidth = 1;
}
@end
