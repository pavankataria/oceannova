//
//  OXNewsFeedViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 09/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXNewsFeedViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
//@property (nonatomic, weak)  BOOL tableViewLoading;

@end
