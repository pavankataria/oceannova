//
//  OXAddTagPresentationViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 19/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXAddTagPresentationViewController.h"
#import "OXTagAffirmTableViewCell.h"
@interface OXAddTagPresentationViewController ()

@end

@implementation OXAddTagPresentationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self registerForKeyboardNotifications];
    [self setupTable];
    
    NSMutableCharacterSet *allowedCharacters = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
    [allowedCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@"_ "]];
    self.blockedCharacters = [allowedCharacters invertedSet];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    
    self.navigationItem.rightBarButtonItem.tintColor = rgb(20, 20, 20);
}
-(void)setupTable{
    self.tagsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tagsTableView setEditing:YES];
    self.tagsTableView.allowsSelectionDuringEditing = YES;
    
    [self.tagsTableView registerNib:[UINib nibWithNibName:@"OXTagTableViewCell" bundle:nil] forCellReuseIdentifier:@"normalTagSearchCell"];
    [self.tagsTableView registerNib:[UINib nibWithNibName:@"OXTagAffirmTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellReuseIdentifierTagAffirm];
    
    [self setupNavigtionBar];
    
}
-(void)viewWillAppear:(BOOL)animated{
    if(self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags){
        self.title = @"Tags";
    }
    else{
        [self.addTagTextField becomeFirstResponder];
    }
    [self updateUI];
}
-(void)updateUI{
    
    NSUInteger minimumTagCount = 0;
    
    if(self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeDetailPOIVC || self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags){
        minimumTagCount = 1;
    }
    else{
        minimumTagCount = kOXAddTagsMinimumTagsRequirement;
    }
    
    if(self.storedTagsArray.count < minimumTagCount){
        [self.finishButtonItem setEnabled:NO];
    }
    else{
        [self.finishButtonItem setEnabled:YES];
    }
}
- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void) keyboardWillBeHidden:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self.tagsTableView setContentInset:contentInsets];
    [self.tagsTableView setScrollIndicatorInsets:contentInsets];
    
    [self.tagsTableView setContentOffset:CGPointMake(0,self.tagsTableView.contentOffset.y) animated:YES];
    
}

- (void) keyboardWasShown:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    [self.tagsTableView setContentInset:contentInsets];
    [self.tagsTableView setScrollIndicatorInsets:contentInsets];
    [self.tagsTableView setContentOffset:CGPointMake(0,self.tagsTableView.contentOffset.y) animated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.addTagTextField resignFirstResponder];
    
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags){
        
        if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
            return 2;
        }
        else{
            return 1;
        }
    }
    else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags){
        if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
            if(section == 0){
                if(self.searchTagsResultsArray.count == 0){
                    return 1;
                }
                else{
                    
//                    if([self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
//                        return 1;
//                    }
                    return self.searchTagsResultsArray.count;
                }
            }
            else{
                return 0;
            }
        }
        else if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
            if(section == 0){
                static NSUInteger kAddTagGuidanceDisplayCellOffset = 1;
                return self.storedTagsArray.count + kAddTagGuidanceDisplayCellOffset;
            }
            else{
                return self.poiObject.tags.count;
            }
        }
    }
    else{
        if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
            if(self.searchTagsResultsArray.count == 0){
                return 1;
            }
            else{
                return self.searchTagsResultsArray.count;
            }
        }
        else if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
            static NSUInteger kAddTagGuidanceDisplayCellOffset = 1;
            return self.storedTagsArray.count + kAddTagGuidanceDisplayCellOffset;
        }
        return 1;
    }
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        OXTagTableViewCell *cell = (OXTagTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"normalTagSearchCell"];
        cell.tagNameLabel.textColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
//            NSLog(@"reached here table view search");
//            if([self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
//                
//                cell.tagNameLabel.text = [NSString stringWithFormat:@"Already created: %@", [self addTagTextWithExcessCharactersRemoved]];
//                cell.tagNameLabel.textColor = [UIColor lightGrayColor];
//                cell.selectionStyle = UITableViewCellSelectionStyleNone;
//                cell.userInteractionEnabled = NO;
//                return cell;
//            }
            if(self.searchTagsResultsArray.count == 0){
                if(![self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
                    cell.tagNameLabel.text = [NSString stringWithFormat:@"Create tag: %@", [self addTagTextWithExcessCharactersRemoved]];
                    cell.tagPlacesCountLabel.text = @"";
                }
            }
            else{
                cell.tagNameLabel.text = [self.searchTagsResultsArray[indexPath.row] tagName];
                cell.tagPlacesCountLabel.text = [self.searchTagsResultsArray[indexPath.row] totalPlacesCountString];
            }
        }
        else if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
            if(indexPath.row == self.storedTagsArray.count){
                if(self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeDetailPOIVC || self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags){
                    [cell setTableCellForUIMessageCellForDetailPOIVCForIndexPath:indexPath];
                }
                else{
                    [cell setTableCellForUIMessageCellForIndexPath:indexPath];
                }
            }
            else{
                OXTagPlaceCountModel *tag = self.storedTagsArray[indexPath.row];
                cell.tagNameLabel.text = tag.tagName;
                cell.tagPlacesCountLabel.text = tag.totalPlacesCountString;
            }
        }
        return cell;
    }
    
    else if(indexPath.section == 1){
        OXTagAffirmTableViewCell *cell = (OXTagAffirmTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierTagAffirm];
        
        OXTagModel *tag = [self.poiObject.tags objectAtIndex:indexPath.row];
        cell.tagNameLabel.text = tag.tagName;
        cell.tagCount.text = [NSString stringWithFormat:@"%lu agree", (long)tag.tagCount];
        [cell.upArrowButton addTarget:self action:@selector(upArrowButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        NSLog(@"cell for row tagname: %@, tagHasAffirmed: %d", [tag tagName], (int)[tag tagHasAffirmed]);
        
        
        [cell upArrowButtonSetSelected:[tag tagHasAffirmed]];
//
//        if([tag tagHasAffirmed]){
//            NSLog(@"setting tag has affirmed");
//            [cell.upArrowButton setSelected:YES];
////            cell.upArrowButton.tintColor = [PKUIAppearanceController darkBlueColor];
//        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kOXAddTagsTableViewRowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath selected: %@", indexPath);
    if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
        NSLog(@"currently showing table view kAddTagViewControllerShowTableViewSearch");
        if(self.searchTagsResultsArray.count == 0){
            OXTagPlaceCountModel *newTag = [[OXTagPlaceCountModel alloc] initWithTagName:[self addTagTextWithExcessCharactersRemoved] andTotalPlacesCount:0];
            [self.storedTagsArray addObject:newTag];
        }
        else{
            [self.storedTagsArray addObject:self.searchTagsResultsArray[indexPath.row]];
            [self.searchTagsResultsArray removeAllObjects];
        }
        self.addTagTextField.text = @"";
        [self updateTableView];
        
    }
    else if(self.currentTableViewShowing ==kAddTagViewControllerShowTableViewNormal){
        NSLog(@"currently showing table view kAddTagViewControllerShowTableViewNormal");
        if(indexPath.section == 0){
            //This is where the auto complete tags cells are
        }
        else{
            //This is where the affirmation cells are that are attached to the poi
            [self pressUpArrowButtonForCellAtIndexPath:indexPath];
        }

    }
    else{
        NSLog(@"currently showing table view else statement");
    }
}
#pragma mark - editing delegate methods
//
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.section == 0){
//        return NO;
//    }
//    else{
//        return YES;
//    }
//}

#pragma mark - Private methods

-(void)setupNavigtionBar{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);

}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - UITextField delegate methods
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)characters{
    //    NSLog(@"textfield.length %lu\nrange.length %lu\nrange.location: %lu", [textField.text length], range.length, range.location);
    if (range.location == 0 && [characters isEqualToString:@" "]) {
        return NO;
    }
    // Prevent crashing undo bug.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [characters length] - range.length;
    
    if(newLength > kOXAddTagsTextFieldMaxCharacterCount+1){
        return NO;
    }
    
    return ([characters rangeOfCharacterFromSet:self.blockedCharacters].location == NSNotFound);
}


-(void)updateTableView{
    if([self.addTagTextField.text length] > 0){
        self.currentTableViewShowing = kAddTagViewControllerShowTableViewSearch;
    }
    else{
        self.currentTableViewShowing = kAddTagViewControllerShowTableViewNormal;
    }
//    [self.poiObject setTags:self.storedTagsArray];
    [self updateUI];
    [self.tagsTableView reloadData];
}
-(IBAction)finishButtonTapped:(id)sender{
    if(self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeDetailPOIVC || self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags){
        [self processStoredTagsToPOI];
    }
}
-(void)processStoredTagsToPOI{
    NSString *howManyTagsBeingAdded = [NSString stringWithFormat:@"Adding %lu %@", (unsigned long)self.storedTagsArray.count, self.storedTagsArray.count == 1 ? @"tag" : @"tags"];
    
    [SVProgressHUD showWithStatus:howManyTagsBeingAdded maskType:SVProgressHUDMaskTypeBlack];
    [OXRestAPICient postAddTags:self.storedTagsArray withPOIID:self.poiObject.poiID successBlock:^(AFHTTPRequestOperation *operation, id response) {
            [SVProgressHUD showSuccessWithStatus:@"Successfully added" maskType:SVProgressHUDMaskTypeBlack];
        NSLog(@"successfully added tags: %@", response);
        [self.navigationController popViewControllerAnimated:YES];
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        NSString *errorMessage;
        if([error objectForKey:@"message"]){
            errorMessage = [error objectForKey:@"message"];
        }
        else{
            errorMessage = @"Adding tags failed. Please try again later";
        }
        [SVProgressHUD showErrorWithStatus:errorMessage maskType:SVProgressHUDMaskTypeBlack];

        NSLog(@"failed to successfully add poi: %@", error);
    }];
    
    

}
#pragma mark - UITable View Editing options
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags){
        if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
            if(indexPath.section == 0){
                if(indexPath.row == self.storedTagsArray.count){
                    return NO;
                }
                else{
                    return YES;
                }
            }
            else if(indexPath.section == 1){
                return NO;
            }
        }
        else if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
            return NO;
        }
    }
    else{
        
        if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
            return NO;
        }
        else if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
            
            if(indexPath.row == self.storedTagsArray.count){
                return NO;
            }
            else{
                return YES;
            }
        }
    }
    return NO;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        [self.storedTagsArray removeObjectAtIndex:indexPath.row];
        
        [self.tagsTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tagsTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.storedTagsArray.count inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self updateUI];
    }
}

-(void)upArrowButtonPressed:(id)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tagsTableView];
    NSIndexPath *indexPath = [self.tagsTableView indexPathForRowAtPoint:buttonPosition];
    [self setAgreeCountForIndexPath:indexPath];
}
-(void)setAgreeCountForIndexPath:(NSIndexPath*)indexPath{
    OXTagAffirmTableViewCell *cell = (OXTagAffirmTableViewCell*)[self.tagsTableView cellForRowAtIndexPath:indexPath];
    OXTagModel * currentTag = self.poiObject.tags[indexPath.row];
    if([cell.upArrowButton isSelected]){
        currentTag.tagCount++;
        if(self.comingFromVC != OXTagsPresentationTableViewComingFromVCTypeTutorial){
            [currentTag setAffirm:YES withPOIID:self.poiObject.poiID];
        }
    }
    else{
        currentTag.tagCount--;
        if(self.comingFromVC != OXTagsPresentationTableViewComingFromVCTypeTutorial){
            [currentTag setAffirm:NO withPOIID:self.poiObject.poiID];
        }
    }
    cell.tagCount.text = [NSString stringWithFormat:@"%lu agree", (long)currentTag.tagCount];
}
-(void)pressUpArrowButtonForCellAtIndexPath:(NSIndexPath*)indexPath{
    OXTagAffirmTableViewCell *cell = (OXTagAffirmTableViewCell*)[self.tagsTableView cellForRowAtIndexPath:indexPath];
    [cell upArrowButtonSetSelected:!cell.upArrowButton.selected];
    [self setAgreeCountForIndexPath:indexPath];
}

@end
