//
//  OXSearchListViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSearchListViewController.h"
#import "OXPlaceDisplayImageTableViewCell.h"
#import "OXPlaceSectionHeaderTableViewCell.h"
#import "OXLikeDislikeBarUITableViewCell.h"
#import "OXTagBannerTableViewCell.h"
#import "OXDetailPOIViewController.h"
#import "OXPlaceSectionHeaderTableViewCell.h"
#import "OXProcessedPicture.h"
#import "SDWebImageManager.h"
#import "OXMapAndListContainerViewController.h"
#import "OXCaptionTableViewCell.h"
#import "UIScrollView+EmptyDataSet.h"
//#import "OXLoadingAnimation.h"
#import "OXPlaceDisplayImageTableViewCell.h"

@interface OXSearchListViewController ()<DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>{
    UIStoryboard * poiDetailSB;
}
@property (nonatomic, assign, getter=isLoading) BOOL loading;
@property (nonatomic, assign) CGFloat cellImageHeight;
@property (nonatomic, assign) OXMultipleSearchViewControllerSenderType filterScreenSenderType;
@property (nonatomic, retain) OXDetailPOIViewController *destinationViewController;
@property (nonatomic, retain) UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UIView *noRecordFoundView;
@property (nonatomic, strong) OXMapAndListContainerViewController *mapAndListContainerViewController;
@property (nonatomic, assign) BOOL didAppearAtLeastOnce;
//@property (nonatomic, strong) OXLoadingAnimation *loadingAnimation;

//#warning todo: Surely there's a better way to automatically detect the height of the contentView in iOS 8? Implement it and get rid of anything to do with prototypeCell in the future - say in 2015 summer.
@property (nonatomic, strong) OXCaptionTableViewCell *prototypeCell;


@end

@implementation OXSearchListViewController
#pragma mark - Lazy loading
-(OXPOISearchModel*)poiSearchObject{
    if(!_poiSearchObject){
        _poiSearchObject = [[OXPOISearchModel alloc] init];
    }
    return _poiSearchObject;
}

#pragma mark - UIViewController life cycle - Methods
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self setupTable];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    self.loadingAnimation.frame = self.view.bounds;
//    [self.loadingAnimation centerAnimation];
    if(!self.didAppearAtLeastOnce){
        NSLog(@"VIEW DID APPEAR: %@", [self.poiSearchObject params]);
        self.didAppearAtLeastOnce = TRUE;
        [self performSearch];
    }
}
- (void)viewDidLoad {
    NSLog(@"view did load");
    [super viewDidLoad];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    //    self.tableView.tableFooterView = [UIView new];
    //
    [self registerNibs];
    self.didAppearAtLeastOnce = FALSE;
    self.loading = YES;
    [self.tableView reloadData];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.loadingAnimation = [[OXLoadingAnimation alloc] init];
//    [self.loadingAnimation beginAnimation];
//    [self.view addSubview:self.loadingAnimation];
    
    
    
    
//    self.loadingAnimation = [[OXLoadingAnimation alloc] init];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        //        //make calculations
//        dispatch_async(dispatch_get_main_queue(),
//                       ^{
//                           [self.view addSubview:self.loadingAnimation];
//                           [self.loadingAnimation beginAnimation];
//                       });
//    });
//
    //[self performSearch];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    if([[self.tableView indexPathsForVisibleRows] count]){
        [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(void)registerNibs{
    poiDetailSB = [UIStoryboard storyboardWithName:@"OXPOIDetailSB" bundle:nil ];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXLoadingTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellIdentifierSearchResultsLoadingCell];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXLikeDislikeBarUITableViewCell" bundle:nil] forCellReuseIdentifier:kOXTableViewCellReuseLikeDislikeBarCellIdentifier];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXTagBannerTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellIdentifierTagBanner];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXSectionPlaceHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:kSectionHeaderViewTableViewCellSegueIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"OXCaptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"reuseIdentifierCaptionCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXPlaceDisplayImageTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellIdentifierSearchResultsImageDisplayCell];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - Private methods

-(void)setupTable{
    
    if(!self.refreshControl){
        UITableViewController *tableViewController = [[UITableViewController alloc] init];
        tableViewController.tableView = self.tableView;
        
        self.refreshControl = [[UIRefreshControl alloc] init];
        [self.refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
        tableViewController.refreshControl = self.refreshControl;
    }
    
    self.cellImageHeight = CGRectGetWidth(self.tableView.bounds);
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}
-(void)handleRefresh:(id)sender{
    
    [self.refreshControl endRefreshing];
    [self performSearch];
}

-(void)performSearch{
    NSLog(@"perform search method");
//    [self.loadingAnimation showAnimation];
    [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Loading places"] maskType:SVProgressHUDMaskTypeNone];

    NSLog(@"perform search: %@", [self.poiSearchObject params]);
    self.noRecordFoundView.hidden = YES;
    [self.tableView reloadData];
    [OXPOISearchByListModel searchByListWithPOISearchObject:self.poiSearchObject
                                           withSuccessBlock:^(AFHTTPRequestOperation *operation, NSArray *response) {
                                               NSLog(@"search by list response: %@", response);
                                               
                                               self.searchResults = [response mutableCopy];
                                               self.loading = NO;
                                               [self.tableView reloadData];
                                               [self.refreshControl endRefreshing];
                                               self.noRecordFoundView.hidden = self.searchResults.count ? YES : NO;
                                               [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
                                               
//                                               [self.loadingAnimation hideAnimation];
                                               [SVProgressHUD dismiss];
                                               
                                           } failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                               NSLog(@"failed");
                                               self.loading = NO;
                                               [self.tableView reloadData];
                                               [self.refreshControl endRefreshing];
                                               
//                                               [self.loadingAnimation hideAnimation];
                                               [SVProgressHUD dismiss];
                                           }];
    
}


-(void)preDownloadNextImagesAfterIndexPath:(NSIndexPath *)indexPath numberOfItems:(NSInteger)numberOfItems{
    
    for (int index = 1; index <= numberOfItems; index ++) {
        if (self.searchResults.count > indexPath.section + index) {
            OXPOISearchByListModel *currentSearchListObject = self.searchResults[indexPath.section+index];
            OXProcessedPicture *processedPicture = [currentSearchListObject.pictures firstObject];
            NSURL *url = [NSURL URLWithString:processedPicture.pictureURL];
            [[SDWebImageManager sharedManager] downloadImageWithURL:url
                                                            options:0
                                                           progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
             {}];
        }
    }
    
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(self.isLoading == FALSE){
        //        static NSString *CellIdentifier = @"SectionHeader";
        //        OXPlaceSectionHeaderTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //        if (headerView == nil){
        //            [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
        //        }
        //        [headerView.contentView setBackgroundColor:[UIColor colorWithWhite:1 alpha:kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha]];
        //        [headerView setCellWithPlaceConformingObject:self.searchResults[section]];
        //        return headerView;
        
        NSLog(@"display header view section: %lu", (long)section);
        OXPlaceSectionHeaderTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:kSectionHeaderViewTableViewCellSegueIdentifier];
        if (headerView == nil){
            [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier: %@ could be loaded", kSectionHeaderViewTableViewCellSegueIdentifier];
        }
        [headerView.contentView setBackgroundColor:[UIColor colorWithWhite:1 alpha:kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha]];
        
        
        //            headerView.poiNameLabel = [self.newsFeedData newsFeedTitleAtSection:indexPath.section];
        [headerView setCellWithPlaceConformingObject:self.searchResults[section]];
        return headerView;
    }
    return nil;
}

//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    if(self.isLoading == FALSE){
//        UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:kOXTableViewCellReuseLikeDislikeBarCellIdentifier];
//        if (headerView == nil){
//            [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
//        }
//        [headerView.contentView setBackgroundColor:[UIColor colorWithRed:0.5 green:1.0 blue:0.7 alpha:kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha]];
////        [headerView setCellWithPlaceConformingObject:self.searchResults[section]];
//        return headerView;
//    }
//    return nil;
//}
//-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
//    return kOXSearchTableViewControllerTableViewSectionHeaderHeight;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(self.isLoading == FALSE){
        return kOXSearchTableViewControllerTableViewSectionHeaderHeight;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isLoading){
        return 44;
    }
    else{
        if(indexPath.row == OXInstagramStyleCellTypePicture){
            return self.cellImageHeight;
        }
        else if(indexPath.row == OXInstagramStyleCellTypeTagBanner){
            return 0;
            return 30;
        }
        else if(indexPath.row == OXInstagramStyleCellTypeCaption){
            return 0;
            if (!self.prototypeCell)
            {
                self.prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:@"reuseIdentifierCaptionCell"];
            }
            
            [self configureCell:self.prototypeCell forIndexPath:indexPath];
            
            [self.prototypeCell layoutIfNeeded];
            CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
            //                NSLog(@"CGSize: %@", NSStringFromCGSize(size));
            return size.height;
        }
        else if(indexPath.row == OXInstagramStyleCellTypeLikeAndDislikeBar){
            return kOXPOIListTableViewLikeDislikeBarHeight;
        }
        return 44;
    }
    return 10;
}


#pragma mark - UITable View delegate and datasource methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"section: %lu", indexPath.section);
    OXPOISearchByListModel *currentSearchListObject = self.searchResults[indexPath.section];
    
    OXDetailPOIViewController *destinationVC = [poiDetailSB instantiateInitialViewController];
    //    destinationVC.poiSearchObject = currentSearchListObject;
    destinationVC.poiID = currentSearchListObject.poiId;
    
    destinationVC.parentPOIObject = currentSearchListObject;
    
    //    self.navigationController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@">" style:UIBarButtonItemStylePlain target:nil action:nil];
    //
    [self.navigationController.navigationItem setHidesBackButton:YES];
    [self.navigationController pushViewController:destinationVC animated:YES];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.isLoading){
        return 1;
    }
    else{
        return 4;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(self.isLoading){
        return 1;
    }
    else{
        return self.searchResults.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isLoading){
        NSLog(@"loading activity indicator cell");
        
        UITableViewCell <OXSearchTableViewCellOperationProtocol>*cell = [self.tableView dequeueReusableCellWithIdentifier:  kTableViewCellIdentifierSearchResultsLoadingCell forIndexPath:indexPath];
        [cell startAnimatingActivityIndicatorView];
        //        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
        
        cell.hidden = TRUE;
        return cell;
    }
    else{
        
        if(indexPath.row == OXInstagramStyleCellTypePicture){
            OXPlaceDisplayImageTableViewCell *imageDisplayCell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsImageDisplayCell forIndexPath:indexPath];
            if (imageDisplayCell == nil){
                [NSException raise:@"imageDisplayCell == nil.." format:@"No cells with matching imageDisplayCell CellIdentifier loaded from your storyboard"];
            }
            
            OXPOISearchByListModel *currentSearchListObject = self.searchResults[indexPath.section];
            
            [imageDisplayCell setupCellWithObject:currentSearchListObject];
            imageDisplayCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self preDownloadNextImagesAfterIndexPath:indexPath numberOfItems:2];

            return imageDisplayCell;
        }
/*
 if(indexPath.row == OXInstagramStyleCellTypePicture){
            OXPlaceDisplayImageTableViewCell *imageDisplayCell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsImageDisplayCell forIndexPath:indexPath];
            if (imageDisplayCell == nil){
                [NSException raise:@"imageDisplayCell == nil.." format:@"No cells with matching imageDisplayCell CellIdentifier loaded from your storyboard"];
            }
            
            OXPOISearchByListModel *currentSearchListObject = self.searchResults[indexPath.section];
            
            [imageDisplayCell setupCellWithObject:currentSearchListObject];
            //            imageDisplayCell.separatorInset = UIEdgeInsetsMake(0.f, imageDisplayCell.bounds.size.width, 0.f, 0.f);
            [self preDownloadNextImagesAfterIndexPath:indexPath numberOfItems:2];
            return imageDisplayCell;
        }*/
        else if(indexPath.row == OXInstagramStyleCellTypeTagBanner){
            OXTagBannerTableViewCell * tagBannerCell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierTagBanner];
//            OXPOISearchByListModel *currentSearchListObject = self.searchResults[indexPath.section];
//            tagBannerCell.tagBannerView.tagsArray = currentSearchListObject.tags;
            [tagBannerCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            //            tagBannerCell.separatorInset = UIEdgeInsetsMake(0.f, tagBannerCell.bounds.size.width, 0.f, 0.f);
            return tagBannerCell;
        }
        else if(indexPath.row == OXInstagramStyleCellTypeCaption){
            OXCaptionTableViewCell *captionCell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifierCaptionCell"];
            OXPOISearchByListModel *currentPOI = self.searchResults[indexPath.section];
            captionCell.captionLabel.text = currentPOI.caption;
            return captionCell;
        }
        else if(indexPath.row == OXInstagramStyleCellTypeLikeAndDislikeBar){
            OXLikeDislikeBarUITableViewCell *likeDislikeBar = [tableView dequeueReusableCellWithIdentifier:kOXTableViewCellReuseLikeDislikeBarCellIdentifier];
            [likeDislikeBar setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            if (likeDislikeBar == nil){
                [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
            }
            OXPOISearchByListModel *currentPOI = self.searchResults[indexPath.section];
            
            [likeDislikeBar.likeButton setSelected:currentPOI.userLiked];
            [likeDislikeBar.dislikeButton setSelected:currentPOI.userDisLiked];
            
            
            [likeDislikeBar.likeButton addTarget:self action:@selector(likeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [likeDislikeBar.dislikeButton addTarget:self action:@selector(dislikeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            likeDislikeBar.poiCaption.text = currentPOI.caption;
            //            [headerView.contentView setBackgroundColor:[UIColor colorWithRed:0.5 green:1.0 blue:0.7 alpha:kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha]];
            return likeDislikeBar;
        }
    }
    return nil;
}


-(void)configureCell:(OXCaptionTableViewCell*)cell forIndexPath:(NSIndexPath*)indexPath{
    OXPOISearchByListModel *currentPOI = self.searchResults[indexPath.section];
    //Ridiculous long variable
    static CGFloat totalSizeOfGapLayoutConstraintsOnLeadingAndTrailingGapsOnCaptioLabelInInterfaceBuilder = 30;
    cell.captionLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.tableView.frame) - totalSizeOfGapLayoutConstraintsOnLeadingAndTrailingGapsOnCaptioLabelInInterfaceBuilder;
    cell.captionLabel.text = currentPOI.caption;
}


-(void)likeButtonTapped:(id)sender{
    NSLog(@"likeButtonTapped");
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    __block OXPOISearchByListModel *currentPOI = self.searchResults[indexPath.section];
    
    if(((UIButton*)sender).isSelected){
        [OXRestAPICient likePOIWithID:currentPOI.poiId
                         successBlock:^(AFHTTPRequestOperation *operation, id response) {
                             NSLog(@"%@", response);
                             currentPOI.userLiked = !currentPOI.userLiked;
                             currentPOI.userDisLiked = !currentPOI.userLiked;
                         }
                            failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                NSLog(@"%@", error);
                                
                            }];
    }
    else{
        [OXRestAPICient neutralPOIWithID:currentPOI.poiId
                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                NSLog(@"%@", response);
                                currentPOI.userLiked = false;
                                currentPOI.userDisLiked = false;
                            }
                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                   NSLog(@"%@", error);
                                   
                               }];
    }
}
-(void)dislikeButtonTapped:(id)sender{
    NSLog(@"dislikeButtonTapped");
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    __block OXPOISearchByListModel *currentPOI = self.searchResults[indexPath.section];
    
    if(((UIButton*)sender).isSelected){
        [OXRestAPICient dislikePOIWithID:currentPOI.poiId
                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                currentPOI.userDisLiked = !currentPOI.userDisLiked;
                                currentPOI.userLiked = !currentPOI.userDisLiked;
                            }
                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                   NSLog(@"%@", error);
                                   
                               }];
    }
    
    else{
        NSLog(@"calling neutral api");
        [OXRestAPICient neutralPOIWithID:currentPOI.poiId
                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                currentPOI.userLiked = false;
                                currentPOI.userDisLiked = false;
                            }
                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                   NSLog(@"%@", error);
                                   
                               }];
    }
}

#pragma mark end -


#pragma mark - public methods
-(void)performSerchWithObject:(id)object {
    NSLog(@"performing search in list view controller: %@", [object params]);
    self.poiSearchObject = object;
    [self performSearch];
}


#pragma mark - DZNEmptyDataSet - Methods
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"No places found";
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}


- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSString *text = @"Try searching in a different area or removing some tags.";
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

//- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
//
//    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0]};
//
//    return [[NSAttributedString alloc] initWithString:@"Continue" attributes:attributes];
//}
//
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    
    return [UIImage imageNamed:@"noPlacesReturned"];
}
-(UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView{
    return [PKUIAppearanceController pkLightGrayColor];
}
- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    
    return [UIColor whiteColor];
}

//- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
//
//    return YES;
//}
//- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
//
//    return YES;
//}


@end