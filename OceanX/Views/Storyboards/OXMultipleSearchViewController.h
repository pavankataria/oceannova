//
//  OXMultipleSearchViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 08/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OXFilterTableViewControllerDelegate.h"
#import "OXViewController.h"

@interface OXMultipleSearchViewController : OXViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) IBOutlet UITextField *searchTagsTextField;
@property (nonatomic, weak) IBOutlet UITextField *searchLocationTextField;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *friendButton;

@property (nonatomic, assign) OXMultipleSearchViewControllerSenderType filterScreenSenderType;


@property (nonatomic, weak) OXPOISearchModel *poiSearchObject;
@property (nonatomic, weak) id<OXFilterTableViewControllerDelegate> delegate;
@end
