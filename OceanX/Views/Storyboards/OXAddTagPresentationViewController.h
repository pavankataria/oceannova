//
//  OXAddTagPresentationViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 19/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXAddTagsBaseViewController.h"


@interface OXAddTagPresentationViewController : OXAddTagsBaseViewController <UITableViewDelegate, UITableViewDataSource,OXPOISearchModelDelegate>
@property (nonatomic, weak) IBOutlet UIBarButtonItem *finishButtonItem;

@property (nonatomic, retain) OXPOIModel *poiObject;
-(IBAction)finishButtonTapped:(id)sender;

@property (nonatomic, assign) OXTagsPresentationTableViewComingFromVCType comingFromVC;
@end
