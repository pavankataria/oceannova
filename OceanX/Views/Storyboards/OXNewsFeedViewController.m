//
//  OXNewsFeedViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 09/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXNewsFeedViewController.h"
#import "OXRestAPICient.h"
#import "NewsFeedDataModel.h"
#import "OXPlaceSectionHeaderTableViewCell.h"

#import "OXNewsFeedSectionHeaderTableViewCell.h"
#import "OXPlaceDisplayImageTableViewCell.h"
#import "OXTagBannerTableViewCell.h"
#import "OXLikeDislikeBarUITableViewCell.h"
#import "OXDetailPOIViewController.h"
#import "OXProcessedPicture.h"
#import "SDWebImageManager.h"
#import "OXCaptionTableViewCell.h"


#import "OXLoadingAnimation.h"


@interface OXNewsFeedViewController (){
    UIStoryboard * poiDetailSB;
    
}
@property (nonatomic, retain) NewsFeedDataModel *newsFeedData;
@property (nonatomic, assign, getter=isNewsFeedDataLoading) BOOL newsFeedDataLoading;
@property (nonatomic, assign) NSUInteger totalNumberOfSections;
@property (nonatomic, assign) CGFloat cellImageHeight;
@property (nonatomic, retain) UIRefreshControl *refreshControl;


//#warning todo: Surely there's a better way to automatically detect the height of the contentView in iOS 8? Implement it and get rid of anything to do with prototypeCell in the future - say in 2015 summer.
@property (nonatomic, strong) OXCaptionTableViewCell *prototypeCell;

@property (nonatomic, strong) OXLoadingAnimation *loadingAnimation;

@end

@implementation OXNewsFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"\n\n\n\n");
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //make calculations
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           [UIView setAnimationsEnabled:YES];
                       });
    });
    [self registerNibs];
    self.newsFeedDataLoading = YES;

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.loadingAnimation = [[OXLoadingAnimation alloc] init];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
////        //make calculations
//        dispatch_async(dispatch_get_main_queue(),
//                       ^{
//                           [self.view addSubview:self.loadingAnimation];
//                           [self.loadingAnimation beginAnimation];
//                       });
//    });

    [self.tableView reloadData];

    [self loadNewsfeedEntries];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.cellImageHeight = CGRectGetWidth(self.tableView.bounds);

}
-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    [self setupTable];
//    self.loadingAnimation.frame = self.view.bounds;
//    [self.loadingAnimation centerAnimation];
    
    
}
-(void)transitionNavigationBarTitle:(NSString*)title withAnimation:(BOOL)animate{
    if(![self.parentViewController.title isEqualToString:title]){
        CATransition *animation = [CATransition animation];
        animation.duration = 0.4;
        animation.type = kCATransitionFade;
        [self.parentViewController.navigationController.navigationBar.layer addAnimation: animation forKey: @"fadeText"];
        self.parentViewController.navigationItem.title = title;
    }
}
-(void)setupTable{
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableView;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = self.refreshControl;
    
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    self.cellImageHeight = CGRectGetWidth(self.tableView.bounds);
}

-(void)handleRefresh:(id)sender{
    [self.refreshControl endRefreshing];
    [self loadNewsfeedEntries];
}

-(void)registerNibs{
    
    poiDetailSB = [UIStoryboard storyboardWithName:@"OXPOIDetailSB" bundle:nil ];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXLoadingTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellIdentifierSearchResultsLoadingCell];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXSectionPlaceHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:kSectionHeaderViewTableViewCellSegueIdentifier];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXNewsFeedSectionHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"reuseOXNewsFeedSectionHeaderTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXLikeDislikeBarUITableViewCell" bundle:nil] forCellReuseIdentifier:kOXTableViewCellReuseLikeDislikeBarCellIdentifier];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXTagBannerTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellIdentifierTagBanner];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXCaptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"reuseIdentifierCaptionCell"];
}

-(void)loadNewsfeedEntries{
    NSLog(@"loading news feed entries");
//    [self.loadingAnimation showAnimation];
    if([OXLoginManager isProfileTutorialViewed]){
        [SVProgressHUD showWithStatus:@"Loading places" maskType:SVProgressHUDMaskTypeNone];
    }
    
    //    self.newsFeedDataLoading = YES;
    //    [self.tableView reloadData];
    
    [OXRestAPICient getNewsFeedWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
        self.newsFeedDataLoading = FALSE;
        [self handleNewsFeedResponse:response];
        [self.refreshControl endRefreshing];
//        [self.loadingAnimation hideAnimation];
        [SVProgressHUD dismiss];
        self.parentViewController.navigationItem.title = [self.newsFeedData appropriateTitleForPOITrickSection:0];
        
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        self.newsFeedDataLoading = FALSE;
        [self.refreshControl endRefreshing];
//        [self.loadingAnimation hideAnimation];
        [SVProgressHUD dismiss];
        NSLog(@"error %@", error);
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)handleNewsFeedResponse:(NSDictionary*)response{
    if(response){
        self.newsFeedData = [[NewsFeedDataModel alloc] initWithDictionary:response];
    }
    [self.tableView reloadData];
}

#pragma mark - Private Methods
//                TODO: Pavan will add proper condition to load next two place images
-(void)preDownloadNextImagesAfterIndexPath:(NSIndexPath *)indexPath  numberOfItems:(NSInteger)numberOfItems{
    for (int index = 1; index <= numberOfItems; index ++) {
        OXPOISearchByListModel *currentSearchListObject = [self.newsFeedData poiForSection:indexPath.section+index];
        if(currentSearchListObject){
            OXProcessedPicture *processedPicture = [currentSearchListObject.pictures firstObject];
            NSURL *url = [NSURL URLWithString:processedPicture.pictureURL];
            
            [[SDWebImageManager sharedManager] downloadImageWithURL:url
                                                            options:0
                                                           progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                               
                                                           }];
        }
    }
}

#pragma mark - UITableViewDelegate - Methods


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(self.isNewsFeedDataLoading){
        return 0;
    }
    return self.newsFeedData.totalNumberOfSectionsToDisplay;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.isNewsFeedDataLoading){
        return 0;
    }
    else{
        if([self.newsFeedData shouldDisplaySectionHeaderForSection:section]){
            if([self.newsFeedData areThereAnyPOISForTrickSection:section]){
//                NSLog(@"section: %lu", section);
                //We dont want the first title to display
                if(section == 0){
                    return 0;
                }
                return 1;
            }
            else{
                return 0; //dont display any section title row if the section contains no pois.
            }
        }
        else{
            return 4;
        }
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if( ! self.isNewsFeedDataLoading){
            if(![self.newsFeedData shouldDisplaySectionHeaderForSection:indexPath.section]){
                OXPOISearchByListModel *currentSearchListObject = [self.newsFeedData poiForSection:indexPath.section];//self.searchResults[indexPath.section];
                __weak OXDetailPOIViewController *destinationVC = [poiDetailSB instantiateInitialViewController];
                destinationVC.poiID = currentSearchListObject.poiId;
                destinationVC.parentPOIObject = currentSearchListObject;
                NSLog(@"trying to remove back button");
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                                   self.title = @"";
                                   [self.navigationController pushViewController:destinationVC animated:YES];
                               });
            }
        }
    });
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"HelveticaNeue" size:25],
      NSFontAttributeName, [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0], NSForegroundColorAttributeName, nil]];
    

    if([[self.tableView indexPathsForVisibleRows] count]){
        [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isNewsFeedDataLoading){
        UITableViewCell *loadingCell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsLoadingCell];
        loadingCell.hidden = YES;
        return loadingCell;
    }
    else{
        if([self.newsFeedData shouldDisplaySectionHeaderForSection:indexPath.section]){
            OXNewsFeedSectionHeaderTableViewCell *headerViewCell = [tableView dequeueReusableCellWithIdentifier:@"reuseOXNewsFeedSectionHeaderTableViewCell"];
            if (headerViewCell == nil){
                [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier: %@ could be loaded", @"reuseOXNewsFeedSectionHeaderTableViewCell"];
            }
            headerViewCell.sectionHeaderLabel.text = [self.newsFeedData newsFeedTitleAtSection:indexPath.section];
            headerViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return headerViewCell;
        }
        else{
            if(indexPath.row == OXInstagramStyleCellTypePicture){
                //                NSLog(@"PICTURE: indexPath row:%  lu seciton:%lu", indexPath.row, indexPath.section);
                
                OXPlaceDisplayImageTableViewCell *imageDisplayCell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsImageDisplayCell forIndexPath:indexPath];
                if (imageDisplayCell == nil){
                    [NSException raise:@"imageDisplayCell == nil.." format:@"No cells with matching imageDisplayCell CellIdentifier loaded from your storyboard"];
                }
                
                OXPOISearchByListModel *currentSearchListObject = [self.newsFeedData poiForSection:indexPath.section];
                
                [imageDisplayCell setupCellWithObject:currentSearchListObject];

//                TODO: add proper condition to load next two place images
                
                [self preDownloadNextImagesAfterIndexPath:indexPath numberOfItems:2];

                imageDisplayCell.separatorInset = UIEdgeInsetsMake(0.f, imageDisplayCell.bounds.size.width, 0.f, 0.f);
                imageDisplayCell.selectionStyle = UITableViewCellSelectionStyleNone;

                return imageDisplayCell;
            }
            else if(indexPath.row == OXInstagramStyleCellTypeTagBanner){
                //                NSLog(@"TAG BANNER: indexPath row:%lu seciton:%lu", indexPath.row, indexPath.section);
                OXTagBannerTableViewCell * tagBannerCell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierTagBanner];
//                OXPOISearchByListModel *currentSearchListObject = [self.newsFeedData poiForSection:indexPath.section];
                tagBannerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                
//                tagBannerCell.tagBannerView.tagsArray = currentSearchListObject.tags;
                //                tagBannerCell.separatorInset = UIEdgeInsetsMake(0.f, tagBannerCell.bounds.size.width, 0.f, 0.f);
                return tagBannerCell;
            }
            else if(indexPath.row == OXInstagramStyleCellTypeCaption){
                OXCaptionTableViewCell *captionCell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifierCaptionCell"];
                OXPOISearchByListModel *currentPOI = [self.newsFeedData poiForSection:indexPath.section];
                captionCell.captionLabel.text = currentPOI.caption;
                captionCell.selectionStyle = UITableViewCellSelectionStyleNone;

                return captionCell;
            }
            else if(indexPath.row == OXInstagramStyleCellTypeLikeAndDislikeBar){
                OXLikeDislikeBarUITableViewCell *likeDislikeBar = [tableView dequeueReusableCellWithIdentifier:kOXTableViewCellReuseLikeDislikeBarCellIdentifier];
                [likeDislikeBar setSelectionStyle:UITableViewCellSelectionStyleNone];
                if (likeDislikeBar == nil){
                    [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
                }
                OXPOISearchByListModel *currentPOI = [self.newsFeedData poiForSection:indexPath.section];
                
                [likeDislikeBar.likeButton setSelected:currentPOI.userLiked];
                [likeDislikeBar.dislikeButton setSelected:currentPOI.userDisLiked];
                
                
                [likeDislikeBar.likeButton addTarget:self action:@selector(likeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                [likeDislikeBar.dislikeButton addTarget:self action:@selector(dislikeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                likeDislikeBar.poiCaption.text = currentPOI.caption;
                //            [headerView.contentView setBackgroundColor:[UIColor colorWithRed:0.5 green:1.0 blue:0.7 alpha:kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha]];
                return likeDislikeBar;
            }
        }
    }
    return nil;
}

-(void)likeButtonTapped:(id)sender{
    NSLog(@"likeButtonTapped");
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    __block OXPOISearchByListModel *currentPOI = [self.newsFeedData poiForSection:indexPath.section];
    
    if(((UIButton*)sender).isSelected){
        [OXRestAPICient likePOIWithID:currentPOI.poiId
                         successBlock:^(AFHTTPRequestOperation *operation, id response) {
                             NSLog(@"%@", response);
                             currentPOI.userLiked = !currentPOI.userLiked;
                             currentPOI.userDisLiked = !currentPOI.userLiked;
                         }
                            failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                NSLog(@"%@", error);
                                
                            }];
    }
    else{
        [OXRestAPICient neutralPOIWithID:currentPOI.poiId
                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                NSLog(@"%@", response);
                                currentPOI.userLiked = false;
                                currentPOI.userDisLiked = false;
                            }
                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                   NSLog(@"%@", error);
                                   
                               }];
    }
}

-(void)dislikeButtonTapped:(id)sender{
    NSLog(@"dislikeButtonTapped");
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    __block OXPOISearchByListModel *currentPOI = [self.newsFeedData poiForSection:indexPath.section];
    
    if(((UIButton*)sender).isSelected){
        [OXRestAPICient dislikePOIWithID:currentPOI.poiId
                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                currentPOI.userDisLiked = !currentPOI.userDisLiked;
                                currentPOI.userLiked = !currentPOI.userDisLiked;
                            }
                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                   NSLog(@"%@", error);
                                   
                               }];
    }
    
    else{
        NSLog(@"calling neutral api");
        [OXRestAPICient neutralPOIWithID:currentPOI.poiId
                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                currentPOI.userLiked = false;
                                currentPOI.userDisLiked = false;
                            }
                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                   NSLog(@"%@", error);
                                   
                               }];
    }
}


//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    return [[self.newsFeedData.sections objectAtIndex:section] sectionTitle];
//}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if( ! self.isNewsFeedDataLoading){
        if([self.newsFeedData shouldDisplaySectionHeaderForSection:section]){
            return nil;
        }
        else{
            //            NSLog(@"display header view section: %lu", section);
            OXPlaceSectionHeaderTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:kSectionHeaderViewTableViewCellSegueIdentifier];
            if (headerView == nil){
                [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier: %@ could be loaded", kSectionHeaderViewTableViewCellSegueIdentifier];
            }
            [headerView.contentView setBackgroundColor:[UIColor colorWithWhite:1 alpha:kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha]];
            //            headerView.poiNameLabel = [self.newsFeedData newsFeedTitleAtSection:indexPath.section];
            [headerView setCellWithPlaceConformingObject:[self.newsFeedData poiForSection:section]];
            //            NSLog(@"headerView poiScore: %@", headerView.poiNameLabel.text);
            return headerView;
        }
    }
    return nil;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if( ! self.isNewsFeedDataLoading){
        
        if([self.newsFeedData shouldDisplaySectionHeaderForSection:section]){
            return 0;
        }
        else{
            return kOXSearchTableViewControllerTableViewSectionHeaderHeight;
        }
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isNewsFeedDataLoading){
        return kOXTableViewControllerLoadingCellSize;
    }
    else{
        if([self.newsFeedData shouldDisplaySectionHeaderForSection:indexPath.section]){
            return 75;
        }
        else{
            if(indexPath.row == OXInstagramStyleCellTypePicture){
                return self.cellImageHeight;
            }
            else if(indexPath.row == OXInstagramStyleCellTypeTagBanner){
                return 0;
            }
            else if(indexPath.row == OXInstagramStyleCellTypeCaption){
                return 0;
                if (!self.prototypeCell){
                    self.prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:@"reuseIdentifierCaptionCell"];
                }
                
                [self configureCell:self.prototypeCell forIndexPath:indexPath];
                
                [self.prototypeCell layoutIfNeeded];
                CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
                //                NSLog(@"CGSize: %@", NSStringFromCGSize(size));
                return size.height;
            }
            else if(indexPath.row == OXInstagramStyleCellTypeLikeAndDislikeBar){
                return kOXPOIListTableViewLikeDislikeBarHeight;
            }
            return 44;
        }
        return 10;
    }
    return 100;
}
-(void)configureCell:(OXCaptionTableViewCell*)cell forIndexPath:(NSIndexPath*)indexPath{
    OXPOISearchByListModel *currentPOI = [self.newsFeedData poiForSection:indexPath.section];
    //Ridiculous long variable
    static CGFloat totalSizeOfGapLayoutConstraintsOnLeadingAndTrailingGapsOnCaptioLabelInInterfaceBuilder = 30;
    cell.captionLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.tableView.frame) - totalSizeOfGapLayoutConstraintsOnLeadingAndTrailingGapsOnCaptioLabelInInterfaceBuilder;
    cell.captionLabel.text = currentPOI.caption;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - ScrollView - Methods

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSLog(@"did end displaying cell: %@ at indexPath: %@", cell, indexPath);
    NSIndexPath *indexPathSection = [tableView indexPathForCell:[[tableView visibleCells] firstObject]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           [self transitionNavigationBarTitle:[self.newsFeedData appropriateTitleForPOITrickSection:indexPathSection.section] withAnimation:YES];
                       });
    });

}
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    //    if ([tableView.indexPathsForVisibleRows indexOfObject:indexPath] == NSNotFound){
//    //
//    //    }
//    
//    //    if([self.newsFeedData shouldDisplaySectionHeaderForSection:indexPath.section]){
//    //        if(![self.parentViewController.title isEqualToString:[self.newsFeedData newsFeedTitleAtSection:indexPath.section]]){
//    
//    
//    //            [self transitionNavigationBarTitle:[self.newsFeedData appropriateTitleForPOITrickSection:indexPath.section] withAnimation:YES];
//    //        }
//    //    }
//    
//    //    if([[tableView visibleCells] count] > 1){
//    //        NSIndexPath *indexPathSection = [tableView indexPathForCell:[[tableView visibleCells] objectAtIndex:1]];
//    //        [self transitionNavigationBarTitle:[self.newsFeedData appropriateTitleForPOITrickSection:indexPathSection.section] withAnimation:YES];
//    //    }
//}
@end

