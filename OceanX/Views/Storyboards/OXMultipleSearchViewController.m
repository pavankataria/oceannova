//
//  OXMultipleSearchViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 08/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXMultipleSearchViewController.h"
#import "OXSearchTagTableViewCell.h"
#import "OXFilterTableViewCell.h"
#import "OXLoadingTableViewCell.h"
#import "OXTagTableViewCell.h"
#import "OXFriendTableViewCell.h"
#import "OXFriendModel.h"
#import "AppDelegate.h"
#import "NSString+Levenshtein.h"
#import "UIScrollView+EmptyDataSet.h"

const double LONDON_LONGITUDE = 0.1275;
const double LONDON_LATITUDE = 51.5072;

typedef NS_ENUM(NSUInteger, OXSearchViewControllerShowTableViewType) {
    OXSearchViewControllerShowTableViewTypeTags = 0,
    OXSearchViewControllerShowTableViewTypeTagsSearch,
    OXSearchViewControllerShowTableViewTypeLocation,
    OXSearchViewControllerShowTableViewTypeFriends,
};
@interface OXMultipleSearchViewController ()<DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (nonatomic, retain) NSCharacterSet *tagBlockedCharacters;
@property (nonatomic, retain) NSCharacterSet *locationBlockedCharacters;

@property (nonatomic, retain) NSMutableArray *locationsArray;

@property (nonatomic, retain) NSMutableArray *searchTagsArray;
@property (nonatomic, retain) NSMutableArray *storedTagsArray;
@property (nonatomic, retain) NSMutableArray *friendsArray;
@property (nonatomic, retain) NSMutableArray *selectedFriendsArray;


@property (nonatomic, assign) OXSearchViewControllerShowTableViewType currentTableViewShowing;
@property (nonatomic, assign, getter=isTagSearching) BOOL tagSearching;
@property (nonatomic, assign, getter=isLocationSearching) BOOL locationSearching;
@property (nonatomic, assign, getter=isFriendsSearching) BOOL friendsSearching;



@end

@implementation OXMultipleSearchViewController
#pragma mark - Lazy Loading - Methods
-(NSMutableArray *)locationsArray{
    if(!_locationsArray){
        _locationsArray = [[NSMutableArray alloc] init];
    }
    return _locationsArray;
}
-(NSMutableArray *)searchTagsArray{
    if(!_searchTagsArray){
        _searchTagsArray = [[NSMutableArray alloc] init];
    }
    return _searchTagsArray;
}
-(NSMutableArray *)storedTagsArray{
    if(!_storedTagsArray){
        _storedTagsArray = [[NSMutableArray alloc] init];
    }
    return _storedTagsArray;
}

-(NSMutableArray *)friendsArray{
    if(!_friendsArray){
        _friendsArray = [[NSMutableArray alloc] init];
    }
    return _friendsArray;
}
-(NSMutableArray *)selectedFriendsArray{
    if(!_selectedFriendsArray){
        _selectedFriendsArray = [[NSMutableArray alloc] init];
    }
    return _selectedFriendsArray;
}
#pragma mark - UIViewController LifeCycle - Methods

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    //if we only try and resignFirstResponder on textField or searchBar,
    //the keyboard will not dissapear (at least not on iPad)!
    self.poiSearchObject.textSearchString = @"";
    return YES;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.friendsArray = [AppDelegate getAppDelegateReference].friendsArray;
//    NSLog(@"friends array count: %lu", (unsigned long)self.friendsArray.count);
    self.title = @"Search";
    [self setupTextFields];
    
    [self setupTableView];
    
    [self setupFriendButton];
    [self registerForKeyboardNotifications];
    
}


-(void)setupFriendButton{
    [self.friendButton.layer setCornerRadius:5];
    [self.friendButton addTarget:self action:@selector(addFriendButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}
-(void)addFriendButtonPressed{
    self.currentTableViewShowing = OXSearchViewControllerShowTableViewTypeFriends;
    [self.view endEditing:YES];
    [self.tableView reloadData];
    [self grabFriendsWithRetries:1];
}

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void) keyboardWillBeHidden:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self.tableView setContentInset:contentInsets];
    [self.tableView setScrollIndicatorInsets:contentInsets];
    
    [self.tableView setContentOffset:CGPointMake(0,self.tableView.contentOffset.y) animated:YES];
    
}

- (void) keyboardWasShown:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    [self.tableView setContentInset:contentInsets];
    [self.tableView setScrollIndicatorInsets:contentInsets];
    [self.tableView setContentOffset:CGPointMake(0,self.tableView.contentOffset.y) animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
-(void)setupTableView{
    [self.tableView registerNib:[UINib nibWithNibName:@"OXLoadingTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellIdentifierSearchResultsLoadingCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"OXTagTableViewCell" bundle:nil] forCellReuseIdentifier:@"normalTagSearchCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXSearchTagTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellTagIdentifier];
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(cancelActionMethod)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);

    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    NSLog(@"tags: %@", self.poiSearchObject.tags);
    if([self.poiSearchObject.tags count] > 0){
        self.storedTagsArray = self.poiSearchObject.tags;
    }
    if([self.poiSearchObject.friendsIds count] > 0){
        self.selectedFriendsArray = self.poiSearchObject.friendsIds;
    }
    
    if([self.poiSearchObject.coordinate.placeMarkTitleAndSubtitle length] == 0){
        self.searchLocationTextField.text = @"Current Location";
    }
    else{
        self.searchLocationTextField.text = self.poiSearchObject.coordinate.placeMarkTitleAndSubtitle;
    }
    if(self.filterScreenSenderType == OXMultipleSearhViewControllerSenderTypeSecondButton){
        NSLog(@"SHOW FRIENDS TABLE");
        self.currentTableViewShowing = OXSearchViewControllerShowTableViewTypeFriends;
        self.tableView.editing = NO;
        NSLog(@"selected friends list: %@", self.selectedFriendsArray);
        NSLog(@"friends list: %@", self.friendsArray);

        NSLog(@"poi friends list: %@", self.poiSearchObject.friendsIds);
    }
//    else if(self.filterScreenSenderType == OXMultipleSearhViewControllerSenderTypeFirstButton){
//        NSLog(@"");
//        if(self.poiSearchObject.textSearchString.length > 0){
//            self.searchTagsTextField.text = self.poiSearchObject.textSearchString;
//    self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeTagsSearch
//        }
//        self.currentTableViewShowing = OXSearchViewControllerShowTableViewType;
//
//    }
    else{
        [self.searchTagsTextField becomeFirstResponder];
        if(self.poiSearchObject.textSearchString.length > 0){
            self.searchTagsTextField.text = self.poiSearchObject.textSearchString;
            self.currentTableViewShowing = OXSearchViewControllerShowTableViewTypeTagsSearch;
            OXTagPlaceCountModel *createNewTag = [[OXTagPlaceCountModel alloc] initWithTagName:[NSString stringWithFormat:@"%@", self.searchTagsTextField.text] andTotalPlacesCount:0 andCreateNewTag:YES];
            [self.searchTagsArray insertObject:createNewTag atIndex:0];

        }
        else{
            self.currentTableViewShowing = OXSearchViewControllerShowTableViewTypeTags;
        }
        self.tableView.editing = YES;
    }

    [self updateTable];
}
-(void)updateTable{
    for(OXFriendModel *friend in self.friendsArray){
        for(NSNumber *friendId in self.selectedFriendsArray){
            if(friend.personId == [friendId integerValue]){
                friend.friendSelected = TRUE;
            }
        }
    }
    [self.tableView reloadData];
}
-(void)cancelActionMethod{
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)setupTextFields{
    [self setupBlockedCharacters];
    self.searchLocationTextField.delegate = self;
    self.searchTagsTextField.delegate = self;
    
    
    [self.searchTagsTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.searchLocationTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissAndPerformSearch];
    return NO;
}
-(void)textFieldDidChange:(UITextField*)textField{
    if(textField == self.searchTagsTextField){
        self.currentTableViewShowing = OXSearchViewControllerShowTableViewTypeTagsSearch;
        
        [self.tableView reloadData];
        [self performTagSearchWithText:self.searchTagsTextField.text];
    }
    else if(textField == self.searchLocationTextField){
        [self updateTableViewResultsForCurrentActiveTextField:textField];
        
        [self performLocationSearchWithText:textField.text];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self updateTableViewResultsForCurrentActiveTextField:textField];
    
}

-(void)updateTableViewResultsForCurrentActiveTextField:(UITextField*)textField{
    if(textField == self.searchTagsTextField){
        if([textField.text length] > 0){
            [self setCurrentTableViewShowing:OXSearchViewControllerShowTableViewTypeTagsSearch];
            self.tableView.editing = NO;
            
        }
        else{
            [self setCurrentTableViewShowing:OXSearchViewControllerShowTableViewTypeTags];
            self.tableView.editing = YES;
        }
    }
    else if(textField == self.searchLocationTextField){
        self.tableView.editing = NO;
        
        [self setCurrentTableViewShowing:OXSearchViewControllerShowTableViewTypeLocation];
        
    }
}
-(void)setCurrentTableViewShowing:(OXSearchViewControllerShowTableViewType)currentTableViewShowing{
    if(_currentTableViewShowing != currentTableViewShowing){
        _currentTableViewShowing = currentTableViewShowing;
        [self.tableView reloadData];
    }
}

-(void)setupBlockedCharacters{
    NSMutableCharacterSet *locationAllowedCharacters = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
    [locationAllowedCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@", "]];
    self.locationBlockedCharacters = [locationAllowedCharacters invertedSet];
    
    NSMutableCharacterSet *tagAllowedCharacters = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
    [tagAllowedCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@" @"]];
    self.tagBlockedCharacters = [tagAllowedCharacters invertedSet];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate - Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeTags){
        if(self.storedTagsArray.count == 0){
            return 1;
        }
        else{
            return self.storedTagsArray.count;
        }
    }
    else if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeTagsSearch){
        if(self.isTagSearching){
            return 1;
        }
        else{
            //We do this so that we can display the text cell which should only be there
            //if no tags have been inserted so far
            if(self.storedTagsArray.count == 0){
                return self.searchTagsArray.count;
            }
            else{
                return self.searchTagsArray.count;
            }
        }
    }
    else if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeLocation){
        if(self.isLocationSearching){
            return 1;
        }
        else{
            return self.locationsArray.count+1;
        }
    }
    
    else if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeFriends){
        if(self.isFriendsSearching){
            return 1;
        }
        else{
            if(self.friendsArray.count == 0){
                return 0;
            }
            return self.friendsArray.count;
        }
    }
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    int value = 0;
    
    if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeTags){
        
        value = 1;
        
    }
    else if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeTagsSearch){
        
        value = 2;
    }
    else if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeLocation){
        
        value = 3;
        
    }
    else if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeFriends){
        
        value = 4;
        
    }
    
    
    
    
    
    
    switch (value) {
        case 1:{
            
            
            OXSearchTagTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellTagIdentifier];
            if(self.storedTagsArray.count == 0){
                [cell setSearchTableCellForUIMessageCellForIndexPath:indexPath];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else{
                [cell setCellWithObjectFollowingProtocol:self.storedTagsArray[indexPath.row]];
                [cell.tagDeleteButton addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                [cell.tagDeleteButton setHidden:YES];
                
                OXTagPlaceCountModel *currentPlaceObject = self.storedTagsArray[indexPath.row];
                if(currentPlaceObject.shouldCreateNewTag){
                    cell.tagNameLabel.text = [NSString stringWithFormat:@"Ready for named search: %@", [currentPlaceObject OXTagModelOperationTagNameString]];
                }
                
                return cell;
            }
            
            
        }break;
            
            
        case 2:{
            
            if(self.isTagSearching){
                UITableViewCell <OXSearchTableViewCellOperationProtocol>*cell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsLoadingCell forIndexPath:indexPath];
                [cell startAnimatingActivityIndicatorView];
                return cell;
            }
            else{
                
                
                OXTagPlaceCountModel *currentPlaceObject = self.searchTagsArray[indexPath.row];
                if(currentPlaceObject.shouldCreateNewTag){
                    OXSearchTagTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellTagIdentifier];
                    [cell setCellForSearchWithObjectFollowingProtocol:currentPlaceObject];
                    cell.tagNameLabel.textColor = [UIColor blackColor];
                    cell.tagNameLabel.text = [NSString stringWithFormat:@"Search by name: %@", [currentPlaceObject OXTagModelOperationTagNameString]];
                    [cell.tagDeleteButton setHidden:YES];
                    return cell;
                    
                }
                else{
                    OXTagTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"normalTagSearchCell"];
                    [cell setCellWithObjectFollowingProtocol:self.searchTagsArray[indexPath.row]];
                    return cell;
                }
            }
            
            
        }break;
            
        case 3:{
            
            if(self.isLocationSearching){
                UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsLoadingCell forIndexPath:indexPath];
                return cell;
            }
            else{
                OXFilterTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"reuseFilterCell" forIndexPath:indexPath];
                
                if(indexPath.row == 0){
                    cell.headerLabel.text = @"Current Location";
                }
                else{
                    
                    id <OXSearchModelOperationProtocol>object = [self.locationsArray objectAtIndex:indexPath.row-1];
                    
                    cell.headerLabel.text = [object searchResultTitle];
                    
                }
                return cell;
            }
        }
            break;
            
        case 4:{
            
            if(self.isFriendsSearching){
                UITableViewCell <OXSearchTableViewCellOperationProtocol>*cell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsLoadingCell forIndexPath:indexPath];
                [cell startAnimatingActivityIndicatorView];
                return cell;
            }
            else{
//                if(self.friendsArray.count == 0){
//                    OXTagTableViewCell *emptycell = [self.tableView dequeueReusableCellWithIdentifier:@"normalTagSearchCell"];
//                    emptycell.tagNameLabel.text = @"No friends yet! Go to your profile to add some.";
//                    emptycell.tagPlacesCountLabel.text = @"";
//                    return emptycell;
//                }
//                
                id <GenericDataModelForCellConfiguration>genericObject = [self.friendsArray objectAtIndex:indexPath.row];
                NSString *cellIdentifier = [genericObject cellIdentifier];
                OXFriendTableViewCell <GenericTableViewCellConfiguration> *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                [cell updateForModelObject:genericObject];
                
                //            TODO :remove if not needed
                //            [cell.friendStateButton setHidden:YES];// .friendStateButton addTarget:self action:@selector(cellAddFriendButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                self.tableView.editing = NO;
                cell.accessoryType = [genericObject isFriendSelected] == YES ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
                return cell;
            }
        }
            break;
        default:{
            return nil;
            
        }break;
    }
}
- (void)checkButtonTapped:(id)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil){
        [self.storedTagsArray removeObjectAtIndex:indexPath.row];
        [self updateTableViewForTags];
    }
}

- (void)cellAddFriendButtonPressed:(id)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil){
        OXFriendTableViewCell * cell = (OXFriendTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        //        [self.friendsArray removeObjectAtIndex:indexPath.row];
        //            TODO :remove if not needed
        //        [cell.friendStateButton setSelected:!cell.friendStateButton.selected];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeTags){
        
    }
    else if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeTagsSearch){
        
        NSLog(@"search row selected: %@", [self.searchTagsArray[indexPath.row] OXTagModelOperationTagNameString]);
        OXTagPlaceCountModel *currentPlaceObject = self.searchTagsArray[indexPath.row];
        if(currentPlaceObject.shouldCreateNewTag){
            self.poiSearchObject.textSearchString = [currentPlaceObject OXTagModelOperationTagNameString];
            [self dismissAndPerformSearch];
        }
        else{
            NSMutableArray *tagsToRemove = [[NSMutableArray alloc] init];
            for(OXTagPlaceCountModel *currentItem in self.storedTagsArray){
                if(currentItem.shouldCreateNewTag){
                    [tagsToRemove addObject:currentItem];
                }
            }
            [self.storedTagsArray removeObjectsInArray:tagsToRemove];
            [self.storedTagsArray addObject:self.searchTagsArray[indexPath.row]];
            self.searchTagsTextField.text = @"";
            if(self.storedTagsArray.count == 1){
                [self dismissAndPerformSearch];
            }
        }
        self.currentTableViewShowing = OXSearchViewControllerShowTableViewTypeTags;
        [self.searchTagsArray removeAllObjects];
        self.tableView.editing = YES;
        [self.tableView reloadData];

    }
    else if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeLocation){
        OXCoordinateModel *coordinateObject;
        if(indexPath.row == 0){
            CLLocationCoordinate2D coordinate = [[OXLocationManager sharedTracker] currentLocation];
            coordinateObject = [[OXCoordinateModel alloc] init];
            coordinateObject.coordinate = coordinate;
            coordinateObject.placeMarkTitleAndSubtitle = @"Current Location";
        }
        else{
            coordinateObject = self.locationsArray[indexPath.row-1];
        }
        
        NSLog(@"LADIDAAAA");
        [self.poiSearchObject setWithCoordinateModel:coordinateObject];
        self.searchLocationTextField.text = self.poiSearchObject.coordinate.placeMarkTitleAndSubtitle;
        NSLog(@"self.poiSearchObject params: %@", [self.poiSearchObject params]);
        //        [self dismissAndPerformSearch];
        self.editing = NO;
        [self.searchTagsTextField becomeFirstResponder];
    }
    else if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeFriends){
        OXFriendModel *genericObject = [self.friendsArray objectAtIndex:indexPath.row];
        
        genericObject.friendSelected = !genericObject.friendSelected;
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kOXAddTagsTableViewRowHeight;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    if(textField == self.searchTagsTextField){
        NSLog(@"tag update: %@", string);
        return ([string rangeOfCharacterFromSet:self.tagBlockedCharacters].location == NSNotFound);
    }
    else if(textField == self.searchLocationTextField){
        NSLog(@"location update: %@", string);
        return ([string rangeOfCharacterFromSet:self.locationBlockedCharacters].location == NSNotFound);
    }
    return YES;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}


#pragma mark - Search - Methods
-(void)performLocationSearchWithText:(NSString*)needle{
    
    NSLog(@"beginning search with text: %@", needle);
    self.locationsArray = nil;
    self.locationSearching = YES;
    [self.tableView reloadData];
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = needle;
    
    
    CLLocationCoordinate2D londonCoord = CLLocationCoordinate2DMake(LONDON_LATITUDE, LONDON_LONGITUDE);
    MKCoordinateSpan span = MKCoordinateSpanMake(1.0, 1.0);
    
    MKCoordinateRegion currentRegion = MKCoordinateRegionMake(londonCoord, span);
    request.region = currentRegion;
    
    MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
    
    [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        [self processLocalSearchResponse:response withSearchQuery:needle];
    }];
}

-(void)processLocalSearchResponse:(MKLocalSearchResponse*)response  withSearchQuery:(NSString*)searchQueryNeedle{
    
    /*
     for(MKMapItem *currentItem in response.mapItems){
     NSString *needle = currentItem.placemark.title;//[self getUIStringForMKMapItem:currentItem];
     if([needle length] > 2){
     OXCoordinateModel *coordinate = [[OXCoordinateModel alloc] init];
     [coordinate setCoordinate:currentItem.placemark.coordinate];
     [coordinate setPlaceMarkTitleAndSubtitle:needle];
     [self.locationsArray addObject:coordinate];
     }
     }
     */
    
    
    self.locationsArray = nil;
    
    NSLog(@"map items count: %lu", (unsigned long)response.mapItems.count);
    
    
    
    for(MKMapItem *currentItem in response.mapItems){
        
        NSString *needle = currentItem.name;
        OXCoordinateModel *coordinate = [[OXCoordinateModel alloc] init];
        [coordinate setCoordinate:currentItem.placemark.coordinate];
        [coordinate setPlaceMarkTitleAndSubtitle:needle];
        
        
        if ([self isPlaceInsideLondon:coordinate.coordinate]) {
            
            [self.locationsArray addObject:coordinate];
            
        }
        
    }
    
    NSMutableArray *results = [NSMutableArray array];
    
    for (OXCoordinateModel *item in self.locationsArray) {
        // note the modified weighting, this ends up working similiar to Alfred / TextMate searching method
        // TextMate takes into account camelcase while matching and is a little smarter, but you get the idea
        NSInteger score = [searchQueryNeedle compareWithWord:item.placeMarkTitleAndSubtitle matchGain:10 missingCost:1];
        
        [results addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:score], @"score", item, @"item", nil]];
    }
    
    // sort list
    results = [[results sortedArrayUsingComparator: (NSComparator)^(id obj1, id obj2) {
        
        
        return [[obj1 valueForKey:@"score"] compare:[obj2 valueForKey:@"score"]];
        
    }] mutableCopy];
    
    self.locationsArray = [results valueForKeyPath:@"item"];
    
    
    self.locationSearching = NO;
    [self.tableView reloadData];
}


- (BOOL)isPlaceInsideLondon:(CLLocationCoordinate2D) location {
    MKCoordinateRegion region = [[OCNLocationTracker tracker] londonMapRegion];
    
    CLLocationCoordinate2D center   = region.center;
    CLLocationCoordinate2D northWestCorner, southEastCorner;
    
    northWestCorner.latitude  = center.latitude  - (region.span.latitudeDelta  / 2.0);
    northWestCorner.longitude = center.longitude - (region.span.longitudeDelta / 2.0);
    southEastCorner.latitude  = center.latitude  + (region.span.latitudeDelta  / 2.0);
    southEastCorner.longitude = center.longitude + (region.span.longitudeDelta / 2.0);
    
    if (
        location.latitude  >= northWestCorner.latitude &&
        location.latitude  <= southEastCorner.latitude &&
        
        location.longitude >= northWestCorner.longitude &&
        location.longitude <= southEastCorner.longitude
        )
    {
        // User location (location) in the region - OK :-)
        
        NSLog(@"Center (%f, %f) span (%f, %f) user: (%f, %f)| IN!", region.center.latitude, region.center.longitude, region.span.latitudeDelta, region.span.longitudeDelta, location.latitude, location.longitude);
        
        
        
        return YES;
        
    }else {
        
        // User location (location) out of the region - NOT ok :-(
        
        NSLog(@"Center (%f, %f) span (%f, %f) user: (%f, %f)| OUT!", region.center.latitude, region.center.longitude, region.span.latitudeDelta, region.span.longitudeDelta, location.latitude, location.longitude);
        
        return NO;
    }
}



-(NSString *)addTagTextWithExcessCharactersRemoved{
    NSString *searchString = self.searchTagsTextField.text;
    NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
    NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
    
    NSArray *parts = [searchString componentsSeparatedByCharactersInSet:whitespaces];
    NSArray *filteredArray = [parts filteredArrayUsingPredicate:noEmptyStrings];
    searchString = [filteredArray componentsJoinedByString:@" "];
    NSLog(@".%@.", searchString);
    //    self.searchTagsTextField.text = searchString;
    return searchString;
}


-(BOOL)doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:(NSString*)needleTagString{
    for (OXTagPlaceCountModel *storedTag in self.storedTagsArray) {
        if([[storedTag.tagName lowercaseString] isEqualToString:[needleTagString lowercaseString]]){
            return YES;
        }
    }
    return NO;
}

-(void)performTagSearchWithText:(NSString *)text{
    //    NSLog(@"tag iing");
    self.tagSearching = YES;
    [self.tableView reloadData];
    if([self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
//        [self.searchTagsArray removeAllObjects];
//        //        NSLog(@"tag NOT searching");
//        self.tagSearching = NO;
//  /      [self updateTableViewForTags];
//        return;
    }
    //    NSLog(@"get request tags");
    [OXRestAPICient getTagsAutoSuggestionWithString:text
                                   withSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
                                       //                                       NSLog(@"GET AUTO TAGS SUCCESS: %@", response);
                                       [self handleAutoTagsArrayResponse:response];
                                       [self updateTableViewForTags];
                                   }
                                          failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                              NSString *responseBody = operation.responseString;
                                              NSLog(@"failure operation: %@\n\n\n\nresponse: %@ \n\n\nresponseString %@", operation, error, responseBody);
                                              [self.searchTagsArray removeAllObjects];
                                              [self updateTableViewForTags];
                                          }];
}

-(void)handleAutoTagsArrayResponse:(NSArray*)response{
    self.searchTagsArray = [OXTagPlaceCountModel createAutoSuggestionArrayFromResponse:response filteringOutStoredTags:self.storedTagsArray];
    
    if(self.storedTagsArray.count == 0){
        OXTagPlaceCountModel *createNewTag = [[OXTagPlaceCountModel alloc] initWithTagName:[NSString stringWithFormat:@"%@", self.searchTagsTextField.text] andTotalPlacesCount:0 andCreateNewTag:YES];
        [self.searchTagsArray insertObject:createNewTag atIndex:0];
    }
}
-(void)updateTableViewForTags{
    if([self.searchTagsTextField.text length] > 0){
        self.currentTableViewShowing = OXSearchViewControllerShowTableViewTypeTagsSearch;
        self.tableView.editing = NO;
        
    }
    else{
        self.currentTableViewShowing = OXSearchViewControllerShowTableViewTypeTags;
        self.tableView.editing = YES;
    }
    self.tagSearching = NO;
    
    [self.tableView reloadData];
}


-(void)dismissAndPerformSearch{
    NSLog(@"tag count: %lu", (unsigned long)self.storedTagsArray.count);
  
        [self.poiSearchObject setTags:self.storedTagsArray];
  
    
    
    [self.selectedFriendsArray removeAllObjects];
    for(OXFriendModel *friend in self.friendsArray){
        if(friend.isFriendSelected){
            [self.selectedFriendsArray addObject:[NSNumber numberWithInteger:friend.personId]];
        }
    }
//    if(self.selectedFriendsArray.count > 0){
        [self.poiSearchObject setFriendsIds:self.selectedFriendsArray];
//    }
    NSLog(@"PK self.poiSearchObject params: %@", [self.poiSearchObject params]);
    [self.delegate performSearchWithPOISearchObject:self.poiSearchObject];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
-(IBAction)searchAction{
    [self dismissAndPerformSearch];
}

-(void)grabFriendsWithRetries:(int)counter{
    __block int retriesLeft = counter;
    [OXRestAPICient getFriendsListWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"FRIENDS LIST %@", response);
        self.friendsArray = [OXFriendModel getFriendsArrayFromJSONResponse:response];
        [self.tableView reloadData];
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        retriesLeft -= 1;
        if(retriesLeft > 0){
            [self grabFriendsWithRetries:retriesLeft];
        }
        else{
            NSLog(@"error: %@", error);
        }
    }];
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.storedTagsArray.count == 0){
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}
-(BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.storedTagsArray.count == 0){
        return NO;
        
    }
    return YES;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        [self.storedTagsArray removeObjectAtIndex:indexPath.row];
        [self updateTableViewForTags];
    }
}


#pragma mark - Private - Methods


#pragma mark - DZNEmptyDataSet - Methods

- (void)emptyDataSetDidTapButton:(UIScrollView *)scrollView {
    [self dismissViewControllerAnimated:NO completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kTabBarViewControllerProfileViewShowFriendsNotification
                                                        object:self];
}
- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeFriends){

        NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0], NSForegroundColorAttributeName : [UIColor whiteColor]};
        return [[NSAttributedString alloc] initWithString:@"Go to profile" attributes:attributes];
    }
    return nil;
}
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSString *text = @"";
    if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeFriends){
        text = @"You currently have no friends";
    }
    else{
        text = @"No such tag found";
    }
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
-(UIImage *)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state{
    return [self imageFromColor:[PKUIAppearanceController darkBlueColor]];
}
- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"";
    if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeFriends){
        text = @"Ocean search works with groups too. Go to the profile page and invite your friends to find the places all of you will enjoy!";
    }
    else{
        text = [NSString stringWithFormat:@"There are no places associated with the tag \"%@\"", self.searchTagsTextField.text];
    }
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

//- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
//
//    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0]};
//
//    return [[NSAttributedString alloc] initWithString:@"Continue" attributes:attributes];
//}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    UIImage *image;
    if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeFriends){
        image = [UIImage imageNamed:@"emptyDataSetNoFriends"];

    }
    else{
        image = [UIImage imageNamed:@"noPlacesReturned"];
    }
    return image;
}
-(UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView{
    return [PKUIAppearanceController pkLightGrayColor];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    
    return [UIColor whiteColor];
}
//
//- (CGPoint)offsetForEmptyDataSet:(UIScrollView *)scrollView {
//    if(self.currentTableViewShowing == OXSearchViewControllerShowTableViewTypeFriends){
//        return CGPointMake(0, 0);
//        
//    }
//    else
//        return CGPointMake(0, 0);
//}
@end


/*            if(self.storedTagsArray.count == 0){
 if(indexPath.row == 0){
 OXSearchTagTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellTagIdentifier];
 [cell setCellForSearchWithObjectFollowingProtocol:self.searchTagsArray[indexPath.row]];
 cell.tagNameLabel.textColor = [UIColor blackColor];
 return cell;
 }
 else{
 OXTagTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"tagCellIdentifier"];
 [cell setCellWithObjectFollowingProtocol:self.searchTagsArray[indexPath.row-1]];
 return cell;
 
 
 }
 }*/