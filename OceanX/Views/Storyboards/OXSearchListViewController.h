//
//  OXSearchListViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXFilterTableViewController.h"
@interface OXSearchListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, OXFilterTableViewControllerDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *searchResults;

@property (nonatomic, strong) OXPOISearchModel *poiSearchObject;

//@property (nonatomic, retain) IBOutlet UIView *searchBarView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintForSummaryBanner;

-(void)performSerchWithObject:(id)object;
@end
