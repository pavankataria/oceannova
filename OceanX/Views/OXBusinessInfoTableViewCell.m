//
//  OXBusinessInfoTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 12/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXBusinessInfoTableViewCell.h"
@implementation OXBusinessInfoTableViewCell

- (void)awakeFromNib {
    // Initialization code
    

    [self.businessNameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.businessImageView.layer setCornerRadius:10];
    [self.businessImageView.layer setMasksToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)textFieldDidChange:(UITextField*)textField{
    [self.delegate performSelector:@selector(textUpdatedForBusinessDetailCell:) withObject:self];
}
-(NSDictionary *)getRowValueDictionary{
    return @{kBusinessDetailCellConfigurationGetValuesDictionaryText : self.businessNameTextField.text,
             kBusinessDetailCellConfigurationGetValuesDictionaryIndexPath : self.indexPath};
}
@end
