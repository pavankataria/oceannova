//
//  OXTagTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 19/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXTagTableViewCell.h"

@implementation OXTagTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - GenericTBVCCellConfiguration Protocol

-(void)updateForModelObject:(OXTagPlaceCountModel*)tagObject{
    self.tagNameLabel.text = tagObject.name;
    self.tagPlacesCountLabel.text = tagObject.totalPlacesCountString;
    self.userInteractionEnabled = YES;
}

-(void)setTableCellForUIMessageCellForDetailPOIVCForIndexPath:(NSIndexPath*)indexPath{
    self.tagNameLabel.textColor = [UIColor lightGrayColor];
    self.userInteractionEnabled = NO;
    
    NSString *stringText;
    switch (indexPath.row) {
        case 0:
            stringText = @"Tags describe places, add one!";
            break;
        default:
            stringText = @"Add as many as you like!";
            break;
    }
    self.tagNameLabel.text = stringText;
    self.tagPlacesCountLabel.text = @"";

}
-(void)setTableCellForUIMessageCellForIndexPath:(NSIndexPath*)indexPath{
    self.tagNameLabel.textColor = [UIColor lightGrayColor];
    self.userInteractionEnabled = NO;

    NSString *stringText;
    switch (indexPath.row) {
        case 0:
            stringText = @"Tags describe places, add at least 3!";
            break;
        case 1:
            stringText = @"Nice, now add 2 more.";
            break;
        case 2:
            stringText = @"1 more tag to go";
            break;
        default:
            stringText = @"Add as many as you like!";
            break;
    }
    self.tagNameLabel.text = stringText;
    self.tagPlacesCountLabel.text = @"";
}

-(void)setSearchTableCellForUIMessageCellForIndexPath:(NSIndexPath*)indexPath{
    self.tagNameLabel.textColor = [UIColor lightGrayColor];
    self.userInteractionEnabled = NO;
    
    NSString *stringText;
    switch (indexPath.row) {
        case 0:
            stringText = @"Tags describe places. Type..";
            break;
        default:
            stringText = @"";
            break;
    }
    self.tagNameLabel.text = stringText;
    self.tagPlacesCountLabel.text = @"";
}

-(void)setCellForCreateEmptyCellWithTagName:(NSString *)tagName{
    self.tagNameLabel.text = [NSString stringWithFormat:@"Create tag: %@", tagName];
    self.tagPlacesCountLabel.text = @"";
}
-(void)setCellWithObjectFollowingProtocol:(id<OXTagModelOperationProtocol>)object{
    if(object.OXTagModelOperationShouldCreateNewTag){
        [self setCellForCreateEmptyCellWithTagName:object.OXTagModelOperationTagNameString];
    }
    else{
        self.tagNameLabel.text =  [object OXTagModelOperationTagNameString];
        self.tagPlacesCountLabel.text = [object OXTagModelOperationPlaceCountString];
        self.tagNameLabel.textColor = [UIColor blackColor];
        self.userInteractionEnabled = YES;
    }
}
-(void)setCellWithObjectFollowingSearchModelProtocol:(id<OXSearchModelOperationProtocol>)object{
    self.tagNameLabel.text =  [object searchResultTitle];
    NSLog(@"tag: %@", self.tagNameLabel.text);
    self.tagPlacesCountLabel.text = @"";
}

@end
