//
//  OXBusinessDetailCellDelegate.h
//  OceanX
//
//  Created by Pavan Kataria on 25/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OXBusinessDetailCellConfiguration <NSObject>
@required
-(NSDictionary*)getRowValueDictionary;
@end

@protocol OXBusinessDetailCellDelegate <NSObject>

@required
-(void)textUpdatedForBusinessDetailCell:(UITableViewCell <OXBusinessDetailCellConfiguration>*)cell;
@end

@interface OXBusinessDetailCellDelegate : NSObject

@end
