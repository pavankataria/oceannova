//
//  OXTagAffirmTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 18/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXTagAffirmTableViewCell.h"

@implementation OXTagAffirmTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.upArrowButton addTarget:self action:@selector(upArrowButtonChangeSelectionState:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(void)upArrowButtonChangeSelectionState:(UIButton*)sender{
    BOOL newValueForButtonSelected = ![sender isSelected];
    [self upArrowButtonSetSelected:newValueForButtonSelected];
}
-(void)upArrowButtonSetSelected:(BOOL)selected{
    NSLog(@"up arrow button change selection state");
    [self.upArrowButton setSelected:selected];
//    if(selected){
//        self.upArrowButton.tintColor = [PKUIAppearanceController darkBlueColor];
//    }
//    else{
//        self.upArrowButton.tintColor = [UIColor lightGrayColor];
//        
//    }
}

@end
