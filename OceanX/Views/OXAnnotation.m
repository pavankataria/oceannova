//
//  OXAnnotation.m
//  OceanX
//
//  Created by Systango on 03/02/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//


const CGRect kOXAnnotationBadgeViewFrame = { { 0.0f, 0.0f}, { 20.0f, 20.0f } };
const CGRect kOXAnnotationImageViewFrame = { { 2.0f, 2.0f}, { 46.0f, 46.0f } };
const int kOXAnnotationBadgeViewTag = 100;
const NSInteger kSingleAnnotationCount = 0;
NSString * const kScoreKey = @"score";


#import "OXAnnotation.h"
#import "OXClusterAnnotationBadgeView.h"

@interface OXAnnotation()

@property (nonatomic, strong) MKAnnotationView *mapAnnotationView;
@property (nonatomic, strong) OXClusterAnnotationBadgeView *badgeView;

@end

@implementation OXAnnotation


#pragma mark - Init methods

- (id)initWithID:(NSInteger)poiID Location:(CLLocationCoordinate2D)coordinate badgeCount:(NSInteger)badgeCount imageURL:(NSURL *)imageURL poiScrore:(NSNumber *)poiScrore
{
    self = [super init];
    
    if(self)
    {
        _annotationID = poiID;
        _coordinate = coordinate;
        _imageURL = imageURL;
        _score = poiScrore;
        self.badgeCount = badgeCount;
    }
    
    return self;
}


#pragma mark - Public methods

- (MKAnnotationView *)annotationView
{
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:kOXAnnotationIdentifier];
    annotationView.enabled = YES;
    annotationView.canShowCallout = NO;
    
    annotationView.image = [UIImage imageNamed:@"blankImage"];
    
    [annotationView.image size];
    
    if([annotationView viewWithTag:kOXAnnotationBadgeViewTag])
    {
        [[annotationView viewWithTag:kOXAnnotationBadgeViewTag] removeFromSuperview];
    }
    
    self.mapAnnotationView = annotationView;
    
    //[self updateImage];
    
    if (self.badgeCount > kSingleAnnotationCount) {
        [self showClusterCount];
    }
    
    [self.mapAnnotationView bringSubviewToFront:annotationView];
    
    
    
    return annotationView;
}

#pragma mark - Private methods

- (void)showClusterCount
{
    self.badgeView = [[OXClusterAnnotationBadgeView alloc] initWithFrame:kOXAnnotationBadgeViewFrame];
    self.badgeView.tag = kOXAnnotationBadgeViewTag;
    [self.badgeView setCount:self.badgeCount];
    [self.mapAnnotationView addSubview:self.badgeView];
    
    
}

/*- (void)updateImage
 {
 if([self.imageURL absoluteString].length)
 {
 [self performSelectorInBackground:@selector(downloadImage) withObject:nil];
 
 }
 }
 
 //TODO: this needs to be replaced by SDWebImageCache lib or something similar
 - (void)downloadImage
 {
 UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.imageURL]];
 
 dispatch_async(dispatch_get_main_queue(), ^
 {
 UIImageView *placeImageView = [[UIImageView alloc] initWithImage:image];
 [placeImageView setFrame:kOXAnnotationImageViewFrame];
 [self.mapAnnotationView addSubview:placeImageView];
 [self.mapAnnotationView sendSubviewToBack:placeImageView];
 });
 }*/

@end
