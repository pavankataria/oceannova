//
//  OXNewsFeedSectionHeaderTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 15/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXNewsFeedSectionHeaderTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *sectionHeaderLabel;
@property (nonatomic, weak) IBOutlet UIView *partialBackgroundView;
@end
