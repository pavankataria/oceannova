//
//  OXTagBannerTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 11/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXTagBannerView.h"
@interface OXTagBannerTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet OXTagBannerView *tagBannerView;

@end
