//
//  OXLoginButtonTableViewCell.m
//  Ocean
//
//  Created by Pavan Kataria on 13/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXLoginButtonTableViewCell.h"

@implementation OXLoginButtonTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.loginButton.layer setCornerRadius:5];
    [self.loginButton setTitle:@"Sign Up" forState:UIControlStateNormal];
    [self.loginButton setBackgroundColor:[PKUIAppearanceController darkBlueColor]];
    [self setBackgroundColor:[UIColor clearColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
