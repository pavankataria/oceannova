//
//  OXCaptionPresentationViewController.m
//  Ocean
//
//  Created by Pavan Kataria on 19/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXCaptionPresentationViewController.h"

@interface OXCaptionPresentationViewController ()
@property (nonatomic, retain) UITextView *addressLabel;
@property (nonatomic, assign) CGFloat labelPadding;

@end

@implementation OXCaptionPresentationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.labelPadding = 10;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setPoiObject:(OXPOIModel *)poiObject{
    _poiObject = poiObject;
    
    [self.view layoutIfNeeded];
    CGRect viewFrame = self.view.frame;
    self.view.frame = viewFrame;
    
    
//    [self.delegate updateDetailPOIScrollViewContentSize];
    [self.view setBackgroundColor:[UIColor whiteColor]];
//    [self.view setBackgroundColor:[UIColor colorWithRed:0.5 green:0.5 blue:1 alpha:1.0]];

    
    [self setupAddressLabel];
}
-(void)setupAddressLabel{
    if(!self.addressLabel){
        self.addressLabel = [[UITextView alloc] initWithFrame:CGRectMake(self.labelPadding, 5, self.view.frame.size.width - 2 * self.labelPadding, self.view.frame.size.height)];
    }
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    self.addressLabel.backgroundColor = [UIColor clearColor];
    self.addressLabel.textColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];
    self.addressLabel.textAlignment = NSTextAlignmentCenter;
//    self.addressLabel.adjustsFontSizeToFitWidth = YES;
    self.addressLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    [self.addressLabel setEditable:NO];
    [self.addressLabel setScrollEnabled:NO];
//    self.addressLabel.numberOfLines = 0;
    
    if(![self.poiObject.caption isKindOfClass:[NSNull class]]){
        [self.addressLabel setText:/*@"In Soho? Hit up Ronnie Scott’s. This European vanguard for jazz and blues has seen the world’s best play on it’s stage. Better still, it is only £6 entry for students after 1am. This is somewhere every Londoner needs to go. Also checkout this awesome page if you want to hire a profressional wedding photographer: http://www.stephencaseyphotography.co.uk"];//*/self.poiObject.caption];
        self.addressLabel.dataDetectorTypes = UIDataDetectorTypeAll;
        [self.addressLabel sizeToFit];
        CGRect viewFrame = self.view.frame;
        viewFrame.size.height = self.addressLabel.frame.size.height+10;
        self.view.frame = viewFrame;
    }
    
    

    NSLog(@"addressLabel.frame %@", NSStringFromCGRect(self.addressLabel.frame));
    [self.addressLabel setTextAlignment:NSTextAlignmentLeft];
    [self.view addSubview:self.addressLabel];
    [self.view layoutIfNeeded];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
