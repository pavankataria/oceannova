//
//  CardView.h
//  ZLSwipeableViewDemo
//
//  Created by Zhixuan Lai on 11/1/14.
//  Copyright (c) 2014 Zhixuan Lai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardView : UIView
@property (nonatomic, retain) OXPOISearchByListModel *poi;
@property (nonatomic, retain) NSString *poisSourceStringEnum;

-(instancetype)initWithFrame:(CGRect)frame withPOI:(OXPOISearchByListModel*)poi;
-(void)setupValues;

@end
