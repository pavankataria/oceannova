//
//  OXFriendTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 18/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXFriendButton.h"
#import "OXFriendModel.h"


typedef NS_ENUM(NSInteger, FriendStatus){
    StatusAddFriend,
    StatusUnFriend,
    StatusPending
};

@class OCNFriendTableViewCell;

@protocol FriendCellDelegate <NSObject>

@required
-(void) userTappedFollowForFriendCell: (OCNFriendTableViewCell*)  cell atIndexPath: (NSIndexPath*) path;

@end


@interface OXFriendTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel * usernameLabel;
@property (nonatomic,strong) IBOutlet OXFriendButton * addFriendButton;
@property (nonatomic,strong) IBOutlet OXFriendButton * followUnfollow;
@property (nonatomic,strong) IBOutlet OXFriendButton * friendRequestPendingButton;
@property (nonatomic,weak) id <FriendCellDelegate> delegate;
@property (nonatomic,strong) NSIndexPath * indexPath;

-(void) updateWithFriend:(OXFriendModel *)friend withDelegate: (id <FriendCellDelegate>) delegate atIndexPath:(NSIndexPath*) path friendStatus:(FriendStatus) friendStatus;

-(void) updateWithFriend:(OXFriendModel *)friend withDelegate: (id <FriendCellDelegate>) delegate atIndexPath:(NSIndexPath*) path showButton:(BOOL) show;

-(void) updateWithPlace:(OXFriendModel *) place withDelegate: (id <FriendCellDelegate>) delegate atIndexPath:(NSIndexPath*) path;

-(void)updateForModelObject:(OXFriendModel*)friendObject;
@end