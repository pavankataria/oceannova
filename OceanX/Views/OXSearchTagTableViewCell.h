//
//  OXSearchTagTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 08/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXSearchTagTableViewCell : UITableViewCell<OXTagModelOperationProtocol>
@property (nonatomic, weak) IBOutlet UILabel *tagNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *tagPlacesCountLabel;

@property (nonatomic, weak) IBOutlet UIButton *tagDeleteButton;
-(void)setCellWithObjectFollowingProtocol:(id<OXTagModelOperationProtocol>)object;
-(void)setSearchTableCellForUIMessageCellForIndexPath:(NSIndexPath*)indexPath;
-(void)setCellForSearchWithObjectFollowingProtocol:(id<OXTagModelOperationProtocol>)object;
@end
