//
//  OXFilterTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXFilterTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *headerLabel;

-(void)setCellWithSearchPOIObject:(OXPOISearchModel*)object andWithSection:(NSUInteger)section;

@end
