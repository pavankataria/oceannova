//
//  OXPrivacyAndTermsTableViewHeaderFooterView.m
//  Ocean
//
//  Created by Pavan Kataria on 17/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXPrivacyAndTermsTableViewHeaderFooterView.h"

@implementation OXPrivacyAndTermsTableViewHeaderFooterView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setupTextViewWithHyperLinks{
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"By tapping to continue, you are indicating that you have read the Privacy Policy and agree to the Terms and Conditions."];
    
    //Overall text
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    UIFont *linkFont = [UIFont fontWithName:@"HelveticaNeue" size:12];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:font
                             range:NSMakeRange(0,[attributedString length])];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor grayColor]
                             range:NSMakeRange(0, [attributedString length])];
    
    
    
    //Center text
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attributedString length])];
    
    //Create links
    [attributedString addAttribute:NSLinkAttributeName
                             value:@"privacypolicy://"
                             range:[[attributedString string] rangeOfString:@"Privacy Policy"]];
    
    [attributedString addAttribute:NSLinkAttributeName
                             value:@"termsAndConditions://"
                             range:[[attributedString string] rangeOfString:@"Terms and Conditions."]];
    
    //Set font for links
    [attributedString addAttribute:NSFontAttributeName
                             value:linkFont
                             range:[[attributedString string] rangeOfString:@"Privacy Policy"]];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:linkFont
                             range:[[attributedString string] rangeOfString:@"Terms and Conditions."]];
    
    
    //Set link colours
    NSDictionary *linkAttributes = @{NSForegroundColorAttributeName: [UIColor darkGrayColor],
                                     NSUnderlineColorAttributeName: [UIColor lightGrayColor],
                                     NSUnderlineStyleAttributeName: @(NSUnderlinePatternSolid)};
    
    // assume that textView is a UITextView previously created (either by code or Interface Builder)
    self.privacyConditionsTextView.linkTextAttributes = linkAttributes; // customizes the appearance of links
    self.privacyConditionsTextView.attributedText = attributedString;
    self.privacyConditionsTextView.delegate = self;
}
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    if ([[URL scheme] isEqualToString:@"privacypolicy"]) {
        [self.delegate privacyScreenLinkPressed];
        return NO;
    }
    else if ([[URL scheme] isEqualToString:@"termsAndConditions"]) {
        [self.delegate termsAndConditionsLinkPressed];
        return NO;
    }
    return YES; // let the system open this URL
}

@end
