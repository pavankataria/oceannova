//
//  OXAnnotation.h
//  OceanX
//
//  Created by Systango on 03/02/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

const NSInteger kSingleAnnotationCount;

NSString * const kScoreKey;

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface OXAnnotation : NSObject<MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) NSInteger annotationID;
@property (nonatomic, strong) NSURL *imageURL;
@property (assign, nonatomic) NSInteger badgeCount;
@property (strong, nonatomic) NSNumber *score;

@property (strong, nonatomic) NSArray *clusterAnnotations;

- (id)initWithID:(NSInteger)poiID Location:(CLLocationCoordinate2D)coordinate badgeCount:(NSInteger)badgeCount imageURL:(NSURL *)imageURL poiScrore:(NSNumber *)poiScrore;

- (MKAnnotationView *)annotationView;

@end
