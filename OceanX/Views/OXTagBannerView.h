//
//  OXTagBannerView.h
//  OceanX
//
//  Created by Pavan Kataria on 11/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXTagBannerView : UIView
@property (nonatomic, retain) NSArray <OXTagsProtocolConfiguration>*tagsArray;

@end
