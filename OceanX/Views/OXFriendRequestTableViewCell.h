//
//  OCNFriendRequestTableViewCell.h
//  Ocean
//
//  Created by Sam Payne on 09/07/2014.
//  Copyright (c) 2014 Ocean. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXNotificationModel.h"

@class OXFriendRequestTableViewCell;

@protocol FriendRequestCellDelegate <NSObject>

@required
-(void) userDidTapAccept:(OXNotificationModel *) notification;
-(void) userDidTapReject:(OXNotificationModel *) notification;

@end

@interface OXFriendRequestTableViewCell : UITableViewCell

@property (nonatomic, strong) OXNotificationModel *notification;
@property (nonatomic,strong) NSIndexPath * indexPath;
@property (nonatomic,weak)   id <FriendRequestCellDelegate> delegate;
@property (nonatomic,strong) IBOutlet UILabel  * usernameLabel;
@property (nonatomic,strong) IBOutlet UIButton * accept;
@property (nonatomic,strong) IBOutlet UIButton * reject;

-(void) updateWithNotification:(OXNotificationModel *) notification forIndexPath:(NSIndexPath*) path withDelegate:(id<FriendRequestCellDelegate>) delegate;

@end
