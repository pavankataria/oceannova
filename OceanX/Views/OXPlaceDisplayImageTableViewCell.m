//
//  OXPlaceDisplayImageTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 03/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXPlaceDisplayImageTableViewCell.h"
#import "OXProcessedPicture.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"

@implementation OXPlaceDisplayImageTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.picContainerView.layer setCornerRadius:15];
    [self.picContainerView setClipsToBounds:YES];
    
    [self.picContainerView.layer setBorderWidth:2];
    [self.picContainerView.layer setBorderColor:[UIColor colorWithRed:0.89 green:0.89 blue:0.89 alpha:1].CGColor];
}
-(void)layoutSubviews{
    [super layoutSubviews];
//    NSLog(@"frame of image cell: %@", NSStringFromCGRect(self.frame));
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupCellWithPOIModelObject:(OXPOIModel*)object{
    self.placeImageView.image = nil;
    OXProcessedPicture *processedPicture = [object.pictures firstObject];
    if(processedPicture.processedPictures.count > 0){
        //            [NSException raise:@"processed pictures is > 0" format:@"Please write some code here to take the appropriate image as required from the processPictures array"];
    }

    NSURL *url = [NSURL URLWithString:processedPicture.pictureURL];
    [self.placeImageView sd_setImageWithURL:url];
}

-(void)setupCellWithObject:(OXPOISearchByListModel *)object{
    self.placeImageView.image = nil;
    
    self.placePOIScoreImageView.image = [object getPOIScoreImage];

    OXProcessedPicture *processedPicture = [object.pictures firstObject];
//    if(processedPicture.processedPictures.count > 0){
        //            [NSException raise:@"processed pictures is > 0" format:@"Please write some code here to take the appropriate image as required from the processPictures array"];
//    }
    
    NSURL *url = [NSURL URLWithString:processedPicture.pictureURL];
    [self.placeImageView sd_setImageWithURL:url];
    [self.wouldYouLikeView setPoi:object];
    
}
@end
