//
//  OXBusinessInfoPresentationTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface OXBusinessInfoPresentationTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *headerlabel;
@property (nonatomic, weak) IBOutlet TTTAttributedLabel *infoLabel;

@end
