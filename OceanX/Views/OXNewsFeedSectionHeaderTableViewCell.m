//
//  OXNewsFeedSectionHeaderTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 15/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXNewsFeedSectionHeaderTableViewCell.h"

@implementation OXNewsFeedSectionHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.sectionHeaderLabel.textColor = [UIColor whiteColor];
    [self.partialBackgroundView setBackgroundColor:[PKUIAppearanceController mainHighlightDarkBlueColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
