//
//  OXCaptionTableViewCell.m
//  Ocean
//
//  Created by Pavan Kataria on 02/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXCaptionTableViewCell.h"

@implementation OXCaptionTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
