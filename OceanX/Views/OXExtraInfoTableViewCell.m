//
//  OXExtraInfoTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 12/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXExtraInfoTableViewCell.h"

@interface OXExtraInfoTableViewCell(){
    NSCharacterSet *blockedCharacters;
    NSCharacterSet *blockedCharactersForPhoneNumberSet;
}

@property (nonatomic, retain) NSIndexPath *indexPath;


@end
@implementation OXExtraInfoTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    NSMutableCharacterSet *allowedCharacters = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
    [allowedCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@"_"]];
    blockedCharacters = [allowedCharacters invertedSet];
    
    
    NSMutableCharacterSet *allowedPhoneCharacters =  [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
    [allowedPhoneCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@"+*#"]];
    blockedCharactersForPhoneNumberSet = [allowedPhoneCharacters invertedSet];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(instancetype)getPreparedCellWithIndexPath:(NSIndexPath*)indexPath{
    if(indexPath.section == kBusinessInfoTableSectionDetailsInfo){
        switch (indexPath.row) {
            case kExtraInfoTableViewCellIndexPathRowAddress:
                self.headerlabel.text = kExtraInfoTableViewCellIndexPathRowLabelNameAddress;
                self.infoTextField.placeholder = @"Know their address?";
                self.infoTextField.keyboardType = UIKeyboardTypeAlphabet;
                break;
            case kExtraInfoTableViewCellIndexPathRowPhone:
                self.headerlabel.text = kExtraInfoTableViewCellIndexPathRowLabelNamePhone;
                self.infoTextField.placeholder = @"# number";
                self.infoTextField.keyboardType = UIKeyboardTypePhonePad;
                break;
            case kExtraInfoTableViewCellIndexPathRowWebsite:
                self.headerlabel.text = kExtraInfoTableViewCellIndexPathRowLabelNameWebsite;
                self.infoTextField.placeholder = @"Web address";
                self.infoTextField.keyboardType = UIKeyboardTypeURL;

                break;
            case kExtraInfoTableViewCellIndexPathRowTwitter:
                self.headerlabel.text = kExtraInfoTableViewCellIndexPathRowLabelNameTwitter;
                self.infoTextField.placeholder = @"Social Profile";
                self.infoTextField.keyboardType = UIKeyboardTypeAlphabet;

                // Add a "textFieldDidChange" notification method to the text field control.
                break;
            default:
                break;
        }
    }
    self.infoTextField.delegate = self;
    self.indexPath = indexPath;
    
    //    if(self.indexPath.row != kExtraInfoTableViewCellIndexPathRowAddress){
    NSLog(@"indexpath: %@ setting up textfielddidchange", indexPath);
    [self.infoTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    return self;
}


-(void)updateCellForPretifyOpeningHour:(OXPrettifiedBusinessOpeningTimeModel*)pretifyBusinessOHObject{
    self.headerlabel.text = [pretifyBusinessOHObject daysRepresentation];
    self.headerlabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
    self.headerlabel.textAlignment = NSTextAlignmentRight;
    self.infoTextField.text = [pretifyBusinessOHObject hoursRepresentation];
    [self.infoTextField setEnabled:NO];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)characters{
    if([self isPhoneField]){
        return ([characters rangeOfCharacterFromSet:blockedCharactersForPhoneNumberSet].location == NSNotFound);
    }
    else if([self isTwitterTextField]){
        static NSUInteger twitterAtSymbolCharacterOffset = 1;
        if ((textField.text.length >= kTwitterUsernameCharacterCountLimit + twitterAtSymbolCharacterOffset) && range.length == 0) {
            return NO;
        }
        return ([characters rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);
    }
    
    return YES;
}
-(void)textFieldDidChange:(UITextField*)textField{
    if([self isTwitterTextField]){
        if( [textField.text length] != 0 && ! [[textField.text substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"@"]){
            textField.text = [NSString stringWithFormat:@"@%@", textField.text];
        }
    }
    
    NSLog(@"textFieldDidChange: %@", textField);
    [self.delegate performSelector:@selector(textUpdatedForBusinessDetailCell:) withObject:self];
}
-(BOOL)isAddressField{
    return self.indexPath.row == kExtraInfoTableViewCellIndexPathRowAddress;
}
-(BOOL)isTwitterTextField{
    return self.indexPath.row == kExtraInfoTableViewCellIndexPathRowTwitter;
}
-(BOOL)isWebsiteField{
    return self.indexPath.row == kExtraInfoTableViewCellIndexPathRowWebsite;
}

-(BOOL)isPhoneField{
    return self.indexPath.row == kExtraInfoTableViewCellIndexPathRowPhone;
}


-(NSDictionary *)getRowValueDictionary{
    return @{kBusinessDetailCellConfigurationGetValuesDictionaryText : self.infoTextField.text,
             kBusinessDetailCellConfigurationGetValuesDictionaryIndexPath : self.indexPath};
}

@end