//
//  PKUIAppearanceController.h
//  OceanX
//
//  Created by Pavan Kataria on 17/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#define rgb(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]

@interface PKUIAppearanceController : NSObject
+(void)appStyle;
+(UIColor*)mainColor;
+(UIColor*)darkBlueColor;
+(UIColor*)lightBlueColor;
+(UIColor*)grayColor;
+(UIColor*)tealColor;
+(UIColor*)pkLightGrayColor;

+(UIColor*)mainHighlightDarkBlueColor;

+(UIColor*)facebookLoginButtonColour;
@end
