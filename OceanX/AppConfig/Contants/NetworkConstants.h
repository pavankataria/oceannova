//
//  NetworkConstants.h
//  OceanX
//
//  Created by Pavan Kataria on 22/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkConstants : NSObject

extern NSString * const kHTTPClientURLStringBase;
extern NSString * const kHTTPClientURLStringBaseDemo;
extern NSString * const kHTTPClientURLStringRESTURL;
extern NSString * const kHTTPClientURLStringAPIVersion;
extern NSString * const kHTTPClientURLStringTagAutoSuggestion;
extern NSString * const kHTTPClientURLStringGetTags;
extern NSString * const kHTTPClientURLStringAddPoi;
extern NSString * const kHTTPClientURLStringGetFullPOI;


extern NSString * const kHTTPClientURLStringSecurityCheckLogin;
extern NSString * const kHTTPClientURLStringSearchByMap;
extern NSString * const kHTTPClientURLStringSearchByList;
extern NSString * const kHTTPClientURLStringGetQuickInfo;
extern NSString * const kHTTPClientURLStringReportPOI;
extern NSString * const kHTTPClientURLStringReportPicture;
extern NSString * const kHTTPClientURLStringGetUserProfile;
extern NSString * const kHTTPClientURLStringActivateUserProfile;
extern NSString * const kHTTPClientURLStringGetFacetsQuestions;
extern NSString * const kHTTPClientURLStringGetMetaData;
extern NSString * const kHTTPClientURLStringRequestPasswordReset;
extern NSString * const kHTTPClientURLStringSignUpUser;
extern NSString * const kHTTPClientURLStringAnswerQuestions;

extern NSString * const kHTTPClientURLStringGetNewsFeed;
extern NSString * const kHTTPClientURLStringLike;
extern NSString * const kHTTPClientURLStringDislike;
extern NSString * const kHTTPClientURLStringNeutral;


extern NSString * const kHTTPClientURLStringAddTagsToPOI;
extern NSString * const kHTTPClientURLStringAddPicturesToPOI;
extern NSString * const kHTTPClientURLStringPUTAffirmTag;
extern NSString * const kHTTPClientURLStringPUTDisaffirmTag;
extern NSString * const kHTTPClientURLStringPUTUpdateUserLocation;

extern NSString * const kHTTPClientURLStringPOSTFacebookNeedInfo;

extern NSString * const kHTTPClientURLStringPOSTAndGetFacebookRecommendations;

extern NSString * const kHTTPClientURLStringGETFacebookCheck;
extern NSString * const kHTTPClientURLStringPUTUpdateDeviceToken;
@end
