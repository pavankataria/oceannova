//
//  NotificationConstants.h
//  OceanX
//
//  Created by Pavan Kataria on 26/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kTabBarViewControllerAddPOIButtonSetToUnselectStateNotification;
extern NSString * const kTabBarViewControllerProfileViewShowFriendsNotification;


#pragma mark - PUSH NOTIFICATION - Constants

extern NSString * const kNotificationPushNameFBRecommendation;
extern NSString * const kNotificationPushNameNotifyAllInactiveWhoAreInTheAppNotLoggedIn;
extern NSString * const kNotificationPushNameNotifyAll;




extern NSString * const kTutorialViewDidFinishNowDoesFBRecommendationsNeedToBeDisplayedNotification;
//
//
//[self.navigationController dismissViewControllerAnimated:YES completion:^{
//    [[NSNotificationCenter defaultCenter] postNotificationName:kTutorialViewDidFinishNowDoesFBRecommendationsNeedToBeDisplayedNotification object:self];
//    
//}];
//
//[OXLoginManager setReplayTutorialViewed];
//[[NSUserDefaults standardUserDefaults] synchronize];



