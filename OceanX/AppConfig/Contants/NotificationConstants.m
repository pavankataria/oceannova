//
//  NotificationConstants.m
//  OceanX
//
//  Created by Pavan Kataria on 26/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "NotificationConstants.h"

NSString * const kTabBarViewControllerAddPOIButtonSetToUnselectStateNotification = @"TabBarViewControllerAddPOIButtonSetToUnselectStateNotification";

NSString * const kTabBarViewControllerProfileViewShowFriendsNotification = @"TabBarViewControllerProfileViewShowFriendsNotification";

#pragma mark - PUSH NOTIFICATION - Constants

NSString * const kNotificationPushNameFBRecommendation = @"FB_RECOMMENDATION";
NSString * const kNotificationPushNameNotifyAllInactiveWhoAreInTheAppNotLoggedIn = @"NotifyAllInactiveWhoAreInTheAppNotLoggedIn";
NSString * const kNotificationPushNameNotifyAll = @"NotifyAll";


NSString * const kTutorialViewDidFinishNowDoesFBRecommendationsNeedToBeDisplayedNotification = @"kTutorialViewDidFinishNowDoesFBRecommendationsNeedToBeDisplayedNotification";

