//
//  Constants.h
//  OceanX
//
//  Created by Pavan Kataria on 17/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
//## Integer constant shortcuts


typedef NS_ENUM(NSUInteger, CoreDataManagerState){
    CoreDataManagerStateUndefined,
    CoreDataManagerStateInitializing,
    CoreDataManagerStateMigrating,
    CoreDataManagerStateOperational,
};
#define __eIC extern __iC
#define __iC NSInteger const
#define FACET_NULL_VALUE 0
#define FACET_SCORE_RANGE 20
#define FACET_SCORE_POSITIVE_ADJUSTMENT 10
#define FACET_COUNT 30
#define FACET_GROUPS 5
#define DESCRIPTION_LABEL_ORIGIN 55


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
//Usage: http://pastie.org/9687735


@interface Constants : NSObject
extern NSString *const FBSessionStateChangedNotification;

typedef NS_ENUM(NSUInteger, OXSignUpVCTableViewType) {
    OXSignUpVCTableViewTypeFacebook = 0,
    OXSignUpVCTableViewTypeEmail = 1,
    OXSignUpVCTableViewTypeLogin = 2,
};
typedef NS_ENUM(NSUInteger, kSignupPageTableViewControllerFieldSectionType){
    kSignupPageTableViewControllerFieldSectionTypeEmail = 0,
    kSignupPageTableViewControllerFieldSectionTypeUsername = 1,
    kSignupPageTableViewControllerFieldSectionTypePassword = 2,
    kSignupPageTableViewControllerFieldSectionTypeYearOfBirth = 3,
    kSignupPageTableViewControllerFieldSectionTypeGender = 4,
};

typedef NS_ENUM(NSUInteger, kAddPlaceComingFromVCType){
    kAddPlaceComingFromVCTypeNormal = 0,
    kAddPlaceComingFromVCTypeDetailPOIPage
};

typedef NS_ENUM(NSUInteger, OXTagsPresentationTableViewComingFromVCType){
    OXTagsPresentationTableViewComingFromVCTypeOriginal = 0,
    OXTagsPresentationTableViewComingFromVCTypeTutorial,
    OXTagsPresentationTableViewComingFromVCTypeDetailPOIVC,
    OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags,
};


typedef NS_ENUM(NSUInteger, OXMultipleSearchViewControllerSenderType){
    OXMultipleSearhViewControllerSenderTypeNormal = 0,
    OXMultipleSearhViewControllerSenderTypeFirstButton,
    OXMultipleSearhViewControllerSenderTypeSecondButton,
};
typedef NS_ENUM(NSUInteger, OXInstagramStyleCellType){
    OXInstagramStyleCellTypePicture = 0,
    OXInstagramStyleCellTypeTagBanner,
    OXInstagramStyleCellTypeCaption,
    OXInstagramStyleCellTypeLikeAndDislikeBar,
};

typedef NS_ENUM(NSUInteger, OXFilterTableSectionType) {
    OXFilterTableSectionTypeLocation = 0,
    OXFilterTableSectionTypeTags,
    OXFilterTableSectionTypeFriends,
    OXFilterTableSectionTypeText
};
typedef NS_ENUM(NSUInteger, kExtraInfoTableViewCellIndexPathRows) {
    kExtraInfoTableViewCellIndexPathRowAddress = 0,
    kExtraInfoTableViewCellIndexPathRowPhone,
    kExtraInfoTableViewCellIndexPathRowWebsite,
    kExtraInfoTableViewCellIndexPathRowTwitter,
};

typedef NS_ENUM(NSUInteger, kBusinessInfoTableHeaderNames) {
    kBusinessInfoTableSectionBusinessInfo = 0,
    kBusinessInfoTableSectionDetailsInfo,
    kBusinessInfoTableSectionHoursInfo,
};

typedef NS_ENUM(NSUInteger, kDetailPOIVCBusinessInfoTableHeaderNames) {
    kDetailPOIVCBusinessInfoTableSectionDetailsInfo = 0,
    kDetailPOIVCBusinessInfoTableSectionHoursInfo,
};


typedef NS_ENUM(NSUInteger, kAmendHoursTableSection) {
    kAmendHoursTableSectionSaveChanges = 0,
    kAmendHoursTableSectionOpeningHours,
    kAmendHoursTableSectionTotalCount,

};

typedef NS_ENUM(NSUInteger, kAmendHoursPickerView) {
    kAmendHoursPickerViewOpeningHour = 0,
    kAmendHoursPickerViewOpeningMinute,
    kAmendHoursPickerViewOpeningMeridiem,
    kAmendHoursPickerViewOpeningtoClosingSeparator,
    kAmendHoursPickerViewClosingHour,
    kAmendHoursPickerViewClosingMinute,
    kAmendHoursPickerViewClosingMeridiem,
    kAmendHoursPickerViewTotalCount,
};
typedef NS_ENUM(NSUInteger, kAmendHoursPickerViewWithoutMeridiem) {
    kAmendHoursPickerViewWithoutMeridiemOpeningHour = 0,
    kAmendHoursPickerViewWithoutMeridiemOpeningMinute,
    kAmendHoursPickerViewWithoutMeridiemOpeningtoClosingSeparator,
    kAmendHoursPickerViewWithoutMeridiemClosingHour,
    kAmendHoursPickerViewWithoutMeridiemClosingMinute,
    kAmendHoursPickerViewWithoutMeridiemTotalCount,
};
typedef NS_ENUM(NSUInteger, kProfileInfoTableHeaderNames) {
    kProfileInfoTableSectionProfileInfo = 0,
    kProfileInfoTableSectionTagsInfo,
    kProfileInfoTableSectionFacetInfo,
};
typedef NS_ENUM(NSUInteger, kQuestionAnswered) {
    kQuestionAnswerStronglyDisagree = 0,
    kQuestionAnswerDisagree,
    kQuestionAnswerNeither,
    kQuestionAnswerAgree,
    kQuestionAnswerStronglyAgree,
};
typedef NS_ENUM(NSUInteger, kSettingsTableSection) {
    kSettingsTableSectionSocialNetworking = 0,
    kSettingsTableSectionSupport,
    kSettingsTableSectionLogout

};
typedef NS_ENUM(NSUInteger, kSettingsSocialNetworkingTableViewCell) {
    kSettingsSocialNetworkingTableViewCellFacebook = 0,
    kSettingsSocialNetworkingTableViewCellTwitter
};

typedef NS_ENUM(NSUInteger, ReportSource){
    ReportTypePOI = 0,
    ReportTypePicture,
};
extern NSUInteger const kInitialYearOfBornToShow;
extern NSUInteger const kMinimumAgeRequirementToSignUp;


extern NSUInteger const kTwitterUsernameCharacterCountLimit;
extern NSString * const kBusinessDetailCellConfigurationGetValuesDictionaryText;
extern NSString * const kBusinessDetailCellConfigurationGetValuesDictionaryIndexPath;


extern NSString * const kAmendHoursPickerViewSeparator;
extern NSString * const kPrettifiedBusinessOpeningTimeSeparatorDays;
extern NSString * const kPrettifiedBusinessOpeningTimeSeparatorTime;

extern NSString * const kProfileViewControllerNavigationTitle;
extern NSString * const kSettingsViewControllerNavigationTitle;

extern CGFloat const kOXGenericPresentationTableViewRowHeight;
extern CGFloat const kOXDaysSelectionTableViewRowHeight;
extern CGFloat const kOXAddTagsTableViewRowHeight;
extern CGFloat const kOXTableViewControllerLoadingCellSize;
extern CGFloat const kOXPOIListTableViewLikeDislikeBarHeight;


extern CGFloat const kCreateNewUserGenderTableViewRowHeight;
extern CGFloat const kCreateNewUserYearOfBornTableViewRowHeight;
extern CGFloat const kPickerViewControllerContainerViewTopPostionToShow;
extern CGFloat const kPickerViewControllerContainerViewTopPostionToHide;

extern NSString * const kOXAddTagsTextFieldPlaceHolderText;
extern NSString * const kOXSearchTextFieldPlaceHolderText;
extern NSString * const kOXSearchTextFieldAddressPlaceHolderText;

extern NSUInteger const kOXAddTagsTextFieldMaxCharacterCount;

extern NSUInteger const kOXAddTagsMinimumTagsRequirement;
extern NSUInteger const kOXQuestionTotalQuestionPerFacet;

extern NSString * const kAddTagViewControllerTagPlaceCountModelTagName;
extern NSString * const kAddTagViewControllerTagPlaceCountModelTagPlaceCount;

extern NSString * const kSegueIdentifierFriendsViewController;
extern NSString * const kSegueIdentifierTagsViewController;
extern NSString * const kSegueIdentifierShowCameraScreenViewController;
extern NSString * const kSegueIdentifierCameraThumbnailPreview;
extern NSString * const kSegueIdentifierEditImagesViewController;
extern NSString * const kSegueIdentifierBusinessInfoViewController;
extern NSString * const kSegueIdentifierDaysSelectionViewController;
extern NSString * const kSegueIdentifierAmendHoursViewController;
extern NSString * const kSegueIdentifierAddTagsViewController;
extern NSString * const kSegueIdentifierDetailSearchByMapViewController;
extern NSString * const kSegueIdentifierCreateProfileTableViewController;
extern NSString * const kSegueIdentifierPickerViewController;
extern NSString * const kSegueIdentifierQuestionnaireViewController;
extern NSString * const kSegueIdentifierQuestionSaveViewController;
extern NSString * const kSegueIdentifierQuestionViewController;

extern NSString * const kSegueIdentifierReportTextViewController;

extern NSString * const kSegueIdentifierAboutWebviewViewController;
extern NSString * const kSegueIdentifierPrivacyWebviewViewController;
extern NSString * const kSegueIdentifierTermsAndConditionWebviewViewController;

extern NSString * const kSegueIdentifierShowSignupPage;


extern NSString * const kTableViewCellFriendRequestIdentifier;
extern NSString * const kTableViewCellFriendIdentifier;
extern NSString * const kTableViewCellTagIdentifier;
extern NSString * const kTableViewCellGenderIdentifier;
extern NSString * const kTableViewCellYearOfBornIdentifier;

extern NSString * const kOXAnnotationIdentifier;

extern NSString * const kTableViewCellIdentifierAddPlaceBusinessInfo;
extern NSString * const kTableViewCellIdentifierAddPlaceExtraInfo;
extern NSString * const kTableViewCellIdentifierAddPlaceHoursInfo;

extern NSString * const kTableViewCellIdentifierProfileInfoCell;
extern NSString * const kTableViewCellFacetBarTableCellIdentifier;
extern NSString * const kSectionHeaderViewTableViewCellSegueIdentifier;


extern NSString * const kTableViewCellIdentifierDaysSelection;
extern NSString * const kTableViewCellIdentifierSearchResultsLoadingCell;

extern NSUInteger const kAddPlaceCameraCaptureMaxPhotosAllowed;
extern NSString * const kAddPlaceCameraCaptureNavigationButtonTitle;


extern NSString *const kExtraInfoTableViewCellIndexPathRowLabelNameAddress;
extern NSString *const kExtraInfoTableViewCellIndexPathRowLabelNamePhone;
extern NSString *const kExtraInfoTableViewCellIndexPathRowLabelNameWebsite;
extern NSString *const kExtraInfoTableViewCellIndexPathRowLabelNameTwitter;
extern NSString *const kExtraInfoTableViewCellIndexPathRowLabelNameFacebook;


extern NSUInteger const kExtraInfoTableViewCellIndexPathRowLabelTagAddress;
extern NSUInteger const kExtraInfoTableViewCellIndexPathRowLabelTagPhone;
extern NSUInteger const kExtraInfoTableViewCellIndexPathRowLabelTagWebsite;
extern NSUInteger const kExtraInfoTableViewCellIndexPathRowLabelTagTwitter;

extern NSUInteger const kBusinessInfoTableTotalRowsBusinessInfo;
extern NSUInteger const kBusinessInfoTableTotalRowsExtraInfo;

extern NSString *const kBusinessInfoTableTitleHeaderBusinessInfo;
extern NSString *const kBusinessInfoTableTitleHeaderDetailsInfo;
extern NSString *const kBusinessInfoTableTitleHeaderHoursInfo;

extern NSUInteger const kDaysSelectionDaysOfTheWeek;
extern NSUInteger const kDaysSelectionWeeksToShow;


extern NSString * const kMetadataKey;
extern NSString * const kSavedUserNameKey;
extern NSString * const kSavedPasswordKey;
extern NSString * const kCookieName;
extern NSString * const kCookieKey;
extern NSString * const kLoggedInKey;
extern NSString * const kNSUserDefaultsReplayTutorialDidViewKey;
extern NSString * const kNSUserDefaultsFBRecommendationShouldDisplayView;

extern NSString * const kNSUserDefaultsFBCheckLikePermissionActive;


extern NSString * const kSessionCookiesKey;
extern NSString * const kUserDeviceTokenStringKey;

extern NSString * const kRequestNodeLocationUserPosition;
extern NSString * const kRequestNodeUsersFriends;
extern NSString * const kJSONNullString;
extern NSString * const kRequestNodeNotificationFriends;
extern NSString * const kRequestNodeNotificationSentFriends;
extern NSString * const kRequestNodeUsersSearchName;


extern CGFloat const kOXSearchTableViewControllerTableViewSectionHeaderHeight;
extern CGFloat const kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha;
extern NSString * const kOXSearchTableViewControllerTableViewCellIdentifierImageCell;
extern NSString * const kTableViewCellIdentifierSearchResultsImageDisplayCell;

extern NSString * const kOXSearchFilterScreenSegueIdentifier;
extern NSString * const kOXSearchResultsScreenSegueIdentifier;
extern NSString * const kOXSearchScreenEmbeddedViewControllerSegue;
extern NSString * const kOXSummaryBannerViewControllerSegueIdentifier;

extern NSString * const kOXTableViewCellReuseLikeDislikeBarCellIdentifier;


//Maps
extern const CGPoint kMapPinPointOffsetPointForSelected;
extern const CGPoint kMapPinPointOffsetPointForUnselected;
extern const CGSize kMapCenterButtonSize;
__eIC kMapCenterButtonPadding;
__eIC kMapPlaceViewViewHeight;

//Profile
extern NSUInteger const  kProfileDimensionCount;
extern NSUInteger const kProfileDimensionCountIndexOffset;

// Signup
extern NSString * const kCreateNewUserGenderCellMaleString;
extern NSString * const kCreateNewUserGenderCellFemaleString;
extern NSString * const kCreateNewUserSuccessTitle;
extern NSString * const kCreateNewUserSuccessMessage;
extern NSString * const kCreateNewUserFailureMessage;
extern NSString * const kCreateNewUserInvalidBirthYearMessage;

extern CGFloat const kValidateUsernameMinimumLength;
extern CGFloat const kValidateUsernameMaximumLength;



//forgot password
extern NSString * const kForgotPasswordEmailSentTitle;
extern NSString * const kForgotPasswordSuccessMessage;

extern NSString * const kInvalidUsernameMessage;


extern NSString * const kUserModelFacetScoreKey;

extern NSString * const kFacetModelFacetTextKey;
extern NSString * const kFacetModelFacetDescriptionKey;
extern NSString * const kFacetModelFacetIdKey;
extern NSString * const kFacetModelDimensionDetailsKey;
extern NSString * const kFacetModelQuestionsKey;

extern NSString *const kProfileActivationFailureTitle;
extern NSString *const kProfileActivationFailureMessage;
extern NSString *const kProfileActivationConfirmationTitle;
extern NSString *const kProfileActivationConfirmationMessage;
extern NSString *const kProfileViewControllerFacetDetailsToList;
extern NSString *const kProfileActivateProfileButtonTitle;

extern NSString *const kQuestionSaveFacetsCompleteTitle;
extern NSString *const kQuestionSaveFacetsCompleteMessage;
extern NSString *const kQuestionCloseConfirmationAlertTitle;
extern NSString *const kQuestionCloseConfirmationAlertMessage;

extern NSString *const kUnfriendConfirmationAlertTitle;

extern CGFloat const kTagBannerViewYOrigin;
extern NSString * const kTableViewCellIdentifierTagBanner;

extern NSString * const kSettingTutorialFirstScreen;
extern NSString * const kSettingTutorialSecondScreen;
//extern NSString * const kSettingTutorialThirdScreen;
extern NSString * const kSettingTutorialFourthScreen;
extern NSString * const kSettingTutorialFifthScreen;
extern NSString * const kSettingTutorialSixthScreen;
extern NSString * const kSettingTutorialSeventhScreen;
extern NSString * const kSettingTutorialEighthScreen;
extern NSString * const kSettingTutorialNinthScreen;
extern NSString * const kSettingTutorialTenthScreen;
extern NSString * const kSettingTutorialEleventhScreen;
extern NSString * const kSettingTutorialTwelfthScreen;

extern NSString * const kTagSeperatorCharacter;
extern NSString * const kTagFinalSeperatorCharacter;
extern NSString * const kFacebookMessageSheetDownloadPhrase;
extern NSUInteger const kMaxTwitterSheetCharacterCount;
extern NSUInteger const kMaxFacebookSheetCharacterCount;

extern NSString * const kCameraCaptureScreenViewControllerNavigationBarTitle;

extern NSString * const KFlurryAppID;

//__eIC kTagSearchTagPadding;
//__eIC kTagMinimumSearchBarTextFieldWidth;
//__eIC kMaxTextLengthForTagSearchQuery;
//__eFC kTagEntryFadeSpeed;
//__eFC kTagTextFieldMovementSpeed;

extern CGFloat const kDetailPOIVCScrollViewViewControllerSpaceDivider;

// Flurry constant

extern NSString * const kFlurryManualLogin;
extern NSString * const kFlurryFBLogin;
extern NSString * const kFlurryForgotPassword;
extern NSString * const kFlurryManualSignUp;
extern NSString * const kFlurryFBSignup;

extern NSString * const kFlurryStaffPicks;
extern NSString * const kFlurryPlaceDetail;
extern NSString * const kFlurryPlaceSharedThroughEmail;
extern NSString * const kFlurryPlaceMapView;
extern NSString * const kFlurryPlaceTwitterHandle;
extern NSString * const kFlurryPlaceWebsite;
extern NSString * const kFlurryPlaceTags;
extern NSString * const kFlurryMorePlaceTags;
extern NSString * const kFlurryReportPlace;

extern NSString * const kFlurrySearch;
extern NSString * const kFlurrySearchMapView;
extern NSString * const kFlurryDetailSearch;

extern NSString * const kFlurryUserProfile;
extern NSString * const kFlurryProfileQuestionView;
extern NSString * const kFlurryFacetDetail;
extern NSString * const kFlurryFriendView;
extern NSString * const kFlurrySettingScreen;
extern NSString * const kFlurryTutorial;
extern NSString * const kPOIContentViewWouldYouVisitQuestionString;
@end
