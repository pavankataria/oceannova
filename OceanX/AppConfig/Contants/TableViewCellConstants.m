//
//  TableViewCellConstants.m
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "TableViewCellConstants.h"

NSString * const kTableViewCellReuseIdentifierBusinessInfo = @"reuseIdentifierBusinessInfoPresentationCell";
NSString * const kTableViewCellReuseIdentifierTagAffirm = @"reuseIdentifierTagAffirmTableViewCell";
NSString * const kTableViewCellReuseIdentifierTagCenteredTag = @"reuseIdentifierCenteredTagCell";
NSString * const kTableViewCellReuseIdentifierHoursPresentation = @"reuseHoursPresentationTableViewCell";

NSString * const kTableViewCellReuseIdentifierEmptyTableViewCell = @"reuseIdentifierEmptyTableViewCell";

NSString * const kTableViewCellReuseIdentifierSignupTableViewCell = @"reuseIdentifierSignupTableViewCell";

NSString * const kTableViewCellReuseIdentifierLoginButtonTableViewCell = @"reuseIdentifierLoginButtonTableViewCell";
NSString * const kTableViewCellReuseIdentifierTCAndPrivacyTableViewCell = @"reuseIdentifierPrivacyTableViewCell";

NSString * const kTableViewCellReuseIdentifierPrivacyAndTermsCell = @"reuseIdentifierPrivacyAndTermsCell";