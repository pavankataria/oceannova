//
//  TableViewCellConstants.h
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kTableViewCellReuseIdentifierBusinessInfo;
extern NSString * const kTableViewCellReuseIdentifierTagAffirm;
extern NSString * const kTableViewCellReuseIdentifierTagCenteredTag;
extern NSString * const kTableViewCellReuseIdentifierHoursPresentation;

extern NSString * const kTableViewCellReuseIdentifierEmptyTableViewCell;

extern NSString * const kTableViewCellReuseIdentifierSignupTableViewCell;

extern NSString * const kTableViewCellReuseIdentifierLoginButtonTableViewCell;
extern NSString * const kTableViewCellReuseIdentifierTCAndPrivacyTableViewCell;

extern NSString * const kTableViewCellReuseIdentifierPrivacyAndTermsCell;