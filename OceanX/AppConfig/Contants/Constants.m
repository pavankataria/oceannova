//
//  Constants.m
//  OceanX
//
//  Created by Pavan Kataria on 17/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSString *const FBSessionStateChangedNotification = @"facebookStatechangedNotification";

NSUInteger const kInitialYearOfBornToShow = 1900;
NSUInteger const kMinimumAgeRequirementToSignUp = 13;

NSUInteger const kTwitterUsernameCharacterCountLimit = 15;

NSString * const kBusinessDetailCellConfigurationGetValuesDictionaryText = @"text";
NSString * const kBusinessDetailCellConfigurationGetValuesDictionaryIndexPath = @"indexPath";

NSString * const kAmendHoursPickerViewSeparator = @"-";
NSString * const kPrettifiedBusinessOpeningTimeSeparatorDays = @" - ";
NSString * const kPrettifiedBusinessOpeningTimeSeparatorTime = @" - ";

NSString * const kProfileViewControllerNavigationTitle = @"Profile";
NSString * const kSettingsViewControllerNavigationTitle = @"Settings and Info";


CGFloat const kOXGenericPresentationTableViewRowHeight = 80.0;
CGFloat const kOXDaysSelectionTableViewRowHeight = 50.0;
CGFloat const kOXAddTagsTableViewRowHeight = 50;

CGFloat const kOXTableViewControllerLoadingCellSize = 0;
CGFloat const kOXPOIListTableViewLikeDislikeBarHeight = 110;

CGFloat const kCreateNewUserGenderTableViewRowHeight = 120;
CGFloat const kCreateNewUserYearOfBornTableViewRowHeight = 50;
CGFloat const kPickerViewControllerContainerViewTopPostionToShow = 215;
CGFloat const kPickerViewControllerContainerViewTopPostionToHide = 0;

NSString * const kOXAddTagsTextFieldPlaceHolderText = @"Type a tag";
NSString * const kOXSearchTextFieldPlaceHolderText = @"Search.. 'Pizza', 'Clubs'";
NSString * const kOXSearchTextFieldAddressPlaceHolderText = @"Search in location..";
NSUInteger const kOXAddTagsTextFieldMaxCharacterCount = 20;


NSUInteger const kOXAddTagsMinimumTagsRequirement = 3;
NSUInteger const kOXQuestionTotalQuestionPerFacet = 4;

NSString * const kAddTagViewControllerTagPlaceCountModelTagName = @"tagName";
NSString * const kAddTagViewControllerTagPlaceCountModelTagPlaceCount = @"numberOfTimeUsed";


NSString * const kSegueIdentifierFriendsViewController = @"segueIdentifierFriendsViewController";
NSString * const kSegueIdentifierTagsViewController = @"segueIdentifierTagsViewController";
NSString * const kSegueIdentifierShowCameraScreenViewController = @"segueIdentifierShowCameraScreenViewController";
NSString * const kSegueIdentifierCameraThumbnailPreview = @"segueIdentifierCameraThumbnailPreview";
NSString * const kSegueIdentifierEditImagesViewController = @"segueIdentifierEditImagesViewController";
NSString * const kSegueIdentifierBusinessInfoViewController = @"segueIdentifierBusinessInfoViewController";
NSString * const kSegueIdentifierDaysSelectionViewController = @"segueIdentifierDaysSelectionViewController";
NSString * const kSegueIdentifierAmendHoursViewController = @"segueIdentifierAmendHours";
NSString * const kSegueIdentifierAddTagsViewController = @"segueIdentifierAddTags";
NSString * const kSegueIdentifierCreateProfileTableViewController = @"segueIdentifierCreateProfileTableViewController";
NSString * const kSegueIdentifierPickerViewController= @"segueIdentifierpickerViewController";
NSString * const kSegueIdentifierQuestionnaireViewController = @"segueIdentifierQuestionnaireViewController";

NSString * const kSegueIdentifierQuestionSaveViewController = @"segueIdentifierQuestionSaveViewController";
NSString * const kSegueIdentifierQuestionViewController = @"segueIdentifierQuestionViewController";

NSString * const kSegueIdentifierReportTextViewController = @"segueIdentifierReportTextViewController";

NSString * const kSegueIdentifierAboutWebviewViewController = @"segueIdentifierAboutWebviewViewController";
NSString * const kSegueIdentifierPrivacyWebviewViewController = @"segueIdentifierPrivacyWebviewViewController";
NSString * const kSegueIdentifierTermsAndConditionWebviewViewController = @"segueIdentifierTermsAndConditionWebviewViewController";

NSString * const kSegueIdentifierShowSignupPage = @"segueIdentifierShowSignupPage";

NSString * const kTableViewCellFriendIdentifier = @"friendCellIdentifier";
NSString * const kTableViewCellFriendRequestIdentifier = @"friendRequestCellIdentifier";
NSString * const kTableViewCellTagIdentifier = @"tagCellIdentifier";
NSString * const kTableViewCellGenderIdentifier = @"genderCellIdentifier";
NSString * const kTableViewCellYearOfBornIdentifier = @"yearOfBornCellIdentifier";


NSString * const kOXAnnotationIdentifier = @"annotationViewIdentifier";

NSString * const kTableViewCellIdentifierAddPlaceBusinessInfo = @"riaddpoibusinessinfo";
NSString * const kTableViewCellIdentifierAddPlaceExtraInfo = @"riaddpoiextrainfo";
NSString * const kTableViewCellIdentifierAddPlaceHoursInfo = @"riaddpoihoursinfo";

NSString * const kTableViewCellIdentifierDaysSelection = @"reuseIdentifierDaysSelection";
NSString * const kTableViewCellIdentifierSearchResultsLoadingCell = @"reuseIdentifierLoadingCell";
NSString * const kTableViewCellIdentifierSearchResultsImageDisplayCell = @"reuseImageDisplayCellReuseIdentifier";
NSString * const kTableViewCellIdentifierProfileInfoCell = @"profileInfoCellIdentifier";
NSString * const kTableViewCellFacetBarTableCellIdentifier = @"facetBarTableCellIdentifier";
NSString * const kSectionHeaderViewTableViewCellSegueIdentifier = @"OXSectionPlaceHeaderTableViewCell";


NSUInteger const kAddPlaceCameraCaptureMaxPhotosAllowed = 5;
NSString * const kAddPlaceCameraCaptureNavigationButtonTitle = @"Done";






NSString *const kExtraInfoTableViewCellIndexPathRowLabelNameAddress = @"Address";
NSString *const kExtraInfoTableViewCellIndexPathRowLabelNamePhone = @"Phone";
NSString *const kExtraInfoTableViewCellIndexPathRowLabelNameWebsite = @"Website";
NSString *const kExtraInfoTableViewCellIndexPathRowLabelNameTwitter = @"Twitter";
NSString *const kExtraInfoTableViewCellIndexPathRowLabelNameFacebook = @"Facebook";


NSUInteger const kExtraInfoTableViewCellIndexPathRowLabelTagAddress = 0;
NSUInteger const kExtraInfoTableViewCellIndexPathRowLabelTagPhone = 1;
NSUInteger const kExtraInfoTableViewCellIndexPathRowLabelTagWebsite = 2;
NSUInteger const kExtraInfoTableViewCellIndexPathRowLabelTagTwitter = 3;


NSUInteger const kBusinessInfoTableTotalRowsBusinessInfo = 1;
NSUInteger const kBusinessInfoTableTotalRowsExtraInfo = 3;

NSString *const kBusinessInfoTableTitleHeaderBusinessInfo = @"Business";
NSString *const kBusinessInfoTableTitleHeaderDetailsInfo = @"Extras";
NSString *const kBusinessInfoTableTitleHeaderHoursInfo = @"";
NSUInteger const kDaysSelectionDaysOfTheWeek = 7;
NSUInteger const kDaysSelectionWeeksToShow = 1;



//Userdefaults keys
NSString * const kMetadataKey = @"METADATA";
NSString * const kSavedUserNameKey = @"USERNAME";
NSString * const kSavedPasswordKey = @"PASSWORD";
NSString * const kCookieName = @"JSESSIONID";
NSString * const kCookieKey = @"COOKIE";
NSString * const kLoggedInKey = @"LOGGED_IN";
NSString * const kNSUserDefaultsReplayTutorialDidViewKey = @"hasOceanTutorialBeenShown";
NSString * const kNSUserDefaultsFBRecommendationShouldDisplayView = @"shouldDisplayFBRecommendationView";

NSString * const kNSUserDefaultsFBCheckLikePermissionActive = @"kNSUserDefaultsFBCheckLikePermissionActive";



NSString * const kSessionCookiesKey = @"SESSIONCOOKIEKEY";
NSString * const kUserDeviceTokenStringKey = @"OXUserDeviceTokenString";


NSString * const kRequestNodeLocationUserPosition = @"location/user/position";
NSString * const kRequestNodeUsersFriends = @"users/friends";
NSString * const kRequestNodeNotificationFriends = @"notifications/friends";
NSString * const kRequestNodeNotificationSentFriends = @"notifications/friends/sent";
NSString * const kRequestNodeUsersSearchName =  @"users/search/name";



NSString * const kJSONNullString = @"";

CGFloat const kOXSearchTableViewControllerTableViewSectionHeaderHeight = 80;
CGFloat const kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha = 0.95;
NSString *const kOXSearchTableViewControllerTableViewCellIdentifierImageCell = @"imageCellIdentifier";

NSString *const kOXSearchFilterScreenSegueIdentifier = @"searchFilterScreenSegueIdentifier";
NSString *const kOXSearchResultsScreenSegueIdentifier = @"searchResultsScreenSegueIdentifier";
NSString *const kOXSearchScreenEmbeddedViewControllerSegue = @"searchScreenEmbeddedViewControllerSegue";
NSString *const kSegueIdentifierDetailSearchByMapViewController =  @"detailSearchByMapViewControllerIdentifier";

NSString *const kOXSummaryBannerViewControllerSegueIdentifier = @"SummaryBannerViewControllerSegueIdentifier";

NSString *const kOXTableViewCellReuseLikeDislikeBarCellIdentifier = @"reuseLikeDislikeBarCellIdentifier";

//Maps
const CGPoint kMapPinPointOffsetPointForSelected = { 0.5f, 1.0f };
const CGPoint kMapPinPointOffsetPointForUnselected = { 0.5f, 1.0f };
const CGSize kMapCenterButtonSize = {40,40};
__iC kMapCenterButtonPadding = 10;
__iC kMapPlaceViewViewHeight = 300;

//Profile
NSUInteger const  kProfileDimensionCount = 5;
NSUInteger const kProfileDimensionCountIndexOffset = 1;

// Signup
NSString * const kCreateNewUserGenderCellMaleString = @"male";
NSString * const kCreateNewUserGenderCellFemaleString = @"female";
NSString *const kCreateNewUserSuccessTitle = @"Successfully signed Up";
NSString *const kCreateNewUserSuccessMessage = @"Welcome to Ocean!";
NSString *const kCreateNewUserFailureMessage = @"User not created, something went wrong.";
NSString *const kCreateNewUserInvalidBirthYearMessage = @"Please select year of your birth.";

//forgot password
NSString *const kForgotPasswordEmailSentTitle = @"Email sent";
NSString *const kForgotPasswordSuccessMessage = @"Please click on the link in the email to reset your password.";

NSString *const kInvalidUsernameMessage = @"Username Invalid. Please enter a valid username address.";
CGFloat const kValidateUsernameMinimumLength = 3;
CGFloat const kValidateUsernameMaximumLength = 30;

NSString * const kUserModelFacetScoreKey = @"facetScore";

NSString * const kFacetModelFacetTextKey = @"facetText";
NSString * const kFacetModelFacetDescriptionKey = @"facetDescription";
NSString * const kFacetModelFacetIdKey = @"facetId";
NSString * const kFacetModelDimensionDetailsKey = @"dimensionDetails";
NSString * const kFacetModelQuestionsKey = @"questions";

NSString *const kProfileActivationFailureMessage = @"Please try again";
NSString *const kProfileActivationFailureTitle = @"Could Not Activate Your Profile";
NSString *const kProfileActivationConfirmationTitle = @"Activate Profile";
NSString *const kProfileActivationConfirmationMessage = @"This will activate your personality profile, with a set of questions you can complete whenever you want to. We use it to improve your recommendations.";
NSString *const kProfileViewControllerFacetDetailsToList = @"facetDetails2List";
NSString *const kProfileActivateProfileButtonTitle = @"Activate Your Profile";

NSString *const kQuestionSaveFacetsCompleteTitle = @"You have Completed Your Profile!";
NSString *const kQuestionSaveFacetsCompleteMessage = @"Tap 'Save and Exit' to save your final set of answers.";
NSString *const kQuestionCloseConfirmationAlertTitle = @"Are you sure you want to exit?";
NSString *const kQuestionCloseConfirmationAlertMessage = @"you will lose any answers you have given";

NSString *const kUnfriendConfirmationAlertTitle = @"Are you sure you want to unfriend %@ ?";

CGFloat const kTagBannerViewYOrigin = 3;
NSString * const kTableViewCellIdentifierTagBanner = @"tagBannerTableViewCellIdentifier";


NSString * const kSettingTutorialFirstScreen = @"settingTutorialFirstScreen";
NSString * const kSettingTutorialSecondScreen = @"settingTutorialSecondScreen";
//NSString * const kSettingTutorialThirdScreen = @"settingTutorialThirdScreen";
NSString * const kSettingTutorialFourthScreen = @"settingTutorialFourthScreen";
NSString * const kSettingTutorialFifthScreen = @"settingTutorialFifthScreen";
NSString * const kSettingTutorialSixthScreen = @"settingTutorialSixthScreen";
NSString * const kSettingTutorialSeventhScreen = @"settingTutorialSeventhScreen";
NSString * const kSettingTutorialEighthScreen = @"settingTutorialEighthScreen";
NSString * const kSettingTutorialNinthScreen = @"settingTutorialNinthScreen";
NSString * const kSettingTutorialTenthScreen = @"settingTutorialTenthScreen";
NSString * const kSettingTutorialEleventhScreen = @"settingTutorialEleventhScreen";
NSString * const kSettingTutorialTwelfthScreen = @"settingTutorialTwelfthScreen";

NSString * const kTagSeperatorCharacter = @" ";
NSString * const kTagFinalSeperatorCharacter = @"& ";
NSString * const kFacebookMessageSheetDownloadPhrase = @"Download Ocean for instant access to the coolest places in London";
NSUInteger const kMaxTwitterSheetCharacterCount = 140;
NSUInteger const kMaxFacebookSheetCharacterCount = 140;


NSString * const kCameraCaptureScreenViewControllerNavigationBarTitle = @"Add new place";


CGFloat const kDetailPOIVCScrollViewViewControllerSpaceDivider = 100;
NSString * const KFlurryAppID = @"NRYDRSFP8VFPG83B9286";

NSString * const kFlurryManualLogin = @"Manual Login";
NSString * const kFlurryFBLogin = @"FB Login";
NSString * const kFlurryForgotPassword = @"Forgot Password";
NSString * const kFlurryManualSignUp  = @"Manual Sign up";
NSString * const kFlurryFBSignup = @"FB Sign up";

NSString * const kFlurryStaffPicks =  @"Staff Picks";
NSString * const kFlurryPlaceDetail = @"Place Detail";
NSString * const kFlurryPlaceSharedThroughEmail = @"Place shared through email";
NSString * const kFlurryPlaceMapView = @"Map View";
NSString * const kFlurryPlaceTwitterHandle = @"Place Twitter Handle";
NSString * const kFlurryPlaceWebsite = @"Place Website";
NSString * const kFlurryPlaceTags = @"Place Tags";
NSString * const kFlurryMorePlaceTags = @"More Place Tags";
NSString * const kFlurryReportPlace = @"Report Place";

NSString * const kFlurrySearch = @"Search";
NSString * const kFlurrySearchMapView = @"Search Map View";
NSString * const kFlurryDetailSearch = @"Detail Search";

NSString * const kFlurryUserProfile = @"User Profile";
NSString * const kFlurryProfileQuestionView = @"Profile Question View";
NSString * const kFlurryFacetDetail = @"Facet Detail";
NSString * const kFlurryFriendView = @"Friend View";
NSString * const kFlurrySettingScreen = @"Setting Screen";
NSString * const kFlurryTutorial = @"Tutorial";



NSString * const kPOIContentViewWouldYouVisitQuestionString = @"Would you visit this place?";
@end
