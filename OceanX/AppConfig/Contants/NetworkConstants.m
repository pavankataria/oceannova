//
//  NetworkConstants.m
//  OceanX
//
//  Created by Pavan Kataria on 22/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "NetworkConstants.h"

@implementation NetworkConstants

NSString * const kHTTPClientURLStringBase = @"https://oceanbackend.oceanapp.com/";
NSString * const kHTTPClientURLStringBaseDemo = @"http://oceanqa.elasticbeanstalk.com/";
//NSString * const kHTTPClientURLStringBaseDemo = @"https://oceanbackend.oceanapp.com/";
NSString * const kHTTPClientURLStringRESTURL = @"ws/rest/";

NSString * const kHTTPClientURLStringAPIVersion = @"v2";
NSString * const kHTTPClientURLStringTagAutoSuggestion = @"v2/tag/autosuggestion";
NSString * const kHTTPClientURLStringGetTags = @"tags";
NSString * const kHTTPClientURLStringAddPoi = @"v2/poi/add";
NSString * const kHTTPClientURLStringGetFullPOI = @"poi";

//NSString * const kHTTPClientURLStringSecurityCheckLogin = @"v2/login";
NSString * const kHTTPClientURLStringSecurityCheckLogin = @"security_check";


NSString * const kHTTPClientURLStringSearchByMap = @"v2/poi/searchbymap";
NSString * const kHTTPClientURLStringSearchByList = @"v2/poi/searchbylist";
NSString * const kHTTPClientURLStringGetQuickInfo = @"v2/poi/%@/quickinfo";

NSString * const kHTTPClientURLStringReportPOI = @"v2/poi/%@/report";
NSString * const kHTTPClientURLStringReportPicture = @"v2/picture/%@/report";

NSString * const kHTTPClientURLStringGetUserProfile = @"users/profile";
NSString * const kHTTPClientURLStringActivateUserProfile = @"users/activateprofile";
NSString * const kHTTPClientURLStringGetFacetsQuestions = @"v2/questions";
NSString * const kHTTPClientURLStringGetMetaData = @"/metadata/";
NSString * const kHTTPClientURLStringRequestPasswordReset = @"v2/user/reset/password";
NSString * const kHTTPClientURLStringSignUpUser = @"v2/user/new";
NSString * const kHTTPClientURLStringAnswerQuestions = @"questionnaire";

NSString * const kHTTPClientURLStringGetNewsFeed = @"ws/rest/v2/newsfeed";

NSString * const kHTTPClientURLStringLike = @"/like";
NSString * const kHTTPClientURLStringDislike = @"/dislike";
NSString * const kHTTPClientURLStringNeutral = @"/neutral";

NSString * const kHTTPClientURLStringAddTagsToPOI = @"tag/add";
NSString * const kHTTPClientURLStringAddPicturesToPOI = @"/picture/add";

NSString * const kHTTPClientURLStringPUTAffirmTag = @"tag/affirm";
NSString * const kHTTPClientURLStringPUTDisaffirmTag = @"tag/disaffirm";
NSString * const kHTTPClientURLStringPUTUpdateUserLocation = @"user/location/update";

NSString * const kHTTPClientURLStringPOSTFacebookNeedInfo = @"facebook/login";//@"facebook/needinfo";
NSString * const kHTTPClientURLStringPOSTAndGetFacebookRecommendations = @"facebook/recommendation";
NSString * const kHTTPClientURLStringGETFacebookCheck = @"facebook/check";
NSString * const kHTTPClientURLStringPUTUpdateDeviceToken = @"user/devicetoken";


@end
