//
//  PKQuickMethods.h
//  OceanX
//
//  Created by Pavan Kataria on 18/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotationView.h>

@interface PKQuickMethods : NSObject

+(void)setCornerRadiusForView:(UIView*)view withCornerRadius:(NSUInteger)radius;
+(NSMutableArray*)prettifyBusinessOpeningHours:(NSArray*)businessOpeningHours;
+ (NSString*) nameOfDimension:(NSInteger) index;
+ (BOOL)validateEmailWithString:(NSString *)email;
+ (void)listSubviewsOfView:(UIView *)view;
+(void)mapSetPinPointUnhighlightedImageAndOffsetForMKAnnotationView:(MKAnnotationView*)annotationView;
+(void)mapSetPinForUserLocationAnnotationView:(MKAnnotationView*)annotationView;


+(UIView*)aNewDividerViewBelowFrame:(CGRect)frame;

+(UIImage*)fadedLeftArrowImage;
@end
