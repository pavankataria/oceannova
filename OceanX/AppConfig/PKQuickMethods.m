//
//  PKQuickMethods.m
//  OceanX
//
//  Created by Pavan Kataria on 18/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "PKQuickMethods.h"
#import "OXPrettifiedBusinessOpeningTimeModel.h"

@implementation PKQuickMethods
+(void)setCornerRadiusForView:(UIView *)view withCornerRadius:(NSUInteger)radius{
    view.layer.cornerRadius = radius;
    view.layer.masksToBounds = YES;
}


+(NSMutableArray*)prettifyBusinessOpeningHours:(NSArray*)businessOpeningHours{
    NSMutableArray *prettifiedBusinessOpeningHours = [[NSMutableArray alloc] init];
    
    for(int kStartingSearch = 0; kStartingSearch < businessOpeningHours.count; kStartingSearch++){
        OXBusinessHoursModel *currentOH = businessOpeningHours[kStartingSearch];
        
        OXPrettifiedBusinessOpeningTimeModel *prettifiedOHObject = [[OXPrettifiedBusinessOpeningTimeModel alloc] initWithBusinessHoursObject:currentOH];
//        NSLog(@"i = %d, starting saved hour: %@", kStartingSearch, [currentOH hoursRepresentation]);

        for(int j = kStartingSearch+1; j < businessOpeningHours.count; j++){

            OXBusinessHoursModel *nextOH = businessOpeningHours[j];
//            NSLog(@"j = %d, ending saved hour: %@", j, [nextOH hoursRepresentation]);
//
//
//            NSLog(@"currently comparing: current day: %@ &time: %@ with jDay: %@ &time: %@", [currentOH daysRepresentation], [currentOH hoursRepresentation], [nextOH daysRepresentation], [nextOH hoursRepresentation]);
            if([currentOH isEqualToBusinessOpeningHourModel:nextOH]){
                [prettifiedOHObject setEndingWeekDay:nextOH.weekday];
                kStartingSearch = j;
            }
            else{
                break;
            }
        }
        NSLog(@"prettifiedObject: %@ - %@", prettifiedOHObject.startingWeekDay, prettifiedOHObject.endingWeekDay);
        [prettifiedBusinessOpeningHours addObject:prettifiedOHObject];
    }
    return prettifiedBusinessOpeningHours;
}

+(NSString*) nameOfDimension:(NSInteger) index{
    
    NSArray * array = @[@"Openness", @"Conscientiousness",@"Extraversion", @"Agreeableness", @"Neuroticism"];
    return array[index];
    
}

+ (BOOL)validateEmailWithString:(NSString *)email
{
    NSString *laxString = @".+@.+\\.[A-Za-z]+(\\.)?([A-Za-z])*";
    NSString *emailRegex = laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}
+ (void)listSubviewsOfView:(UIView *)view {
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    // Return if there are no subviews
    if ([subviews count] == 0) return; // COUNT CHECK LINE
    for (UIView *subview in subviews) {
        // Do what you want to do with the subview
        NSLog(@"%@", subview);
        // List the subviews of subview
        [self listSubviewsOfView:subview];
    }
}

+(void)mapSetPinPointUnhighlightedImageAndOffsetForMKAnnotationView:(MKAnnotationView*)annotationView{
    annotationView.image = [UIImage imageNamed:@"pinUnselected"];
    CGRect annotationFrame = annotationView.frame;
    CGFloat scaleFactor = 1.2;
    annotationFrame.size.width *= scaleFactor;
    annotationFrame.size.height *= scaleFactor;
    [annotationView setFrame:annotationFrame];
    annotationView.layer.anchorPoint = kMapPinPointOffsetPointForUnselected ;
}
+(void)mapSetPinForUserLocationAnnotationView:(MKAnnotationView *)annotationView{
    annotationView.image = [UIImage imageNamed:@"userLocationPin"];
    CGRect annotationFrame = annotationView.frame;
    CGFloat scaleFactor = 1.2;
    annotationFrame.size.width *= scaleFactor;
    annotationFrame.size.height *= scaleFactor;
    [annotationView setFrame:annotationFrame];
    annotationView.layer.anchorPoint = CGPointMake(0.5f, 0.5f);
}
+(UIView*)aNewDividerViewBelowFrame:(CGRect)frame{
    
    UIView *dividerView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(frame), CGRectGetWidth(frame), 20)];
    [dividerView setBackgroundColor:[UIColor clearColor]];
    CGFloat margin = 20;
    CGFloat heightOfDividerView = 1;
    
    UIView *dividerViewLine = [[UIView alloc] initWithFrame:CGRectMake(margin, dividerView.frame.size.height/2-heightOfDividerView/2, CGRectGetWidth(dividerView.frame)-(2*margin), heightOfDividerView)];
    [dividerViewLine setBackgroundColor:[UIColor colorWithRed:0.89 green:0.89 blue:0.89 alpha:1]];
    [dividerView addSubview:dividerViewLine];
    
    return dividerView;
}
+(UIImage*)fadedLeftArrowImage{
    return [[UIImage imageNamed:@"fadedLeftArrow"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}
@end
