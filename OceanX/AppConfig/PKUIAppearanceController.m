//
//  PKUIAppearanceController.m
//  OceanX
//
//  Created by Pavan Kataria on 17/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "PKUIAppearanceController.h"
#import "AppDelegate.h"
#import "OXDetailPOIViewController.h"


@implementation PKUIAppearanceController
+(void)appStyle{
    [self setNavigationBarAppearance];
    [self setTableViewAppearance];

}

+(void)setNavigationBarAppearance{
    [[UINavigationBar appearance] setBarTintColor:self.darkBlueColor];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    
    [[UINavigationBar appearanceWhenContainedIn:[OXDetailPOIViewController class], nil] setBarTintColor:[UIColor whiteColor]];

    
    NSDictionary *navigationBarTitleTextAttributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:25.0], NSForegroundColorAttributeName:self.mainColor};
    [[UINavigationBar appearance] setTitleTextAttributes:navigationBarTitleTextAttributes];
    
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationController class], nil] setTintColor:self.mainColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]]; // this will change the back button tint
}

+(void)setTableViewAppearance{
    [[UITableView appearance] setBackgroundColor:self.mainColor];
    [[UITableView appearance] setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [[UITableView appearance] setSeparatorColor:[UIColor lightGrayColor]];
    
}


//+(void)setUIButtonAppearance{
//    //This is for the friend and tag buttons when theyre contained in a tableview.
//    [UIButton appearanceWhenContainedIn:[UITableViewCell class], nil]
//}





+(UIColor*)mainColor{
    return [UIColor colorWithRed:1 green:1 blue:1 alpha:1.0];
}
+(UIColor*)heartColor{
    return [UIColor colorWithRed:252/255.0 green:67/255.0 blue:73/255.0 alpha:1.0];
}
+(UIColor*)grayColor{
    return [UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1.0];
}
+(UIColor*)lightBlueColor{
//    return [UIColor colorWithRed:109/255.0 green:188/255.0 blue:219/255.0 alpha:1.0];
    
    
//    return [UIColor colorWithRed:94/255.0 green:166/255.0 blue:234/255.0 alpha:1.0];
    
    return [UIColor colorWithRed:109/255.0 green:166/255.0 blue:219/255.0 alpha:1.0];


}
+(UIColor*)darkBlueColor{
//    return [UIColor colorWithRed:42/255.0 green:63/255.0 blue:110/255.0 alpha:1.0];
//    return [UIColor colorWithRed:45/255.0 green:74/255.0 blue:127/255.0 alpha:1.0];

//    return [UIColor colorWithRed:63/255.0 green:102/255.0 blue:176/255.0 alpha:1.0];
//    return [UIColor colorWithRed:57/255.0 green:94/255.0 blue:161/255.0 alpha:1.0]; //MAIN COLOR DARK BLUE
//    return [UIColor colorWithRed:72.0/255.0 green:216.0/255.0 blue:240.0/255.0 alpha:1.0];
        return [UIColor colorWithRed:72.0/255.0 green:205.0/255.0 blue:223.0/255.0 alpha:1.0];

}
//
//+(UIColor*)mainColor{
//    return [UIColor colorWithRed:1 green:1 blue:1 alpha:1.0];
//}
//+(UIColor*)color2{
//    return [UIColor colorWithRed:13/255.0 green:76/255.0 blue:115/255.0 alpha:1.0];
//}
//+(UIColor*)color3{
//    return [UIColor colorWithRed:154/255.0 green:217/255.0 blue:187/255.0 alpha:1.0];
//}
//+(UIColor*)color4{
//    return [UIColor colorWithRed:60/255.0 green:155/255.0 blue:166/255.0 alpha:1.0];
//}
//+(UIColor*)color5{
////    return [UIColor colorWithRed:240/255.0 green:240/255.0 blue:235/255.0 alpha:1.0];
//    return [UIColor colorWithRed:237/255.0 green:237/255.0.0 blue:239/255.0 alpha:1.0];
//}
+(UIColor*)tealColor{
     return [UIColor colorWithRed:1.0/255.0 green:154.0/255.0 blue:172.0/255.0 alpha:1.0];
}

+(UIColor*)mainHighlightDarkBlueColor{
    return [UIColor colorWithRed:18.0/255.0 green:137.0/255.0 blue:156.0/255.0 alpha:1.0];
}
+(UIColor*)facebookLoginButtonColour{
    return rgb(59, 89, 152);
}
+(UIColor*)pkLightGrayColor{
    return rgb(179, 179, 179);
}
@end
