//
//  UIImage+imageWithColor.h
//  OceanX
//
//  Created by Pavan Kataria on 28/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (imageWithColor)


+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
+ (UIImage *)imageWithColor:(UIColor *)color;


@end
