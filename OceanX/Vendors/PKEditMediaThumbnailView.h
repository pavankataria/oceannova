//
//  PKEditMediaThumbnailView.h
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKPreviewBaseView.h"


#import "PKNewEditMediaContentViewProtocols.h"

@interface PKEditMediaThumbnailView : PKPreviewBaseView
@property (nonatomic, weak) id <PKEditMediaThumbnailViewDelegate> delegate;
@property (nonatomic, assign, getter=isCameraIcon) BOOL cameraIcon;

@property (nonatomic, retain) UIButton *button;
-(void)setupDeleteButton;
@end
