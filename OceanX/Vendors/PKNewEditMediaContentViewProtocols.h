//
//  PKNewEditMediaContentViewProtocols.h
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PKThumbnailSliderView;
@class PKPreviewSliderView;

#pragma mark - PKThumbnailSliderDelegate - Methods

@protocol PKThumbnailSliderViewDelegate <NSObject>
@optional
-(void)thumbnailSlider:(PKThumbnailSliderView*)thumbnailSlider didTapOnThumbnailIndex:(NSUInteger)index;
-(void)thumbnailSlider:(PKThumbnailSliderView*)thumbnailSlider didDeleteThumbnailPreviewAtIndexPosition:(NSUInteger)index;
-(void)thumbnailSliderImageDownloaded:(UIImage*)image atIndex:(NSUInteger)index;
-(void)didSelectCameraIcon;
-(void)didSelectThumbnail;

@end

#pragma mark - PKPreviewSliderViewDelegate - Methods

@protocol PKPreviewSliderViewDelegate <NSObject>
@optional
-(void)previewSliderView:(PKPreviewSliderView*)previewSliderView didSlideToPreviewIndex:(NSUInteger)index;
@end


#pragma mark - PKEditMediaThumbnailViewDelegate - Methods

@protocol PKEditMediaThumbnailViewDelegate <NSObject>
@optional
-(void)didSelectThumbnailPreviewAtIndexPosition:(NSUInteger)index;
-(void)cameraIconDidSelect;
@end

#pragma mark - PKLikeDislikeShareBarViewDelegate - Methods
@protocol PKLikeDislikeShareBarViewDelegate <NSObject>
@optional
-(void)didPressLike;
-(void)didPressDislike;
-(void)didPressShare;
@end


