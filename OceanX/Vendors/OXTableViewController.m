//
//  OXTableViewController.h.m
//  Systango
//
//  Created by Systango on 24/09/14.
//  Copyright (c) 2014 Systango. All rights reserved.
//

#import "OXTableViewController.h"


@interface OXTableViewController () <UIGestureRecognizerDelegate>

@end

@implementation OXTableViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    UIGestureRecognizer *viewTapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
    viewTapGestureRecognizer.delegate = self;
    viewTapGestureRecognizer.cancelsTouchesInView = FALSE;
    [self.view addGestureRecognizer:viewTapGestureRecognizer];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
}

#pragma mark - Private Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (![touch.view isKindOfClass:[UIControl class]] && ![touch.view isKindOfClass:[UITextField class]] &&  ![touch.view isKindOfClass:[UITextView class]]) {
        [self endEditing];
    }
    return true;
}

- (void)endEditing {
    [self.view endEditing:YES];
    [self.navigationController.navigationBar endEditing:YES];
}


#pragma mark - Memory

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
