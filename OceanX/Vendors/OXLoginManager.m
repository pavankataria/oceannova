//
//  OXLoginManager.m
//  OceanX
//
//  Created by Pavan Kataria on 02/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXLoginManager.h"
#import "OXServerRequest.h"
#import "AppDelegate.h"
#import "OXRestAPICient.h"

@implementation OXLoginManager


+ (OXLoginManager*)sharedManager {
    static OXLoginManager * sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}


- (void)performLoginWithUserName:(NSString*)username andPassword:(NSString*)password
                   successBlock:(SuccessCompletionBlock)successBlock
                      failBlock:(FailureCompletionBlock)failBlock{
    
    [OXRestAPICient loginWithUsername:username password:password successBlock:^(AFHTTPRequestOperation *operation, id response) {
        
//        OXUserModel *user = [[OXUserModel alloc] initWithUsername:username password:password];
//        
//        [user saveUserCredentials];

        //perform metadata API
        successBlock(operation, response);
        
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        failBlock(operation, error);
    }];
}

- (void)performRenewLogin{
    NSString *username = [[NSUserDefaults standardUserDefaults]objectForKey:kSavedUserNameKey];
    NSString *password = [[NSUserDefaults standardUserDefaults]objectForKey:kSavedPasswordKey];
    
    NSLog(@"username: %@ password: %@", username, password);
    if(username && password)
    {
        [OXRestAPICient loginWithUsername:username password:password successBlock:^(AFHTTPRequestOperation *operation, id response) {
            NSLog(@"successfully logged in");
            NSLog(@"operation: %@\n\nresponse: %@", operation, response);
            [[OXServerRequest sharedManager] executePendingRequests];
        } failBlock:^(AFHTTPRequestOperation *operation, id error) {
            NSLog(@"failed to log in");
            [[OXServerRequest sharedManager] cancelAndRemoveAllPendingRequests];
            [self performLogoutSegue];
        }];
    }
    else{
        [self performLogoutSegue];
    }
}

-(void)performLoginSegue{
    
}


- (void)performLogoutSegue{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self logout];
        [[AppDelegate getAppDelegateReference] showLoginScreen];
    });
}

-(void)logout{
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        
        // If the session state is not any of the two "open" states when the button is clicked
    }
    [[OXServerRequest sharedManager] cancelAllRequests];
    [[OCNLocationTracker tracker] pause];
//    [[OCN Server Request sharedInstance] cancelAllRequests];
//    [[OXLocationManager sharedTracker] pause];
    [[OXCookieManager sharedManager] clearSavedSessionCookies];
    NSLog(@"logout method called");
}

+(BOOL) isLoggedIn{
    NSLog(@"\n\nisLoggedIn method called\n\n");
    if([[NSUserDefaults standardUserDefaults] boolForKey:kLoggedInKey]){
        //[[OXLocationManager sharedTracker] start];
        return YES;
    }
    return NO;
}

-(void)loginWithFacebookWithSuccessBlock:(SuccessCompletionBlock)successBlock
                               failBlock:(FailureCompletionBlock)failBlock{

/*//     If the session state is any of the two "open" states when the button is clicked
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        
        // If the session state is not any of the two "open" states when the button is clicked
    }
    else {
 */
    NSLog(@"PERMISSIONS: %@", [[AppDelegate getAppDelegateReference] facebookPermissions]);
        if (FBSession.activeSession.state == FBSessionStateOpen
            /*|| FBSession.activeSession.state == FBSessionStateOpenTokenExtended*/) {
            
            NSString *accessToken = FBSession.activeSession.accessTokenData.accessToken;
            NSLog(@"\n\n\nSession is open. Access token: %@\nConnecting to server now to do a quick check to see if the user exists", accessToken);
            
            
//            [OXRestAPICient postFacebookNeedInfoWithFacebookAccessTokenString:accessToken
//                                                                 successBlock:^(AFHTTPRequestOperation *operation, id response) {
//                                                                     successBlock(operation, response);
//                                                                 }
//                                                                    failBlock:^(AFHTTPRequestOperation *operation, id error) {
//                                                                        failBlock(operation, error);
//                                                                    }];
            [OXRestAPICient postLoginWithFacebookWithAccessTokenString:accessToken
                                                  withParamsDictionary:nil
                                                          successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                                              successBlock(operation, response);
                                                          } failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                                              failBlock(operation, error);
                                                          }];
        }
        else{
            // Open a session showing the user the login UI
            // You must ALWAYS ask for public_profile permissions when opening a session
            [FBSession openActiveSessionWithReadPermissions:[[AppDelegate getAppDelegateReference] facebookPermissions]
                                               allowLoginUI:YES
                                          completionHandler:
             ^(FBSession *session, FBSessionState state, NSError *error) {
                 
                 // Retrieve the app delegate
                 // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
                 [[AppDelegate getAppDelegateReference] sessionStateChanged:session state:state error:error];
             }];
            
        }
//    }
    
}
+(BOOL)isProfileTutorialViewed{
    //Could put this in one line.
    return [[NSUserDefaults standardUserDefaults] boolForKey:kNSUserDefaultsReplayTutorialDidViewKey];
}

+(void)setReplayTutorialViewed{
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:kNSUserDefaultsReplayTutorialDidViewKey];
}
+(void)setReplayTutorialViewedTo:(BOOL)viewed{
    [[NSUserDefaults standardUserDefaults] setBool:viewed forKey:kNSUserDefaultsReplayTutorialDidViewKey];
}

+(BOOL)isThereAnyFBRecommendationsToBeViewed{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kNSUserDefaultsFBRecommendationShouldDisplayView];
}
+(void)setFBRecommendationNeedsDisplayingWithBOOL:(BOOL)viewed{
    [[NSUserDefaults standardUserDefaults] setBool:viewed forKey:kNSUserDefaultsFBRecommendationShouldDisplayView];
}


+(void)setCheckFBLikePermissionActiveWithBool:(BOOL)viewed{
    [[NSUserDefaults standardUserDefaults] setBool:viewed forKey:kNSUserDefaultsFBCheckLikePermissionActive];
}
+(BOOL)isCheckFBLikePermissionActive{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kNSUserDefaultsFBCheckLikePermissionActive];
}

@end
