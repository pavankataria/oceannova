//
//  OXCookieManager.m
//  OceanX
//
//  Created by Systango on 30/01/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXCookieManager.h"

@implementation OXCookieManager

+ (OXCookieManager*)sharedManager {
    static OXCookieManager * sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

#pragma mark public method

// store cookies just after Login/Signup
- (void)saveSessionCookiesForURL:(NSURL *)url{
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    NSArray *httpCookies = [cookies cookiesForURL:url];
    NSMutableArray *sessionCookies = [[NSMutableArray alloc]init];
    for (NSHTTPCookie* cookie in httpCookies)
    {
        [sessionCookies addObject:cookie.properties];
        if([cookie.name isEqualToString:kCookieName]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedInKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"COOKIES STORED");
            break;
        }
    }
    if(sessionCookies.count){
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:sessionCookies forKey:kSessionCookiesKey];
        [userDefaults synchronize];
    }
    
    [[OXLocationManager sharedTracker] start];
}
//-(void)pkSave
//NSLog(@"SAVED SESSION COOKIES: %@", sessionCookies);
//
//for (NSHTTPCookie* cookie in httpCookies){
//    if([cookie.name isEqualToString:kCookieName]){
//        NSDictionary * cookieProperties = cookie.properties;
//        [[NSUserDefaults standardUserDefaults] setObject:cookieProperties forKey:kCookieKey];
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedInKey];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        NSLog(@"COOKIES STORED");
//        break;
//    }
//}

// clear cookies on logout
- (void)clearSavedSessionCookies
{
    for (NSHTTPCookie *cookie in [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:kSessionCookiesKey];
    [userDefaults removeObjectForKey:kLoggedInKey];
    [userDefaults removeObjectForKey:kSavedUserNameKey];

    [userDefaults synchronize];
}

// restore cookies after app restart
- (void)assignedSavedSessionCookies
{
    NSMutableArray *httpCookies = [self getSavedSessionCookies];
    if(httpCookies.count)
    {
        for (NSDictionary* cookieProperties in httpCookies)
        {
            NSHTTPCookie *cookie = [[NSHTTPCookie alloc] initWithProperties:cookieProperties];
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        }
    }
}

#pragma mark private method

// get cookies from user defaults
- (NSMutableArray *)getSavedSessionCookies
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *httpCookies = nil;
    
    httpCookies = [userDefaults objectForKey:kSessionCookiesKey];
    return httpCookies;
}

@end
