//
//  OXServerRequest.m
//  OceanX
//
//  Created by Systango on 1/30/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#define SESSION_EXPIRED 403
#import "OXServerRequest.h"


NSString * const kErrorCode = @"errorCode";
NSString * const kErrorCodeWithDescription = @"errorCode:";

@interface OXServerRequest()
@property (nonatomic,strong) NSMutableArray *pendingRequests;
@property (atomic, assign) BOOL sessionExpiredFlag;

@end

@implementation OXServerRequest

+ (OXServerRequest*)sharedManager {
    static OXServerRequest * sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id) init{
    
    self = [super init];
    self.securityPolicy.validatesDomainName = YES;
    [self setJSONHeaders];
    self.pendingRequests = [NSMutableArray array];
    
    return self;
}

-(void) setJSONHeaders{
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
}
-(void)setHTTPHeaders{
    self.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.requestSerializer = [AFHTTPRequestSerializer serializer];
    [self.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];

}
# pragma mark server call management methods

-(AFHTTPRequestOperation *) httpPostRequest: (NSString*) url parameters:(NSDictionary*) dictionary success: (void (^)(AFHTTPRequestOperation *operation, id response))successBlock fail:(void (^)(AFHTTPRequestOperation *operation, id error)) failBlock{
    
    self.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.requestSerializer = [AFHTTPRequestSerializer serializer];
    [self.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
     ];

    return [self POST:url parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self setJSONHeaders];
        NSDictionary * dictReponse = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if ([self isFailureResponse:dictReponse]){
            failBlock(operation, dictReponse);
        }else{
            successBlock(operation, dictReponse);
        }
        [self setJSONHeaders];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self setJSONHeaders];
        failBlock(operation, error);
    }];
}

-(AFHTTPRequestOperation *) getRequest: (NSString*) url parameters:(NSDictionary*) parameters success:(SuccessCompletionBlock)successBlock fail:(FailureCompletionBlock) failBlock{
    
    return [self GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isFailureResponse:responseObject]) {
            failBlock(operation, responseObject);
        }
        else {
            successBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        if(![self checkExpiredSession:operation requestType:RequestGET url:url params:parameters successBlock:successBlock failBlock:failBlock]){
            failBlock(operation, error);
        }
    }];
}

-(AFHTTPRequestOperation *) postRequest: (NSString*) url parameters:(NSDictionary*) parameters success: (SuccessCompletionBlock) successBlock fail:(FailureCompletionBlock) failBlock{
    
    return [self POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isFailureResponse:responseObject]) {
            failBlock(operation, responseObject);
        }else{
            successBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        if(![self checkExpiredSession:operation requestType:RequestPOST url:url params:parameters successBlock:successBlock failBlock:failBlock]){
            failBlock(operation, error);
        }
        else{
//            failBlock(operation, error);
            NSLog(@"LANDS HERE AND NOTHING HAPPENS");
        }
    }];
}

-(AFHTTPRequestOperation *) putRequest: (NSString*) url parameters:(NSDictionary*) parameters success:(SuccessCompletionBlock)successBlock fail:(FailureCompletionBlock) failBlock{
    return [self PUT:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject){
        if ([self isFailureResponse:responseObject]) {
            failBlock(operation, responseObject);
        }else{
            successBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation * operation, NSError * error){
        if(![self checkExpiredSession:operation requestType:RequestPUT url:url params:parameters successBlock:successBlock failBlock:failBlock]){
            failBlock(operation, error);
        }
    }];
}

-(AFHTTPRequestOperation *) deleteRequest: (NSString*) url parameters:(NSDictionary*) parameters success:(SuccessCompletionBlock)successBlock fail:(FailureCompletionBlock) failBlock{
    
    return [self DELETE:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([self isFailureResponse:responseObject]) {
            failBlock(operation, responseObject);
        }else{
            successBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        if(![self checkExpiredSession:operation requestType:RequestDELETE url:url params:parameters successBlock:successBlock failBlock:failBlock]){
            failBlock(operation, error);
        }
    }];
}

#pragma mark - Private methods

- (BOOL)isFailureResponse:(id)responseObject{
//    NSLog(@"response Obkect: %@", responseObject);
    return ([responseObject isKindOfClass:NSDictionary.class] && ([responseObject objectForKey:kErrorCode] || [responseObject objectForKey:kErrorCodeWithDescription]));
}

#pragma mark session expiry handling methods

-(BOOL) checkExpiredSession:(AFHTTPRequestOperation *) operation requestType:(ServerRequestType)requestType url:(NSString *)url params:(NSDictionary *)params successBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock)failBlock{
    
    if ([url isEqualToString:[OXRestAPICient completeURLByURLPath:kHTTPClientURLStringSignUpUser]] || [url isEqualToString:[OXRestAPICient completeURLByURLPath:kHTTPClientURLStringRequestPasswordReset]]) {
        return NO;
    }
    else if(self.sessionExpiredFlag){
        NSLog(@"session expired");
        //call fails due to session expired
        //add canceled request to |pendingRequests|
        [self addPendingRequestInfoWithRequestType:requestType url:url params:params successBlock:successBlock failBlock:failBlock];
        return YES;
    }
    else if (operation.response.statusCode == SESSION_EXPIRED){
        NSLog(@"session expired status code = %lu", (long)operation.response.statusCode);
        self.sessionExpiredFlag = YES;
        [self.pendingRequests removeAllObjects];
        //add failed request(due to session expire) to |pendingRequests|
        [self addPendingRequestInfoWithRequestType:requestType url:url params:params successBlock:successBlock failBlock:failBlock];
        NSLog(@"pending requests count: %lu", (unsigned long)self.pendingRequests.count);
        //cancel all requests
        [self cancelAllRequests];
        
        // perform renew-login after 1 sec, so that we have all the failed requests in |pendingRequests| array before Re-Login
        [[OXLoginManager sharedManager] performSelector:@selector(performRenewLogin) withObject:nil afterDelay:1.0];
        return YES;
    }
    return NO;
}

- (void)addPendingRequestInfoWithRequestType:(ServerRequestType)requestType url:(NSString *)url params:(NSDictionary *)params successBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock)failBlock{
    
    OCNRequestMetadata *requestInfo = [OCNRequestMetadata initWithRequestType:requestType url:url params:params successBlock:successBlock failBlock:failBlock];
    if([self.pendingRequests indexOfObject:requestInfo] == NSNotFound)
        [self.pendingRequests addObject:requestInfo];
}



- (void)executePendingRequests{
    self.sessionExpiredFlag = NO;
    for (OCNRequestMetadata *requestInfo in self.pendingRequests){
        // perform failure calls because of session expired
        switch (requestInfo.requestType){
            case RequestGET:
                NSLog(@"DOING GET REQUEST AGAIN");
                [self getRequest:requestInfo.requestURL parameters:requestInfo.requestParameters success:requestInfo.successCompletionBlock fail:requestInfo.failureCompletionBlock];
                break;
                
            case RequestPOST:
                [self postRequest:requestInfo.requestURL parameters:requestInfo.requestParameters success:requestInfo.successCompletionBlock fail:requestInfo.failureCompletionBlock];
                break;
            case RequestPUT:
                [self putRequest:requestInfo.requestURL parameters:requestInfo.requestParameters success:requestInfo.successCompletionBlock fail:requestInfo.failureCompletionBlock];
                break;
            case RequestDELETE:
                [self deleteRequest:requestInfo.requestURL parameters:requestInfo.requestParameters success:requestInfo.successCompletionBlock fail:requestInfo.failureCompletionBlock];
                break;
            default:
                break;
        }
    }
    [self.pendingRequests removeAllObjects];
}

-(void) cancelAllRequests{
    [self.operationQueue cancelAllOperations];
}

-(void)cancelAndRemoveAllPendingRequests{
    self.sessionExpiredFlag = NO;
    [self cancelAllRequests];
    [self.pendingRequests removeAllObjects];
}


@end
