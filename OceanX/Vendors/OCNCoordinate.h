//
//  OCNCoordinate.h
//  Ocean
//
//  Created by Sam Payne on 30/06/2014.
//  Copyright (c) 2014 Ocean. All rights reserved.
//

@interface OCNCoordinate : MTLModel <MTLJSONSerializing>

@property (nonatomic,strong) NSString * positionTime;
@property (nonatomic) double coordinateX;
@property (nonatomic) double coordinateY;


@end
