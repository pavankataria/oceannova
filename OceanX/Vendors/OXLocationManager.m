//
//  OXLocationManager.m
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXLocationManager.h"
#import "OXLoginManager.h"

#define X_DISTANCE 1000
#define Y_DISTANCE 1000
#define DEFAULT_X0 -0.53009
#define DEFAULT_X1 0.299377
#define DEFAULT_Y0 51.287688
#define DEFAULT_Y1 51.728729


#define DEFAULT_X_CENTER -0.127758
#define DEFAULT_Y_CENTER 51.507351

@interface OXLocationManager()

@property (strong, nonatomic) CLLocationManager* manager;
@property (strong, nonatomic) NSMutableArray *observers;

@end

@implementation OXLocationManager
static int errorCount = 0;
static bool shownMessage = false;
#define MAX_LOCATION_ERROR 3


+ (OXLocationManager*)sharedTracker {
    static OXLocationManager *sharedInstance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype) init {
    self = [super init];
    if(self) {
        _manager = [[CLLocationManager alloc] init];
        _manager.delegate = self;
        
        //Must check authorizationStatus before initiating a CLLocationManager
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        if (status == kCLAuthorizationStatusRestricted && status == kCLAuthorizationStatusDenied) {
            NSLog(@"DENIED RESTRICTED ACCESS");
        } else {
            _manager = [[CLLocationManager alloc] init];
            _manager.delegate = self;
            _manager.desiredAccuracy = kCLLocationAccuracyBest;
        }
        if (status == kCLAuthorizationStatusNotDetermined) {
            //Must check if selector exists before messaging it
            if ([_manager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [_manager requestWhenInUseAuthorization];
            }
        }
        _observers = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) addLocationManagerDelegate:(id<GFLocationManagerDelegate>)delegate {
    if (![self.observers containsObject:delegate]) {
        [self.observers addObject:delegate];
    }
    [self.manager startUpdatingLocation];
}

- (void) removeLocationManagerDelegate:(id<GFLocationManagerDelegate>)delegate {
    if ([self.observers containsObject:delegate]) {
        [self.observers removeObject:delegate];
    }
}


#pragma mark - Location Manager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    [self.manager stopUpdatingLocation];
    CLLocation *crnLoc = [locations lastObject];

    for(id<GFLocationManagerDelegate> observer in self.observers) {
        if (observer) {
            [observer locationManagerDidUpdateLocation:crnLoc];
        }
    }
    
    
    NSLog(@"did update locations: lat: %.8f long: %.8f", crnLoc.coordinate.latitude, crnLoc.coordinate.longitude);
    [self sendUserCoordinateToTheServerWithCoordinate:crnLoc.coordinate];
}

-(void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    errorCount += 1;
    if(errorCount >= MAX_LOCATION_ERROR) {
        [self.manager stopUpdatingLocation];
        errorCount = 0;
    }
}




-(void)start{
    NSLog(@"\n\nSTARTED UPDATING LOCATION\n\n");
    [self.manager startUpdatingLocation];
    [OXRestAPICient setUserLocationWithCoordinate:[[OXLocationManager sharedTracker] userLocation] successBlock:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"success user location response: %@ ", operation);
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        NSLog(@"set user location FAILED WITH ERROR: %@", error);
        
    }];
}

-(void)pause{
    [self.manager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            NSLog(@"User still thinking..");
        } break;
        case kCLAuthorizationStatusDenied: {
            NSLog(@"User hates you");
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            NSLog(@"location updated immediately");

            
            
            
            [self.manager startUpdatingLocation]; //Will update location immediately
        } break;
        default:
            break;
    }
}

-(CLLocationCoordinate2D) currentLocation{
    if ([[OCNLocationTracker defaultSearchArea] doesContainPoint:self.manager.location.coordinate]){
        return self.manager.location.coordinate;
    }
    else{
        return CLLocationCoordinate2DMake(DEFAULT_Y_CENTER, DEFAULT_X_CENTER);
    }
}
//This method returns the user location if location services are enabled other it returns nil

-(OCNCoordinate*) userLocation{
    OCNCoordinate * coordinate = [[OCNCoordinate alloc] init];
    CLLocationCoordinate2D location = [self currentLocation];
    coordinate.coordinateX = location.longitude;
    coordinate.coordinateY = location.latitude;
    
    NSNumberFormatter *format = [[NSNumberFormatter alloc]init];
    [format setRoundingMode:NSNumberFormatterRoundHalfUp];
    coordinate.positionTime = [format stringFromNumber:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]*1000]];
    return coordinate;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex: %lu", (long)buttonIndex);
    if(alertView.tag == 121){
        shownMessage = TRUE;
        if(buttonIndex == 1){
            //code for opening settings app in iOS 8
            [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
}

-(BOOL)isLocationsServicesAvailable{
    if([CLLocationManager locationServicesEnabled]&&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied){
        return YES;
    }
    else{
        return NO;
    }
}



-(MKCoordinateRegion) currentMapRegion{
    return MKCoordinateRegionMakeWithDistance([self currentLocation], X_DISTANCE*2 , Y_DISTANCE*2);
}

-(MKCoordinateRegion) regionWithLocation:(CLLocationCoordinate2D)location{
    return MKCoordinateRegionMakeWithDistance(location, X_DISTANCE*2 , Y_DISTANCE*2);
}

-(void)sendUserCoordinateToTheServerWithCoordinate:(CLLocationCoordinate2D)coordinate{
    
    if([OXLoginManager isLoggedIn]){
        [OXRestAPICient updateUserLocationWithCoordinate:coordinate
                                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                                NSLog(@"user location updated succesfully. Response: %@", response);
                                            } failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                                NSLog(@"user location update failed. Error: %@", error);
                                            }];
    }
}

@end