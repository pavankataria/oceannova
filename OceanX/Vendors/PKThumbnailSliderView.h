//
//  PKThumbnailSliderView.h
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKNewEditMediaContentViewProtocols.h"
#import "PKThumbnailView.h"

#pragma mark - PKThumbnailSliderView interface - Methods

@interface PKThumbnailSliderView : UIView<PKPreviewSliderViewDelegate, PKEditMediaThumbnailViewDelegate, PKThumbnailViewDelegate>

@property (nonatomic, retain) IBOutlet UIScrollView *thumbnailSlider;
@property (nonatomic, weak) id <PKThumbnailSliderViewDelegate> delegate;
@property (nonatomic, weak) id <PKThumbnailSliderViewDelegate> parentDelegate;

//OXProcessedPictures
@property (nonatomic, retain) NSArray *images;
@property (nonatomic, retain) NSArray *urlLoadedImageThumbnails;
-(void)setAlreadyLoadedImages:(NSArray*)images;

-(instancetype)initWithFrame:(CGRect)frame;

@end
