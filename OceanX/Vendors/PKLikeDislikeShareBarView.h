//
//  PKLikeDislikeShareBarView.h
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKNewEditMediaContentViewProtocols.h"

@interface PKLikeDislikeShareBarView : UIView
@property (nonatomic, retain) UIButton *likeButton;
@property (nonatomic, retain) UIButton *dislikeButton;
@property (nonatomic, retain) UIButton *maybeButton;

@property (nonatomic, retain) UIButton *shareButton;
@property (nonatomic, weak) id <PKLikeDislikeShareBarViewDelegate> delegate;
-(instancetype)initWithFrame:(CGRect)frame;
@property (nonatomic, retain) OXPOIModel *poiObject;

@end
