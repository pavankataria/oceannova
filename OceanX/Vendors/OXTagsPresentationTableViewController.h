//
//  OXTagsPresentationTableViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 18/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXDetailPOIProtocol.h"

@interface OXTagsPresentationTableViewController : UITableViewController<UITextFieldDelegate>
@property (nonatomic, retain) OXPOIModel *poiObject;
@property (nonatomic, weak) id <OXDetailPOIProtocol> delegate;

@property (nonatomic, assign) OXTagsPresentationTableViewComingFromVCType comingFromVC;


@end
