//
//  StatusHUDSBViewController.m
//  OceanX
//
//  Created by Systango on 1/27/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "StatusHUDSBViewController.h"

@interface StatusHUDSBViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *statusImageView;
@property (strong, nonatomic) IBOutlet UILabel *statusTitleLabel;
@property (strong, nonatomic) IBOutlet UITextView *statusMessageTextView;
@property (strong, nonatomic) IBOutlet UIButton *okButton;
@property (strong, nonatomic) IBOutlet UIButton *tryAgainButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;

@end

@implementation StatusHUDSBViewController

+(instancetype)sharedManager{
    static StatusHUDSBViewController * sharedManager = nil;
    
   static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StatusHUDSB" bundle:nil];
        sharedManager = [storyboard instantiateInitialViewController];
    });
    return sharedManager;
}

#pragma  mark - ViewController Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    [self prefersStatusBarHidden];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Public Methods

- (void)displaySuccessStatusWithTitle:(NSString*)title message:(NSString *)message

{
    [self displaySuccessStatusWithTitle:title message:message imageName:nil];
}

- (void)displayErrorStatusWithTitle:(NSString *)title message:(NSString *)message
{
    [self displayErrorStatusWithTitle:title message:message imageName:nil];
}

//#warning todo: We intentionally call |setlayoutViewWithTitle| method because we have to differentiate Success and Failure for future
- (void)displaySuccessStatusWithTitle:(NSString *)title message:(NSString *)message imageName:(NSString *)imageName
{
    [self setlayoutViewWithTitle:title message:message imageName:imageName displayTryAgain:NO];
}

//#warning todo: We intentionally call |setlayoutViewWithTitle| method because we have to differentiate Success and Failure for future
- (void)displayErrorStatusWithTitle:(NSString *)title message:(NSString *)message imageName:(NSString *)imageName
{
    [self setlayoutViewWithTitle:title message:message imageName:imageName displayTryAgain:NO];
}

//#warning todo: We intentionally call |setlayoutViewWithTitle| method because we have to differentiate TryAgain button style with OK button style for future
- (void)displayTryAgainWithSuccessTitle:(NSString*)title message:(NSString *)message imageName:(NSString *)imageName

{
    [self setlayoutViewWithTitle:title message:message imageName:imageName displayTryAgain:YES];
}

- (void)displayTryAgainWithErrorTitle:(NSString*)title message:(NSString *)message imageName:(NSString *)imageName
{
    [self setlayoutViewWithTitle:title message:message imageName:imageName displayTryAgain:YES];
}

- (void)setlayoutViewWithTitle:(NSString*)title message:(NSString *)message imageName:(NSString *)imageName displayTryAgain:(BOOL)displayTryAgain
{
    
    [[UIApplication sharedApplication].keyWindow addSubview:self.view];

//    [ presentViewController:self animated:NO completion:nil];
    [self.statusImageView setImage:[UIImage imageNamed:imageName]];
    self.statusTitleLabel.text = title;
    self.statusMessageTextView.text = message;
    self.tryAgainButton.hidden = !displayTryAgain;
    self.cancelButton.hidden = !displayTryAgain;
    self.okButton.hidden = displayTryAgain;
}

- (void)dismiss
{
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.view removeFromSuperview];

}

#pragma mark - IBoutlet Method

- (IBAction)okButtonPressed:(id)sender {
    [self dismiss];
}
- (IBAction)tryAgainButtonPressed:(id)sender {
    
}
- (IBAction)cancelButtonPressed:(id)sender {
    [self dismiss];
}

#pragma mark - Private Method
- (BOOL)prefersStatusBarHidden {
    return YES;
}
@end
