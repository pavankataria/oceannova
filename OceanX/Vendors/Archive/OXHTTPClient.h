//
//  OXHTTPClient.h
//
//  Created by Pavan Kataria on 20/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface OXHTTPClient : AFHTTPRequestOperationManager
+ (OXHTTPClient *)sharedOXHTTPClient;
- (instancetype)initWithBaseURL:(NSURL *)url;

+(void)getTagsAutoSuggestionWithParams:(NSDictionary*)params
                   withCompletionBlock:(void (^)(AFHTTPRequestOperation *operation, id object, NSError *error))completionBlock;
+(void)loginWithUsername:(NSString*)username andPassword:(NSString*)password withCompletionBlock:(void (^)(AFHTTPRequestOperation *operation, id object, NSError *error))completionBlock;

+(void)getMetaDataWithCompletionBlock:(void(^)(id object, NSError *error))completionBlock;
@end
