//
//  TBCoordinateQuadTree.m
//  TBAnnotationClustering
//
//  Created by Theodore Calmes on 9/27/13.
//  Copyright (c) 2013 Theodore Calmes. All rights reserved.
//

#import "TBCoordinateQuadTree.h"
#import "OXAnnotation.h"

#import "OXBasePOIModel.h"


typedef struct PoiInfo {
    long poiID;
    double poiScore;
    char* poiURLString;
} PoiInfo;


TBQuadTreeNodeData TBDataFromPOI(OXBasePOIModel *poiModel)
{
    double latitude = poiModel.coordinates.coordinate.latitude;
    double longitude = poiModel.coordinates.coordinate.longitude;
    
    PoiInfo* poiInfo = malloc(sizeof(PoiInfo));
    
    NSInteger placeID = poiModel.poiID;
    poiInfo->poiID = placeID;
    
    NSNumber *poiScore = poiModel.poiScore;
    poiInfo->poiScore = [poiScore doubleValue];
    
    if (poiModel.thumbnailUrl != nil) {
        NSString *placeURLString = poiModel.thumbnailUrl.absoluteString;
        poiInfo->poiURLString = malloc(sizeof(char) * placeURLString.length + 1);
        strncpy(poiInfo->poiURLString, [placeURLString UTF8String], placeURLString.length + 1);
    }
    
    return TBQuadTreeNodeDataMake(latitude, longitude, poiInfo);
}

TBBoundingBox TBBoundingBoxForMapRect(MKMapRect mapRect)
{
    CLLocationCoordinate2D topLeft = MKCoordinateForMapPoint(mapRect.origin);
    CLLocationCoordinate2D botRight = MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMaxX(mapRect), MKMapRectGetMaxY(mapRect)));

    CLLocationDegrees minLat = botRight.latitude;
    CLLocationDegrees maxLat = topLeft.latitude;

    CLLocationDegrees minLon = topLeft.longitude;
    CLLocationDegrees maxLon = botRight.longitude;

    return TBBoundingBoxMake(minLat, minLon, maxLat, maxLon);
    
}

MKMapRect TBMapRectForBoundingBox(TBBoundingBox boundingBox)
{
    MKMapPoint topLeft = MKMapPointForCoordinate(CLLocationCoordinate2DMake(boundingBox.x0, boundingBox.y0));
    MKMapPoint botRight = MKMapPointForCoordinate(CLLocationCoordinate2DMake(boundingBox.xf, boundingBox.yf));

    return MKMapRectMake(topLeft.x, botRight.y, fabs(botRight.x - topLeft.x), fabs(botRight.y - topLeft.y));
}

NSInteger TBZoomScaleToZoomLevel(MKZoomScale scale)
{
    double totalTilesAtMaxZoom = MKMapSizeWorld.width / 256.0;
    NSInteger zoomLevelAtMaxZoom = log2(totalTilesAtMaxZoom);
    NSInteger zoomLevel = MAX(0, zoomLevelAtMaxZoom + floor(log2f(scale) + 0.5));

    return zoomLevel;
}

float TBCellSizeForZoomScale(MKZoomScale zoomScale)
{
    NSInteger zoomLevel = TBZoomScaleToZoomLevel(zoomScale);
    
    switch (zoomLevel) {
        case 13:
        case 14:
        case 15:
            return 64;
        case 16:
        case 17:
        case 18:
            return 32;
        case 19:
            return 16;

        default:
            return 88;
    }
}

@implementation TBCoordinateQuadTree


- (void)buildTreeWithElements:(NSArray *)mapElements
{
    @autoreleasepool {
        
//       Library have one less annotation and we passed all elements count
//        NSInteger count = mapElements.count - 1;
        NSInteger count = mapElements.count;
        
        TBQuadTreeNodeData *dataArray = malloc(sizeof(TBQuadTreeNodeData) * count);
        for (NSInteger i = 0; i < count; i++) {
            dataArray[i] = TBDataFromPOI(mapElements[i]);
        }

//Points were not plotting on map due to commented code, passed visible rect for London
//        TBBoundingBox world = TBBoundingBoxMake(19, -166, 72, -53);
        TBBoundingBox world = TBBoundingBoxForMapRect(self.mapView.visibleMapRect);
        _root = TBQuadTreeBuildWithData(dataArray, (int)count, world, 4);
    }
}

- (NSArray *)clusteredAnnotationsWithinMapRect:(MKMapRect)rect withZoomScale:(double)zoomScale
{
    double TBCellSize = TBCellSizeForZoomScale(zoomScale);
    double scaleFactor = zoomScale / TBCellSize;

    NSInteger minX = floor(MKMapRectGetMinX(rect) * scaleFactor);
    NSInteger maxX = floor(MKMapRectGetMaxX(rect) * scaleFactor);
    NSInteger minY = floor(MKMapRectGetMinY(rect) * scaleFactor);
    NSInteger maxY = floor(MKMapRectGetMaxY(rect) * scaleFactor);

    NSMutableArray *clusteredAnnotations = [[NSMutableArray alloc] init];
    for (NSInteger x = minX; x <= maxX; x++) {
        for (NSInteger y = minY; y <= maxY; y++) {
            MKMapRect mapRect = MKMapRectMake(x / scaleFactor, y / scaleFactor, 1.0 / scaleFactor, 1.0 / scaleFactor);
            
            __block double totalX = 0;
            __block double totalY = 0;
            __block int count = 0;

            NSMutableArray *placeIDs = [[NSMutableArray alloc] init];
            NSMutableArray *poiScores = [[NSMutableArray alloc] init];
            NSMutableArray *placesImageURLStrings = [[NSMutableArray alloc] init];
            NSMutableArray *clusterAnnotations = [[NSMutableArray alloc] init];
            TBQuadTreeGatherDataInRange(self.root, TBBoundingBoxForMapRect(mapRect), ^(TBQuadTreeNodeData data) {
                totalX += data.x;
                totalY += data.y;
                count++;

                PoiInfo poiInfo = *(PoiInfo *)data.data;
                [placeIDs addObject:[NSNumber numberWithLong:poiInfo.poiID]];
                [poiScores addObject:[NSNumber numberWithDouble:poiInfo.poiScore]];
                
                
                [placesImageURLStrings addObject:[NSString stringWithFormat:@"%s", poiInfo.poiURLString]];
                
                CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(data.x, data.y);
                NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%s", poiInfo.poiURLString]];
                OXAnnotation *annotation = [[OXAnnotation alloc] initWithID:poiInfo.poiID Location:coordinate badgeCount:count imageURL:URL poiScrore:[NSNumber numberWithFloat:poiInfo.poiScore]];
                [clusterAnnotations addObject:annotation];
            });

            
            if (count == 1) {
                CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(totalX, totalY);
                OXAnnotation *annotation = [[OXAnnotation alloc] initWithID:[[placeIDs lastObject] integerValue] Location:coordinate badgeCount:count imageURL:[NSURL URLWithString:[placesImageURLStrings lastObject]] poiScrore:[poiScores lastObject]];
                [clusteredAnnotations addObject:annotation];
            }
            
            if (count > 1) {
                CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(totalX / count, totalY / count);
                OXAnnotation *annotationWithHighestPOIScore = [self getHighestPoiScoreAnnotation:clusterAnnotations];
                OXAnnotation *annotation = [[OXAnnotation alloc] initWithID:annotationWithHighestPOIScore.annotationID Location:coordinate badgeCount:count imageURL:annotationWithHighestPOIScore.imageURL poiScrore:[NSNumber numberWithDouble:0.0]];
                
                annotation.clusterAnnotations = [NSArray arrayWithArray:clusterAnnotations];
                [clusteredAnnotations addObject:annotation];
            }
        }
    }
    return [NSArray arrayWithArray:clusteredAnnotations];
}

- (OXAnnotation *)getHighestPoiScoreAnnotation:(NSArray*)mapElements{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kScoreKey ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    return [[mapElements sortedArrayUsingDescriptors:sortDescriptors] lastObject];
}


@end
