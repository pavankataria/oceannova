//
//  PKThumbnailSliderView.m
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKThumbnailSliderView.h"
#import "PKEditMediaThumbnailView.h"
#import "OXProcessedPicture.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "OXImageStackItem.h"
#import "OXCameraCaptureScreenViewController.h"



@implementation PKThumbnailSliderView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(void)awakeFromNib{
    [self setViewProperties];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.thumbnailSlider = [[UIScrollView alloc] initWithFrame:frame];
        [self addSubview:self.thumbnailSlider];
        [self setViewProperties];
        
        self.thumbnailSlider.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        //        self.thumbnailSlider.pagingEnabled = YES;
    }
    return self;
}
-(instancetype)init{
    return [self initWithFrame:CGRectZero];
}
-(void)setViewProperties{
    NSLog(@"set view properties");
    [self.thumbnailSlider setBackgroundColor:[UIColor blackColor]];
}

-(CGRect)getNextFrameForAlreadyLoadedImagesThumbnailPosition:(NSUInteger)position{
    return [self getNextFrameForThumbnailPosition:position withPreferredHeight:170];
}
-(void)setAlreadyLoadedImages:(NSArray*)images{
    _images = images;
    [self updateThumbnailImages];
}

-(void)updateThumbnailImages{
    [self.thumbnailSlider.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//    NSLog(@"already Loaded Images: %@", self.images);
    CGFloat maxContentWidthForThumbnailsTray = 0;
    for(NSUInteger position = 0; position < _images.count/* + cameraIconThumbnailOffset*/; position++){
        CGRect nextThumbnailFrame = [self getNextFrameForAlreadyLoadedImagesThumbnailPosition:position];
        CGRect currentThumbnailFrame = CGRectMake(0, 0, CGRectGetWidth(nextThumbnailFrame), CGRectGetHeight(nextThumbnailFrame));
//        NSLog(@"next thumbnail frame: %@", NSStringFromCGRect(nextThumbnailFrame));
//        NSLog(@"current thumbnail frame: %@", NSStringFromCGRect(currentThumbnailFrame));
        
        PKThumbnailView *currentThumbnail = [[PKThumbnailView alloc] initWithFrame:currentThumbnailFrame];
        [currentThumbnail setupDeleteButton];
        currentThumbnail.margin = 5;
        currentThumbnail.frame = nextThumbnailFrame;
        currentThumbnail.index = position/* + cameraIconThumbnailOffset*/;
        currentThumbnail.delegate = self;
        
        OXImageStackItem *currentPicture = _images[position];
        
        currentThumbnail.previewThumbnail.image = currentPicture.thumbnailImage;
        [currentThumbnail.previewThumbnail.layer setCornerRadius:15];
        [currentThumbnail.previewThumbnail.layer setMasksToBounds:YES];
        maxContentWidthForThumbnailsTray += CGRectGetWidth(currentThumbnail.frame);
        [self.thumbnailSlider addSubview:currentThumbnail];
    }
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.thumbnailSlider.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.thumbnailSlider.contentSize = contentRect.size;
}


CGRect CGRectIntegralCenteredInRect(CGRect innerRect, CGRect outerRect){
    CGFloat originX = floorf((outerRect.size.width - innerRect.size.width) * 0.5f);
    CGFloat originY = floorf((outerRect.size.height - innerRect.size.height) * 0.5f);
    return CGRectMake(originX, originY, innerRect.size.width, innerRect.size.height);
}




-(void)setImages:(NSArray *)images{
    [self.thumbnailSlider.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSLog(@"IMAGES: %@", images);
    
    
    _images = [images mutableCopy];
    CGFloat maxContentWidthForThumbnailsTray = 0;
    NSUInteger cameraIconThumbnailOffset = 1;
    
    NSMutableArray *tempURLLoadedImageThumbnails = [[NSMutableArray alloc] init];

    for(NSUInteger position = 0; position < _images.count + cameraIconThumbnailOffset; position++){
        CGRect nextThumbnailFrame = [self getNextFrameForThumbnailPosition:position];
        CGRect currentThumbnailFrame = CGRectMake(0, 0, CGRectGetWidth(nextThumbnailFrame), CGRectGetHeight(nextThumbnailFrame));
        PKEditMediaThumbnailView *currentThumbnail = [[PKEditMediaThumbnailView alloc] initWithFrame:currentThumbnailFrame];
        currentThumbnail.frame = nextThumbnailFrame;
        currentThumbnail.index = position + cameraIconThumbnailOffset;
        currentThumbnail.delegate = self;
        OXProcessedPicture *currentPicture;
        if(position == images.count){
            currentPicture = [[OXProcessedPicture alloc] init];
//            currentPicture.mainImageToBeUsedTemporarilyUntilProcessedImageURLSAreComingThrough = [UIImage imageNamed:@"cameraIcon"];
            currentThumbnail.previewThumbnail.image = currentPicture.mainImageToBeUsedTemporarilyUntilProcessedImageURLSAreComingThrough;
            currentThumbnail.cameraIcon = YES;
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = currentThumbnailFrame;
            [button setImage:[UIImage imageNamed:@"cameraIcon"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"cameraIcon-highlighted"] forState:UIControlStateHighlighted];
            [currentThumbnail addSubview:button];
            [button addTarget:self action:@selector(cameraIconDidSelect) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
            currentThumbnail.cameraIcon = FALSE;
            NSUInteger adjustedPosition = position;
            currentPicture = _images[adjustedPosition];
            [self loadImageForThumbnail:currentThumbnail andURL:[NSURL URLWithString:currentPicture.pictureURL] atIndex:adjustedPosition];
            [tempURLLoadedImageThumbnails addObject:currentThumbnail];
        }
        maxContentWidthForThumbnailsTray += CGRectGetWidth(currentThumbnail.frame);
        [self.thumbnailSlider addSubview:currentThumbnail];
        self.urlLoadedImageThumbnails = tempURLLoadedImageThumbnails;
    }
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.thumbnailSlider.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.thumbnailSlider.contentSize = contentRect.size;
    
    if(self.thumbnailSlider.contentSize.width > CGRectGetWidth(self.thumbnailSlider.bounds)){
        CGPoint endOffset = CGPointMake(self.thumbnailSlider.contentSize.width - self.thumbnailSlider.bounds.size.width, 0);
        [self.thumbnailSlider setContentOffset:endOffset animated:NO];
    }
}



-(void)loadImageForThumbnail:(PKEditMediaThumbnailView*)thumbnail andURL:(NSURL*)imageURL atIndex:(NSUInteger)index{
    
    NSString *imageStringURL = [[imageURL absoluteString] stringByReplacingOccurrencesOfString:@"ocean-bucket-test" withString:@"ocean-bucket"];
    NSURL *url = [NSURL URLWithString:imageStringURL];
    
    
    //    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //
    //    __weak PKEditMediaThumbnailView *weakThumbnail = thumbnail;
    //    __weak typeof(self) weakSelf = self;
    //
    ////    TODO : Need to replace delegate with SDImage method
    //    [weakThumbnail.previewThumbnail setImageWithURLRequest:request
    //                                          placeholderImage:nil
    //                                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
    //                                                       [UIView transitionWithView:weakThumbnail.previewThumbnail
    //                                                                         duration:0.1f
    //                                                                          options:UIViewAnimationOptionTransitionCrossDissolve
    //                                                                       animations:^{
    ////                                                                           weakThumbnail.previewThumbnail.image = image;
    ////                                                                           [weakSelf.delegate thumbnailSliderImageDownloaded:image atIndex:index];
    //                                                                       } completion:nil];
    //                                                       [weakThumbnail setNeedsLayout];
    //                                                   } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
    //                                                       NSLog(@"failed to load image: %@", error);
    //                                                   }];
    
    [thumbnail.previewThumbnail sd_setImageWithURL:url];
}



-(CGRect)getNextFrameForThumbnailPosition:(NSUInteger)position{
    return [self getNextFrameForThumbnailPosition:position withPreferredHeight:0];
}
-(CGRect)getNextFrameForThumbnailPosition:(NSUInteger)position withPreferredHeight:(CGFloat)preferredHeight{
    CGFloat thumbnailPreferredHeight = preferredHeight;
    CGFloat thumbnailHeight;
    
    if(preferredHeight != 0 && CGRectGetHeight(self.frame) > thumbnailPreferredHeight){
        thumbnailHeight = thumbnailPreferredHeight;
    }
    else{
        thumbnailHeight = CGRectGetHeight(self.frame);
    }
    CGFloat thumbnailWidth = thumbnailHeight;
    CGRect currentRect = CGRectMake(thumbnailWidth * position, CGRectGetHeight(self.frame) * 0.5 - thumbnailHeight * 0.5, thumbnailWidth, thumbnailHeight);
    return currentRect;
}

#pragma mark - PKEditMediaThumbnailViewDelegate - Methods
-(void)didSelectThumbnailPreviewAtIndexPosition:(NSUInteger)index{
    if ([self.parentDelegate respondsToSelector:@selector(didSelectThumbnail)]) {
        [self.parentDelegate didSelectThumbnail];
    }
}
-(void)cameraIconDidSelect{
    [self.parentDelegate didSelectCameraIcon];
}

#pragma mark - PKPreviewSliderDelegate methods - Methods
-(void)previewSliderView:(PKPreviewSliderView *)previewSliderView didSlideToPreviewIndex:(NSUInteger)index{
//    NSLog(@"did slide to preview index: %lu", index);
    [self highlightThumbnailAtIndex:index];
}


#pragma mark - instance - Methods

-(void)highlightThumbnailAtIndex:(NSUInteger)index{
    CGRect viewFrame = [self getNextFrameForAlreadyLoadedImagesThumbnailPosition:index];
//    NSLog(@"rect: %@", NSStringFromCGRect(viewFrame));
    CGPoint scrollPoint = viewFrame.origin;
//    NSLog(@"scroll point: %@", NSStringFromCGPoint(scrollPoint));
    [self.thumbnailSlider scrollRectToVisible:viewFrame animated:YES];
    
}


#pragma mark - PKThumbnailView delegate - Methods
-(void)pkThumbnailView:(PKThumbnailView*)thumbnailView didSelectThumbnailPreviewAtIndexPosition:(NSUInteger)index{
    [self.delegate thumbnailSlider:self didTapOnThumbnailIndex:index];
}
-(void)pkThumbnailView:(PKThumbnailView*)thumbnailView didDeleteThumbnailPreviewAtIndexPosition:(NSUInteger)index{
    if([self.parentDelegate respondsToSelector:@selector(thumbnailSlider:didDeleteThumbnailPreviewAtIndexPosition:)]){
        [self.parentDelegate thumbnailSlider:self didDeleteThumbnailPreviewAtIndexPosition:index];
    }
}

@end
