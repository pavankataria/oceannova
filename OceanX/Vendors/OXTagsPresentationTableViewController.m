//
//  OXTagsPresentationTableViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 18/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXTagsPresentationTableViewController.h"
#import "OXTagAffirmTableViewCell.h"
#import "OXProtocolsHeader.h"
#import "OXTagCenteredTableViewCell.h"
#import "OXTextfieldTableViewCell.h"
#import "OXAddTagPresentationViewController.h"


@interface OXTagsPresentationTableViewController ()

@end

@implementation OXTagsPresentationTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setScrollEnabled:NO];
    [self.tableView registerNib:[UINib nibWithNibName:@"OXTagAffirmTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellReuseIdentifierTagAffirm];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXTagCenteredTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellReuseIdentifierTagCenteredTag];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellReuseIdentifierEmptyTableViewCell];
    NSLog(@"self.tags.count: %lu",(unsigned long)[self.poiObject.tags count]);
    
    if(self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeTutorial){
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView methods - Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0 && self.comingFromVC != OXTagsPresentationTableViewComingFromVCTypeTutorial){
        return 60;
    }
    return 44;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeTutorial){

        if(indexPath.row < 3){
            OXTagAffirmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierTagAffirm];
            id <OXTagsProtocolConfiguration> object = self.poiObject.tags[indexPath.row];
            cell.tagNameLabel.text = [object tagName];
            
            NSString *firstCapChar = [cell.tagNameLabel.text substringToIndex:1];
            NSString *cappedString = [cell.tagNameLabel.text stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
            cell.tagNameLabel.text = cappedString;
            
            
            cell.tagCount.text = [NSString stringWithFormat:@"%lu", (unsigned long)[object tagCount]];
            [cell.upArrowButton addTarget:self action:@selector(upArrowButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            return cell;
        }
        else{
            NSUInteger tagCount = self.poiObject.tags.count;
            OXTagCenteredTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierTagCenteredTag];
            cell.centeredTagDisplayLabel.text = [self getShowMoreTagMessageFromCount:tagCount-3];
            
            cell.selectionStyle = UITableViewCellEditingStyleNone;
            return cell;
        }
    }
    else{
        
        if(indexPath.row == 0){
            NSLog(@"indexPath == 0");
            OXTextfieldTableViewCell *textFieldCell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierEmptyTableViewCell];
            textFieldCell.textField.delegate = self;
            
            [textFieldCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            textFieldCell.separatorInset = UIEdgeInsetsMake(0, textFieldCell.bounds.size.width, 0, 0);
            return textFieldCell;
        }
        else if(indexPath.row < 4){
            OXTagAffirmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierTagAffirm];
            id <OXTagsProtocolConfiguration> object = self.poiObject.tags[indexPath.row-1];
            
            cell.tagNameLabel.text = [object tagName];
            cell.tagCount.text = [NSString stringWithFormat:@"%lu", (unsigned long)[object tagCount]];            
            NSString *firstCapChar = [[cell.tagNameLabel.text substringToIndex:1] capitalizedString];
            NSString *cappedString = [cell.tagNameLabel.text stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
            cell.tagNameLabel.text = cappedString;

            [cell.upArrowButton addTarget:self action:@selector(upArrowButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"cell for row tagname: %@, tagHasAffirmed: %d", [object tagName], (int)[object tagHasAffirmed]);
            [cell upArrowButtonSetSelected:[object tagHasAffirmed]];
            
            if([object tagHasAffirmed]){
                cell.tagCount.textColor = [PKUIAppearanceController darkBlueColor];
            }
            else{
                cell.tagCount.textColor = rgb(140, 140, 140);
            }
            cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            return cell;
        }
        else{
            NSUInteger tagCount = self.poiObject.tags.count;
            OXTagCenteredTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierTagCenteredTag];
            cell.centeredTagDisplayLabel.text = [self getShowMoreTagMessageFromCount:tagCount-3];
            cell.centeredTagDisplayLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
            cell.centeredTagDisplayLabel.textColor = rgb(110, 110, 110);

            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width, 0, 0);

            cell.selectionStyle = UITableViewCellEditingStyleNone;
            return cell;
        }
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.comingFromVC == OXTagsPresentationTableViewComingFromVCTypeTutorial){
        return self.poiObject.tags.count;
    }
    else{
        if(self.poiObject.tags.count <= 3){
            return self.poiObject.tags.count+1;
        }
        else{
            return 5;
        }
    }
}


#pragma mark - Other methods - Methods
-(void)setPoiObject:(OXPOIModel *)poiObject{
    _poiObject = poiObject;
    [self.tableView reloadData];
    
//    [self.view setBackgroundColor:[UIColor blueColor]];
    
    [self.tableView layoutIfNeeded];
    CGSize tableViewSize = self.tableView.contentSize;
    CGRect viewFrame = self.view.frame;
    viewFrame.size = tableViewSize;
    self.view.frame = viewFrame;
    
    
    
//    [self.delegate updateDetailPOIScrollViewContentSize];
}



-(void)upArrowButtonPressed:(id)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    [self setAgreeCountForIndexPath:indexPath];
}
-(void)setAgreeCountForIndexPath:(NSIndexPath*)indexPath{
    OXTagAffirmTableViewCell *cell = (OXTagAffirmTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    NSInteger indexPathRow = 0;

    NSUInteger addTagsCellInIndexRowZeroOffset = 0;
    
    if(self.comingFromVC != OXTagsPresentationTableViewComingFromVCTypeTutorial){
        addTagsCellInIndexRowZeroOffset = 1;
        indexPathRow = indexPath.row - addTagsCellInIndexRowZeroOffset;
        NSLog(@"not coming from type tutorial indexpathrow: %lu", (long)indexPathRow);

    }
    else{
        NSLog(@"indexpathrow: %lu", (long)indexPathRow);
        indexPathRow = indexPath.row;
    }
    OXTagModel * currentTag = self.poiObject.tags[indexPath.row - addTagsCellInIndexRowZeroOffset];
    if([cell.upArrowButton isSelected]){
        currentTag.tagCount++;
        if(self.comingFromVC != OXTagsPresentationTableViewComingFromVCTypeTutorial){
            [currentTag setAffirm:YES withPOIID:self.poiObject.poiID];
            [currentTag setTagHasAffirmed:YES];
        }
    }
    else{
        currentTag.tagCount--;
        if(self.comingFromVC != OXTagsPresentationTableViewComingFromVCTypeTutorial){
            [currentTag setAffirm:NO withPOIID:self.poiObject.poiID];
            [currentTag setTagHasAffirmed:NO];
        }
    }
    cell.tagCount.text = [NSString stringWithFormat:@"%lu", (long)currentTag.tagCount];
    
    if([currentTag tagHasAffirmed]){
        cell.tagCount.textColor = [PKUIAppearanceController darkBlueColor];
    }
    else{
        cell.tagCount.textColor = rgb(140, 140, 140);
    }
}
-(void)pressUpArrowButtonForCellAtIndexPath:(NSIndexPath*)indexPath{
    OXTagAffirmTableViewCell *cell = (OXTagAffirmTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    NSLog(@"what kind of cell are you: %@", cell);
    [cell upArrowButtonSetSelected:!cell.upArrowButton.selected];
    [self setAgreeCountForIndexPath:indexPath];
}

//-(void)upArrowButtonPressed:(id)sender{
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
//    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
//    OXTagAffirmTableViewCell *cell = (OXTagAffirmTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
//    OXTagModel * currentTag = self.poiObject.tags[indexPath.row];
//    NSLog(@"tagname: %@, tagHasAffirmed: %d", [currentTag tagName], (int)[currentTag tagHasAffirmed]);
//    if([sender isSelected]){
//        NSLog(@"DESELECTED");
//        [sender setSelected:NO];
//        currentTag.tagCount--;
//        ((UIButton*)sender).tintColor = [UIColor lightGrayColor];
//        
//        if(self.comingFromVC != OXTagsPresentationTableViewComingFromVCTypeTutorial){
//            [OXRestAPICient disaffirmTagID:currentTag.tagId withPOIID:self.poiObject.poiID
//                              successBlock:^(AFHTTPRequestOperation *operation, id response) {
//                                  NSLog(@"successfulled disaffirm tag id");
//                              } failBlock:^(AFHTTPRequestOperation *operation, id error) {
//                                  NSLog(@"failed update tag id: %@", operation);
//                              }];
//        }
//    }
//    else{
//        NSLog(@"SELECTED");
//        [sender setSelected:YES];
//        currentTag.tagCount++;
//        ((UIButton*)sender).tintColor = [PKUIAppearanceController darkBlueColor];
//        if(self.comingFromVC != OXTagsPresentationTableViewComingFromVCTypeTutorial){
//
//            [OXRestAPICient affirmTagID:currentTag.tagId withPOIID:self.poiObject.poiID
//                           successBlock:^(AFHTTPRequestOperation *operation, id response) {
//                               NSLog(@"successfulled affirm tag id");
//                           } failBlock:^(AFHTTPRequestOperation *operation, id error) {
//                               NSLog(@"failed update tag id: %@", operation);
//                           }];
//
//        }
//    }
//    cell.tagCount.text = [NSString stringWithFormat:@"%lu agree", currentTag.tagCount];
//}

#pragma mark - UITextFieldDelegate - Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//    [self performSegueWithIdentifier:@"OXMultipleSearchViewControllerSegueIdentifier" sender:self];
    NSLog(@"text field should begin editing");
    
    if([self.delegate respondsToSelector:@selector(displayAddTagsViewController)])
        [self.delegate displayAddTagsViewController];
    
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"did select method clicked:%@", indexPath);

    if(self.poiObject.tags.count < 3){
        if(indexPath.row == self.poiObject.tags.count+1){
            [self showMoreTagsButtonWasClicked];
        }
        else{
            if(indexPath.row == 0){
                
            }
            else{
                [self pressUpArrowButtonForCellAtIndexPath:indexPath];
            }
        }
    }
    else{
        if(indexPath.row == 4){
            [self showMoreTagsButtonWasClicked];
        }
        else{
            if(indexPath.row == 0){
                
            }
            else{
                [self pressUpArrowButtonForCellAtIndexPath:indexPath];
            }
        }
    }
}
-(void)showMoreTagsButtonWasClicked{
    if([self.delegate respondsToSelector:@selector(displayAddTagsViewControllerWithControllerType:)])
        [self.delegate displayAddTagsViewControllerWithControllerType:OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags];
}




#pragma mark - Private methods - Methods

-(NSString *)getShowMoreTagMessageFromCount:(NSInteger)count {
    /*
    if (count == 1 || count == 0 ) {
        return [NSString stringWithFormat:@"Show %lu more tag", count];
    } else {
        return [NSString stringWithFormat:@"Show %lu more tags", count];
    }
    */
    
    return [NSString stringWithFormat:@"More Tags"];
}

@end
