//
//  OCNPlaneModel.h
//  Ocean
//
//  Created by Sam Payne on 01/07/2014.
//  Copyright (c) 2014 Ocean. All rights reserved.
//
//#import "OCNServerRequest.h"
//#import "OCNNetworkLayer.h"
#import <MapKit/MapKit.h>
@interface OCNPlaneModel : MTLModel <MTLJSONSerializing>

@property (nonatomic) double x0;
@property (nonatomic) double y0;
@property (nonatomic) double x1;
@property (nonatomic) double y1;

+(OCNPlaneModel*) placeModelWithCoordinatesX0:(double) x0 x1:(double) x1 y0:(double) y0 y1:(double) y1;
+(OCNPlaneModel*) planeModelFromMapRegion:(MKCoordinateRegion) mapRegion;
-(BOOL) doesContainPoint:(CLLocationCoordinate2D) postition;

@end
