//
//  UIImage+SPImageFunctions.m
//  Created by Sam on 14/09/2014.
//  Copyright (c) 2014 Sam. All rights reserved.
//

#import "UIImage+SPImageFunctions.h"

@implementation UIImage (SPImageFunctions)

- (UIImage *) scaledCopyOfWidth:(CGFloat)width{
    
    CGFloat newHeight = width;//self.size.height*(width/self.size.width);
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, newHeight), NO, 1.0);
    [self drawInRect:CGRectMake(0, 0, width, newHeight)];
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}

@end
