//
//  OXServerRequest.h
//  OceanX
//
//  Created by Systango on 1/30/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"
#import "OXServerRequest.h"
#import "OCNRequestMetadata.h"

@interface OXServerRequest : AFHTTPRequestOperationManager

+(OXServerRequest*)sharedManager;
-(void)executePendingRequests;
-(void)cancelAndRemoveAllPendingRequests;

-(AFHTTPRequestOperation *) httpPostRequest: (NSString*) url parameters:(NSDictionary*) dictionary success: (void (^)(AFHTTPRequestOperation *operation, id response))successBlock fail:(void (^)(AFHTTPRequestOperation *operation, id error)) failBlock;

-(AFHTTPRequestOperation *) getRequest: (NSString*) url parameters:(NSDictionary*) parameters success:(SuccessCompletionBlock)successBlock fail:(FailureCompletionBlock) failBlock;

-(AFHTTPRequestOperation *) postRequest: (NSString*) url parameters:(NSDictionary*) parameters success: (SuccessCompletionBlock) successBlock fail:(FailureCompletionBlock) failBlock;

-(AFHTTPRequestOperation *) putRequest: (NSString*) url parameters:(NSDictionary*) parameters success:(SuccessCompletionBlock)successBlock fail:(FailureCompletionBlock) failBlock;

-(AFHTTPRequestOperation *) deleteRequest: (NSString*) url parameters:(NSDictionary*) parameters success:(SuccessCompletionBlock)successBlock fail:(FailureCompletionBlock) failBlock;

-(void) cancelAllRequests;
    
-(void) setJSONHeaders;
-(void)setHTTPHeaders;
@end
