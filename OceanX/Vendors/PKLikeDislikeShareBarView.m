//
//  PKLikeDislikeShareBarView.m
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKLikeDislikeShareBarView.h"

@implementation PKLikeDislikeShareBarView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        CGFloat buttonHeight = frame.size.height * 0.85;
        CGFloat buttonWidth = buttonHeight;
        CGRect  buttonSize = CGRectMake(0, 0, buttonWidth, buttonHeight);
        CGFloat padding = 20;
        
        buttonSize.origin.x = padding;
        buttonSize.origin.y = frame.size.height/2-buttonSize.size.height/2;
        
        self.dislikeButton = [[UIButton alloc] initWithFrame:buttonSize];
        [self.dislikeButton setBackgroundImage:[UIImage imageNamed:@"dislikeUnselected"] forState:UIControlStateNormal];
        [self.dislikeButton setBackgroundImage:[UIImage imageNamed:@"dislike"] forState:UIControlStateSelected];
        [self.dislikeButton addTarget:self action:@selector(dislikeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

        buttonSize.origin.x = CGRectGetMaxX(self.dislikeButton.frame)+padding;
        
        self.likeButton = [[UIButton alloc] initWithFrame:buttonSize];
        [self.likeButton setBackgroundImage:[UIImage imageNamed:@"likeUnselected"] forState:UIControlStateNormal];
        [self.likeButton setBackgroundImage:[UIImage imageNamed:@"like"] forState:UIControlStateSelected];
        [self.likeButton addTarget:self action:@selector(likeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        buttonSize.origin.x = CGRectGetMaxX(self.likeButton.frame)+padding;
        
        self.maybeButton = [[UIButton alloc] initWithFrame:buttonSize];
        [self.maybeButton setBackgroundImage:[UIImage imageNamed:@"confusedUnselected"] forState:UIControlStateNormal];
        [self.maybeButton setBackgroundImage:[UIImage imageNamed:@"ConfusedButton"] forState:UIControlStateSelected];
        [self.maybeButton addTarget:self action:@selector(maybeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        buttonSize.size.height = frame.size.height*0.4;
        buttonSize.size.width = buttonSize.size.height*0.8888;
        
        
        self.shareButton = [[UIButton alloc] initWithFrame:buttonSize];
        
        CGRect shareButtonFrame = self.shareButton.frame;
        shareButtonFrame.origin.x = CGRectGetWidth(frame) - padding - CGRectGetWidth(_shareButton.frame);
        shareButtonFrame.origin.y = CGRectGetMidY(self.frame)-shareButtonFrame.size.height/2;
        _shareButton.frame = shareButtonFrame;
//        [_shareButton setTitle:@"Share" forState:UIControlStateNormal];
        
        UIImage *image = [[UIImage imageNamed:@"shareIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_shareButton setImage:image forState:UIControlStateNormal];
        _shareButton.tintColor = [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0];

        [_shareButton setTitleColor:[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0] forState:UIControlStateNormal];
        
        [_shareButton addTarget:self action:@selector(shareButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self addSubview:_likeButton];
        [self addSubview:_dislikeButton];
        [self addSubview:_maybeButton];
        [self addSubview:_shareButton];
//        [self setBackgroundColor:[UIColor redColor]];
    }
    return self;
}
-(void)likeButtonTapped:(id)sender{
    NSLog(@"like button tapped");
    [self.likeButton setSelected:![self.likeButton isSelected]];
    [self.dislikeButton setSelected:NO];
    [self.maybeButton setSelected:NO];

}
-(void)dislikeButtonTapped:(id)sender{
    NSLog(@"dislike button tapped");
    [self.dislikeButton setSelected:![self.dislikeButton isSelected]];
    [self.likeButton setSelected:NO];
    [self.maybeButton setSelected:NO];

}
-(void)maybeButtonTapped:(id)sender{
    NSLog(@"maybe button tapped");
    [self.maybeButton setSelected:![self.maybeButton isSelected]];
    [self.likeButton setSelected:NO];
    [self.dislikeButton setSelected:NO];
}

-(instancetype)init{
    [NSException raise:@"cant use init" format:@"Use initWithFrame to instantiate LikeDislikeShareBar view"];
    return nil;
}
-(void)setPoiObject:(OXPOIModel *)poiObject{
    _poiObject = poiObject;
    [self.likeButton setSelected:poiObject.userLiked];
    [self.dislikeButton setSelected:poiObject.userDisliked];
    [self.maybeButton setSelected:poiObject.userConfused];
}
-(void)shareButtonPressed{
    NSLog(@"didPress share");
    [self.delegate didPressShare];
}
@end
