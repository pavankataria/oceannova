//
//  OCNLocationTracker.h
//  Ocean
//
//  Created by Sam Payne on 01/07/2014.
//  Copyright (c) 2014 Ocean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OCNPlaneModel.h"
#import <MapKit/MapKit.h>
#import "OCNCoordinate.h"

@interface OCNLocationTracker : NSObject <CLLocationManagerDelegate>

@property (nonatomic,strong) CLLocationManager * manager;
@property (nonatomic) CLLocationCoordinate2D currentLocation;

+ (OCNLocationTracker*) tracker;
- (id) init;
- (OCNPlaneModel*) currentSearchArea;
+ (OCNPlaneModel*) defaultSearchArea;
- (OCNCoordinate*) userLocation;
- (MKCoordinateRegion) currentMapRegion;
-(MKCoordinateRegion) londonMapRegion;
-(MKCoordinateRegion) regionWithRadius:(CGFloat) radius;
-(MKCoordinateRegion) regionWithLocation:(CLLocationCoordinate2D)location radius:(CGFloat) radius;
-(MKCoordinateRegion) regionWithLocation:(CLLocationCoordinate2D)location;

-(void) pause;
-(void) start;

@end
