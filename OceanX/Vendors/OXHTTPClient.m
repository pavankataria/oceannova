//
//  OXHTTPClient.m
//  OceanX
//
//  Created by Pavan Kataria on 20/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXHTTPClient.h"
@interface OXHTTPClient (){
    
}
@property (nonatomic, retain) NSMutableArray *pendingRequests;

@end
@implementation OXHTTPClient

//#define AWS_URL @"https://oceanbackend.oceanapp.com/"

+ (OXHTTPClient *)sharedOXHTTPClient{
    static OXHTTPClient *_sharedOXHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedOXHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@ws/rest/%@", kHTTPClientURLStringBase, kHTTPClientURLStringAPIVersion]]];
//        NSLog(@"request: %@", _sharedOXHTTPClient.requestSerializer);
//        NSLog(@"response: %@", _sharedOXHTTPClient.responseSerializer);
    });
    return _sharedOXHTTPClient;
}


- (instancetype)initWithBaseURL:(NSURL *)url{
    self = [super initWithBaseURL:url];
    if (self) {
        self.securityPolicy.validatesDomainName = YES;
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"]; //redundant
        
        
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        self.pendingRequests = [NSMutableArray array];

    }
    
    return self;
}

+(void)loginWithUsername:(NSString*)username andPassword:(NSString*)password withCompletionBlock:(void (^)(AFHTTPRequestOperation *operation, id object, NSError *error))completionBlock{
    
    NSDictionary * params = @{@"j_username": username,
                              @"j_password":password};
    
    [self sharedOXHTTPClient].responseSerializer = [AFHTTPResponseSerializer serializer];
    [self sharedOXHTTPClient].requestSerializer = [AFHTTPRequestSerializer serializer];

    [[self sharedOXHTTPClient] POST:kHTTPClientURLStringSecurityCheckLogin
                        parameters:params
                           success:^(AFHTTPRequestOperation *operation, id responseObject) {
                               NSDictionary * dictReponse = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                               NSLog(@"response object raw: %@", dictReponse);
                               NSLog(@"objectForKey:error: %@", [dictReponse objectForKey:@"errorCode:"]);
                               
                               if ([dictReponse objectForKey:@"errorCode:"] || [dictReponse objectForKey:@"errorCode"]) {
                                   NSError *error = [NSError errorWithDomain:@"error" code:100 userInfo:dictReponse];
                                   completionBlock(operation, dictReponse, error);
                               }else{
                                   completionBlock(operation, dictReponse, nil);
                               }

                           }
                           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                               completionBlock(operation, nil, error);
                           }];
    
    
}



+(void)getTagsWithCompletionBlock:(void (^)(AFHTTPRequestOperation *operation, id object, NSError *error))completionBlock{
    [self sharedOXHTTPClient].responseSerializer = [AFHTTPResponseSerializer serializer];
    [self sharedOXHTTPClient].requestSerializer = [AFHTTPRequestSerializer serializer];
    [[self sharedOXHTTPClient] GET:kHTTPClientURLStringGetTags
                        parameters:nil
                           success:^(AFHTTPRequestOperation *operation, id responseObject) {
                               
                               NSDictionary * dictReponse = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                               
                               completionBlock(operation, dictReponse, nil);
                           }
                           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                               
                               NSLog(@"erorororor: %@", operation.responseString);
                               
                               completionBlock(operation, nil, error);
                           }];
    
    
    

}
+(void)getTagsAutoSuggestionWithParams:(NSDictionary*)params withCompletionBlock:(void (^)(AFHTTPRequestOperation *operation, id object, NSError *error))completionBlock{
    [self sharedOXHTTPClient].responseSerializer = [AFHTTPResponseSerializer serializer];
    [self sharedOXHTTPClient].requestSerializer = [AFJSONRequestSerializer serializer];
    

    [[self sharedOXHTTPClient] GET:kHTTPClientURLStringTagAutoSuggestion
                        parameters:params
                           success:^(AFHTTPRequestOperation *operation, id responseObject) {
                               
                               NSDictionary * dictReponse = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];

                               completionBlock(operation, dictReponse, nil);
                           }
                           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                               
                               NSLog(@"erorororor: %@", operation.responseString);
                               
                               completionBlock(operation, nil, error);
                           }];
    
}


-(void)displayZoebScreen{
    
    
}


+(void)getMetaDataWithCompletionBlock:(void(^)(id object, NSError *error))completionBlock{
    
}@end