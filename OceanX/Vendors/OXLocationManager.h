//
//  OXLocationManager.h
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol GFLocationManagerDelegate

- (void)locationManagerDidUpdateLocation:(CLLocation *)location;

@end

@interface OXLocationManager : NSObject<CLLocationManagerDelegate>

+ (OXLocationManager *)sharedTracker;
-(void) start;
-(void) pause;
- (CLLocationCoordinate2D) currentLocation;
-(OCNCoordinate*) userLocation;
- (MKCoordinateRegion) currentMapRegion;
-(BOOL)isLocationsServicesAvailable;

-(MKCoordinateRegion) regionWithLocation:(CLLocationCoordinate2D)location;
@end
