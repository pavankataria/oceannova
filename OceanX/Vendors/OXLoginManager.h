//
//  OXLoginManager.h
//  OceanX
//
//  Created by Pavan Kataria on 02/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OXLoginManager : NSObject

+(instancetype)sharedManager;


//-(void)performLoginWithUserName:(NSString*)username andPassword:(NSString*)password successBlock:(SuccessCompletionBlock)successBlock failBlock:(void(^)(NSString * errorMessage))failBlock;
-(void)performLoginWithUserName:(NSString*)username andPassword:(NSString*)password
                   successBlock:(SuccessCompletionBlock)successBlock
                      failBlock:(FailureCompletionBlock)failBlock;
-(void)performRenewLogin;
-(void)performLoginSegue;
-(void)performLogoutSegue;
+(BOOL)isLoggedIn;
-(void)loginWithFacebookWithSuccessBlock:(SuccessCompletionBlock)successBlock
                               failBlock:(FailureCompletionBlock)failBlock;
+(BOOL)isProfileTutorialViewed;
+(void)setReplayTutorialViewed;
+(void)setReplayTutorialViewedTo:(BOOL)viewed;

+(BOOL)isThereAnyFBRecommendationsToBeViewed;
+(void)setFBRecommendationNeedsDisplayingWithBOOL:(BOOL)viewed;

+(void)setCheckFBLikePermissionActiveWithBool:(BOOL)viewed;
+(BOOL)isCheckFBLikePermissionActive;

@end