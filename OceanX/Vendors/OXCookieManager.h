//
//  OXCookieManager.h
//  OceanX
//
//  Created by Systango on 30/01/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OXCookieManager : NSObject

+ (OXCookieManager*)sharedManager;

- (void)saveSessionCookiesForURL:(NSURL *)url;

- (void)clearSavedSessionCookies;

- (void)assignedSavedSessionCookies;


@end
