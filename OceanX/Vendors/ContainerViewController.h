//
//  ContainerViewController.h
//  EmbeddedSwapping
//
//  Created by Michael Luton on 11/13/12.
//  Copyright (c) 2012 Sandmoose Software. All rights reserved.
//
//@protocol <#protocol name#> <NSObject>
//
//<#methods#>
//
//@end
#import "OXSearchResultsViewController.h"
#import "OXFilterScreenViewController.h"

@interface ContainerViewController : UIViewController{
    
}
@property (strong, nonatomic) OXSearchResultsViewController *firstViewController;
@property (strong, nonatomic) OXFilterScreenViewController *secondViewController;

-(void)swapViewControllers;
-(void)swapToSecondViewController;
-(void)swapToFirstViewController;

-(void)setParentViewControllerForChildViewControllers:(UIViewController<OXSearchContainerScreenViewControllerOperationProtocol>*)parentViewControllerObject;
@end
