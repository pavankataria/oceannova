//
//  PKDismissSegue.m
//  OceanX
//
//  Created by Pavan Kataria on 18/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKDismissSegue.h"

@implementation PKDismissSegue
-(void)perform{
    UIViewController *sourceViewController = self.sourceViewController;
    [[NSNotificationCenter defaultCenter] postNotificationName:kTabBarViewControllerAddPOIButtonSetToUnselectStateNotification object:nil];
    [sourceViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
@end
