//
//  OXRestAPICient.h
//
//
//  Created by Pavan Kataria on 02/02/2015.
//
//

#import <Foundation/Foundation.h>
#import "OXPOISearchModel.h"
#import "OXBasePOIModel.h"
#import "OXReportDataModel.h"
#import "OXHCAddPoiModel.h"
#import <MapKit/MapKit.h>

@interface OXRestAPICient : NSObject

+(NSString *)completeURLByURLPath:(NSString *)urlPath;
//+(void) loginWithUsername:(NSString*) username password:(NSString*)password successBlock:(SuccessCompletionBlock)successBlock failBlock:(void (^)(NSString * errorMessage)) failBlock;
//+(void) loginWithUsername:(NSString*) username password:(NSString*)password successBlock:(SuccessCompletionBlock)successBlock failBlock:(void (^)(NSString * errorMessage)) failBlock;
+(void) loginWithUsername:(NSString*) username password:(NSString*)password successBlock: (SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock;

+(void)postAddPoiWithAddPoiModel:(OXHCAddPoiModel*)object
                withSuccessBlock: (void (^)(AFHTTPRequestOperation *operation, id response))successBlock
                       failBlock:(void (^)(AFHTTPRequestOperation *operation, id error)) failBlock;

+(void)searchByMapWithPOISearchObject:(OXPOISearchModel*)searchModel
                     withSuccessBlock:(SuccessCompletionBlock) successBlock
                            failBlock:(FailureCompletionBlock) failBlock;

+(void)searchByListWithPOISearchObject:(OXPOISearchModel*)searchModel
                      withSuccessBlock:(SuccessCompletionBlock) successBlock
                             failBlock:(FailureCompletionBlock) failBlock;

+(void)setUserLocationWithCoordinate:(OCNCoordinate*)coordinate
                         successBlock:(SuccessCompletionBlock)successBlock
                            failBlock:(void (^)(AFHTTPRequestOperation *operation, id error))failBlock;

+(void)getQuickPoiInfoWithBasePOIObject:(OXBasePOIModel*)basePOIModel
                           successBlock:(SuccessCompletionBlock)
                 successBlock failBlock:(FailureCompletionBlock)failBlock;

+(void)getUserProfileWithSuccessBlock:(SuccessCompletionBlock)successBlock
                            failBlock:(FailureCompletionBlock)failBlock;

+(void)reportIssueWithReportData:(OXReportDataModel *)hashCode
                    successBlock:(SuccessCompletionBlock)successBlock
                       failBlock:(FailureCompletionBlock)failBlock;

+(void)activateUserProfileWithSuccessBlock:(SuccessCompletionBlock)successBlock
                                 failBlock:(FailureCompletionBlock)failBlock;

+(void)getFacetsQuestionsWithHashCode:(NSString *)hashCode
                         successBlock:(SuccessCompletionBlock)successBlock
                            failBlock:(FailureCompletionBlock)failBlock;

+(void) requestPasswordResetForUser:(OXUserModel*) user successBlock:(SuccessCompletionBlock) successBlock failBlock:(FailureCompletionBlock) failBlock;

+(void) signUpWithUser:(OXUserModel*) user successBlock:(SuccessCompletionBlock) successBlock failBlock:(FailureCompletionBlock) failBlock;

+(void)getMetaDataWithSuccessBlock:(SuccessCompletionBlock)successBlock
                         failBlock:(FailureCompletionBlock)failBlock;

+(void)getTagsAutoSuggestionWithString:(NSString*)searchString withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock)failBlock;



+(void)addAnswersForQuestions:(NSArray *)questions
                 successBlock:(SuccessCompletionBlock) successBlock
                    failBlock:(FailureCompletionBlock) failBlock;
+ (void)getFriendsListWithSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock;
+ (void)getFriendRequestsWithSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock;
+ (void)getSentFriendRequestsWithSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock;
+ (void)searchUsersWithSearchTerm:(NSString *)searchTerm withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock;

+(void)acceptFriendRequestForNotification:(NSInteger) notification withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock;
+(void)denyFriendRequestForNotification:(NSInteger)notification withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock;
+ (void)unfriendUserWithID:(NSInteger) userID  withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock;
+(void)addFriendWithID:(NSInteger)userID withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock;

+(void)getNewsFeedWithSuccessBlock: (void (^)(AFHTTPRequestOperation *operation, id response))successBlock
                       failBlock:(void (^)(AFHTTPRequestOperation *operation, id error)) failBlock;

+(void) likePOIWithID:(NSInteger)poiID successBlock:(SuccessCompletionBlock) successBlock failBlock:(FailureCompletionBlock) failBlock;

+(void) dislikePOIWithID:(NSInteger) poiID successBlock:(SuccessCompletionBlock) successBlock failBlock:(FailureCompletionBlock) failBlock;
+(void)neutralPOIWithID:(NSInteger) poiID successBlock:(SuccessCompletionBlock) successBlock failBlock:(FailureCompletionBlock) failBlock;

+(void)getFullPOIDetailsWithPOIID:(NSInteger)poiID
                     successBlock:(SuccessCompletionBlock) successBlock
                        failBlock:(FailureCompletionBlock) failBlock;

+(void)postAddTags:(NSArray *)tagArray
         withPOIID:(NSInteger)poiID
      successBlock:(SuccessCompletionBlock) successBlock
         failBlock:(FailureCompletionBlock) failBlock;


+(void)addImagesToPOI:(NSArray *)tagArray
            withPOIID:(NSInteger)poiID
         successBlock:(SuccessCompletionBlock) successBlock
            failBlock:(FailureCompletionBlock) failBlock;

+(void)affirmTagID:(NSUInteger)tagID
         withPOIID:(NSUInteger)poiID
      successBlock:(SuccessCompletionBlock) successBlock
         failBlock:(FailureCompletionBlock) failBlock;

+(void)disaffirmTagID:(NSUInteger)tagID
            withPOIID:(NSUInteger)poiID
         successBlock:(SuccessCompletionBlock) successBlock
            failBlock:(FailureCompletionBlock) failBlock;


//+(void)postFacebookNeedInfoWithFacebookAccessTokenString:(NSString*)facebookAccessTokenString
//                                            successBlock:(SuccessCompletionBlock) successBlock
//                                               failBlock:(FailureCompletionBlock) failBlock;


+(void)postLoginWithFacebookWithAccessTokenString:(NSString*)facebookAccessTokenString
                             withParamsDictionary:(NSDictionary*)params
                                     successBlock:(SuccessCompletionBlock) successBlock
                                        failBlock:(FailureCompletionBlock) failBlock;


+(void)postFacebookRecommendationsWithAccessTokenString:(NSString*)facebookAccessTokenString
                                      andPlaneRestModel:(OCNPlaneModel*)planeRestModel
                                           successBlock:(SuccessCompletionBlock)successBlock
                                              failBlock:(FailureCompletionBlock)failBlock;

+(void)getFacebookRecommendationsWithSuccessBlock:(SuccessCompletionBlock)successBlock
                                        failBlock:(FailureCompletionBlock)failBlock;

+(void)getFacebookCheck:(SuccessCompletionBlock)successBlock
              failBlock:(FailureCompletionBlock)failBlock;

+(void)updateUserDeviceToken:(NSString*)deviceToken
                successBlock:(SuccessCompletionBlock) successBlock
                   failBlock:(FailureCompletionBlock) failBlock;

+(void)updateUserLocationWithCoordinate:(CLLocationCoordinate2D)userLocationCoordinate
                           successBlock:(SuccessCompletionBlock) successBlock
                              failBlock:(FailureCompletionBlock) failBlock;
@end
