//
//  FadeSegue.m
//  OceanX
//
//  Created by Pavan Kataria on 08/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "FadeSegue.h"

@interface FadeSegue()

@end

@implementation FadeSegue

-(void)perform{
    [self.destinationViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.sourceViewController presentModalViewController:self.destinationViewController animated:YES];
}


@end
