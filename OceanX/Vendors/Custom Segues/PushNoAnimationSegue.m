//
//  PushNoAnimationSegue.m
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PushNoAnimationSegue.h"

@implementation PushNoAnimationSegue
-(void) perform{
    [[[self sourceViewController] navigationController] pushViewController:[self destinationViewController] animated:NO];
}

@end
