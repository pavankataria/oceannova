//
//  PushFadeSegue.m
//  Ocean
//
//  Created by Pavan Kataria on 11/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PushFadeSegue.h"

@implementation PushFadeSegue

-(void)perform{

    [UIView transitionWithView:((UIViewController*)self.sourceViewController).navigationController.view duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [((UIViewController*)self.sourceViewController).navigationController pushViewController:self.destinationViewController animated:NO];;
                    }
                    completion:NULL];
}

@end
