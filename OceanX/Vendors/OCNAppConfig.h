//
//  OCNAppConfig.h
//  Oceanapp
//
//  Created by Pavan Kataria on 21/11/2014.
//  Copyright (c) 2014 Ocean Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OCNAppConfig : NSObject

+(void)mapSetPinPointImageAndOffsetForMKAnnotationView:(MKAnnotationView*)annotationView;
+(void)mapSetPinPointHighlightedImageAndOffsetForMKAnnotationView:(MKAnnotationView*)annotationView;
+(void)mapAnimateToUserLocationWithMap:(MKMapView*)mapView;
+(UIButton*)mapInitCenterButtonForMapFrame:(CGRect)mapFrame;
+(void)setEmoticonImageForScoreButton:(UIButton*)scoreButton basedOnScore:(long)score;

@end
