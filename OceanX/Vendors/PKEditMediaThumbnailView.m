//
//  PKEditMediaThumbnailView.m
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKEditMediaThumbnailView.h"

@implementation PKEditMediaThumbnailView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialiseOtherComponents];
    }
    return self;
}
-(instancetype)init{
    self = [self initWithFrame:CGRectZero];
    return self;
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    //we dont want the preview thumbnail to also update its coordinates whenever the frame changes
    CGFloat height = CGRectGetHeight(self.bounds) - self.margin * 2;
    CGFloat width = CGRectGetWidth(self.bounds) - self.margin * 2;
    self.previewThumbnail.frame = CGRectMake(self.margin, self.margin, width, height);
}

-(void)initialiseOtherComponents{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap)];
    [self addGestureRecognizer:tapGesture];
    
    
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.button addTarget:self action:@selector(deleteButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
    NSLog(@"adding thumbnail delete button");
    [self.button setTitle:@"X" forState:UIControlStateNormal];
    
    [self.button setHidden:YES];
    [self addSubview:self.button];
}
-(void)setThumbnailRadius:(CGFloat)radius{
    [self.previewThumbnail.layer setCornerRadius:radius];
    [self.previewThumbnail.layer setMasksToBounds:YES];
}

-(void)didTap{
    NSLog(@"detail thumbnail slider did tap index self.index: %lu", (unsigned long)self.index);

    if(self.isCameraIcon){
        if([self.delegate respondsToSelector:@selector(cameraIconDidSelect)]){
            [self.delegate cameraIconDidSelect];
        }
    }
    else{
        if([self.delegate respondsToSelector:@selector(didSelectThumbnailPreviewAtIndexPosition:)]){
            [self.delegate didSelectThumbnailPreviewAtIndexPosition:self.index];
        }
    }
}


//These methods might not be in use:
-(void)deleteButtonDidPress:(id)sender{
    
}

-(void)setupDeleteButton{
    
}
@end
