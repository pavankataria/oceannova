//
//  OCNPlaneModel.m
//  Ocean
//
//  Created by Sam Payne on 01/07/2014.
//  Copyright (c) 2014 Ocean. All rights reserved.
//

#import "OCNPlaneModel.h"

@implementation OCNPlaneModel


+(NSDictionary*) JSONKeyPathsByPropertyKey{
    
    return nil;
    
}

+(OCNPlaneModel*) placeModelWithCoordinatesX0:(double) x0 x1:(double) x1 y0:(double) y0 y1:(double) y1{
    
    return [[OCNPlaneModel alloc] initModelWithCoordinatesX0:x0 x1:x1 y0:y0 y1:y1];
    
}


-(BOOL) doesContainPoint:(CLLocationCoordinate2D) postition{
    if (postition.longitude > self.x0 && postition.longitude < self.x1 && postition.latitude > self.y0 && postition.latitude < self.y1 ) {
        return YES;
    }
    return NO;
}

-(id) initModelWithCoordinatesX0:(double) x0 x1:(double) x1 y0:(double) y0 y1:(double) y1{
    self = [super init];
    if (self) {
        _x0 = x0;
        _x1 = x1;
        _y0 = y0;
        _y1 = y1;
    }
    
    return self;
    
}

+(OCNPlaneModel*) planeModelFromMapRegion:(MKCoordinateRegion) mapRegion{
    double x0 = mapRegion.center.longitude - (mapRegion.span.longitudeDelta);
    double x1 = mapRegion.center.longitude + (mapRegion.span.longitudeDelta);
    double y1 = mapRegion.center.latitude + (mapRegion.span.latitudeDelta);
    double y0 = mapRegion.center.latitude - (mapRegion.span.latitudeDelta);
    return [OCNPlaneModel placeModelWithCoordinatesX0:x0 x1:x1 y0:y0 y1:y1];
    
}

@end
