//
//  PKTagsPresentationView.m
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKTagsPresentationView.h"

@implementation PKTagsPresentationView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib{
    [self setViewProperties];
}
-(instancetype)initWithFrame:(CGRect)frame andTags:(NSArray*)array{
    self = [self initWithFrame:frame];
    if(self){
        CGFloat heightOfLabel = 30;
        CGFloat padding = 5;
        
        CGFloat yPosition = 0;
        NSLog(@"tags array: %@", array);
        for (NSUInteger i = 0; i < array.count; i++) {
            
            yPosition = i * (padding + heightOfLabel);
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, yPosition, frame.size.width, heightOfLabel)];
            label.text = @"Tags";
            [self addSubview:label];
        }

    }
    return self;
}



-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
-(instancetype)init{
    return [self initWithFrame:CGRectZero];
}

-(void)setViewProperties{
    NSLog(@"set view properties");
//    [self.thumbnailSlider setBackgroundColor:[UIColor redColor]];
}
@end
