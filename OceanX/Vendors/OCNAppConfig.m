//
//  OCNAppConfig.m
//  Oceanapp
//
//  Created by Pavan Kataria on 21/11/2014.
//  Copyright (c) 2014 Ocean Technologies. All rights reserved.
//

#import "OCNAppConfig.h"

@implementation OCNAppConfig

+(void)setEmoticonImageForScoreButton:(UIButton *)scoreButton basedOnScore:(long)score{
    if (score >= 0) {
        CGRect tempRect = CGRectMake(5, 5, 40, 40);
        scoreButton.frame = tempRect;

        if(score >= 80){
            [scoreButton setImage:[UIImage imageNamed:@"emotionIconVeryHappy"] forState:UIControlStateNormal];
        }
        else if (score >= 60){
            [scoreButton setImage:[UIImage imageNamed:@"emotionIconHappy"] forState:UIControlStateNormal];
        }
        else if (score >= 40){
            [scoreButton setImage:[UIImage imageNamed:@"emotionIconNeutral"] forState:UIControlStateNormal];
        }
        else if (score >= 20){
            [scoreButton setImage:[UIImage imageNamed:@"emotionIconUnamused"] forState:UIControlStateNormal];
        }
        else {
            [scoreButton setImage:[UIImage imageNamed:@"emotionIconVeryUnamused"] forState:UIControlStateNormal];
        }
        [scoreButton setTitle:@"" forState:UIControlStateNormal];
        [scoreButton setBackgroundColor:[UIColor whiteColor]];
    }
    else{
        CGRect tempRect = CGRectMake(5, 5, 69, 40);
        scoreButton.frame = tempRect;
        [scoreButton setTitle:@"?" forState:UIControlStateNormal];
        [scoreButton setImage:[UIImage imageNamed:@"profile-white.png"] forState:UIControlStateNormal];
        [scoreButton setBackgroundColor:[UIColor colorWithRed:20.0f/255.0f green:189.0f/255.9f blue:209.0f/255.0f alpha:0.5f]];
    }
}
+(void)mapSetPinPointImageAndOffsetForMKAnnotationView:(MKAnnotationView*)annotationView{
    annotationView.image = [UIImage imageNamed:@"pinUnselected"];
    annotationView.layer.anchorPoint = kMapPinPointOffsetPointForSelected;
}
+(void)mapSetPinPointHighlightedImageAndOffsetForMKAnnotationView:(MKAnnotationView*)annotationView{
    annotationView.image = [UIImage imageNamed:@"pinUnselected"];
    annotationView.layer.anchorPoint = kMapPinPointOffsetPointForUnselected ;
}

+(void)mapAnimateToUserLocationWithMap:(MKMapView *)mapView{
    [mapView setCenterCoordinate:mapView.userLocation.location.coordinate animated:YES];
}
+(UIButton*)mapInitCenterButtonForMapFrame:(CGRect)mapFrame{
//    CGPoint centerButtonPosition = CGPointMake(
//                                               (mapFrame.size.width - kMapCenterButtonSize.width - kMapCenterButtonPadding),
//                                               (mapFrame.size.height - kMapCenterButtonSize.height - kMapCenterButtonPadding));
//    
    
    CGPoint centerButtonPosition = CGPointMake(mapFrame.size.width - kMapCenterButtonSize.width -kMapCenterButtonPadding,
                                               kMapCenterButtonPadding);
//

    UIButton *mapCenteringButton = [[UIButton alloc] initWithFrame:(CGRect){centerButtonPosition,kMapCenterButtonSize}];
    [mapCenteringButton setBackgroundImage:[UIImage imageNamed:@"centeringButton"] forState:UIControlStateNormal];    
    return mapCenteringButton;
}
@end
