//
//  NSDictionary+HasValueForKey.m
//  OceanX
//
//  Created by Systango on 2/4/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "NSDictionary+HasValueForKey.h"

@implementation NSDictionary (HasValueForKey)

- (BOOL)hasValueForKey:(NSString *)key
{
    if([self valueForKey:key] && [self valueForKey:key] != [NSNull alloc])
        return YES;
    else
        return NO;
}

-(id)getValueForKey:(NSString *)key{
    
    if ([self hasValueForKey:key]) {
        return [self objectForKey:key];
    }
    return nil;
}


@end
