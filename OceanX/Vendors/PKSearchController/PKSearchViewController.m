
//
//  PKSearchViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKSearchViewController.h"
typedef void (^searchTextFieldDidChange)(NSString*);

@interface PKSearchViewController ()

@end

@implementation PKSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupAddTagTextField];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.searchTextField becomeFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupAddTagTextField{
    self.searchBackgroundView.backgroundColor = [PKUIAppearanceController darkBlueColor];
    [self.searchTextField setTextColor:[UIColor whiteColor]];
    UIColor *color = [UIColor whiteColor];
    self.searchTextField.attributedPlaceholder = [self searchBarPlaceHolderText];
    self.searchTextField.delegate = self;
}
-(NSAttributedString*)searchBarPlaceHolderText{
    UIColor *color = [UIColor whiteColor];
    return [[NSAttributedString alloc] initWithString:kOXAddTagsTextFieldPlaceHolderText attributes:@{NSForegroundColorAttributeName:color}];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
