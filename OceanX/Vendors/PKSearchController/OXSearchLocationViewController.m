//
//  OXSearchLocationViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSearchLocationViewController.h"
#import "OXProtocolsHeader.h"

@interface OXSearchLocationViewController ()
@property (nonatomic, retain) NSMutableArray *searchResultsArray;
@property (nonatomic, assign, getter=isSearching) BOOL searching;
@end

@implementation OXSearchLocationViewController
#pragma mark - Lazy Loading - Methods
-(NSMutableArray *)searchResultsArray{
    if(!_searchResultsArray){
        _searchResultsArray = [[NSMutableArray alloc] init];
    }
    return _searchResultsArray;
}

#pragma mark - UIViewController life cycle - Methods
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(cancelActionMethod)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);


    
}

-(void)cancelActionMethod{
    [self.delegate refreshTableView];
    [self.navigationController popViewControllerAnimated:NO];
    [self.searchTextField resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.searchTextField addTarget:self action:@selector(searchBarTextFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXTagTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellTagIdentifier];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXLoadingTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellIdentifierSearchResultsLoadingCell];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Accessor - Methods

-(void)searchBarTextFieldChanged:(UITextField*)textfield{
    [self performLocationSearchWithText:textfield.text];
}

-(void)performLocationSearchWithText:(NSString*)searchText{
    self.searchResultsArray = nil;
    
    //Dont bother showing the search loading animation every single time, only at the beginning when the first few characters are being entered
//    if(self.searchTextField.text.length < 2){
        self.searching = YES;
        [self.tableView reloadData];
//    }
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = searchText;
    
    MKCoordinateRegion currentRegion = [[OXLocationManager sharedTracker] currentMapRegion];
    request.region = currentRegion;
    
    MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];

    [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        [self processLocalSearchResponse:response];
    }];
}


-(NSString *)getUIStringForMKMapItem:(MKMapItem*)mapItem{
    NSString *needle = @"";
    NSUInteger levels = 0;
    static NSUInteger maxLevelsToAdd = 3;
    BOOL checking = YES;
    for(int i = 0; checking == YES; i++){
        NSString *wordToAdd = @"";
        switch (i) {
            case 0:
                if([mapItem.placemark.subLocality length] > 0){
                    wordToAdd = mapItem.placemark.subLocality;
                }
                break;
            case 1:
                if([mapItem.placemark.locality length] > 0){
                    wordToAdd = mapItem.placemark.locality;
                }
                break;
            case 2:
                if([mapItem.placemark.ISOcountryCode length] > 0){
                    wordToAdd = mapItem.placemark.ISOcountryCode;
                }
                break;
            case 3:
                if([mapItem.placemark.country length] > 0){
                    wordToAdd = mapItem.placemark.country;
                }
                break;
            default:
                checking = false;
                break;
        }
        
        if([wordToAdd length] != 0 && levels < maxLevelsToAdd){
            levels++;
            if([needle length] > 0){
                needle = [NSString stringWithFormat:@"%@, %@", needle, wordToAdd];
            }
            else{
                needle = wordToAdd;
            }
        }
    }
    return needle;
}
-(void)processLocalSearchResponse:(MKLocalSearchResponse*)response{
    NSMutableArray *uniqueArray = [[NSMutableArray alloc] init];
    for(MKMapItem *currentItem in response.mapItems){
        NSString *needle = [self getUIStringForMKMapItem:currentItem];
        
        if([needle length] != 0){
            if(![uniqueArray containsObject:needle]){
                [uniqueArray addObject:needle];
                OXCoordinateModel *coordinate = [[OXCoordinateModel alloc] init];
                [coordinate setCoordinate:currentItem.placemark.coordinate];
                [coordinate setPlaceMarkTitleAndSubtitle:needle];
                [self.searchResultsArray addObject:coordinate];
            }
        }
    }
//    for(MKMapItem *item in response.mapItems){
//        NSLog(@"name %@",item.placemark.name); // eg. Apple In
//        NSLog(@"locality %@",item.placemark.locality); // city, eg
//        NSLog(@"subLocality %@",item.placemark.subLocality); // neigh
//        NSLog(@"administrativeArea %@",item.placemark.administrativeArea);
//        NSLog(@"subAdministrativeArea %@",item.placemark.subAdministrativeArea);
//        NSLog(@"postalCode %@",item.placemark.postalCode); // zip co
//        NSLog(@"ISOcountryCode %@",item.placemark.ISOcountryCode); // eg
//        NSLog(@"country %@",item.placemark.country); // eg. Unite
//        NSLog(@"\n\n\n");
//
//    }
//    NSLog(@"place count: %lu", self.searchResultsArray.count);

    self.searching = NO;
    [self.tableView reloadData];
}
#pragma mark end -



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - UITableView Datasource - Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.isSearching) {
        return 1;
    }
    else {
        return self.searchResultsArray.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isSearching){
        UITableViewCell <OXSearchTableViewCellOperationProtocol>*cell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsLoadingCell forIndexPath:indexPath];
        [cell startAnimatingActivityIndicatorView];
        return cell;
    }
    else{
        UITableViewCell <OXSearchTableViewCellOperationProtocol>*cell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellTagIdentifier];
        [cell setCellWithObjectFollowingSearchModelProtocol:self.searchResultsArray[indexPath.row]];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kOXAddTagsTableViewRowHeight;
}

#pragma mark - UITableView Delegate - Methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OXCoordinateModel *coordinate = self.searchResultsArray[indexPath.row];
    [self.poiSearchObject setWithCoordinateModel:coordinate];

    
    [self.delegate refreshTableView];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - Parent overridden methods - Methods
-(NSAttributedString*)searchBarPlaceHolderText{
    UIColor *color = [UIColor whiteColor];
    return [[NSAttributedString alloc] initWithString:@"Find places around a location in london" attributes:@{NSForegroundColorAttributeName:color}];
}
#pragma mark - OXPOISearchDelegate - Methods
-(void)setDelegate:(id<OXSearchDelegate>)delegate{
    _delegate = delegate;
}
-(void)setSearchPOIModel:(OXPOISearchModel *)object{
    _poiSearchObject = object;
}

@end
