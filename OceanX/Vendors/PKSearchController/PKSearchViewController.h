//
//  PKSearchViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, OXSearchViewControllerShowTableViewType) {
    OXSearchViewControllerShowTableViewTypeNormal = 0,
    OXSearchViewControllerShowTableViewTypeSearch,
};

@interface PKSearchViewController : UIViewController<UITextFieldDelegate>
@property (nonatomic, weak) IBOutlet UITextField *searchTextField;
@property (nonatomic, weak) IBOutlet UIView *searchBackgroundView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, assign) OXSearchViewControllerShowTableViewType currentTableViewShowing;

@end
