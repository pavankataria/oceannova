//
//  OXSearchLocationViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKSearchViewController.h"

#import "OXSearchDelegate.h"

@interface OXSearchLocationViewController : PKSearchViewController<UITableViewDelegate, UITableViewDataSource, OXPOISearchModelDelegate>
@property (nonatomic, weak) OXPOISearchModel *poiSearchObject;
@property (nonatomic, weak) id <OXSearchDelegate> delegate;


@end
