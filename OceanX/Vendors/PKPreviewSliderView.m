//
//  PKPreviewSliderView.m
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKPreviewSliderView.h"
#import "PKPreviewItemView.h"
#import "OXImageStackItem.h"

@implementation PKPreviewSliderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(NSMutableArray *)previewItems{
    if(!_previewItems){
        _previewItems = [[NSMutableArray alloc] init];
    }
    return _previewItems;
}

-(void)awakeFromNib{
    [self setViewProperties];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.previewSlider = [[UIScrollView alloc] initWithFrame:frame];
        [self addSubview:self.previewSlider];
        self.previewSlider.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        [self setViewProperties];
    }
    return self;
}
-(instancetype)init{
    return [self initWithFrame:CGRectZero];
}
-(void)setFrame:(CGRect)tempFrame{
    [super setFrame:tempFrame];
    [self setNeedsLayout];
}

-(void)setViewProperties{
    [self.previewSlider setBackgroundColor:[UIColor blackColor]];
    self.previewSlider.delegate = self;
}

#pragma mark - PKThumbnailSliderViewDelegate - Methods
-(void)thumbnailSliderImageDownloaded:(UIImage *)image atIndex:(NSUInteger)index{
//    NSLog(@"loaded image: %@ at index: %lu", image, index);
    PKPreviewItemView *item = [[PKPreviewItemView alloc] init];
    item.previewThumbnail.image = image;
    item.index = index;
    [self.previewItems addObject:item];
    
    for(int i = 0; i < self.previewItems.count; i++){
        for(int j = i; j < self.previewItems.count; j++){
            PKPreviewItemView *itemI = self.previewItems[i];
            PKPreviewItemView *itemJ = self.previewItems[j];
            if(itemI.index > itemJ.index){
                PKPreviewItemView *temp = [itemI mutableCopy];
                itemI = itemJ;
                itemJ = temp;
            }
        }
    }
    [self updatePreviewItems];
}


-(void)updatePreviewItems{
    [self.previewSlider.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat maxContentWidthForPreviewItemsTray = 0;
    for(int position = 0; position < self.previewItems.count; position++){
        
        CGRect nextPreviewItemBox = [self getPreviewItemRectForPosition:position];
        PKPreviewItemView *currentPreviewItem = self.previewItems[position];
        currentPreviewItem.frame = nextPreviewItemBox;
        maxContentWidthForPreviewItemsTray += CGRectGetWidth(currentPreviewItem.frame);
        [self.previewSlider addSubview:currentPreviewItem];
    }
    [self updatePreviewItemScrollViewWithWidth:maxContentWidthForPreviewItemsTray];
}



-(CGRect)getPreviewItemRectForPosition:(NSUInteger)position{
    CGFloat thumbnailHeight = CGRectGetHeight(self.previewSlider.frame);
    CGFloat thumbnailWidth = CGRectGetWidth(self.previewSlider.frame);
    CGRect currentRect = CGRectMake(thumbnailWidth * position, 0, thumbnailWidth, thumbnailHeight);
//    NSLog(@"rect: {%@} position: %lu", NSStringFromCGRect(currentRect), position);
    return currentRect;
}
-(void)updatePreviewItemScrollViewWithWidth:(CGFloat)maxContentWidth{
    self.previewSlider.contentSize = CGSizeMake(maxContentWidth, CGRectGetHeight(self.previewSlider.frame));
//    for(UIView *view in self.previewSlider.subviews){
//        NSLog(@"preview scroll view subviews: %@", view);
//    }
    self.previewSlider.pagingEnabled = YES;

}






-(void)setAlreadyLoadedImages:(NSMutableArray*)images{
    _images = images;
//    NSLog(@"already Loaded main Images: %@", images);
    [self updatePreviewImages];

}

-(void)updatePreviewImages{
    [self.previewSlider.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat maxContentWidthForPreviewItemsTray = 0;
    for(int position = 0; position < self.images.count; position++){
        
        CGRect nextPreviewItemBox = [self getPreviewItemRectForPosition:position];
        OXImageStackItem *currentPicture = _images[position];
        PKPreviewItemView *currentPreviewItem =  [[PKPreviewItemView alloc] init];
        currentPreviewItem.frame = nextPreviewItemBox;
        currentPreviewItem.index = position;
        //        currentPreviewItem.delegate = self;
        
        currentPreviewItem.previewThumbnail.image = currentPicture.originalImage    ;
        maxContentWidthForPreviewItemsTray += CGRectGetWidth(currentPreviewItem.frame);
        [self.previewSlider addSubview:currentPreviewItem];
    }
    [self updatePreviewItemScrollViewWithWidth:maxContentWidthForPreviewItemsTray];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    static NSInteger previousPage = 0;
    CGFloat pageWidth = self.previewSlider.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    if (previousPage != page) {
        // Page has changed, do your thing!
        // ...
        // Finally, update previous page
//        NSLog(@"page changed: %lu", page);
        [self scrolledToPage:page];
        previousPage = page;
    }
}


-(void)scrolledToPage:(NSUInteger)page{
    //previewSliderView:didSlideToPreviewIndex:;
    [self.delegate previewSliderView:self didSlideToPreviewIndex:page];
}

#pragma mark - PKThumbnailSliderDelegate - Methods
-(void)thumbnailSlider:(PKThumbnailSliderView *)thumbnailSlider didTapOnThumbnailIndex:(NSUInteger)index{
    CGRect viewFrame = [self getPreviewItemRectForPosition:index];
    [self.previewSlider scrollRectToVisible:viewFrame animated:YES];
}




@end
