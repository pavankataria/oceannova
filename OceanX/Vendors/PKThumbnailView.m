//
//  PKThumbnailView.m
//  OceanX
//
//  Created by Pavan Kataria on 26/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKThumbnailView.h"
@interface PKThumbnailView ()
    @property (nonatomic, retain) UIButton *button;
@end

@implementation PKThumbnailView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialiseOtherComponents];
    }
    return self;
}
-(instancetype)init{
    self = [self initWithFrame:CGRectZero];
    return self;
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    //we dont want the preview thumbnail to also update its coordinates whenever the frame changes
    CGFloat height = CGRectGetHeight(self.bounds) - self.margin * 2;
    CGFloat width = CGRectGetWidth(self.bounds) - self.margin * 2;
    self.previewThumbnail.frame = CGRectMake(self.margin, self.margin, width, height);
    
    CGSize deleteButtonSize = CGSizeMake(50, 50);
//    CGFloat deleteButtonSizeEdgePadding = 10;
    CGFloat deleteButtonXPosition = CGRectGetWidth(self.bounds) - deleteButtonSize.width;
    self.button.frame = (CGRect){{deleteButtonXPosition, 0},deleteButtonSize};
}

-(void)initialiseOtherComponents{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap)];
    [self addGestureRecognizer:tapGesture];
}

-(void)deleteButtonDidPress:(id)sender{
    [self.delegate pkThumbnailView:self didDeleteThumbnailPreviewAtIndexPosition:self.index];
}
-(void)setupDeleteButton{
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.button addTarget:self action:@selector(deleteButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
    [self.button setTitle:@"X" forState:UIControlStateNormal];
    
    [self.button setTitleShadowColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    self.button.titleLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    
    [self.button setHidden:NO];
    [self addSubview:self.button];
}
-(void)didTap{
    if([self.delegate respondsToSelector:@selector(pkThumbnailView:didSelectThumbnailPreviewAtIndexPosition:)]){
        [self.delegate pkThumbnailView:self didSelectThumbnailPreviewAtIndexPosition:self.index];
    }
}

@end
