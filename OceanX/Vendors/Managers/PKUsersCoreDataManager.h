////
////  PKUsersCoreDataManager.h
////  Tinder Auto Background Liker
////
////  Created by Pavan Kataria on 23/10/2014.
////  Copyright (c) 2014 Pavan Kataria. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//#import <CoreData/CoreData.h>
//#import "AppDelegate.h"
//#import "LoginSession.h"
//
//@interface PKUsersCoreDataManager : NSObject
//
//@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
//@property (nonatomic, retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;
//
//
//@property (readwrite, nonatomic, copy) void (^matchesLikedProgressBlock)(NSString *likedUserTinderId);
//
//
//+(void)checkForAnyPendingUsersThatNeedLiking;
//+(void)saveCurrentUsersBatchToStore:(NSArray*)usersBatchArray
//                            success:(void (^)(int totalUsersJustSaved))successBlock
//                       failedToSave:(void(^)(NSError* error))failureBlock;
//
//+(void)updateUserSwipedWithTinderUserId:(NSString*)tinderUserId
//                            success:(void (^)())successBlock
//                       failedToSave:(void(^)(NSError* error))failureBlock;
//
//
////+(void)setMatchesLikedProgressBlock:(void (^)(NSUInteger matchesLIked))matchesLikedProgressBlock;
//+(void)setMatchesLikedProgressBlock:(void (^)(NSString *likedUserTinderId))matchesLikedProgressBlock;
//+(NSUInteger)countOfLikedUsers;
//
//
//
//+(LoginSession*)retrieveLoginSession;
//+(void)setLoginSessionPropertyAndSaveForFBUserId:(NSString*)fbUserId;
//+(void)setLoginSessionPropertyAndSaveForFBAccessToken:(NSString*)fbAccessToken;
//+(void)setLoginSessionPropertyAndSaveForTinderAuthToken:(NSString*)tinderAuthToken;
//+(void)clearLoginSessionData;
//
//+(NSUInteger)retrieveTotalRemainingMatchingCount;
//@end
