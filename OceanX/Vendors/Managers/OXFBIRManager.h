//
//  OXFBIRManager.h
//  Ocean
//
//  Created by Pavan Kataria on 01/04/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OXFBIRManager : NSObject

+(instancetype)sharedManager;

@end
