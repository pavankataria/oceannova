//
//  LoginSession.m
//  LightningTinderLiker
//
//  Created by Pavan Kataria on 30/10/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "LoginSession.h"


@implementation LoginSession

@dynamic loginSessionFBUserId;
@dynamic loginSessionFBAccessToken;
@dynamic loginSessionTinderAuthToken;

@end
