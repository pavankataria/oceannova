//
//  LoginSession.h
//  LightningTinderLiker
//
//  Created by Pavan Kataria on 30/10/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LoginSession : NSManagedObject

@property (nonatomic, retain) NSString * loginSessionFBUserId;
@property (nonatomic, retain) NSString * loginSessionFBAccessToken;
@property (nonatomic, retain) NSString * loginSessionTinderAuthToken;

@end
