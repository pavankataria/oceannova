//
//  FacebookSession.h
//  Tinder Auto Background Liker
//
//  Created by Pavan Kataria on 21/10/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "FBGraphHeaders.h"
#import <UIKit/UIWebView.h>
@interface PKUserSession : NSObject

//Facebook
@property (nonatomic, retain) NSString *facebookAccessToken;
@property (nonatomic, retain) NSString *facebookUserId;
@property (nonatomic, retain) NSDictionary *facebookUserProfileData;


//Tinder
@property (nonatomic, retain) NSDictionary *tinderUserProfileData;
@property (nonatomic, retain) NSString *tinderAuthToken;


/*
 //FBGraph
@property (nonatomic, retain) FBGraph *fbGraph;
@property (nonatomic, retain) FBGraphResponse *fbGraphResponse;
@property (nonatomic, retain) UIWebView *webView;
*/



+ (instancetype)sharedManager;
-(NSDictionary*)getFacebookSessionForTinderAuthorisation;
-(BOOL)isThereASessionStored;


@end
