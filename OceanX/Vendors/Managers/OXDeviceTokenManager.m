//
//  OXDeviceTokenManager.m
//  Ocean
//
//  Created by Pavan Kataria on 31/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXDeviceTokenManager.h"

@implementation OXDeviceTokenManager

+(instancetype)sharedManager{
    static OXDeviceTokenManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

-(void)storeDeviceToken:(NSData*)deviceTokenData{
    NSLog(@"attempting to store device token: %@", deviceTokenData);
    NSString * token = [NSString stringWithFormat:@"%@", deviceTokenData];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSLog(@"formatted device token to: %@", token);
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:token forKey:kUserDeviceTokenStringKey];
    [userDefaults synchronize];
}
-(NSString*)userDeviceToken{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    NSLog(@"device token string: %@", [userDefaults valueForKey:kUserDeviceTokenStringKey]);
    
    if([userDefaults valueForKey:kUserDeviceTokenStringKey]){
        NSString *deviceTokenString = [userDefaults valueForKey:kUserDeviceTokenStringKey];
        return deviceTokenString;
    }
    return nil;
}

@end
