//
//  OXDeviceTokenManager.h
//  Ocean
//
//  Created by Pavan Kataria on 31/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OXDeviceTokenManager : NSObject
+(instancetype)sharedManager;
-(void)storeDeviceToken:(NSData*)deviceTokenData;
-(NSString*)userDeviceToken;


@end
