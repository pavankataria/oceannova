//
//  OXFBIRManager.m
//  Ocean
//
//  Created by Pavan Kataria on 01/04/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXFBIRManager.h"
#import "DraggableViewBackground.h"

@implementation OXFBIRManager
+(instancetype)sharedManager{
    static OXFBIRManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[OXFBIRManager alloc] initRightNow];
    });
    return manager;
}

-(instancetype)initRightNow{
//    static DraggableViewBackground *draggableBackground = [[DraggableViewBackground alloc] initWithFrame:self.view];
    return nil;
}
-(instancetype)init{
    //There's a better way to avoid users from call the init function
    //perhaps by firing an NSAssert crash letting the user know to call the class method instead
    return nil;
}
@end
