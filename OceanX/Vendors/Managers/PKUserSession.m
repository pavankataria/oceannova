////
////  FacebookSession.m
////  Tinder Auto Background Liker
////
////  Created by Pavan Kataria on 21/10/2014.
////  Copyright (c) 2014 Pavan Kataria. All rights reserved.
////
//
#import "PKUserSession.h"
#import "AppDelegate.h"

//@interface PKUserSession(){
////    NSUserDefaults *standardUserDefaults;
//}
//@end
@implementation PKUserSession
//@synthesize tinderAuthToken = _tinderAuthToken;
//- (id)init{
//    if (self = [super init]){
//        self.fbGraphResponse = [[FBGraphResponse alloc] init];
//        self.fbGraph = [[FBGraph alloc] init];
////        standardUserDefaults = [NSUserDefaults standardUserDefaults];
//
//
//    }
//    return self;
//}
//
//- (void)dealloc {
//    // Should never be called, but just here for clarity really.
//}
//
//+ (instancetype)sharedManager {
//    
//    static PKUserSession *sharedMyManager = nil;
//    static dispatch_once_t onceToken;
//    
//    dispatch_once(&onceToken, ^{
//        sharedMyManager = [[self alloc] init];
//    });
//    return sharedMyManager;
//}
//
//
//-(NSDictionary*)getFacebookSessionForTinderAuthorisation{
//    NSLog(@"%s  get facebook session for tinder authorisation: FBUSERID: %@\nFBACCESSTOKEN: %@", __PRETTY_FUNCTION__, self.facebookUserId, self.facebookAccessToken);
//
//    return @{@"facebook_token" : self.facebookAccessToken , @"facebook_id" : self.facebookUserId};
//}
//
//-(void)setFacebookUserProfileData:(NSDictionary *)userProfileData{
//    _facebookUserProfileData = userProfileData;
//    self.facebookUserId = [self.facebookUserProfileData objectForKey:@"id"];
//
//}
//#pragma mark -
//#pragma mark setters
//-(void)setFacebookUserId:(NSString *)userId{
//    _facebookUserId = userId;
//    [PKUsersCoreDataManager setLoginSessionPropertyAndSaveForFBUserId:userId];
//}
//
//-(void)setFacebookAccessToken:(NSString*)accessToken{
//    _facebookAccessToken = accessToken;
//    [PKUsersCoreDataManager setLoginSessionPropertyAndSaveForFBAccessToken:accessToken];
//}
//-(void)setTinderAuthToken:(NSString *)tinderAuthToken{
//    _tinderAuthToken = tinderAuthToken;
//    [PKUsersCoreDataManager setLoginSessionPropertyAndSaveForTinderAuthToken:tinderAuthToken];
//}
//-(void)setTinderUserProfileData:(NSDictionary *)tinderUserProfileData{
////    NSLog(@"Tinder user profile data set to: %@:", tinderUserProfileData);
//    
//    _tinderUserProfileData = tinderUserProfileData;
//
//    if([tinderUserProfileData objectForKey:@"token"]){
//        /*
//        NSError *error;
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tinderUserProfileData
//                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
//                                                             error:&error];
//        
//        if (! jsonData) {
//            NSLog(@"Got an error: %@", error);
//        } else {
//            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
////            NSLog(@"tinder user profile json string: %@", jsonString);
//        }
//        */
//        NSString *authToken = [self.tinderUserProfileData objectForKey:@"token"];
//        NSLog(@"AUTTHHH TOKEN: %@", authToken);
//        self.tinderAuthToken = authToken;
//    }
//
//}
//
//
//#pragma mark - 
//#pragma mark Getters
////-(NSString *)tinderAuthToken{
////    NSLog(@"tinder auth token: %@ id: %@ at: %@", _tinderAuthToken, _facebookUserId, _facebookAccessToken	);
////    NSLog(@"retrieving tinder auth token %@", [NSString stringWithUTF8String:[(NSData*)[standardUserDefaults objectForKey:@"PKTABLTinderAuthToken"] bytes]]);
////    return _tinderAuthToken;
////}
//
//
///*
//-(void)saveTinderUserAuthTokenToUserDefaults:(NSString*)tinderAuthToken{
//    NSLog(@"ABOUT TO SET TINDER AUTH TOKEN FROM INPUT VALUE: %@", tinderAuthToken);
//    [standardUserDefaults setObject:[tinderAuthToken dataUsingEncoding:NSUTF8StringEncoding] forKey:@"PKTABLTinderAuthToken"];
//[[NSUserDefaults standardUserDefaults] synchronize];
//    //    NSLog(@"tinder auth ns userdefaults set to: %@",[standardUserDefaults objectForKey:@"PKTABLTinderAuthToken"]);
////    NSLog(@"nsuserdefaults: %@", [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys]);
//
//}
//-(void)saveFacebookAccessTokenToUserDefaults:(NSString*)facebookAccessToken{
//    [standardUserDefaults setObject:[facebookAccessToken  dataUsingEncoding:NSUTF8StringEncoding] forKey:@"PKTABLFacebookAccessToken"];
//[[NSUserDefaults standardUserDefaults] synchronize];
//}
//-(void)saveFacebookUserIdToUserDefaults:(NSString*)facebookUserId{
//    [standardUserDefaults setObject:[facebookUserId dataUsingEncoding:NSUTF8StringEncoding] forKey:@"PKTABLFacebookUserId"];
//[[NSUserDefaults standardUserDefaults] synchronize];
//}
//
// */
//-(BOOL)isThereASessionStored{
//    
//    
//    
////    NSLog(@"nsuserdefaults: %@", [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys]);
//    LoginSession *currentLoginSession = [PKUsersCoreDataManager retrieveLoginSession];
//    NSString *fbUserId = currentLoginSession.loginSessionFBUserId;
//    NSString *fbAccessToken = currentLoginSession.loginSessionFBAccessToken;
//    NSString *tinderAuthToken1 = currentLoginSession.loginSessionTinderAuthToken;
//    NSLog(@"analysing FBUSERID: %@\nFBACCESSTOKEN: %@\nTINDERAUTHTOKEN: %@", fbUserId, fbAccessToken, tinderAuthToken1);
//    if([fbUserId length] > 0 &&
//       [fbAccessToken length] > 0 &&
//       [tinderAuthToken1 length] > 0){
//        _facebookUserId = fbUserId;
//        _facebookAccessToken = fbAccessToken;
//        _tinderAuthToken = tinderAuthToken1;
//        NSLog(@"There is already a session stored, now attempt log in into tinder");
//        return YES;
//    }
//    else{
//        NSLog(@"There is no session, probably first time running.");
//        
//        return  NO;
//    }
///*    @try {
//         fbUserId = [NSString stringWithUTF8String:[(NSData*)[standardUserDefaults objectForKey:@"PKTABLFacebookUserId"] bytes]] ;
//        
//        fbAccessToken = [NSString stringWithUTF8String:[(NSData*)[standardUserDefaults objectForKey:@"PKTABLFacebookAccessToken"] bytes]];
//        
//        tinderAuthToken1 = [NSString stringWithUTF8String:[(NSData*)[standardUserDefaults objectForKey:@"PKTABLTinderAuthToken"] bytes]];
//        
//    }
//    @catch (NSException *exception) {
//        NSLog(@"REMOVING NS USER DEFAULT PKTABL KEYS");
//         [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PKTABLFacebookUserId"];
//         [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PKTABLFacebookAccessToken"];
//         [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PKTABLTinderAuthToken"];
//    }
//    @finally {
//        NSLog(@"analysing FBUSERID: %@\nFBACCESSTOKEN: %@\nTINDERAUTHTOKEN: %@", fbUserId, fbAccessToken, tinderAuthToken1);
//        if([fbUserId length] > 0 &&
//           [fbAccessToken length] > 0 &&
//           [tinderAuthToken1 length] > 0){
//            _facebookUserId = fbUserId;
//            _facebookAccessToken = fbAccessToken;
//            _tinderAuthToken = tinderAuthToken1;
//            NSLog(@"There is already a session stored, now attempt log in into tinder");
//            return YES;
//        }
//        else{
//            NSLog(@"There is no session, probably first time running.");
//            
//            return  NO;
//        }
//    }
// */
//}
//@end




@end
