////
////  PKUsersCoreDataManager.m
////  Tinder Auto Background Liker
////
////  Created by Pavan Kataria on 23/10/2014.
////  Copyright (c) 2014 Pavan Kataria. All rights reserved.
////
//
//#import "PKUsersCoreDataManager.h"
//@interface PKUsersCoreDataManager(){
//    BOOL busyLikingUsers;
//}
//
//@end
//@implementation PKUsersCoreDataManager
//
//
//static PKUsersCoreDataManager *sharedCoreDataManager;
//
//+ (PKUsersCoreDataManager*)sharedManager{
//    @synchronized(self){
//        if(!sharedCoreDataManager){
//            NSLog(@"shared manager");
//            static dispatch_once_t onceToken;
//            dispatch_once(&onceToken, ^{
//                sharedCoreDataManager = [[PKUsersCoreDataManager alloc] init];
//            });
//            
//            
//        }
//    }
//    return sharedCoreDataManager;
//}
//
//
//-(id)init{
//    self = [super init];
//    if(!self) return nil;
//    
//    self.managedObjectContext = [AppDelegate getAppDelegateReference].managedObjectContext;
//    self.persistentStoreCoordinator = [AppDelegate getAppDelegateReference].persistentStoreCoordinator;
//    
//    busyLikingUsers = false;
//    return self;
//}
////success:(void (^)(NSURLSessionDataTask *, NSDictionary*))success
////failure:(void (^)(NSURLSessionDataTask *, NSError *))failure{
////
//
//+(void)checkForAnyPendingUsersThatNeedLiking{
//    [[PKUsersCoreDataManager sharedManager] checkForAnyPendingUsersThatNeedLiking];
//}
//+(NSUInteger)retrieveTotalRemainingMatchingCount{
//    return [[PKUsersCoreDataManager sharedManager] retrieveTotalRemainingMatchingCount];
//}
//+(NSUInteger)countOfLikedUsers{
//    return [[PKUsersCoreDataManager sharedManager] countOfLikedUsers];
//}
//-(NSUInteger)countOfLikedUsers{
//    NSLog(@"start of counting liked users");
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:NSStringFromClass([TinderUser class]) inManagedObjectContext:self.managedObjectContext];
//    [fetchRequest setEntity:entityToBeFetched];
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.%@ == 1", Key(TinderUser, tinderUserSwiped)];
//    [fetchRequest setPredicate:predicate];
//    
//    NSError *error = nil;
//    
//   NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
////    NSLog(@"facebook tinder user id: %@", [[results firstObject] facebookUserId]);
//    
//        NSLog(@"finish:estart of counting liked users");
//    return [results count];
//}
//-(NSUInteger)retrieveTotalRemainingMatchingCount{
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:@"TinderUser" inManagedObjectContext:self.managedObjectContext];
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.%@ == 0", Key(TinderUser, tinderUserSwiped)];
//    [fetchRequest setPredicate:predicate];
//    [fetchRequest setEntity:entityToBeFetched];
//    NSError *error = nil;
//    return [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] count];
//
//}
//-(void)checkForAnyPendingUsersThatNeedLiking
//{
//    NSLog(@"checking for users that need liking");
//    if(!busyLikingUsers){
//        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//        NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:@"TinderUser" inManagedObjectContext:self.managedObjectContext];
//        
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.%@ == 0", Key(TinderUser, tinderUserSwiped)];
//        [fetchRequest setPredicate:predicate];
//        [fetchRequest setEntity:entityToBeFetched];
//        [fetchRequest setFetchLimit:10];
//        NSError *error = nil;
//        NSArray *pendingUsersToLike = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
//
////        NSLog(@"error: %@", [error localizedDescription]);
////        NSLog(@"pendingUsersToLike count: %d", [pendingUsersToLike count]);
//        
//        if([pendingUsersToLike count] > 0){
//            busyLikingUsers = true;
//            [self startLikingPendingUsersWithArray:pendingUsersToLike];
//        }
//        else{
//            busyLikingUsers = false;
//        }
//    }
//    
//        NSLog(@"finished: checking for users that need liking");
//}
//-(void)startLikingPendingUsersWithArray:(NSArray*)pendingUsersToLike{
////    NSLog(@"startLikingPendingUsersWithArray %@", pendingUsersToLike);
////
////    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
////    
////    manager.responseSerializer = [AFJSONResponseSerializer serializer];
////    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
////    
////    manager.requestSerializer = [AFJSONRequestSerializer serializer];
////    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
////    [manager.requestSerializer setValue:[[PKUserSession sharedManager] tinderAuthToken] forHTTPHeaderField:@"X-Auth-Token"];
//
//    
//   
//
//    NSMutableArray *muiltipleOperations = [[NSMutableArray alloc] init];
////    NSMutableArray *arrayOfUsersIdsToSwipeAndSave = [[NSMutableArray alloc] init];
//    for(int i = 0; i < [pendingUsersToLike count]; i++){
////        TinderUser *currentTinderUser = [pendingUsersToLike objectAtIndex:i];
////        [arrayOfUsersIdsToSwipeAndSave addObject:currentTinderUser.tinderUserId];
////        NSLog(@"about to send like for name: %@", currentTinderUser.tinderUserName);
//        AFHTTPRequestOperation *currentOperation = [PKTinderAPIClientManager
//                                                    likeOrPass:@"like"
//                                                    tinderUser:[pendingUsersToLike objectAtIndex:i]
//                                                    success:^(AFHTTPRequestOperation *operation, NSString *likedUserTinderId, NSData *json) {
////                                                          NSLog(@"TinderUser: %@", [currentTinderUser description]);
////                                                        NSLog(@"response: %@ LIKED userId: %@, %@, %d", [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding], likedUserTinderId, [currentTinderUser tinderUserName], [currentTinderUser ageFromBirthday] );
//                                                        
//                                                        [PKUsersCoreDataManager updateUserSwipedWithTinderUserId:likedUserTinderId
//                                                                                                         success:^{
////                                                                                                             NSLog(@"successfuly saved: %@", likedUserTinderId);
//                                                                                                             dispatch_async(dispatch_get_main_queue(), ^{
//                                                                                                                 NSAssert(self.matchesLikedProgressBlock!=nil,@"MatchesLikedProgressBlock needs to be set");
//                                                                                                                 if (self.matchesLikedProgressBlock) {
////                                                                                                                      printf("updating matches liked progress block");
//                                                                                                                     self.matchesLikedProgressBlock(likedUserTinderId);
//                                                                                                                 }
//                                                                                                                 
//                                                                                                             });
//                                                                                                             
//                                                                                                             
//                                                                                                         } failedToSave:^(NSError *error) {
//                                                                                                             NSLog(@"failed to save error: %@", [error localizedDescription]);
//                                                                                                         }];
//
//                                                        
//                                                    }
//                                                    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                                        NSLog(@"LIKE OR PASS: ERROR :%@", error.localizedDescription);
//                                                        
//                                                    }];
//        
//        
//        [muiltipleOperations addObject:currentOperation];
//    }
//    NSArray *operations = [AFURLConnectionOperation
//                           batchOfRequestOperations:muiltipleOperations
//                           progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
////                               NSLog(@"progress: %f", (float)numberOfFinishedOperations / totalNumberOfOperations);
////                               [self.progressBar setProgress:(float)numberOfFinishedOperations / totalNumberOfOperations animated:YES];
//                           } completionBlock: ^(NSArray *operations) {
//                               
//                                NSLog(@"All operations in batch complete");//: %@", operations);
//
//                               NSError *error;
//                               int i = 0;
//                               
//                               for (AFHTTPRequestOperation *op in operations) {
//                                   if (op.isCancelled){
//                                       NSLog(@"operation: %d %@ was cancelled:", i, op.name);
////                                       return ;
//                                   }
//                                   else if (op.error){
//                                       error = op.error;
//                                       NSLog(@"operation %d error: %@ response: %@", i, [error localizedDescription], [[NSString alloc] initWithData:op.responseObject encoding:NSUTF8StringEncoding]);
////                                       return;
//                                   }
//                                   else if (op.responseObject){
//                                        NSLog(@"operation %d response: %@", i, [[NSString alloc] initWithData:op.responseObject encoding:NSUTF8StringEncoding]);
//
//                                       // process your responce here
//                                   }
//                                   else{
//                                       NSLog(@"ELSE operation %d response: %@", i, [[NSString alloc] initWithData:op.responseObject encoding:NSUTF8StringEncoding]);
//                                   }
//                                   i++;
//                               }
//                               
//                               
//                               //Have to be grouped together
//                               busyLikingUsers = false;
//                               [self checkForAnyPendingUsersThatNeedLiking];
//
//                               /*
//                               [PKUsersCoreDataManager updateUsersSwipeWithArray:arrayOfUsersIdsToSwipeAndSave
//                                                                                success:^{
//                                                                                    NSLog(@"successfuly saved: %@", arrayOfUsersIdsToSwipeAndSave);
//                                                                                    
//                                                                                    //                                                            [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
//                                                                                    //                                                                totalMatchesLiked++;
//                                                                                    //                                                                [self updateTextForLabel:_matchesLiked withText:[NSString stringWithFormat:@"%d", totalMatchesLiked]];
//                                                                                    //                                                            }];
//                                                                                    
//
//                                                                                    
//                                                                                } failedToSave:^(NSError *error) {
//                                                                                    NSLog(@"failed to save error: %@", [error localizedDescription]);
//                                                                                }];
//
//                                */
//
//                           }];
//    
//    
//    [[NSOperationQueue mainQueue] addOperations:operations waitUntilFinished:NO];
//    
//}
//
//+(void)updateUserSwipedWithTinderUserId:(NSString*)tinderUserId
//                                success:(void (^)())successBlock
//                           failedToSave:(void(^)(NSError* error))failureBlock{
//    [[PKUsersCoreDataManager sharedManager] updateUserSwipedWithTinderUserId:tinderUserId
//                                                                     success:successBlock
//                                                                failedToSave:failureBlock];
//    
//}
//
//-(void)updateUserSwipedWithTinderUserId:(NSString*)tinderUserId
//                success:(void (^)())successBlock
//           failedToSave:(void(^)(NSError* error))failureBlock{
//    NSParameterAssert(tinderUserId);
//
//
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    
//    //Setting Entity to be Queried
//    NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:@"TinderUser" inManagedObjectContext:self.managedObjectContext];
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.%@ == %@", Key(TinderUser, tinderUserId), tinderUserId];
//    //    NSLog(@"predicate to use: %@", [predicate description]);
////    [fetchRequest setFetchLimit:1];
//    
//    
//    [fetchRequest setEntity:entityToBeFetched];
//    [fetchRequest setPredicate:predicate];
//    NSError *fetchError = nil;
//    NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
//    
//    TinderUser *user;
//
//    if([results count] > 1){
////        NSLog(@"multiple copies of %@ - total copies count: %d", [[results firstObject] tinderUserName], [results count]);
//        //Delete every single instance except the last one
//        for(int i = 0; i < [results count]-1; i++){
////            NSLog(@"swipe value : %d", [[[results objectAtIndex:i] tinderUserSwiped] intValue]);
//            [self.managedObjectContext deleteObject:[results objectAtIndex:i]];
//        }
//        //Last object because the last object would be the one without
//        user = [results lastObject];
//    }
//    else{
////        NSLog(@"Unique: %@", [[results firstObject] tinderUserName]);
//        user = [results firstObject];
//    }
//
//    
//    
////    NSLog(@"saving user %@ before swipe: %d", user.tinderUserName, [user.tinderUserSwiped intValue]);
//    NSAssert(user != nil, @"Tinder user cannot be nil");
//    
//    [user setTinderUserSwiped:[NSNumber numberWithBool:YES]];
////    NSLog(@"saving user %@ after swipe: %d", user.tinderUserName, [user.tinderUserSwiped intValue]);
//    
//    NSError *error = nil;
//    if([self.managedObjectContext save:&error]){
//        if(successBlock){
//            successBlock();
//        }
//    }
//    else{
//        if(failureBlock){
//            failureBlock(error);
//        }
//    }
//       // }];
//    
//}
//+(void)saveCurrentUsersBatchToStore:(NSArray*)usersBatchArray
//                            success:(void(^)(int totalUsersJustSaved))successBlock
//                       failedToSave:(void(^)(NSError* error))failureBlock {
//    [[PKUsersCoreDataManager sharedManager]saveCurrentUsersBatchToStore:usersBatchArray success:successBlock failedToSave:failureBlock];
//}
//
//+(void)updateUsersSwipeWithArray:(NSArray*)usersToSaveSwipe
//                         success:(void (^)())successBlock
//                    failedToSave:(void(^)(NSError* error))failureBlock{
//    
//    [[PKUsersCoreDataManager sharedManager] updateUsersSwipeWithArray:usersToSaveSwipe
//                                                              success:successBlock
//                                                         failedToSave:failureBlock];
//    
//}
//
//
//
//-(void)updateUsersSwipeWithArray:(NSArray*)usersToSaveSwipeArray
//                         success:(void (^)())successBlock
//                    failedToSave:(void(^)(NSError* error))failureBlock{
//    
//    NSParameterAssert(usersToSaveSwipeArray);
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    
//    //Setting Entity to be Queried
//    NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:@"TinderUser" inManagedObjectContext:self.managedObjectContext];
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.%@ in %@", Key(TinderUser, tinderUserId), usersToSaveSwipeArray];
//    //    NSLog(@"predicate to use: %@", [predicate description]);
//    [fetchRequest setEntity:entityToBeFetched];
//    [fetchRequest setPredicate:predicate];
//    NSError *fetchError = nil;
//    NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
//    // [self.managedObjectContext performBlock:^{
//    NSLog(@"COUNT OF RESULT WHERE TO SAVE SWIPED ARRAY %d", [results count]);
//    TinderUser *user = [results firstObject];
//    
////    NSLog(@"saving user %@ before swipe: %d", user.tinderUserName, [user.tinderUserSwiped intValue]);
//    NSAssert(user != nil, @"Tinder user cannot be nil");
//    
//    [user setTinderUserSwiped:[NSNumber numberWithBool:YES]];
////    NSLog(@"saving user %@ after swipe: %d", user.tinderUserName, [user.tinderUserSwiped intValue]);
//    
//    NSError *error = nil;
//    if([self.managedObjectContext save:&error]){
//        if(successBlock){
//            successBlock();
//        }
//    }
//    else{
//        if(failureBlock){
//            failureBlock(error);
//        }
//    }
//    
//    
//}
//
//-(void)saveCurrentUsersBatchToStore:(NSArray*)usersBatchArray
//                            success:(void (^)(int totalUsersJustSaved))successBlock
//                       failedToSave:(void(^)(NSError* error))failureBlock {
//    
//    /*
//     NSLog(@"count of users batch array: %d", [usersBatchArray count]);
//     for(int i = 0; i < [usersBatchArray count]; i++){
//     TinderUser * currentTinderUser = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([TinderUser class])
//     inManagedObjectContext:self.managedObjectContext];
//     [currentTinderUser initWithJSONDictionary:[usersBatchArray objectAtIndex:i]];
//     NSLog(@"add id: %@ name: %@", currentTinderUser.tinderUserId, currentTinderUser.tinderUserName);
//     }
//     NSError *errorInside;
//     if (![self.managedObjectContext save:&errorInside]) {
//     if(failureBlock){
//     failureBlock(errorInside);
//     }
//     }
//     else{
//     if(successBlock){
//     successBlock([usersBatchArray count]);
//     }
//     }
//     
//     */
//    
//    
//    
//    
//    
//    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
//    temporaryContext.parentContext = self.managedObjectContext;
//    
//    [temporaryContext performBlock:^{
//        for(int i = 0; i < [usersBatchArray count]; i++){
//            TinderUser * currentTinderUser = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([TinderUser class])
//                                                                           inManagedObjectContext:temporaryContext];
////            [currentTinderUser initWithJSONDictionary:[usersBatchArray objectAtIndex:i]];
//            if([((TinderUser*)currentTinderUser) respondsToSelector:@selector(initWithJSONDictionary:)]){
//                [currentTinderUser initWithJSONDictionary:[usersBatchArray objectAtIndex:i]];
//                
//            }
//            else{
//                [self.managedObjectContext undo];
//
//                
//                NSLog(@"object does not response to selector: initWithJSONDictionary");
//            };
////            NSLog(@"add id: %@ name: %@", currentTinderUser.tinderUserId, currentTinderUser.tinderUserName);
//            
////            NSError *error = nil;
////            NSCAssert([self.managedObjectContext obtainPermanentIDsForObjects:@[currentTinderUser] error:&error], @"perm id error: %@", error);
//            
////            NSLog(@"perm a.objectID = %@", currentTinderUser.objectID);
//            
//            /*
//            AFHTTPRequestOperation *operation = [PKTinderAPIClientManager
//                                                 likeOrPass:@"like" tinderUser:currentTinderUser
//                                                 success:^(AFHTTPRequestOperation *operation, NSString *likedUserTinderId, NSData *json) {
//                                                     NSLog(@"likeOrPass Response: %@", [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);
//                                                     
////                                                     NSLog(@"outside of block LIKED TINDER ID: %@", likedUserTinderId);
//
//                                                     dispatch_async(dispatch_get_main_queue(), ^{
//                                                         if (self.matchesLikedProgressBlock) {
//                                                             
////                                                             NSLog(@"inside block LIKED TINDER ID: %@", likedUserTinderId);
//
////                                                             NSAssert([currentTinderUser.tinderUserId isEqualToString:likedUser.tinderUserId], @"the user ids are not the same, USE given tinder user id in success block instead of CurrentTinderUser in for loop");
////                                                             
//                                                             self.matchesLikedProgressBlock(likedUserTinderId);
//                                                         }
//                                                     });
//                                                     
//                                                     
//                                                 }
//                                                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                                     int statusCode = [operation.response statusCode];
//                                                     if(statusCode == 429){
//                                                         [PKTinderAPIClientManager addOperationToQueue:[operation copy]];
//                                                     }
//                                                     else{
//                                                         NSLog(@"likeOrPass Error: %@", [error localizedDescription]);
//                                                         [PKTinderAPIClientManager addOperationToQueue:[operation copy]];
//                                                     }
//                                                     
//                                                 }];
//            [PKTinderAPIClientManager addOperationToQueue:operation];
//            
//            */
//        }
//        // push to parent
//        NSError *error;
//        if (![temporaryContext save:&error])
//        {
//            failureBlock(error);
//        }
//        
//        // save parent to disk asynchronously
//        [self.managedObjectContext performBlock:^{
//            NSError *error;
//            if (![self.managedObjectContext save:&error])
//            {
//                failureBlock(error);
//            }
//            else{
//                successBlock([usersBatchArray count]);
//            }
//        }];
//    }];
//    
//    [self checkForAnyPendingUsersThatNeedLiking];
//}
//
//+(void)setMatchesLikedProgressBlock:(void (^)(NSString *likedUserTinderId))matchesLikedProgressBlock{
//    [[PKUsersCoreDataManager sharedManager] setMatchesLikedProgressBlock:matchesLikedProgressBlock];
//}
//
//
//
//
////-(void)setMatchesLikedProgressBlock:(void (^)())matchesLikedProgressBlock{
////    self.matchesLikedProgressBlock =  matchesLikedProgressBlock;
////}
//
//+(LoginSession*)retrieveLoginSession{
//    return [[PKUsersCoreDataManager sharedManager] retrieveLoginSession];
//}
//-(LoginSession*)retrieveLoginSession{
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entityToBeFetched = [NSEntityDescription entityForName:NSStringFromClass([LoginSession class]) inManagedObjectContext:self.managedObjectContext];
//    [fetchRequest setEntity:entityToBeFetched];
//    [fetchRequest setFetchLimit:1];
//    
//    NSError *fetchError = nil;
//    NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
//    if([results count] == 0){
//        LoginSession *loginSession = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([LoginSession class]) inManagedObjectContext:self.managedObjectContext];
//        [loginSession setLoginSessionFBUserId:@""];
//        [loginSession setLoginSessionFBAccessToken:@""];
//        [loginSession setLoginSessionTinderAuthToken:@""];
//        
//        // save parent to disk asynchronously
//
//        NSError *saveError;
//        if ([self.managedObjectContext save:&saveError]){
//            NSLog(@"saved user session must have been first time");
//        }
//        else{
//            NSLog(@"was not able to save sessions");
//        }
//    
//        return (LoginSession*)[[self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError] firstObject];
//    }
//    else{
//        return (LoginSession*)[results firstObject];
//    }
//}
//+(void)setLoginSessionPropertyAndSaveForFBUserId:(NSString*)fbUserId{
//    [[PKUsersCoreDataManager sharedManager] setLoginSessionPropertyAndSaveForFBUserId:fbUserId];
//}
//+(void)setLoginSessionPropertyAndSaveForFBAccessToken:(NSString*)fbAccessToken{
//    [[PKUsersCoreDataManager sharedManager] setLoginSessionPropertyAndSaveForFBAccessToken:fbAccessToken];
//}
//+(void)setLoginSessionPropertyAndSaveForTinderAuthToken:(NSString*)tinderAuthToken{
//    [[PKUsersCoreDataManager sharedManager] setLoginSessionPropertyAndSaveForTinderAuthToken:tinderAuthToken];
//    
//}
//+(void)clearLoginSessionData{
//    [[PKUsersCoreDataManager sharedManager] clearLoginSessionData];
//}
//-(void)setLoginSessionPropertyAndSaveForFBUserId:(NSString*)fbUserId{
//    [[self retrieveLoginSession] setLoginSessionFBUserId:fbUserId];
//    [self save];
//}
//-(void)setLoginSessionPropertyAndSaveForFBAccessToken:(NSString*)fbAccessToken{
//    [[self retrieveLoginSession] setLoginSessionFBAccessToken:fbAccessToken];
//    [self save];
//}
//-(void)setLoginSessionPropertyAndSaveForTinderAuthToken:(NSString*)tinderAuthToken{
//    [[self retrieveLoginSession] setLoginSessionTinderAuthToken:tinderAuthToken];
//    
//    [self save];
//}
//-(void)save{
//    NSError *saveError;
//    if ([self.managedObjectContext save:&saveError]){
//        NSLog(@"saved property for user session");
//    }
//    else{
//        NSLog(@"was not able to save sessions");
//    }
//    
//    NSLog(@"AFTER SAVE LOGIN SESSION: %@", [[self retrieveLoginSession] description]);
//}
//
//-(void)clearLoginSessionData{
//    [self setLoginSessionPropertyAndSaveForFBAccessToken:@""];
//    [self setLoginSessionPropertyAndSaveForFBUserId:@""];
//    [self setLoginSessionPropertyAndSaveForTinderAuthToken:@""];
//}
//@end
