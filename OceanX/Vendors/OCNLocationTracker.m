//
//  OCNLocationTracker.m
//  Ocean
//
//  Created by Sam Payne on 01/07/2014.
//  Copyright (c) 2014 Ocean. All rights reserved.
//

#import "OCNLocationTracker.h"
#import <MapKit/MapKit.h>

#define X_DISTANCE 1000
#define Y_DISTANCE 1000
#define DEFAULT_X0 -0.53009
#define DEFAULT_X1 0.299377
#define DEFAULT_Y0 51.287688
#define DEFAULT_Y1 51.728729
#define DEFAULT_X_CENTER -0.127758
#define DEFAULT_Y_CENTER 51.507351

@implementation OCNLocationTracker

+ (OCNLocationTracker*) tracker{
    
    static OCNLocationTracker * sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
    
}

-(void)pause{
    [self.manager stopUpdatingLocation];
}

-(void)start{
    NSLog(@"\n\nSTARTED UPDATING LOCATION\n\n");
    [self.manager startUpdatingLocation];
    
    [OXRestAPICient setUserLocationWithCoordinate:[[OCNLocationTracker tracker] userLocation] successBlock:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"success user location response: %@ ", response);
        
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        NSLog(@"set user location FAILED WITH ERROR: %@", error);
    }];
}


-(void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    [self.manager startUpdatingLocation];
}

- (id) init{
    
    self = [super init];
    
    self.manager = [CLLocationManager new];
    if([self.manager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
//        [self.manager requestWhenInUseAuthorization];
        [[UIApplication sharedApplication] sendAction:@selector(requestWhenInUseAuthorization)
                                                   to:self.manager
                                                 from:self
                                             forEvent:nil];

    }
    self.manager.delegate = self;
    self.manager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.manager startUpdatingLocation];
    return self;
}

-(CLLocationCoordinate2D) currentLocation{
    NSLog(@"current location method called");
    if ([[OCNLocationTracker defaultSearchArea] doesContainPoint:self.manager.location.coordinate]){
        return self.manager.location.coordinate;
    }
    else{
        return CLLocationCoordinate2DMake(DEFAULT_Y_CENTER, DEFAULT_X_CENTER);
    }
}

-(OCNCoordinate*) userLocation{
    OCNCoordinate * coordinate = [[OCNCoordinate alloc] init];
    CLLocationCoordinate2D location = [self currentLocation];
    coordinate.coordinateX = location.longitude;
    coordinate.coordinateY = location.latitude;
    
    NSNumberFormatter *format = [[NSNumberFormatter alloc]init];
    [format setRoundingMode:NSNumberFormatterRoundHalfUp];
    coordinate.positionTime = [format stringFromNumber:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]*1000]];
    NSLog(@"user location: %@", coordinate);
    return coordinate;
}

+(OCNPlaneModel*) defaultSearchArea{
    return [OCNPlaneModel placeModelWithCoordinatesX0:DEFAULT_X0 x1:DEFAULT_X1 y0:DEFAULT_Y0  y1: DEFAULT_Y1];
}

-(MKCoordinateRegion) regionWithRadius:(CGFloat) radius{
    return MKCoordinateRegionMakeWithDistance([self currentLocation], radius*2 , radius*2);
    
}

-(MKCoordinateRegion) regionWithLocation:(CLLocationCoordinate2D)location radius:(CGFloat) radius{
    return MKCoordinateRegionMakeWithDistance(location, radius*2 , radius*2);
    
}

-(MKCoordinateRegion) regionWithLocation:(CLLocationCoordinate2D)location{
    return MKCoordinateRegionMakeWithDistance(location, X_DISTANCE*2 , Y_DISTANCE*2);
}

-(MKCoordinateRegion) currentMapRegion{
    return MKCoordinateRegionMakeWithDistance([self currentLocation], X_DISTANCE*2 , Y_DISTANCE*2);
}

-(MKCoordinateRegion) londonMapRegion{
    return MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(DEFAULT_Y_CENTER, DEFAULT_X_CENTER), X_DISTANCE*20 , Y_DISTANCE*20);
}

-(OCNPlaneModel*) currentSearchArea{
    MKCoordinateRegion region = [self currentMapRegion];
    return [OCNPlaneModel planeModelFromMapRegion:region];
}

@end
