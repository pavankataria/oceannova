//
//  UIImage+SPImageFunctions.h
//
//  Created by Sam on 14/09/2014.
//  Copyright (c) 2014 Sam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (SPImageFunctions)

- (UIImage *) scaledCopyOfWidth:(CGFloat)width;

@end
