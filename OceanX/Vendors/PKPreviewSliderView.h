//
//  PKPreviewSliderView.h
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKNewEditMediaContentViewProtocols.h"

#pragma mark - PKThumbnailSliderView interface - Methods
@interface PKImageStack : NSObject
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, assign) NSUInteger index;
@end

@interface PKPreviewSliderView : UIView<PKThumbnailSliderViewDelegate, UIScrollViewDelegate>
@property (nonatomic, retain) IBOutlet UIScrollView *previewSlider;
@property (nonatomic, retain) NSMutableArray *previewItems;

@property (nonatomic, weak) id <PKPreviewSliderViewDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *images;

-(void)setAlreadyLoadedImages:(NSArray*)images;

-(instancetype)initWithFrame:(CGRect)frame;
-(void)updatePreviewItems;




@end
