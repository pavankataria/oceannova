//
//  StatusHUDSBViewController.h
//  OceanX
//
//  Created by Systango on 1/27/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusHUDSBViewController : UIViewController

+(instancetype)sharedManager;

- (void)displaySuccessStatusWithTitle:(NSString*)title message:(NSString *)message;

- (void)displayErrorStatusWithTitle:(NSString*)title message:(NSString *)message;

- (void)displaySuccessStatusWithTitle:(NSString*)title message:(NSString *)message imageName:(NSString *)imageName;

- (void)displayErrorStatusWithTitle:(NSString*)title message:(NSString *)message imageName:(NSString *)imageName;

- (void)displayTryAgainWithSuccessTitle:(NSString*)title message:(NSString *)message imageName:(NSString *)imageName;

- (void)displayTryAgainWithErrorTitle:(NSString*)title message:(NSString *)message imageName:(NSString *)imageName;

- (void)dismiss;
@end

