//
//  PKPreviewThumbnail.m
//  OceanX
//
//  Created by Pavan Kataria on 02/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKPreviewThumbnail.h"
NSUInteger const kPreviewMediaViewControllerThumbnailMargin = 10;

@interface PKPreviewThumbnail()
@property (nonatomic, retain) UIButton *button;
@end

@implementation PKPreviewThumbnail
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.margin = kPreviewMediaViewControllerThumbnailMargin;
        
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.button addTarget:self action:@selector(deleteButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
        NSLog(@"adding thumbnail delete button");
        [self.button setTitle:@"X" forState:UIControlStateNormal];

        [self.button setHidden:YES];
        [self addSubview:self.button];
        [self addSubview:self.previewThumbnail];

        [self initialiseOtherComponents];
    }
    return self;
}
-(instancetype)init{
    self = [self initWithFrame:CGRectZero];
    return self;
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    //we dont want the preview thumbnail to also update its coordinates whenever the frame changes
    CGFloat height = CGRectGetHeight(self.bounds) - self.margin*2;
    CGFloat width = CGRectGetWidth(self.bounds) - self.margin*2;
    self.previewThumbnail.frame = CGRectMake(self.margin, self.margin, width, height);
    
    self.button.frame = CGRectMake(width-self.margin, -20, 20, 40.0);
    [self.button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0]];
    
    [self.button setHidden:NO];

}
-(void)initialiseOtherComponents{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap)];
    [self addGestureRecognizer:tapGesture];
    self.margin = kPreviewMediaViewControllerThumbnailMargin;
    [self.previewThumbnail.layer setCornerRadius:10];
    [self.previewThumbnail.layer setMasksToBounds:YES];
}
-(void)deleteButtonDidPress:(id)sender{
    if([self.delegate respondsToSelector:@selector(deleteThumbnailForIndexPosition:)]){
        [self.delegate deleteThumbnailForIndexPosition:self.index];
    }

}
-(void)didTap{
    if([self.delegate respondsToSelector:@selector(didSelectThumbnailPreviewAtIndexPosition:)]){
        [self.delegate didSelectThumbnailPreviewAtIndexPosition:self.index];
    }
}


@end
