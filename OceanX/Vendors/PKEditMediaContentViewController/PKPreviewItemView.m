//
//  PKPreviewItemView.m
//  OceanX
//
//  Created by Pavan Kataria on 08/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKPreviewItemView.h"

@implementation PKPreviewItemView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.margin = 0;
    }
    return self;
}

-(instancetype)init{
    self = [self initWithFrame:CGRectZero];
    return self;
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
//    NSLog(@"PREVIEW ITEM SET FRAME CALLED: %@", NSStringFromCGRect(frame));
    //we dont want the preview thumbnail to also update its coordinates whenever the frame changes
    CGFloat width = CGRectGetWidth(self.bounds) - self.margin*2;
    CGFloat height = width;// CGRectGetHeight(self.bounds) - self.margin*2;
    self.previewThumbnail.frame = CGRectMake(self.margin, self.margin, width, height);
}
@end
