//
//  PKPreviewThumbnail.h
//  OceanX
//
//  Created by Pavan Kataria on 02/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PKPreviewBaseView.h"


@protocol PKPreviewThumbnailDelegate <NSObject>
@required
-(void)didSelectThumbnailPreviewAtIndexPosition:(NSUInteger)index;
-(void)deleteThumbnailForIndexPosition:(NSUInteger)index;
@end


@interface PKPreviewThumbnail : PKPreviewBaseView
@property (nonatomic, weak) id <PKPreviewThumbnailDelegate> delegate;



@end
