//
//  PKPreviewMediaViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 02/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKPreviewThumbnail.h"
#import "PKPreviewItemView.h"
@protocol PKPreviewMediaViewDelegate <NSObject>
@required

@end

@protocol PKPreviewMediaViewDatasource <NSObject>
@required
-(NSUInteger)numberOfPreviewCellsToDisplay;
-(PKPreviewItemView*)previewItemViewForIndexPosition:(NSUInteger)position;

@optional
-(PKPreviewThumbnail*)previewThumbnailViewForIndexPosition:(NSUInteger)position;

@end


@interface PKPreviewMediaViewController : UIViewController<PKPreviewMediaViewDelegate, PKPreviewMediaViewDatasource>

@property (nonatomic, weak) id <PKPreviewMediaViewDelegate> delegate;
@property (nonatomic, weak) id <PKPreviewMediaViewDatasource> datasource;
@property (nonatomic, assign) NSUInteger selectedPreviewItemIndex;

@property (nonatomic, weak) IBOutlet UIScrollView *thumbnailsScrollView;
@property (nonatomic, weak) IBOutlet UIScrollView *previewScrollView;
@property (nonatomic, assign) CGFloat previewMediaTableThumbnailHeight;
@property (nonatomic, assign) CGFloat previewMediaTableMainPreviewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *previewScrollViewHeight;



-(void)reloadMediaView;
-(void)selectPreviewItemAtIndex:(NSUInteger)index withAnimation:(BOOL)animated;


@end
