//
//  PKPreviewItemView.h
//  OceanX
//
//  Created by Pavan Kataria on 08/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKPreviewBaseView.h"

@interface PKPreviewItemView : PKPreviewBaseView

@end
