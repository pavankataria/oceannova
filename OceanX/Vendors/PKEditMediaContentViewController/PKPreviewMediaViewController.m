//
//  PKPreviewMediaViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 02/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKPreviewMediaViewController.h"

CGFloat const kThumbnailScrollerHeight = 150;

@interface PKPreviewMediaViewController ()<PKPreviewThumbnailDelegate>{
    NSUInteger _previewItemsCount;
}
@end

@implementation PKPreviewMediaViewController
@synthesize selectedPreviewItemIndex = _selectedPreviewItemIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = self;
    self.datasource = self;
    _previewItemsCount = 0;
    self.previewMediaTableThumbnailHeight = 200;
    self.previewMediaTableMainPreviewHeight = 200;

    [self setSelectedPreviewItemIndex:0];
    [self initViews];
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}



-(CGRect)rectOfVisibleView{
    //can only be in portrait orientation because of the `supportedInterfaceOrientation.
    
    CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
    CGRect navigationBarFrame = self.navigationController.navigationBar.frame;
    
    return CGRectMake(0, 0, navigationBarFrame.size.width, CGRectGetHeight(statusBarFrame) + CGRectGetHeight(navigationBarFrame));
}
-(void)initViews{
    //Do any init views here
//    self.previewScrollView.backgroundColor = [UIColor greenColor];
    self.previewScrollView.pagingEnabled = YES;
    [self.view sendSubviewToBack:self.previewScrollView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reloadMediaView{
    _previewItemsCount = (NSUInteger)[self.datasource performSelector:@selector(numberOfPreviewCellsToDisplay)];
    NSLog(@"numberOfPreviewCells: %lu", (unsigned long)_previewItemsCount);
    [self preparePreviewScreensForItemsCount:_previewItemsCount];
//    [self.thumbnailsScrollView setBackgroundColor:[UIColor redColor]];

}
-(void)setPreviewMediaTableThumbnailHeight:(CGFloat)previewMediaTableThumbnailHeight{
//    [self.thumbnailsScrollView setBackgroundColor:[UIColor redColor]];
    _previewMediaTableThumbnailHeight = previewMediaTableThumbnailHeight;
    [self resizeDisplayTrays];
}

-(void)setPreviewMediaTableMainPreviewHeight:(CGFloat)previewMediaTableMainPreviewHeight{
    _previewMediaTableMainPreviewHeight = previewMediaTableMainPreviewHeight;
    [self resizeDisplayTrays];
}

-(void)resizeDisplayTrays{
    CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    CGFloat navigationBarHeight = self.navigationController.navigationBar.frame.size.height;
    CGFloat height = CGRectGetHeight(self.view.frame)-navigationBarHeight-statusBarHeight;

    CGRect screenFrame = self.view.frame;
    screenFrame.size.height = CGRectGetWidth(self.view.frame);
//    screenFrame.size.width = CGRectGetWidth(self.view.frame);
    
    NSLog(@"screen frame: %@", NSStringFromCGRect(screenFrame));
    
    self.previewScrollView.frame = screenFrame;
    self.previewScrollViewHeight.constant = CGRectGetHeight(self.previewScrollView.frame);
    
    CGFloat remainingSpaceForThumbnailScroller =  height-self.previewScrollViewHeight.constant;
    [self.thumbnailsScrollView setBackgroundColor:[UIColor blackColor]];
    if(remainingSpaceForThumbnailScroller > kThumbnailScrollerHeight){
        self.thumbnailsScrollView.frame = CGRectMake(0, self.previewScrollViewHeight.constant+(remainingSpaceForThumbnailScroller+kThumbnailScrollerHeight)/2, screenFrame.size.width, kThumbnailScrollerHeight);
    }
    else{
        self.thumbnailsScrollView.frame = CGRectMake(0, self.previewScrollViewHeight.constant, screenFrame.size.width, remainingSpaceForThumbnailScroller);
    }
}

-(void)preparePreviewScreensForItemsCount:(NSUInteger)previewItemsCount{
//    NSLog(@"previewItemsCount: %lu", previewItemsCount);
    //Remove any existing thumbnails from thumbnailScrollView to reinitialise
    [self removeAllSubviewsFromPreviewScreens];
    [self createPreviewItems];
    
    [self createThumbnails];
}
-(void)createPreviewItems{
    //creating new previews
    if([self.datasource respondsToSelector:@selector(previewItemViewForIndexPosition:)]){
        
        CGFloat maxContentWidthForPreviewItemsTray = 0;
        for(int currentPreviewItemCounter = 0; currentPreviewItemCounter < _previewItemsCount; currentPreviewItemCounter++){
            
            CGRect nextPreviewItemBox = [self getPreviewItemRectForPosition:currentPreviewItemCounter];
            PKPreviewItemView *currentPreviewItem = [[PKPreviewItemView alloc] init];
//#warning todo: have a ENUM that checks whether to automatically generate preview items or whether to receive the preview items from the datasource for which it should then call the datasource method
            currentPreviewItem = [self.datasource previewItemViewForIndexPosition:currentPreviewItemCounter];
            currentPreviewItem.frame = nextPreviewItemBox;
            currentPreviewItem.index = currentPreviewItemCounter;
//            currentPreviewItem.delegate = self;
            maxContentWidthForPreviewItemsTray += CGRectGetWidth(currentPreviewItem.frame);
            [self.previewScrollView addSubview:currentPreviewItem];
        }
        [self updatePreviewItemScrollViewWithWidth:maxContentWidthForPreviewItemsTray];
    }
}

-(void)updatePreviewItemScrollViewWithWidth:(CGFloat)maxContentWidth{
    self.previewScrollView.contentSize = CGSizeMake(maxContentWidth, CGRectGetHeight(self.previewScrollView.frame));
    for(UIView *view in self.previewScrollView.subviews){
//        NSLog(@"preview scroll view subviews: %@", view);
    }
}
-(CGRect)getPreviewItemRectForPosition:(NSUInteger)position{
    CGFloat thumbnailHeight = CGRectGetHeight(self.previewScrollView.frame);
    CGFloat thumbnailWidth = CGRectGetWidth(self.previewScrollView.frame);
    CGRect currentRect = CGRectMake(thumbnailWidth * position, 0, thumbnailWidth, thumbnailHeight);
//    NSLog(@"rect: {%@} position: %lu", NSStringFromCGRect(currentRect), position);
    return currentRect;
}

-(void)createThumbnails{
    //creating new thumbnails
    if([self.datasource respondsToSelector:@selector(previewThumbnailViewForIndexPosition:)]){
        CGFloat maxContentWidthForThumbnailsTray = 0;
        for(int currentThumbnailCounter = 0; currentThumbnailCounter < _previewItemsCount; currentThumbnailCounter++){
            CGRect nextThumbnailBox = [self getThumbnailRectForPosition:currentThumbnailCounter];
            PKPreviewThumbnail *currentThumbnail = [[PKPreviewThumbnail alloc] init];// = [[PKPreviewThumbnail alloc] initWithFrame:nextThumbnailBox];
//#warning todo: have a ENUM that checks whether to automatically generate thumbnails or whether to receive the thumbnails from the datasource for which it should then call the datasource method
            currentThumbnail = [self.datasource previewThumbnailViewForIndexPosition:currentThumbnailCounter];
            currentThumbnail.frame = nextThumbnailBox;
            currentThumbnail.index = currentThumbnailCounter;
            currentThumbnail.delegate = self;
            maxContentWidthForThumbnailsTray += CGRectGetWidth(currentThumbnail.frame);
            [self.thumbnailsScrollView addSubview:currentThumbnail];
        }
        [self updateThumbnailTrayScrollViewWithWidth:maxContentWidthForThumbnailsTray];
    }
}

-(void)updateThumbnailTrayScrollViewWithWidth:(CGFloat)maxContentWidth{
    
    self.thumbnailsScrollView.contentSize = CGSizeMake(maxContentWidth, CGRectGetHeight(self.thumbnailsScrollView.frame));
    NSLog(@"thumbnail size: {%@}", NSStringFromCGSize(self.thumbnailsScrollView.contentSize));
    NSLog(@"trayView frame: {%@}", NSStringFromCGRect(self.thumbnailsScrollView.frame));
//    
//    for(UIView *view in self.thumbnailsScrollView.subviews){
//            NSLog(@"thumbnails scroll view subviews: %@", view);
//    }

}
-(void)removeAllSubviewsFromPreviewScreens{
    [self.thumbnailsScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.previewScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

-(CGRect)getThumbnailRectForPosition:(NSUInteger)position{
    CGFloat thumbnailHeight = CGRectGetHeight(self.thumbnailsScrollView.frame);
    CGFloat thumbnailWidth = thumbnailHeight;
    CGRect currentRect = CGRectMake(thumbnailWidth * position, self.thumbnailsScrollView.frame.size.height/2, thumbnailWidth, thumbnailHeight);
    
//    NSLog(@"rect: {%@} position: %lu", NSStringFromCGRect(currentRect), position);
    return currentRect;
}

-(void)selectPreviewItemAtIndex:(NSUInteger)index withAnimation:(BOOL)animated{
//#warning - todo: create animation section that should transition the preview screen
    _selectedPreviewItemIndex = index;
}

-(NSUInteger)selectedPreviewItemIndex{
    return _selectedPreviewItemIndex;
}

-(void)setSelectedPreviewItemIndex:(NSUInteger)selectedPreviewItemIndex{
//    NSLog(@"setSelectedPreviewItemIndex: %lu", selectedPreviewItemIndex);
    if(selectedPreviewItemIndex > _previewItemsCount){
        _selectedPreviewItemIndex = 0;
    }
    else{
        _selectedPreviewItemIndex = selectedPreviewItemIndex;
    }
    
    CGRect pageVisibleRect;
    CGSize contentSize = [self.previewScrollView contentSize];
    //We want the visible rect height to be the content view height as we are only scrolling horizontally
    pageVisibleRect.size.height = contentSize.height;
    
    // So the y origin should be 0
    pageVisibleRect.origin.y = 0.0;
    
    // The visible rect width should be as wide as the preview scroller
    pageVisibleRect.size.width = CGRectGetWidth(self.previewScrollView.frame);
    
    pageVisibleRect.origin.x  = _selectedPreviewItemIndex * pageVisibleRect.size.width;
    
    // Why animate?  The view isn't visible yet!
    [self.previewScrollView scrollRectToVisible:pageVisibleRect animated:YES];
    
//    NSLog(@"did tap index self.index: %lu", self.selectedPreviewItemIndex);
}

#pragma mark - PKPreviewMediaViewDatasource methods
-(NSUInteger)numberOfPreviewCellsToDisplay{
    return _previewItemsCount;
}

-(UIView*)previewItemViewForIndex{
    return nil;
}

#pragma mark - PKPreviewThumbnailDelegate methods

-(void)didSelectThumbnailPreviewAtIndexPosition:(NSUInteger)index{

    [self setSelectedPreviewItemIndex:index];
}



@end
