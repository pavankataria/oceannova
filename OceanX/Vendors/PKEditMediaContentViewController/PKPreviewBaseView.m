//
//  PKPreviewBaseView.m
//  OceanX
//
//  Created by Pavan Kataria on 08/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKPreviewBaseView.h"

@implementation PKPreviewBaseView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.previewThumbnail = [[UIImageView alloc] initWithFrame:frame];
        [self addSubview:self.previewThumbnail];
        self.margin = 0;
    }
    return self;
}
-(instancetype)init{
    return [self initWithFrame:CGRectZero];
}

//-(void)setFrame:(CGRect)frame{
//    NSLog(@"super frame being set: %@", NSStringFromCGRect(frame));
//    [super setFrame:frame];
//}


-(NSUInteger)margin{
    return _margin;
}
@end
