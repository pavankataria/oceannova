//
//  PKPreviewBaseView.h
//  OceanX
//
//  Created by Pavan Kataria on 08/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKPreviewBaseView : UIView

@property (nonatomic, retain) UIImageView *previewThumbnail;
@property (nonatomic, assign) NSUInteger index;
@property (nonatomic, assign) NSUInteger margin;


- (instancetype)initWithFrame:(CGRect)frame;
- (instancetype)init;


@end
