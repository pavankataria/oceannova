//
//  NSDictionary+HasValueForKey.h
//  OceanX
//
//  Created by Systango on 2/4/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (HasValueForKey)

- (BOOL)hasValueForKey:(NSString *)key;

- (id)getValueForKey:(NSString *)key;

@end
