//
//  OXRestAPICient.m
//
//
//  Created by Pavan Kataria on 02/02/2015.
//
//

#import "OXRestAPICient.h"
#import "OXServerRequest.h"

@implementation OXRestAPICient
NS_ENUM(NSUInteger, OXHTTPClientURLServerType){
    OXHTTPClientURLServerTypeLive = 0,
    OXHTTPClientURLServerTypeDemo
};
+(NSUInteger)clientBaseURL{
#if DEBUG
    return OXHTTPClientURLServerTypeDemo;
#else
    return OXHTTPClientURLServerTypeLive;
#endif
}
+(NSString*)sharedURL{
    switch ([self clientBaseURL]) {
        case OXHTTPClientURLServerTypeLive:
            return kHTTPClientURLStringBase;
            break;
        case OXHTTPClientURLServerTypeDemo:
            return kHTTPClientURLStringBaseDemo;
        default:
            return @"http://invalid.url";
            break;
    }
}
+(NSString*)baseWithRESTUrlAndVersionAPI{
    return [NSString stringWithFormat:@"%@%@/", [self baseWithRESTUrl], kHTTPClientURLStringAPIVersion];
}
+(NSString*)fullServerURLAndAPIVersionWithNodePoint:(NSString*)nodePoint{
    return [NSString stringWithFormat:@"%@%@", [self baseWithRESTUrlAndVersionAPI], nodePoint];
}

+(NSString*)baseWithRESTUrl{
    return [NSString stringWithFormat:@"%@%@", [self sharedURL], kHTTPClientURLStringRESTURL];
}
+(NSString*)fullServerURLWithNodePoint:(NSString*)nodePoint{
    return [NSString stringWithFormat:@"%@%@", [self baseWithRESTUrl], nodePoint];
}

+(void)loginWithUsername:(NSString *)username password:(NSString *)password successBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock)failBlock{
    
    NSString * url =  [[[self sharedURL] stringByAppendingString:kHTTPClientURLStringRESTURL]stringByAppendingString:kHTTPClientURLStringSecurityCheckLogin];
    
    NSDictionary * defaultParams = @{@"j_username": username, @"j_password": password};
    NSMutableDictionary *params = [defaultParams mutableCopy];
    NSLog(@"DEVICE TOKEN: %@", [[OXDeviceTokenManager sharedManager] userDeviceToken]);
    if([[OXDeviceTokenManager sharedManager] userDeviceToken]){
        [params setObject:[[OXDeviceTokenManager sharedManager] userDeviceToken] forKey:@"j_device_token"];
    }
    NSLog(@"login params: %@", params);

    [[OXServerRequest sharedManager] httpPostRequest:url parameters:params success:^(AFHTTPRequestOperation *operation, id response) {
        [[OXCookieManager sharedManager] saveSessionCookiesForURL:[NSURL URLWithString:url]];
        [OXUserModel saveUsernameWithUsername:[response objectForKey:@"username"]];
        successBlock(operation, response);
        
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        NSLog(@"ZOEB ZOEB responseString: %@, error: %@", operation.responseString, error);
        if ([error isKindOfClass:NSDictionary.class]) {
            NSDictionary * dictionary = (NSDictionary*) error;
            failBlock(operation, [dictionary objectForKey:@"message"]);
            return;
        }
        failBlock(operation, @"We are experiencing server difficulties. We'll be back shortly.");
    }];
}

+(void)searchByMapWithPOISearchObject:(OXPOISearchModel*)searchModel
                     withSuccessBlock:(SuccessCompletionBlock)successBlock
                            failBlock:(FailureCompletionBlock)failBlock{
   
    NSString * url =  [self fullServerURLWithNodePoint:kHTTPClientURLStringSearchByMap];
    
    NSDictionary * params = [searchModel params];

    [[OXServerRequest sharedManager] postRequest:url parameters:params success:^(AFHTTPRequestOperation *operation, id response) {
        successBlock (operation, response);
        
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        failBlock (operation, error);
    }];
}

+(void)searchByListWithPOISearchObject:(OXPOISearchModel*)searchModel
                      withSuccessBlock:(SuccessCompletionBlock) successBlock
                             failBlock:(FailureCompletionBlock) failBlock{
    

    NSString * url =  [self fullServerURLWithNodePoint:kHTTPClientURLStringSearchByList];
    NSDictionary * params = [searchModel params];
    
    NSLog(@"search by list: %@", params);
//    NSString * url =  [[[self sharedURL] stringByAppendingString:kHTTPClientURLStringRESTURL] stringByAppendingString:kHTTPClientURLStringSearchByList];
  
//    NSDictionary * params = [searchModel params];
//    NSLog(@"params: %@", params);
    [[OXServerRequest sharedManager] postRequest:url parameters:params success:^(AFHTTPRequestOperation *operation, id response) {
        successBlock (operation, response);
        
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        failBlock (operation, error);
    }];
}

+(void) requestPasswordResetForUser:(OXUserModel*) user successBlock:(SuccessCompletionBlock) successBlock failBlock:(FailureCompletionBlock) failBlock{
    
    NSString * url =  [[[self sharedURL] stringByAppendingString:kHTTPClientURLStringRESTURL] stringByAppendingString:kHTTPClientURLStringRequestPasswordReset];
    
    
    NSDictionary * params = [user retrieveResetPasswordParams];
    
    [[OXServerRequest sharedManager] postRequest:url parameters:params success:^(AFHTTPRequestOperation *operation, id response) {
        
        NSLog(@"server request success");
        
        successBlock(operation, response);
        
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        
        NSLog(@"server request failed");
        
        failBlock(operation, error);
        
    }];
}

+(void) signUpWithUser:(OXUserModel*) user successBlock:(SuccessCompletionBlock) successBlock failBlock:(FailureCompletionBlock) failBlock{
    
    NSString * url =  [[[self sharedURL] stringByAppendingString:kHTTPClientURLStringRESTURL] stringByAppendingString:kHTTPClientURLStringSignUpUser];
    NSDictionary * params = [user retrieveCreateUserParamsForSignupWithEmail];
    
    [[OXServerRequest sharedManager] postRequest:url parameters:params success:^(AFHTTPRequestOperation *operation, id response) {
        
        [user saveUserCredentials];
        [OXLoginManager setReplayTutorialViewedTo:NO];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [[OXCookieManager sharedManager] saveSessionCookiesForURL:[NSURL URLWithString:url]];
        successBlock(operation, response);
        
        if([response objectForKey:@"username"]){
            NSLog(@"SAVE SESSION COOKIES");
            [[OXCookieManager sharedManager] saveSessionCookiesForURL:[NSURL URLWithString:url]];
            [OXUserModel saveUsernameWithUsername:[response objectForKey:@"username"]];
        }

        
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        
        failBlock(operation, error);
    }];
}

+(NSString *)completeURLByURLPath:(NSString *)urlPath
{
    return  [[[self sharedURL] stringByAppendingString:kHTTPClientURLStringRESTURL] stringByAppendingString:urlPath];
}


+(void)postAddPoiWithAddPoiModel:(OXHCAddPoiModel*)object
                withSuccessBlock: (void (^)(AFHTTPRequestOperation *operation, id response))successBlock
                       failBlock:(void (^)(AFHTTPRequestOperation *operation, id error)) failBlock{
    
    NSDictionary * params = [object params];
        NSString *url = [self fullServerURLWithNodePoint:kHTTPClientURLStringAddPoi];    
    [[OXServerRequest sharedManager] postRequest:url/*@"http://oceanqa.elasticbeanstalk.com/ws/rest/v2/poi/add"*/
                                        parameters:params
                                           success:^(AFHTTPRequestOperation *operation, id response) {
                                               NSLog(@"success");
                                               successBlock(operation, response);
                                           }
                                              fail:^(AFHTTPRequestOperation *operation, id error) {
                                                  NSLog(@"fail");
                                                  failBlock(operation, error);
                                              }];
}

+(void) setUserLocationWithCoordinate: (OCNCoordinate*) coordinate successBlock:(SuccessCompletionBlock)successBlock
                            failBlock:(void (^)(AFHTTPRequestOperation *operation, id error))failBlock{

    [[OXServerRequest sharedManager] postRequest:[self fullServerURLWithNodePoint:kRequestNodeLocationUserPosition]
                                        parameters:[MTLJSONAdapter JSONDictionaryFromModel:coordinate]
                                           success:^(AFHTTPRequestOperation *operation, id response) {
                                               successBlock(operation, response);
                                           }
                                              fail:^(AFHTTPRequestOperation * operation,  id error) {
                                                  failBlock(operation, error);
                                              }];
}

+(void)getQuickPoiInfoWithBasePOIObject:(OXBasePOIModel *)basePOIModel successBlock:(SuccessCompletionBlock)  successBlock failBlock:(FailureCompletionBlock)failBlock{
    
    NSString *url = [self fullServerURLWithNodePoint:[self getQuickInfoNodePointWithBasePOI:basePOIModel]];
    [[OXServerRequest sharedManager] getRequest:url parameters:nil
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            successBlock(operation, response);
                                        }fail:^(AFHTTPRequestOperation * operation,  id error) {
                                            failBlock(operation, error);
                                     }];

}

+(void)getUserProfileWithSuccessBlock:(SuccessCompletionBlock)successBlock
                            failBlock:(FailureCompletionBlock)failBlock
{
    NSString *url = [self fullServerURLWithNodePoint:kHTTPClientURLStringGetUserProfile];
   
    [[OXServerRequest sharedManager] getRequest:url parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        successBlock (operation, response);
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        failBlock (operation , error);
    }];
}


+(void)activateUserProfileWithSuccessBlock:(SuccessCompletionBlock)successBlock
                                 failBlock:(FailureCompletionBlock)failBlock
{
    NSString *url = [self fullServerURLWithNodePoint:kHTTPClientURLStringActivateUserProfile];
    
    [[OXServerRequest sharedManager] putRequest:url parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        successBlock (operation, response);
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        failBlock (operation , error);
    }];
}

+(void)getFacetsQuestionsWithHashCode:(NSString *)hashCode
                         successBlock:(SuccessCompletionBlock)successBlock
                            failBlock:(FailureCompletionBlock)failBlock
{
      NSString *url = [self fullServerURLWithNodePoint:kHTTPClientURLStringGetFacetsQuestions];
    NSDictionary *params = @{@"hashCode" : hashCode};
    [[OXServerRequest sharedManager] getRequest:url parameters:params success:^(AFHTTPRequestOperation *operation, id response) {
        successBlock (operation, response);
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        failBlock (operation , error);
    }];
}

+(void)reportIssueWithReportData:(OXReportDataModel *)reportData
                         successBlock:(SuccessCompletionBlock)successBlock
                            failBlock:(FailureCompletionBlock)failBlock
{
    NSString *url = nil;
    if (reportData.reportSource == ReportTypePOI) {
        url = [self fullServerURLWithNodePoint:[self getReportPOINodePointWithReportData:reportData]];
    } else {
        url = [self fullServerURLWithNodePoint:[self getReportPictureNodePointWithReportData:reportData]];
    }
    NSDictionary * params = [reportData params];
    [[OXServerRequest sharedManager] postRequest:url parameters:params success:^(AFHTTPRequestOperation *operation, id response) {
        successBlock (operation, response);
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        failBlock (operation , error);
    }];
}

+(NSString *)getQuickInfoNodePointWithBasePOI:(OXBasePOIModel *)basePOI
{
    return [NSString stringWithFormat:kHTTPClientURLStringGetQuickInfo,[NSString stringWithFormat:@"%ld", (long)basePOI.poiID ]];
}

+(NSString *)getReportPOINodePointWithReportData:(OXReportDataModel *)reportData
{
    return [NSString stringWithFormat:kHTTPClientURLStringReportPOI,[NSString stringWithFormat:@"%ld", (long)reportData.reportID ]];
}

+(NSString *)getReportPictureNodePointWithReportData:(OXReportDataModel *)reportData
{
    return [NSString stringWithFormat:kHTTPClientURLStringReportPicture,[NSString stringWithFormat:@"%ld", (long)reportData.reportID  ]];
}

+(void)getMetaDataWithSuccessBlock:(SuccessCompletionBlock)successBlock
                            failBlock:(FailureCompletionBlock)failBlock
{
    NSString *url = [self fullServerURLWithNodePoint:kHTTPClientURLStringGetMetaData];
    
    [[OXServerRequest sharedManager] getRequest:url parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        successBlock (operation, response);
        
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        failBlock (operation , error);
    }];
}

+(void)addAnswersForQuestions:(NSArray *)questions
          successBlock:(SuccessCompletionBlock) successBlock
             failBlock:(FailureCompletionBlock) failBlock{
    
    NSString *url = [self fullServerURLWithNodePoint:kHTTPClientURLStringAnswerQuestions];
    NSArray * questionsAsJSON = [OXQuestionModel JSONArrayFromQuestionsArray:questions];
    NSDictionary * params = @{@"questions": questionsAsJSON};
    
    [[OXServerRequest sharedManager] postRequest:url parameters:params success:^(AFHTTPRequestOperation *operation, id response) {
        successBlock (operation, response);
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        failBlock (operation , error);
    }];
}
+(void)getTagsAutoSuggestionWithString:(NSString*)searchString withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock)failBlock{
    
    NSDictionary * params = @{@"tagName": searchString};

    [[OXServerRequest sharedManager] getRequest:[self fullServerURLWithNodePoint:kHTTPClientURLStringTagAutoSuggestion]
                                     parameters:params
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            successBlock(operation, response);
                                        }
                                           fail:^(AFHTTPRequestOperation *operation, id error) {
                                               failBlock(operation, error);
                                           }];
}

+(void) getFriendsListWithSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock{
    NSString *url = [self fullServerURLWithNodePoint:kRequestNodeUsersFriends];

    [[OXServerRequest sharedManager] getRequest:url
                                     parameters:nil
                                        success:^(AFHTTPRequestOperation *operation, id response) {
//                                            NSMutableArray * friends = [OCNFriend arrayOfFriendsFromJSONDictionary:response];
                                            successBlock(operation, response);
                                            
                                        } fail:^(AFHTTPRequestOperation *operation, id error) {
                                            failBlock(operation, error);
                                        }];
        
}

+(void) searchUsersWithSearchTerm:(NSString *)searchTerm withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock{
    
    NSDictionary * params = @{@"username":searchTerm};
    NSString *url = [self fullServerURLWithNodePoint:kRequestNodeUsersSearchName];
    [[OXServerRequest sharedManager] postRequest:url
                                     parameters:params
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            successBlock(operation, response);
                                            
                                        } fail:^(AFHTTPRequestOperation *operation, id error) {
                                            failBlock(operation, error);
                                        }];
    
}



+ (void)acceptFriendRequestForNotification:(NSInteger) notification withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock{
    
    NSDictionary * params = @{@"notificationId": [NSNumber numberWithInteger:notification], @"isAccepted": @1};
    NSString *url = [self fullServerURLWithNodePoint:kRequestNodeNotificationFriends];
    [[OXServerRequest sharedManager] postRequest:url
                                      parameters:params
                                         success:^(AFHTTPRequestOperation *operation, id response) {
                                             successBlock(operation, response);
                                             
                                         } fail:^(AFHTTPRequestOperation *operation, id error) {
                                             failBlock(operation, error);
                                         }];
    
}

+ (void)denyFriendRequestForNotification:(NSInteger)notification withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock{
    
    NSDictionary * params = @{@"notificationId": [NSNumber numberWithInteger:notification], @"isAccepted": @0};
    NSString *url = [self fullServerURLWithNodePoint:kRequestNodeNotificationFriends];
    [[OXServerRequest sharedManager] postRequest:url
                                      parameters:params
                                         success:^(AFHTTPRequestOperation *operation, id response) {
                                             successBlock(operation, response);
                                             
                                         } fail:^(AFHTTPRequestOperation *operation, id error) {
                                             failBlock(operation, error);
                                         }];
    
}


+ (void)unfriendUserWithID:(NSInteger) userID  withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock{
    
    NSDictionary * params = @{@"friendId":[NSNumber numberWithInteger:userID]};
    NSString *url = [self fullServerURLWithNodePoint:kRequestNodeUsersFriends];
    [[OXServerRequest sharedManager] deleteRequest:url
                                      parameters:params
                                         success:^(AFHTTPRequestOperation *operation, id response) {
                                             successBlock(operation, response);
                                             
                                         } fail:^(AFHTTPRequestOperation *operation, id error) {
                                             failBlock(operation, error);
                                         }];
    
}

+ (void)addFriendWithID:(NSInteger)userID withSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock{
    
    NSDictionary * params =  @{@"friendId": [NSNumber numberWithInteger:userID]};
    NSString *url = [self fullServerURLWithNodePoint:kRequestNodeUsersFriends];
    [[OXServerRequest sharedManager] postRequest:url
                                      parameters:params
                                         success:^(AFHTTPRequestOperation *operation, id response) {
                                             successBlock(operation, response);
                                             
                                         } fail:^(AFHTTPRequestOperation *operation, id error) {
                                             failBlock(operation, error);
                                         }];
    
}



+(void) getFriendRequestsWithSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock{
    NSString *url = [self fullServerURLWithNodePoint:kRequestNodeNotificationFriends];
    
    [[OXServerRequest sharedManager] getRequest:url
                                     parameters:nil
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            //                                            NSMutableArray * friends = [OCNFriend arrayOfFriendsFromJSONDictionary:response];
                                            successBlock(operation, response);
                                            
                                        } fail:^(AFHTTPRequestOperation *operation, id error) {
                                            failBlock(operation, error);
                                        }];
    
}
+(void) getSentFriendRequestsWithSuccessBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock) failBlock{
    NSString *url = [self fullServerURLWithNodePoint:kRequestNodeNotificationSentFriends];
    
    [[OXServerRequest sharedManager] getRequest:url
                                     parameters:nil
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            //                                            NSMutableArray * friends = [OCNFriend arrayOfFriendsFromJSONDictionary:response];
                                            successBlock(operation, response);
                                            
                                        } fail:^(AFHTTPRequestOperation *operation, id error) {
                                            failBlock(operation, error);
                                        }];
    
}


+(void)getNewsFeedWithSuccessBlock: (void (^)(AFHTTPRequestOperation *operation, id response))successBlock
                         failBlock:(void (^)(AFHTTPRequestOperation *operation, id error)) failBlock{
    
    NSString * url =  [[self sharedURL] stringByAppendingString:kHTTPClientURLStringGetNewsFeed];

    [[OXServerRequest sharedManager] getRequest:url parameters:nil
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            successBlock(operation, response);
                                        }fail:^(AFHTTPRequestOperation * operation,  id error) {
                                            failBlock(operation, error);
                                        }];
}




+(void) likePOIWithID:(NSInteger)poiID successBlock:(SuccessCompletionBlock) successBlock failBlock:(FailureCompletionBlock) failBlock{
    NSString * url = [NSString stringWithFormat:@"%@poi/%lu%@", [self baseWithRESTUrlAndVersionAPI], (long)poiID, kHTTPClientURLStringLike];

    
    [[OXServerRequest sharedManager] putRequest:url parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        successBlock(operation, response);
        
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        failBlock(operation, error);
    }];
}



+(void) dislikePOIWithID:(NSInteger) poiID successBlock:(SuccessCompletionBlock) successBlock failBlock:(FailureCompletionBlock) failBlock{
    NSString * url = [NSString stringWithFormat:@"%@poi/%lu%@", [self baseWithRESTUrlAndVersionAPI], (long)poiID, kHTTPClientURLStringDislike];
    
    [[OXServerRequest sharedManager] putRequest:url
                                     parameters:nil
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            successBlock(operation, response);
    }
                                           fail:^(AFHTTPRequestOperation *operation, id error) {
                                               failBlock(operation, error);
                                           }];
}
+(void)neutralPOIWithID:(NSInteger) poiID successBlock:(SuccessCompletionBlock) successBlock failBlock:(FailureCompletionBlock) failBlock{
    NSString * url = [NSString stringWithFormat:@"%@poi/%lu%@", [self baseWithRESTUrlAndVersionAPI], (long)poiID, kHTTPClientURLStringNeutral];
    NSLog(@"neutral: %@", url);
    [[OXServerRequest sharedManager] putRequest:url
                                     parameters:nil
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            successBlock(operation, response);
                                        }
                                           fail:^(AFHTTPRequestOperation *operation, id error) {
                                               failBlock(operation, error);
                                           }];
}

+(void)getFullPOIDetailsWithPOIID:(NSInteger)poiID
                     successBlock:(SuccessCompletionBlock) successBlock
                        failBlock:(FailureCompletionBlock) failBlock{
    NSString * url = [NSString stringWithFormat:@"%@%@/%lu", [self baseWithRESTUrlAndVersionAPI], kHTTPClientURLStringGetFullPOI, (long)poiID];

    NSLog(@"getFullPOIDetailsWithPOIID URL: %@", url);
    [[OXServerRequest sharedManager] getRequest:url
                                     parameters:nil
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            successBlock(operation, response);
                                        }
                                           fail:^(AFHTTPRequestOperation *operation, id error) {
                                               failBlock(operation, error);
                                           }];
}

+(void)postAddTags:(NSArray *)tagArray
         withPOIID:(NSInteger)poiID
      successBlock:(SuccessCompletionBlock) successBlock
         failBlock:(FailureCompletionBlock) failBlock{
    
    NSDictionary * params = [OXTagModel paramsForAddTagsForPOIIDWithTagsArray:tagArray andPOIID:poiID];// paramsForAddTagsForPOIIDWithTagsArray];
    
    NSString *url = [self fullServerURLAndAPIVersionWithNodePoint:kHTTPClientURLStringAddTagsToPOI];
    [[OXServerRequest sharedManager] postRequest:url
                                      parameters:params
                                         success:^(AFHTTPRequestOperation *operation, id response) {
                                             successBlock(operation, response);
                                             
                                         } fail:^(AFHTTPRequestOperation *operation, id error) {
                                             failBlock(operation, error);
                                         }];

    
}
+(void)addImagesToPOI:(NSArray *)captureImages
            withPOIID:(NSInteger)poiID
         successBlock:(SuccessCompletionBlock) successBlock
            failBlock:(FailureCompletionBlock) failBlock{
    

    NSDictionary *params = @{@"pictures" : [OXHCAddPoiPicturesModel getArrayOfJSON64PicturesWithCapturedImagesArray:captureImages]};
    
    NSString * url = [NSString stringWithFormat:@"%@poi/%lu%@", [self baseWithRESTUrlAndVersionAPI], (long)poiID, kHTTPClientURLStringAddPicturesToPOI];
    NSLog(@"url: %@", url);
    [[OXServerRequest sharedManager] postRequest:url
                                      parameters:params
                                         success:^(AFHTTPRequestOperation *operation, id response) {
                                             successBlock(operation, response);
                                             
                                         } fail:^(AFHTTPRequestOperation *operation, id error) {
                                             failBlock(operation, error);
                                         }];

    
    
}


+(void)affirmTagID:(NSUInteger)tagID
         withPOIID:(NSUInteger)poiID
      successBlock:(SuccessCompletionBlock) successBlock
         failBlock:(FailureCompletionBlock) failBlock{
    NSString *url = [self fullServerURLAndAPIVersionWithNodePoint:kHTTPClientURLStringPUTAffirmTag];
    NSLog(@"url: %@", url);
    NSDictionary *params = @{@"tagId":[NSNumber numberWithInteger:tagID], @"poiId":[NSNumber numberWithInteger:poiID]};
        NSLog(@"affirm: %@", params);
    [[OXServerRequest sharedManager] putRequest:url
                                     parameters:params
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            successBlock (operation, response);
                                        }
                                           fail:^(AFHTTPRequestOperation *operation, id error) {
                                               failBlock (operation , error);
                                           }];

}

+(void)disaffirmTagID:(NSUInteger)tagID
            withPOIID:(NSUInteger)poiID
         successBlock:(SuccessCompletionBlock) successBlock
            failBlock:(FailureCompletionBlock) failBlock{
    NSString *url = [self fullServerURLAndAPIVersionWithNodePoint:kHTTPClientURLStringPUTDisaffirmTag];
    NSLog(@"url: %@", url);
    NSDictionary *params = @{@"tagId":[NSNumber numberWithInteger:tagID], @"poiId":[NSNumber numberWithInteger:poiID]};
    NSLog(@"disaffirm: %@", params);
    [[OXServerRequest sharedManager] putRequest:url
                                     parameters:params
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            successBlock (operation, response);
                                        }
                                           fail:^(AFHTTPRequestOperation *operation, id error) {
                                               failBlock (operation , error);
                                           }];
}

/*
+(void)postFacebookNeedInfoWithFacebookAccessTokenString:(NSString*)facebookAccessTokenString
                                            successBlock:(SuccessCompletionBlock)successBlock
                                               failBlock:(FailureCompletionBlock)failBlock{
    
    NSString *url = [self fullServerURLAndAPIVersionWithNodePoint:kHTTPClientURLStringPOSTFacebookNeedInfo];
//    NSLog(@"URL: %@", url);

    [[OXServerRequest sharedManager] setJSONHeaders];
    [[OXServerRequest sharedManager].requestSerializer setValue:facebookAccessTokenString forHTTPHeaderField:@"token"];

    
    [[OXServerRequest sharedManager] postRequest:url
                                          parameters:nil
                                             success:^(AFHTTPRequestOperation *operation, id response) {
                                                 
                                                 if([response objectForKey:@"username"]){
                                                     NSLog(@"SAVE SESSION COOKIES");
                                                     [[OXCookieManager sharedManager] saveSessionCookiesForURL:[NSURL URLWithString:url]];
                                                     [OXUserModel saveUsernameWithUsername:[response objectForKey:@"username"]];
                                                 }
                                                 
                                                 successBlock (operation, response);
                                             }
                                                fail:^(AFHTTPRequestOperation *operation, id error) {
                                                    failBlock (operation , error);
                                                }];
}

*/



+(void)postLoginWithFacebookWithAccessTokenString:(NSString*)facebookAccessTokenString
                             withParamsDictionary:(NSDictionary*)params
                                     successBlock:(SuccessCompletionBlock) successBlock
                                        failBlock:(FailureCompletionBlock) failBlock{
    
    NSString *url = [self fullServerURLAndAPIVersionWithNodePoint:kHTTPClientURLStringPOSTFacebookNeedInfo];
    
    [[OXServerRequest sharedManager] setJSONHeaders];
    [[OXServerRequest sharedManager].requestSerializer setValue:facebookAccessTokenString forHTTPHeaderField:@"token"];
    
    if(params){
        for(NSString *key in params){
            [[OXServerRequest sharedManager].requestSerializer setValue:[params objectForKey:key] forHTTPHeaderField:key];
        }
    }
    NSMutableDictionary *moreParams = [params mutableCopy];
    if([[OXDeviceTokenManager sharedManager] userDeviceToken]){
        [moreParams setObject:[[OXDeviceTokenManager sharedManager] userDeviceToken] forKey:@"device_token"];
    }
    NSLog(@"facebook parameters being sent: %@", moreParams);

    [[OXServerRequest sharedManager] postRequest:url
                                      parameters:moreParams
                                         success:^(AFHTTPRequestOperation *operation, id response) {
                                             if([response objectForKey:@"username"]     ||[operation.response statusCode] == 200){
                                                 NSLog(@"facebook login response: %@", response);
//#warning todo: tell francesco to return user data so that we can display the tutorial with the username properly
                                                 
                                                 
                                                 [[OXCookieManager sharedManager] saveSessionCookiesForURL:[NSURL URLWithString:url]];
                                                 [OXUserModel saveUsernameWithUsername:[response objectForKey:@"username"]];
                                             }
                                             successBlock (operation, response);
                                         }
                                            fail:^(AFHTTPRequestOperation *operation, id error) {
                                                failBlock (operation , error);
                                            }];
}

+(void)postFacebookRecommendationsWithAccessTokenString:(NSString*)facebookAccessTokenString
                                      andPlaneRestModel:(OCNPlaneModel*)planeRestModel
                                           successBlock:(SuccessCompletionBlock)successBlock
                                              failBlock:(FailureCompletionBlock)failBlock{
    NSString *url = [self fullServerURLAndAPIVersionWithNodePoint:kHTTPClientURLStringPOSTAndGetFacebookRecommendations];
    
    [[OXServerRequest sharedManager] setJSONHeaders];

//    NSDictionary * defaultParams = @{@"accessToken": facebookAccessTokenString, @"planeRestModel":@{@"x0":    [NSNumber numberWithDouble:-2], @"x1": [NSNumber numberWithDouble:100], @"y0":[NSNumber numberWithDouble:-2], @"y1":[NSNumber numberWithDouble:100]}};//[planeRestModel dictionaryValue]};
//    NSLog(@"run facebook recommendation");
//        NSLog(@"access token: %@", facebookAccessTokenString);
    NSDictionary * defaultParams = @{@"accessToken": facebookAccessTokenString, @"planeRestModel":[planeRestModel dictionaryValue]};
//    NSLog(@"default params: %@", defaultParams);

    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:defaultParams options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    
    NSLog(@"facebook recommendation model dictionary value: %@", myString);

    [[OXServerRequest sharedManager] postRequest:url
                                      parameters:defaultParams
                                         success:^(AFHTTPRequestOperation *operation, id response) {
//                                             NSInteger responseCode = [operation.response statusCode];
//                                             NSLog(@"responseCode: %lu\n\nandresponse: %@", responseCode, response);
                                             successBlock (operation, response);
                                         }
                                            fail:^(AFHTTPRequestOperation *operation, id error) {
                                                NSInteger responseCode = [operation.response statusCode];
                                                NSLog(@"responseCode: %lu\n\nfacebook recommendation error: %@", (long)responseCode, error);
                                                failBlock (operation , error);
                                            }];

    
}

+(void)getFacebookRecommendationsWithSuccessBlock:(SuccessCompletionBlock)successBlock
                                        failBlock:(FailureCompletionBlock)failBlock{
    NSString *url = [self fullServerURLAndAPIVersionWithNodePoint:kHTTPClientURLStringPOSTAndGetFacebookRecommendations];
    
    [[OXServerRequest sharedManager] getRequest:url parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
//        NSInteger responseCode = [operation.response statusCode];
//        NSLog(@"responseCode: %lu\n\nandresponse: %@", responseCode, response);
        successBlock (operation, response);
    } fail:^(AFHTTPRequestOperation *operation, id error) {
//        NSInteger responseCode = [operation.response statusCode];
//        NSLog(@"responseCode: %lu\n\nfacebook recommendation error: %@", responseCode, error);
        failBlock (operation , error);
    }];
}
+(void)getFacebookCheck:(SuccessCompletionBlock)successBlock
              failBlock:(FailureCompletionBlock)failBlock{
    NSString *url = [self fullServerURLAndAPIVersionWithNodePoint:kHTTPClientURLStringGETFacebookCheck];
    [[OXServerRequest sharedManager] getRequest:url parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        successBlock (operation, response);
    } fail:^(AFHTTPRequestOperation *operation, id error) {
        failBlock (operation , error);
    }];
}

+(void)updateUserDeviceToken:(NSString*)deviceToken
                successBlock:(SuccessCompletionBlock) successBlock
                   failBlock:(FailureCompletionBlock) failBlock{
    NSString *url = [self fullServerURLAndAPIVersionWithNodePoint:kHTTPClientURLStringPUTUpdateDeviceToken];
    NSLog(@"url: %@", url);
    NSDictionary *params = @{@"deviceToken":deviceToken};
    NSLog(@"update device token: %@", params);
    [[OXServerRequest sharedManager] putRequest:url
                                     parameters:params
                                        success:^(AFHTTPRequestOperation *operation, id response) {
                                            successBlock (operation, response);
                                        }
                                           fail:^(AFHTTPRequestOperation *operation, id error) {
                                               failBlock (operation , error);
                                           }];
}
+(void)updateUserLocationWithCoordinate:(CLLocationCoordinate2D)userLocationCoordinate
                           successBlock:(SuccessCompletionBlock) successBlock
                              failBlock:(FailureCompletionBlock) failBlock{
    NSString * url = [NSString stringWithFormat:@"%@%@", [self baseWithRESTUrlAndVersionAPI], kHTTPClientURLStringPUTUpdateUserLocation];
    
    NSDictionary *params = @{
                             @"latitude":[NSNumber numberWithDouble:userLocationCoordinate.latitude],
                             @"longitude":[NSNumber numberWithDouble:userLocationCoordinate.longitude]
                             };
    
    [[OXServerRequest sharedManager] putRequest:url
                                     parameters:params
                                        success:^(AFHTTPRequestOperation *operation, id response){
                                            
                                            successBlock(operation, response);
                                        
                                        }
                                           fail:^(AFHTTPRequestOperation *operation, id error){
                                               
                                               failBlock(operation, error);
                                               
                                           }];
}


@end
