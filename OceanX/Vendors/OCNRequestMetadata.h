//
//  OCNRequestMetadata.h
//  Oceanapp
//
//  Copyright (c) 2014 Ocean Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessCompletionBlock)(AFHTTPRequestOperation *operation, id response);

typedef void(^FailureCompletionBlock)(AFHTTPRequestOperation *operation, id error);


typedef NS_ENUM(NSInteger, ServerRequestType){
    RequestGET,
    RequestPOST,
    RequestPUT,
    RequestDELETE
};

@interface OCNRequestMetadata : NSObject

@property (nonatomic, assign) ServerRequestType requestType;
@property (nonatomic,strong) NSString* requestURL;
@property (nonatomic,strong) NSDictionary* requestParameters;
@property (copy) SuccessCompletionBlock successCompletionBlock;
@property (copy) FailureCompletionBlock failureCompletionBlock;

+ (OCNRequestMetadata *)initWithRequestType:(ServerRequestType)requestType url:(NSString *)url params:(NSDictionary *)params successBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock)failBlock;

@end
