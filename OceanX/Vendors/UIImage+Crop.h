//
//  UIImage+Crop.h
//  OceanX
//
//  Created by Pavan Kataria on 12/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Crop)

-(UIImage*)crop:(CGRect)rect;
-(UIImage*)squareCropImageToSideLength:(UIImage *)sourceImage withSideLengthFloat:(CGFloat)sideLength;

@end
