//
//  PKThumbnailView.h
//  OceanX
//
//  Created by Pavan Kataria on 26/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "PKPreviewBaseView.h"
@class PKThumbnailView;

@protocol PKThumbnailViewDelegate <NSObject>
@required
-(void)pkThumbnailView:(PKThumbnailView*)thumbnailView didSelectThumbnailPreviewAtIndexPosition:(NSUInteger)index;
@optional
-(void)pkThumbnailView:(PKThumbnailView *)thumbnailView didDeleteThumbnailPreviewAtIndexPosition:(NSUInteger)index;
@end

@interface PKThumbnailView : PKPreviewBaseView
@property (nonatomic, weak) id <PKThumbnailViewDelegate> delegate;
-(void)setupDeleteButton;
@end
