//
//  OCNRequestMetadata.m
//  Oceanapp
//
//  Copyright (c) 2014 Ocean Technologies. All rights reserved.
//

#import "OCNRequestMetadata.h"

@implementation OCNRequestMetadata

+ (OCNRequestMetadata *)initWithRequestType:(ServerRequestType)requestType url:(NSString *)url params:(NSDictionary *)params successBlock:(SuccessCompletionBlock)successBlock failBlock:(FailureCompletionBlock)failBlock
{
    OCNRequestMetadata *SELF = [OCNRequestMetadata new];
    SELF.requestType = requestType;
    SELF.requestURL = url;
    SELF.requestParameters = params;
    SELF.successCompletionBlock = successBlock;
    SELF.failureCompletionBlock = failBlock;
    
    return SELF;
}

-(BOOL) isEqual:(id)object{
    
    if ([object isKindOfClass:OCNRequestMetadata.class]) {
       
        NSArray* pathComponents = [self.requestURL pathComponents];
        NSString *selfPath = [NSString stringWithFormat:@"%@", pathComponents.lastObject];
        
        OCNRequestMetadata * requestInfo = (OCNRequestMetadata*) object;
        pathComponents = [requestInfo.requestURL pathComponents];
        NSString *requestInfoPath = [NSString stringWithFormat:@"%@", pathComponents.lastObject];
        
        return [selfPath isEqualToString:requestInfoPath];
    }
    
    return NO;
    
}

@end
