//
//  AppDelegate.m
//  OceanX
//
//  Created by Pavan Kataria on 11/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.

#import "AppDelegate.h"
#import "OXServerRequest.h"

#import "OXFriendModel.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "ATAppUpdater.h"
#import "AFNetworkActivityLogger.h"
#import "TSMessage.h"

@interface AppDelegate (){
    NSTimer *timer;
}

@end

@implementation AppDelegate

-(NSMutableArray *)friendsArray{
    if(!_friendsArray){
        _friendsArray = [[NSMutableArray alloc] init];
    }
    return _friendsArray;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Register for Remote Notifications
  
//#if DEBUG 
//    NSLog(@"setting replay tutorial view to NO");
//    [OXLoginManager setReplayTutorialViewedTo:NO];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//#endif
    [self checkIfAppLaunchedWithNotification:launchOptions withApplication:application];
    [PKUIAppearanceController appStyle];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        NSLog(@"system version is greater than or equal to version 8.0");
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound
                                                                                 categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else{
        NSLog(@"system version is less than version 8.0");
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    }
    
    [self performSelector: @selector(finishSetup) withObject: nil afterDelay:0];
    
    return YES;
    
    
    
}
-(void)checkIfAppLaunchedWithNotification:(NSDictionary*)launchOptions withApplication:(UIApplication*)application{
    NSLog(@"launchoptions: %@", launchOptions);
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification) {
        NSLog(@"app recieved notification from remote %@", notification);
        [self application:application didReceiveRemoteNotification:(NSDictionary*)notification];
    }else{
        NSLog(@"app did not recieve notification");
    }
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //Store the device token
    NSLog(@"Did Register for Remote Notifications with Device Token (%@)", deviceToken);
    [[OXDeviceTokenManager sharedManager] storeDeviceToken:deviceToken];
    
    if([OXLoginManager isLoggedIn]){
        if([[OXDeviceTokenManager sharedManager] userDeviceToken]){
            [OXRestAPICient updateUserDeviceToken:[[OXDeviceTokenManager sharedManager] userDeviceToken]
                                     successBlock:^(AFHTTPRequestOperation *operation, id response) {
                NSLog(@"successfully updated device token. Response: %@", response);

            } failBlock:^(AFHTTPRequestOperation *operation, id error) {
                NSLog(@"Failed to update device token: %@", error);
            }];
        }
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    application.applicationIconBadgeNumber = 0;
    //self.textView.text = [userInfo description];
    // We can determine whether an application is launched as a result of the user tapping the action
    // button or whether the notification was delivered to the already-running application by examining
    // the application state.
    
    if (application.applicationState == UIApplicationStateActive){
        // Nothing to do if applicationState is Inactive, the iOS already displayed an alert view.

//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Did receive a Remote Notification" message:[NSString stringWithFormat:@"You've received a notification while it was running:\n%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        
//        [alertView show];
        //Process GET FACEBOOK RECOMMENDATION REQUEST depending on notification type
        NSLog(@"%@", userInfo);
        NSLog(@"all keys: %@", [userInfo allKeys]);
        if([userInfo objectForKey:@"aps"]){
            NSLog(@"aps entered");
            NSLog(@"aps all keys: %@", [[userInfo objectForKey:@"aps"] allKeys]);

            if([[userInfo objectForKey:@"aps"] objectForKey:@"category"]){
                NSString *categoryString = [[userInfo objectForKey:@"aps"] objectForKey:@"category"];
                if([categoryString isEqualToString:kNotificationPushNameFBRecommendation]){
                    NSLog(@"FACEBOOK RECOMMENDATION NOTIFICATION");
                    
                    [OXRestAPICient getFacebookRecommendationsWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
                        NSLog(@"facebook GET recommendations RESPONSE: %@", response);
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPushNameFBRecommendation object:nil userInfo:response];

                    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
                        NSLog(@"failed: facebook GET recommendations ERROR: %@", error);
                    }];
                    
                }
                else if([categoryString isEqualToString:kNotificationPushNameNotifyAllInactiveWhoAreInTheAppNotLoggedIn]){
                    if(![OXLoginManager isLoggedIn]){
                        if([[userInfo objectForKey:@"aps"] objectForKey:@"message"]){
                            NSString *message = [[userInfo objectForKey:@"aps"] objectForKey:@"message"];
                            [TSMessage showNotificationWithTitle:message type:TSMessageNotificationTypeSuccess];
                        }
                    }
                }
                else if([categoryString isEqualToString:kNotificationPushNameNotifyAll]){
                    if([[userInfo objectForKey:@"aps"] objectForKey:@"message"]){
                        NSString *message = [[userInfo objectForKey:@"aps"] objectForKey:@"message"];
                        [TSMessage showNotificationWithTitle:message type:TSMessageNotificationTypeSuccess];
                    }
                }
            }
        }
        

    }
    //Launched
    if (application.applicationState == UIApplicationStateInactive){
        if([[userInfo objectForKey:@"aps"] objectForKey:@"category"]){
            NSString *categoryString = [[userInfo objectForKey:@"aps"] objectForKey:@"category"];
            if([categoryString isEqualToString:@"show POI"]){
                if([[userInfo objectForKey:@"aps"] objectForKey:@"poiNumber"]){
                    int poiNumber = (int)[[userInfo objectForKey:@"aps"] objectForKey:@"poiNumber"];
                    UIAlertView *nagAlert = [[UIAlertView alloc] initWithTitle:@"Display POI" message:[NSString stringWithFormat:@"show poi number: %d", poiNumber] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [nagAlert show];
                }
            }
        }
    }
}

-(void)finishSetup{
    // Override point for customization after application launch.
    self.facebookPermissions = [@[@"public_profile", @"email", @"user_birthday"] mutableCopy];
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0
                                                            diskCapacity:0
                                                                diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    
    
    [FBSession.activeSession closeAndClearTokenInformation];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[ATAppUpdater sharedUpdater] forceOpenNewAppVersion:YES];
    });

    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    
    
    //    [self checkedForFacebookCachedSession];
    
    [self setupNetworkMonitoring];
    [self persistentStoreCoordinator];
    
    
#ifdef DEBUG
    [[AFNetworkActivityLogger sharedLogger] startLogging];
    [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
#endif
    [Flurry startSession:KFlurryAppID];
    
    [self setPermissionsWithLike];
    
    [self callCheckFacebookLikePermissionIsActive];
//    [self callFacebookNotification];
}

-(void)callCheckFacebookLikePermissionIsActive{
    if(![OXLoginManager isCheckFBLikePermissionActive]){
        [OXRestAPICient getFacebookCheck:^(AFHTTPRequestOperation *operation, id response) {
            NSLog(@"facebook check: %@", response);
            if([response objectForKey:@"approved"]){
                if([[response objectForKey:@"approved"] boolValue] == YES){
                    [OXLoginManager setCheckFBLikePermissionActiveWithBool:YES];
                    [self setPermissionsWithLike];
                }
            }
        } failBlock:^(AFHTTPRequestOperation *operation, id error) {
            
        }];
    }
    else{
        [self setPermissionsWithLike];
    }
}
-(void)setPermissionsWithLike{
    self.facebookPermissions = [@[@"public_profile", @"email", @"user_birthday",@"user_likes"] mutableCopy];
}

-(void)callFacebookNotification{
    MKCoordinateRegion currentRegion = [[OXLocationManager sharedTracker] currentMapRegion];
    OCNPlaneModel * plane = [OCNPlaneModel planeModelFromMapRegion:currentRegion];
    
    NSString *accessToken = @"CAAE3rhKZAm8UBALuDm8mDtcCZA3RlTwarJVB6Vl8fTWqMTREK2esScLccs6yNMYOnqow9oUgnxnOuqhgjinQ0TaRIKqt57EodHjcWZC3KK0ZCVbZCVMtUwoEgjoxobsGZAJ9l3uhPeXvXxbaQHNI4dEPGW9wZBHGYZAVZCBtxRBuJ3kfJnFZBIKS1e2BlTdahBcsSiK1Ng5YpIpGBTl2Q7hjKGNeXQ2W7kpof9Tw8XQjOwYwZDZD";
    
//    NSString *accessToken = FBSession.activeSession.accessTokenData.accessToken;
    NSLog(@"ACCESS TOKEN : %@", accessToken);
    if(accessToken.length){
        [OXRestAPICient postFacebookRecommendationsWithAccessTokenString:accessToken
                                                       andPlaneRestModel:plane successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                                           NSInteger responseCode = [operation.response statusCode];
                                                           [timer invalidate];
                                                           NSLog(@"facebook recommendation status code: %d response: %@", (int)responseCode, response);
                                                           if(responseCode == 200){
                                                               
                                                               if(response){
                                                                   [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPushNameFBRecommendation object:nil userInfo:response];
                                                                   
                                                               }
                                                               //                                                           NSLog(@"status code: %d response: %@", (int)responseCode, response);
                                                               //triggered_no_notification
                                                               /*
                                                                output in case no recommendation : server returns null for the iOS to know the recommendation wasn't triggered (not enough data)
                                                                */
                                                               //                                                           [self showHomePageWithInstantRecommendationWithResponse:response];
                                                           }
                                                           else if(responseCode == 202){
                                                               //                                                           NSLog(@"status code: %d response: %@", (int)responseCode, response);
                                                               NSString *responseMessage = [response objectForKey:@"message"];
                                                               [SVProgressHUD showWithStatus:responseMessage maskType:SVProgressHUDMaskTypeBlack];
                                                           }
                                                           
                                                           
                                                           
                                                       }
                                                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                                                   //                                                               [self showHomePage];
                                                                   
                                                                   NSLog(@"signup page error:%@",error);
                                                               }];
    }
    

}


-(void)setupNetworkMonitoring{
    //start monitoring
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    // reachability
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"WIFI");
                [[StatusHUDSBViewController sharedManager] dismiss];
                //[[OXLocationManager sharedTracker] start];
                break;
            case AFNetworkReachabilityStatusNotReachable:
            default:
                NSLog(@"offline");
                
                [[StatusHUDSBViewController sharedManager] displayErrorStatusWithTitle:@"The application is not able to connect to the server." message:@" Only the profile page is usable for now. Please check your network settings" imageName:@"networkError"];
                break;
        }
    }];

}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[ATAppUpdater sharedUpdater] forceOpenNewAppVersion:YES];
    [SVProgressHUD dismiss];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // Logs 'install' and 'app activate' App Events.
    [FBAppEvents activateApp];
    [FBAppCall handleDidBecomeActive];
    [SVProgressHUD dismiss];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

-(void)setupTabBarAndThenShowInstantRecommendationWithResponse:(id)response{
    [self showMainScreen];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPushNameFBRecommendation object:nil userInfo:response];

}
-(void)showMainScreen{
    [self grabFriendsWithRetries:3];
    //    UIStoryboard *addPlaceStoryboard = [UIStoryboard storyboardWithName:@"AddPlace" bundle:nil];
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *addPlaceViewController = [main instantiateInitialViewController];
    self.window.rootViewController = addPlaceViewController;
    
    UITabBarController *tabVC = (UITabBarController *)self.window.rootViewController;
    NSLog(@"root view %@", self.window.rootViewController );
    
//    UITabBar *tabBar = tabBarController.tabBar;
    NSArray *unselectedImages = [NSArray arrayWithObjects:@"tabbarhome", @"tabbarsearch", @"tabbarprofile", nil];
    NSArray *selectedImages = [NSArray arrayWithObjects:@"tabbarhome_active", @"tabbarsearch_active", @"tabbarprofile_active", nil];

    NSArray *items = tabVC.tabBar.items;
    
    for(int idx = 0; idx < 3; idx++){
        UITabBarItem *item;
        if(idx == 2){
            item = [items objectAtIndex:3];
        }
        else{
            item = [items objectAtIndex:idx];
        }
        UIImage *unselectedImage = [UIImage imageNamed:unselectedImages[idx]];
        UIImage *selectedImage = [UIImage imageNamed:selectedImages[idx]];
        
//        [item setFinishedSelectedImage:unselectedImage withFinishedUnselectedImage:selectedImage];
        
        [item setImage:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [item setSelectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    }

//    NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:kSavedUserNameKey];
//    NSLog(@"username: %@", username);
    if([OXLoginManager isProfileTutorialViewed]){
        NSLog(@"tutorial profile has viewed");
    }
    else{
        NSLog(@"tutorial profile has not viewed");
        
        UIStoryboard *main = [UIStoryboard storyboardWithName:@"SettingsScreenSB" bundle:nil];
        UIViewController *tutorialViewController = [main instantiateViewControllerWithIdentifier:@"OXFirstTimeUserTutorialManagerNavigationController"];
        
        [self.window.rootViewController presentViewController:tutorialViewController animated:NO completion:^{
//            [OXLoginManager setReplayTutorialViewed];
//            [[NSUserDefaults standardUserDefaults] synchronize];
        }];
    }
}


-(void)showLoginScreen{
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UIViewController *loginScreenVC = [main instantiateInitialViewController];
    self.window.rootViewController = loginScreenVC;
}


+(AppDelegate*)getAppDelegateReference{
    return [[UIApplication sharedApplication] delegate];
}



-(void)grabFriendsWithRetries:(int)counter{
    __block int retriesLeft = counter;
    [OXRestAPICient getFriendsListWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"FRIENDS LIST %@", response);
        self.friendsArray = [OXFriendModel getFriendsArrayFromJSONResponse:response];
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        retriesLeft -= 1;
        if(retriesLeft > 0){
            [self grabFriendsWithRetries:retriesLeft];
        }
        else{
            NSLog(@"error getting friends: %@", error);
        }
    }];
}




#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;



//Explicitly write Core Data accessors
- (NSManagedObjectContext *) managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Ocean" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (BOOL)progressivelyMigrateURL:(NSURL*)sourceStoreURL
                         ofType:(NSString*)type
                        toModel:(NSManagedObjectModel*)finalModel
                          error:(NSError**)error
{
    
    
    /**
     In this code segment, we first retrieve the metadata from the source URL. If that metadata is not nil, we ask the final model whether the metadata is compatible with it. If it is, we are happy and done. We then set the error pointer to nil and return YES. If it isn’t compatible, we need to try to figure out the mapping model and potentially the interim data model to migrate to.
     
     */
    NSDictionary *sourceMetadata =
    [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:type
                                                               URL:sourceStoreURL
     
                                                             error:error];
    if (!sourceMetadata){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"Source meta data not found" forKey:NSLocalizedDescriptionKey];
        //Populate the error
        *error = [NSError errorWithDomain:@"Zarra" code:8001 userInfo:dict];
        
        return NO;
    }
    if ([finalModel isConfiguration:nil
        compatibleWithStoreMetadata:sourceMetadata]) {
        *error = nil;
        return YES;
    }
    else{
        // Migration is needed
        NSLog(@"\n#######################\n####################### MIGRATION IS NEEDED!!!!\n######################\n\n");
        //        return YES;
    }
    
    
    /**
     To proceed to the next step in the migration, we need to find all the managed object models in the bundle and loop through them. The goal at this point is to get all the models and figure out which one we can migrate to. Since these models will probably be in their own bundles, we have to first look for the bundles and then look inside each of them.
     
     
     In this code block, we first grab all the resource paths from the mainBundle that are of type momd. This gives us a list of all the model bundles. We then loop through the list and look for mom resources inside each and add them to an overall array. Once that’s done, we look inside the mainBundle again for any freestanding mom resources. Finally, we do a failure check to make sure we have some models to look through. If we can’t find any, we populate the NSError and return NO.
     */
    
    //Find the source model
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil
                                                                    forStoreMetadata:sourceMetadata];
    
    NSAssert(sourceModel != nil, ([NSString stringWithFormat:@"Failed to find source model\n%@", sourceMetadata]));
    
    //Find all of the mom and momd files in the Resources directory
    NSMutableArray *modelPaths = [NSMutableArray array];
    NSArray *momdArray = [[NSBundle mainBundle] pathsForResourcesOfType:@"momd"
                                                            inDirectory:nil];
    for (NSString *momdPath in momdArray) {
        NSString *resourceSubpath = [momdPath lastPathComponent]; NSArray *array = [[NSBundle mainBundle]
                                                                                    pathsForResourcesOfType:@"mom"
                                                                                    inDirectory:resourceSubpath];
        [modelPaths addObjectsFromArray:array];
    }
    
    NSArray* otherModels = [[NSBundle mainBundle] pathsForResourcesOfType:@"mom"
                                                              inDirectory:nil];
    [modelPaths addObjectsFromArray:otherModels];
    
    if (!modelPaths || ![modelPaths count]) {
        //Throw an error if there are no models
        NSMutableDictionary *dict = [NSMutableDictionary dictionary]; [dict setValue:@"No models found in bundle"
                                                                              forKey:NSLocalizedDescriptionKey];
        //Populate the error
        *error = [NSError errorWithDomain:@"Zarra" code:8001 userInfo:dict];
        
        return NO;
    }
    
    
    
    
    
    
    /*
     Now the complicated part comes in. Since it is not currently possible to get an NSMappingModel with just the source model and then determine the destina- tion model, we have to instead loop through every model we find, instantiate it, plug it in as a possible destination, and see whether there is a mapping model in existence. If there isn’t, we continue to the next one.
     
     */
    NSMappingModel *mappingModel = nil;
    NSManagedObjectModel *targetModel = nil;
    NSString *modelPath = nil;
    
    for(modelPath in modelPaths){
        targetModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:[NSURL fileURLWithPath:modelPath]];
        mappingModel = [NSMappingModel mappingModelFromBundles:nil
                                                forSourceModel:sourceModel
                                              destinationModel:targetModel];
        
        //If we found a mapping model then proceed
        if(mappingModel) break;
        //Release the target model and keep looking
        targetModel = nil;
    }
    
    //We have tested every model, if nil here we failed
    if(!mappingModel){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"No models found in bundle" forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"Zarra" code:8001 userInfo:dict];
        
        return NO;
    }
    
    
    /*
     This above section is probably the most complicated piece of the progressive migration routine. In this section, we’re looping through all the models that were previously discovered. For each of those models, we’re instantiating the model and then asking NSMappingModel for an instance that will map between our known source model and the current model. If we find a mapping model, we break from our loop and continue. Otherwise, we release the instantiated model and continue the loop. After the loop, if the mapping model is still nil, we generate an error stating that we cannot discover the progression between the source model and the target and return NO. At this point, we should have all the components we need for one migration. The source model, target model, and mapping model are all known quantities. Now it’s time to migrate!
     */
    
    
    /*
     
     Performing the Migration
     In this block, we are instantiating an NSMigrationManager (if we needed something special, we would build our own manager) with the source model and the desti- nation model. We are also building up a unique path to migrate to. In this example, we are using the destination model’s filename as the unique change to the source store’s path. Once the destination path is built, we then tell the migration manager to perform the migration and check to see whether it was successful. If it wasn’t, we simply return NO because the NSError will be populated by the NSMigrationManager. If it’s successful, there are only three things left to do: move the source out of the way, replace it with the new destination store, and finally recurse.
     
     */
    
    NSMigrationManager *manager = [[NSMigrationManager alloc]
                                   initWithSourceModel:sourceModel
                                   destinationModel:targetModel];
    
    
    //Yet to be modified a bit to suit my needs
    self.migrationStepDescription = [NSString stringWithFormat:@"%@ → %@", [sourceModel.versionIdentifiers anyObject], [targetModel.versionIdentifiers anyObject]];
    
    
    [manager addObserver:self forKeyPath:@"migrationProgress" options:NSKeyValueObservingOptionNew context:nil];
    
    
    
    
    
    
    NSString *modelName = [[modelPath lastPathComponent]
                           stringByDeletingPathExtension];
    
    NSString *storeExtension = [[sourceStoreURL path] pathExtension];
    NSString *storePath = [[sourceStoreURL path] stringByDeletingPathExtension];
    //Build a path to write the new store
    storePath = [NSString stringWithFormat:@"%@.%@.%@", storePath, modelName, storeExtension];
    NSURL *destinationStoreURL = [NSURL fileURLWithPath:storePath];
    
    
    NSDictionary *options = @{ NSSQLitePragmasOption: @{@"journal_mode": @"DELETE" }};
    
    if (![manager migrateStoreFromURL:sourceStoreURL
                                 type:type
                              options:options
                     withMappingModel:mappingModel
                     toDestinationURL:destinationStoreURL
                      destinationType:type
                   destinationOptions:options
                                error:error]) {
        
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:[NSString stringWithFormat:@"Couldn't migrate store from: \nSource:%@ \nto: \nDestination%@\nwith the following mapping model's entity mappings: %@", sourceStoreURL, destinationStoreURL, [mappingModel entityMappingsByName]] forKey:NSLocalizedDescriptionKey];
        //Populate the error
        *error = [NSError errorWithDomain:@"Zarra" code:8001 userInfo:dict];
        return NO;
    }
    
    /*
     In this final code block, we first create a permanent location for the original store to be moved to. In this case, we will use a globally unique string gener- ated from the NSProcessInfo class and attach the destination model’s filename and the store’s extension to it. Once that path is built, we move the source to it and then replace the source with the destination. At this point, we are at the same spot we were when we began except that we are now one version closer to the current model version.
     */
    
    
    /*
     Now we need to loop back to step 1 again in our workflow. Therefore, we will recursively call ourselves, returning the result of that recurse. As you can recall from the beginning of this method, if we are now at the current version, we will simply return YES, which will end the recursion.
     */
    
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString];
    guid = [guid stringByAppendingPathExtension:modelName];
    guid = [guid stringByAppendingPathExtension:storeExtension];
    
    NSString *appSupportPath = [storePath stringByDeletingLastPathComponent];
    NSString *backupPath = [appSupportPath stringByAppendingPathComponent:guid];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager moveItemAtPath:[sourceStoreURL path]
                              toPath:backupPath
                               error:error]) {
        //Failed to copy the file
        NSLog(@"Failed to copy the sourceStoreUrl to the backup path url");
        return NO;
    }
    
    
    //Move the destination to the source path
    if (![fileManager moveItemAtPath:storePath toPath:[sourceStoreURL path]
                               error:error]) {
        
        
        //1) I BELIEVE AT THIS POINT the backup path is not delete if THE MIGRATION FAILED
        
        //Try to back out the source move first, no point in checking it for errors
        [fileManager moveItemAtPath:backupPath
                             toPath:[sourceStoreURL path]
                              error:nil];
        return NO;
    }
    else{
        
        //####################################
        //2) AND ALSO AT THIS POINT the backup path is not deleted if the migration WAS SUCCESSFUL
        //I HAD TO ADD THIS BIT A.S.. because the backup path was never deleted?
        //Source store was moved to the backup path which can be deleted.
        [fileManager removeItemAtPath:backupPath error:error];
        
        NSLog(@"Backup path %@ to be deleted", backupPath);
        
    }
    //####################################
    
    
    //We may not be at the "current" model yet, so recurse
    return [self progressivelyMigrateURL:sourceStoreURL ofType:type
                                 toModel:finalModel
                                   error:error];
    
}


- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSLog(@"hello");
    NSURL *storeURL = [NSURL fileURLWithPath:[[self applicationDocumentsDirectory2] stringByAppendingPathComponent:@"Ocean.sqlite"]];

    
    NSLog(@"STORE URL: %@", storeURL);
    
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    
    
    UIBackgroundTaskIdentifier migrationTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"CoreData migration: background task time expired");
    }];
    
    
    if(![self progressivelyMigrateURL:storeURL ofType:NSSQLiteStoreType toModel:[self managedObjectModel] error:&error])
    {
        // reset the persistent store on fail
        NSString *documentDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:[NSBundle pathForResource:@"Ocean" ofType:@"sqlite" inDirectory:documentDir] error:&error];
        
        NSLog(@"ERROR: %@", [error localizedDescription]);
    }
    else
    {
        NSLog(@"migration succeeded!");
    }
    
    [[UIApplication sharedApplication] endBackgroundTask:migrationTask];
    
    NSDictionary *options = @{ NSSQLitePragmasOption: @{@"journal_mode": @"DELETE" }};
    
    //    NSDictionary *options = @{ @"journal_mode": @"DELETE" };
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil
                                                             URL:storeURL options:options error:&error]) {
        NSString *msg = nil;
        
        msg = [NSString stringWithFormat:@"The users database %@%@%@\n%@\n%@",
               @"is either corrupt or was created by a newer ", @"version of Ocean. Please contact ", @"support to assist with this error.",
               [error localizedDescription], [error userInfo]];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:msg
                                                           delegate:self
                                                  cancelButtonTitle:@"Quit"
                                                  otherButtonTitles:nil];
        [alertView show];
        return nil;
    }
    
    //    NSLog(@"end of persistent store method");
    return _persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory2{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}
#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ((self.state == CoreDataManagerStateMigrating) && ([keyPath isEqualToString:@"migrationProgress"]))
    {
        self.migrationStepProgress = [[change valueForKey:NSKeyValueChangeNewKey] floatValue];
        NSLog(@"Migration progress: %2.2f", self.migrationStepProgress);
    }
}


#pragma mark - facebook - Methods

-(void)checkedForFacebookCachedSession{
    NSLog(@"checking for facebook cached session");
    // Whenever a person opens app, check for a cached session
    
    NSLog(@"current activeSessionState: %lu", (unsigned long)FBSession.activeSession.state);
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        NSLog(@"CACHED SESSION AVAILABLE");
        // If there's one, just open the session silently, without showing the user the login UI
        [FBSession openActiveSessionWithReadPermissions:[[AppDelegate getAppDelegateReference] facebookPermissions]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          // Handler for session state changes
                                          // Call this method EACH time the session state changes,
                                          //  NOT just when the session open
                                          [self sessionStateChanged:session state:state error:error];
                                      }];
    }
}


/*
 Handling Callback URL
 // During login, your app passes control to Facebook iOS app or Facebook in a mobile browser.
 // After authentication, your app will be called back with the session information.

 To handle the incoming URL when the Facebook app or Facebook website returns control to your app, override application:openURL:sourceApplication:annotation in your app delegate to call the FBsession object:
 
 By default, FBAppCall will completes the flow for FBSession.activeSession. If you explicitly manage FBSession instances, use the overload that takes an FBSession instance.
 */
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation{
    // Note this handler block should be the exact same as the handler passed to any open calls.
    [FBSession.activeSession setStateChangeHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         
         // Retrieve the app delegate
         AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
         // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
         [appDelegate sessionStateChanged:session state:state error:error];
     }];
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}

/*
 Session State Changes

 You should provide a handler called whenever there's a session state change during the session's entire lifetime. This includes every intermediate session state change during login, not only after the session is open or opening fails.
 
 To read more about the FBSession lifecycle, see Understanding Sessions.

    Here is an example of handler for different state changes and errors:

// Handles session state changes in the app
 */
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error{

    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        [[NSNotificationCenter defaultCenter] postNotificationName:FBSessionStateChangedNotification
                                                            object:session];

        NSLog(@"Session open: %@", session.accessTokenData.accessToken);
        // Show the user the logged-in UI
//        [self userLoggedIn];

//        [self showMainScreen];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        NSLog(@"Session closed");
        // Show the user the logged-out UI
//        [self userLoggedOut];
    }
    
    // Handle errors
    if (error){
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
//            [self showMessage:alertText withTitle:alertTitle];
            NSLog(@"alert title: %@, alert text %@", alertTitle, alertText);

        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
//                [self showMessage:alertText withTitle:alertTitle];
                NSLog(@"alert title: %@, alert text %@", alertTitle, alertText);

                // Here we will handle all other errors with a generic error message.
                // We recommend you check our Handling Errors guide for more information
                // https://developers.facebook.com/docs/ios/errors/
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
//                [self showMessage:alertText withTitle:alertTitle];
                
                NSLog(@"alert title: %@, alert text %@", alertTitle, alertText);
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
//        [self userLoggedOut];
    }
}

@end
