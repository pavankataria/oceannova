//
//  AppDelegate.h
//  OceanX
//
//  Created by Pavan Kataria on 11/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <FacebookSDK/FacebookSDK.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;

#pragma mark - Core data - properties

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (retain, nonatomic) NSString *migrationStepDescription;
@property (assign) CGFloat migrationStepProgress;
@property (assign) CoreDataManagerState state;

@property (nonatomic, retain) NSMutableArray *friendsArray;


@property (nonatomic, retain) NSMutableArray *facebookPermissions;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;



-(void)showMainScreen;
-(void)setupTabBarAndThenShowInstantRecommendationWithResponse:(id)response;
-(void)showLoginScreen;
+(AppDelegate*)getAppDelegateReference;

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error;
@end

