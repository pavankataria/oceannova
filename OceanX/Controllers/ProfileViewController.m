//  ProfileViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 17/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//


#import "ProfileViewController.h"
#import "OXTagPlaceCountModel.h"
#import "OXFriendModel.h"
#import "OXHCAddPoiModel.h"
#import "OXFacetTableViewCell.h"
#import "OXQuestionViewController.h"
#import "OXSettingsTableViewController.h"

@interface ProfileViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *activateProfileButton;
@property (weak, nonatomic) IBOutlet UIView *activateProfileButtonView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activateProfileViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activateProfileViewHeightBottomConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activateProfileLabelTextHeightConstraint;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSArray *constantMetaArray;
@property (strong, nonatomic) NSMutableArray *dimensions;
@property (strong, nonatomic) NSMutableArray *missingFacets;
@property (strong, nonatomic) NSDictionary *metaData;
@property (strong, nonatomic)OXNewFacetDataModel * currentFacet;

@property (strong, nonatomic) OXUserModel *user;
@property (strong, nonatomic) UIRefreshControl * refreshControl;
@property (nonatomic, weak) IBOutlet UILabel *completeProfileLabel;
@property (nonatomic, assign, getter=isLoadingAPICall) BOOL loadingAPICall;
@end

@implementation ProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.completeProfileLabel.textColor = [PKUIAppearanceController tealColor];
    [self setNavigationBar];
    self.selectedIndexPath = [NSIndexPath indexPathForRow:-1 inSection:-1];
    
//    self.refreshControl = [[UIRefreshControl alloc] init];
//    self.refreshControl.tintColor = [UIColor lightGrayColor];
//    self.refreshControl.backgroundColor = [UIColor lightTextColor];
//    [self.refreshControl addTarget:self action:@selector(refreshProfile) forControlEvents:UIControlEventValueChanged];
    
//    [self.tableView addSubview:self.refreshControl];
    
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableView;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshProfile) forControlEvents:UIControlEventValueChanged];
//    [self.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Pull to refresh Profile" attributes:<#(NSDictionary *)#>]]
    tableViewController.refreshControl = self.refreshControl;
    
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"HelveticaNeue" size:25],
      NSFontAttributeName, [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0], NSForegroundColorAttributeName, nil]];

    [Flurry logEvent:kFlurryUserProfile];
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(self.isLoadingAPICall){
        if (!self.metaData){
            [SVProgressHUD showWithStatus:@"Loading Profile Data" maskType:SVProgressHUDMaskTypeNone];
        }
    }
    
//    if(self.refreshControl.isRefreshing)
//    {
//        CGPoint offset = self.tableView.contentOffset;
//        [self.refreshControl endRefreshing];
//        [self.refreshControl beginRefreshing];
//        self.tableView.contentOffset = offset;
//    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    NSLog(@"view will disappear");
    [SVProgressHUD dismiss];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self refreshProfile];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.user && self.user.profileActivated)
    {
        return FACET_GROUPS;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.user && self.user.profileActivated)
    {
        return (FACET_COUNT/FACET_GROUPS);
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OXFacetTableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellFacetBarTableCellIdentifier forIndexPath:indexPath];
    
    // facet does not contain facet score
    OXNewFacetDataModel *facet = [self.dimensions[indexPath.section] objectAtIndex:indexPath.row];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.facetID == %@", facet.facetID];
    
    // self.user.facetscore is an array of OXFacetModels and they do not contain description.
    OXNewFacetDataModel *userFacet = [[self.user.facetScore filteredArrayUsingPredicate:predicate] firstObject];
    [cell setupWithFacet:userFacet];
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == self.selectedIndexPath.row && indexPath.section == self.selectedIndexPath.section)
    {
        OXNewFacetDataModel *facet = [self.dimensions[indexPath.section] objectAtIndex:indexPath.row];
        if (facet.facetScore != FACET_NULL_VALUE)
        {
            return DESCRIPTION_LABEL_ORIGIN + [self labelHeightForText:facet.facetDescription];
        }
    }
    return 60;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *sectionHeaderlabel = [[UILabel alloc] initWithFrame: CGRectMake(15, 10, 0, 0)];
    sectionHeaderlabel.text = [self titleForSection:section];
    sectionHeaderlabel.textColor = [PKUIAppearanceController tealColor];
    sectionHeaderlabel.font =  [UIFont fontWithName:@"HelveticaNeue" size:20.0f],
    [sectionHeaderlabel sizeToFit];
    UIView * view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    [view addSubview:sectionHeaderlabel];
    return view;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    OXNewFacetDataModel *facet = [self.dimensions[indexPath.section] objectAtIndex:indexPath.row];
    
    if (facet.facetScore == FACET_NULL_VALUE){
        NSLog(@"did select 1");
        [self activateProfileButtonTapped:nil];
        return;
    }
    
    else if(self.selectedIndexPath == indexPath)
    {
        self.selectedIndexPath = [NSIndexPath indexPathForRow:-1 inSection:-1];
    }
    else
    {
        self.selectedIndexPath = indexPath;
        [Flurry logEvent:kFlurryFacetDetail];
    }
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma  mark - Private Methods

- (void)scrollTableViewToFacet {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.facetID == %@", self.currentFacet.facetID];
    OXNewFacetDataModel *matchedFacet = [[self.constantMetaArray filteredArrayUsingPredicate:predicate] lastObject];
    
    if (matchedFacet && (matchedFacet.facetScore != FACET_NULL_VALUE)) {
        for (int index = 0; index < self.dimensions.count; index ++) {
            NSArray *facetsArray = [self.dimensions objectAtIndex:index];
            
            NSUInteger facetIndex = [facetsArray indexOfObject:matchedFacet];
            if (facetIndex != NSNotFound) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:facetIndex inSection:index];
                
                [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
                
                [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
                
                break;
            }
        }
    }
}




//TODO : for now dimension array is initialized every time this method is called. We need to do this to access updated facet models from constant meta array
-(NSMutableArray *)dimensions
{
    if(!_dimensions)
    {
        _dimensions = [NSMutableArray array];
        for(int i = 0; i < kProfileDimensionCount; i++)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.dimension.dimensionId == %d", i+kProfileDimensionCountIndexOffset];
            NSArray *dimensionFacets = [self.constantMetaArray filteredArrayUsingPredicate:predicate];
            [_dimensions addObject:dimensionFacets];
        }
    }
    return _dimensions;
}

- (void)getUserFacetsFromData:(NSDictionary *)metaDataDictionary
{
    [self adjustCompleteProfileButton];
    self.dimensions = nil;
    self.constantMetaArray = [OXNewFacetDataModel facetsArrayWithFacetsArray:[metaDataDictionary objectForKey:kProfileViewControllerFacetDetailsToList]];
    
    NSMutableArray *objectsToRemove = [[NSMutableArray alloc] init];
    
    for(OXFacetModel *facet in self.user.facetScore)
    {
        for(OXNewFacetDataModel *facetNew in self.constantMetaArray)
        {
            if([facet.facetID isEqualToNumber:facetNew.facetID])
            {
                facetNew.facetScore = facet.facetScore;
                facet.facetDescription = facetNew.facetDescription;
                facet.facetText = facetNew.facetText;
                [objectsToRemove addObject:facetNew];
            }
        }
    }
    
    NSMutableArray *incompleteFacetsArray = [[NSMutableArray alloc] initWithArray:self.constantMetaArray];
    [incompleteFacetsArray removeObjectsInArray:objectsToRemove];
    [self.tableView reloadData];
    
    if(self.currentFacet)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self scrollTableViewToFacet];
        });
    }
    
}

-(NSString*) titleForSection:(NSInteger)section
{
    NSLog(@"NAME OF DIMENSION: %@", [PKQuickMethods nameOfDimension:section]);
    return  [PKQuickMethods nameOfDimension:section];
}

-(CGFloat) labelHeightForText:(NSString*) text
{
    UILabel * label = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 0)];
    label.font =  [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    label.text = text;
    [label sizeToFit];
    return label.frame.size.height;
}

-(void)setGenericArray:(NSArray*)genericArray forGenericPresentationTableViewController:(GenericPresentationTableViewController*)genericPTBVC
{
    genericPTBVC.genericArray = genericArray;
}

- (void)displayActivationProfileView
{
    //This check is no longer required since the user now is presented the activate
    //profile text on top of the activate button in a uilabel instead.
    
//    if (!self.user.profileActivated)
//    {
//        UIAlertView *confirmActivationProfileAlertView = [[UIAlertView alloc]initWithTitle:kProfileActivationConfirmationTitle message:kProfileActivationConfirmationMessage delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
//        [confirmActivationProfileAlertView show];
//    }
//    else if(self.user.isProfileInComplete){
        [self processActivateProfile];
        
//    }
}

- (void)processActivateProfile
{
    if (!self.user.profileActivated) {
        [self activateProfile];
    }
    else if(self.user.isProfileInComplete){
        self.missingFacets = [NSMutableArray array];
        
        for(OXNewFacetDataModel * constantFacet in self.constantMetaArray){
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.facetID == %@", constantFacet.facetID];
            NSArray *completedFacets = [self.user.facetScore filteredArrayUsingPredicate:predicate];
            
            if([completedFacets count] == 0){
                NSLog(@"Questions: %@", constantFacet.questions);
                NSLog(@"facet Id: %@", constantFacet.facetID);
                [self.missingFacets addObject:constantFacet];
            }
        }
        [self performSegueWithIdentifier:kSegueIdentifierQuestionnaireViewController sender:self];
    }
}

-(void) activateProfile{
    [OXRestAPICient activateUserProfileWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         NSLog(@"%@",response);
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             self.user.profileActivated = YES;
             [self adjustCompleteProfileButton];
             [self.tableView reloadData];
         });
         
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             UIAlertView *profileActivitationFailure = [[UIAlertView alloc]initWithTitle:kProfileActivationFailureTitle  message:kProfileActivationFailureMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
             [profileActivitationFailure show];
             
         });
     }];
}

- (void)adjustCompleteProfileButton
{
    if (!self.user)
    {
        [self adjustTableViewWithNoActivationButton];
        return;
    }
    
    else if (!self.user.profileActivated)
    {
        
        self.activateProfileButtonView.hidden = NO;
        [self.activateProfileButton setTitle:kProfileActivateProfileButtonTitle forState: UIControlStateNormal];
    }
    
    else if (self.user.isProfileInComplete)
    {
        self.activateProfileButtonView.hidden = NO;
        NSString *test = [NSString stringWithFormat:@"Complete your profile (%.f%%)", (self.user.totalFacetCompletion) * 100];
        [self.activateProfileButton setTitle:test forState:UIControlStateNormal];
    }
    
    else
    {
        self.activateProfileButtonView.hidden = YES;
        self.activateProfileButton.hidden = YES;
        [self adjustTableViewWithNoActivationButton];
    }
    if(self.user.profileActivated){
        [self.completeProfileLabel setText:@""] ;
//        [self.completeProfileLabel setNeedsUpdateConstraints];

//        [self.completeProfileLabel layoutIfNeeded];
        self.activateProfileLabelTextHeightConstraint.constant = 0;
        self.activateProfileViewHeightBottomConstraint.constant = 10;
        [self.completeProfileLabel setNeedsLayout];
    }
    else{
        [self.completeProfileLabel setText:@"This will activate your personality profile, with a set of questions you can complete whenever you want to. We use it to improve your recommendations."] ;
        [self.completeProfileLabel setNeedsLayout];

    }
}

- (void)adjustTableViewWithNoActivationButton{
    self.activateProfileViewHeightConstraint.constant = 0;
    [self.activateProfileButtonView layoutIfNeeded];
}

- (void)refreshProfile{
    self.loadingAPICall = YES;
    if (!self.metaData){
        [SVProgressHUD showWithStatus:@"Loading Profile Data" maskType:SVProgressHUDMaskTypeNone];
    }
    [OXRestAPICient getUserProfileWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         self.user =  [[OXUserModel alloc] initWithDictionary:response];
         [self getFacetsQuestions];
         
     } failBlock:^(AFHTTPRequestOperation *operation, id error){
         self.loadingAPICall = NO;
     }];
}

- (void)getFacetsQuestions{
    if (self.metaData){
        [self getUserFacetsFromData:self.metaData];
        [SVProgressHUD dismiss];
        [self.refreshControl endRefreshing];
    }
    else{
//        [self.refreshControl beginRefreshing];
//        [self.tableView setContentOffset:CGPointMake(0,-self.refreshControl.frame.size.height) animated:YES];
        self.loadingAPICall = YES;
        [OXRestAPICient getFacetsQuestionsWithHashCode:@"" successBlock:^(AFHTTPRequestOperation *operation, id response){
            self.metaData = response;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.activateProfileButtonView.hidden = NO;
                [SVProgressHUD dismiss];
                self.loadingAPICall = NO;
                [self.refreshControl endRefreshing];
            });
            [self getUserFacetsFromData:self.metaData];
        }
         failBlock:^(AFHTTPRequestOperation *operation, id error){
             dispatch_async(dispatch_get_main_queue(), ^{
                 [SVProgressHUD dismiss];
                 self.loadingAPICall = NO;
                 [self.refreshControl endRefreshing];
             });
             NSLog(@"%@", error);
         }];
    }
}

- (void)setNavigationBar
{
    self.navigationController.navigationBar.topItem.title = kProfileViewControllerNavigationTitle;
    self.parentViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"Setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] style:UIBarButtonItemStylePlain target:self action:@selector(settingsBarButtonTapped:)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);
    self.parentViewController.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
}

//- (void)performSearchByMap
//{
//    UIStoryboard * SearchByMapStoryboard = [UIStoryboard storyboardWithName:@"SearchByMapStoryboard" bundle:nil];
//    UIViewController *searchByMapViewController = [SearchByMapStoryboard instantiateInitialViewController];
//    [self.navigationController pushViewController:searchByMapViewController animated:YES];
//}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:kSegueIdentifierTagsViewController]){
        GenericPresentationTableViewController *genericPTBC = [segue destinationViewController];
        // TODO Send Tags from user
        //        genericPTBC.genericArray = self.user.tags;
        [self setGenericArray:[OXTagPlaceCountModel createRandomTagPlaceCountsArrayForTestingPurposesWithCount:67] forGenericPresentationTableViewController:genericPTBC];
        genericPTBC.title = @"My tags";
    }
    
    else if ([segue.identifier isEqualToString:kSegueIdentifierQuestionnaireViewController]) {
        
        UINavigationController * navigationController =  (UINavigationController*)[segue destinationViewController];
        OXQuestionViewController * destination = (OXQuestionViewController*) navigationController.viewControllers[0];
        destination.missingFacets = self.missingFacets;
        destination.currentFacetIndex = 1;
        destination.currentQuestionIndex = 1;
        self.currentFacet = [self.missingFacets firstObject];
    }
}

#pragma mark AlertView delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self processActivateProfile];
    }
}

#pragma mark - IBActions

- (IBAction)activateProfileButtonTapped:(id)sender
{
    [self displayActivationProfileView];
}

//- (IBAction)searchByMapButtonTapped:(id)sender {
//    [self performSearchByMap];
//}

- (void)settingsBarButtonTapped:(id)sender {
    // TODO : Handle Setting button Tap
    UIStoryboard * settingsScreenStoryBoard = [UIStoryboard storyboardWithName:@"SettingsScreenSB" bundle:nil ];
    UIViewController *settingsScreen = [settingsScreenStoryBoard instantiateInitialViewController];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    transition.subtype = kCATransitionFromLeft; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:settingsScreen animated:NO];
}
-(void)showFriendsScreen{
    NSLog(@"show friends screen method called");
    [self performSegueWithIdentifier:kSegueIdentifierFriendsViewController sender:self];
}
@end
