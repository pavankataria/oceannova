//
//  OXWebViewViewController.m
//  OceanX
//
//  Created by Systango on 2/12/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXWebViewViewController.h"

@interface OXWebViewViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation OXWebViewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
     [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.URL]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBaction Method

- (IBAction)cancelButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
