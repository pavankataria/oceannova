//
//  OXFacetTableViewCell.h
//  OceanX
//
//  Created by Systango on 2/5/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXFacetTableViewCell : UITableViewCell

-(void) setupWithFacet:(OXNewFacetDataModel*) facet;
-(void)setPercentScore:(double)facetScore;

@end
