//
//  OXSearchContainerScreenViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 01/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXSearchBarView.h"


@interface OXSearchContainerScreenViewController : UIViewController<OXSearchBarDelegate, OXSearchBarDataSource, OXSearchContainerScreenViewControllerOperationProtocol, OXSearchContainerScreenViewControllerDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceTopLayoutGuide;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarHeightConstraint;
@property (nonatomic, weak) IBOutlet OXSearchBarView *searchBarView;


@property (nonatomic, retain) NSMutableArray *sharedSearchResultsArray;

@end
