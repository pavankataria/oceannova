//
//  OXDetailPOIViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKNewEditMediaContentViewProtocols.h"
#import "OXDetailPOIProtocol.h"
#import "RDActivityViewControllerDelegate.h"
#import "OXLikeDislikeBarUITableViewCell.h"

@interface OXDetailPOIViewController : UIViewController<PKLikeDislikeShareBarViewDelegate, OXDetailPOIProtocol, RDActivityViewControllerDelegate, PKThumbnailSliderViewDelegate>
@property (nonatomic, retain) IBOutlet UIScrollView *detailPageScrollView;
//@property (nonatomic, retain) OXPOISearchByListModel *poiSearchObject;
@property (nonatomic, assign) NSInteger poiID;

@property (nonatomic, retain) OXPOIModel *fullPOIObject;
@property (nonatomic, retain) OXLikeDislikeBarUITableViewCell *parentLikeDislikeCell;
@property (nonatomic, retain) OXPOISearchByListModel *parentPOIObject;

@end
