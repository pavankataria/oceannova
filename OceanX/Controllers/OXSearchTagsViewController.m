//
//  OXSearchTagsViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSearchTagsViewController.h"
#import "OXSearchTagTableViewCell.h"
@interface OXSearchTagsViewController ()

@end

@implementation OXSearchTagsViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.addTagTextField becomeFirstResponder];
    self.storedTagsArray = self.poiSearchObject.tags;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tagsTableView registerNib:[UINib nibWithNibName:@"OXSearchTagTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellTagIdentifier];
    
      self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(cancelActionMethod)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark  - UITableView DataSource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
        return self.searchTagsResultsArray.count;
    }
    else if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
        static NSUInteger kAddTagGuidanceDisplayCellOffset = 1;
        return self.storedTagsArray.count + kAddTagGuidanceDisplayCellOffset;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OXSearchTagTableViewCell *cell = (OXSearchTagTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewCellTagIdentifier];
    cell.tagNameLabel.textColor = [UIColor blackColor];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.userInteractionEnabled = YES;
    [cell.tagDeleteButton addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
        OXTagPlaceCountModel *tag = self.searchTagsResultsArray[indexPath.row];
        [cell setCellWithObjectFollowingProtocol:tag];
    }
//        NSLog(@"reached here table view search");
//        if([self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
//            
//            cell.tagNameLabel.text = [NSString stringWithFormat:@"Already created: %@", [self addTagTextWithExcessCharactersRemoved]];
//            cell.tagNameLabel.textColor = [UIColor lightGrayColor];
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            cell.userInteractionEnabled = NO;
//            return cell;
//        }
//        if(self.searchTagsResultsArray.count == 0){
//            if(![self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
//                cell.tagNameLabel.text = [NSString stringWithFormat:@"Create tag: %@", [self addTagTextWithExcessCharactersRemoved]];
//                cell.tagPlacesCountLabel.text = @"";
//            }
//        }
//        else{
//            cell.tagNameLabel.text = [self.searchTagsResultsArray[indexPath.row] tagName];
//            cell.tagPlacesCountLabel.text = [self.searchTagsResultsArray[indexPath.row] totalPlacesCountString];
//        }
//    }*/
    else if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
        if(indexPath.row == self.storedTagsArray.count){
            [cell setSearchTableCellForUIMessageCellForIndexPath:indexPath];
        }
        else{
            OXTagPlaceCountModel *tag = self.storedTagsArray[indexPath.row];
            cell.tagNameLabel.text = tag.tagName;
            cell.tagPlacesCountLabel.text = tag.totalPlacesCountString;
            [cell.tagDeleteButton setHidden:NO];
        }
    }
    return cell;
}

-(void)updateTableView{
//    [self adjustSearchResultsArray];
    if([self.addTagTextField.text length] > 0){
        self.currentTableViewShowing = kAddTagViewControllerShowTableViewSearch;
    }
    else{
        self.currentTableViewShowing = kAddTagViewControllerShowTableViewNormal;
    }
    [self.tagsTableView reloadData];
}
-(void)adjustSearchResultsArray{
    NSString *needleString = [self addTagTextWithExcessCharactersRemoved];
    for(int i = 0; i < self.searchTagsResultsArray.count; i++){
        id <OXTagModelOperationProtocol>tag = self.searchTagsResultsArray[i];
        //check if a tag exists for the text typed. if so then theres no need to have a
        //`create a tag` cell
        if([tag.OXTagModelOperationTagNameString isEqual:needleString]){
            return;
        }
    }
    [self insertCreateNewTagName:needleString andPlaceInPosition:0];
}
-(void)insertCreateNewTagName:(NSString*)needle andPlaceInPosition:(NSUInteger)position{
    OXTagPlaceCountModel *tag = [[OXTagPlaceCountModel alloc] initWithTagName:needle andTotalPlacesCount:0 andCreateNewTag:YES];
    [self.searchTagsResultsArray insertObject:tag atIndex:0];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kOXAddTagsTableViewRowHeight;
}
#pragma mark end -

#pragma mark - UITableView Delegate - Methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
        [self.storedTagsArray addObject:self.searchTagsResultsArray[indexPath.row]];
        [self.searchTagsResultsArray removeAllObjects];
    }
    self.addTagTextField.text = @"";
    [self updateTableView];
}

- (void)checkButtonTapped:(id)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tagsTableView];
    NSIndexPath *indexPath = [self.tagsTableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil){
        [self.storedTagsArray removeObjectAtIndex:indexPath.row];
        [self updateTableView];
    }
}



-(IBAction)doneActionMethod:(id)sender{
    if(self.storedTagsArray.count > 0){
        [self.poiSearchObject setTags:self.storedTagsArray];
    }
    [self.delegate refreshTableView];
 [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)cancelActionMethod{
    [self.delegate refreshTableView];
 [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:NO];
}
#pragma mark - OXPOISearchDelegate - Methods
-(void)setDelegate:(id<OXSearchDelegate>)delegate{
    _delegate = delegate;
}
-(void)setSearchPOIModel:(OXPOISearchModel *)object{
    _poiSearchObject = object;
}


@end
