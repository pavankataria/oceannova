//
//  OXLoveThePlacesViewController.m
//  OceanX
//
//  Created by Systango on 2/16/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXLoveThePlacesViewController.h"

@interface OXLoveThePlacesViewController ()
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *dislikeButton;
@property (strong, nonatomic) IBOutlet UIButton *maybeButton;


@end

@implementation OXLoveThePlacesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Method

- (IBAction)likeButtonTapped:(UIButton *)sender
{
    sender.selected = !sender.selected;
    self.dislikeButton.selected = NO;
    self.maybeButton.selected = NO;
}

- (IBAction)dislikeButtonTapped:(UIButton *)sender{
    sender.selected = !sender.selected;
    self.likeButton.selected = NO;
    self.maybeButton.selected = NO;
    
}
- (IBAction)maybeButtonTapped:(UIButton *)sender{
    sender.selected = !sender.selected;
    self.likeButton.selected = NO;
    self.dislikeButton.selected = NO;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
