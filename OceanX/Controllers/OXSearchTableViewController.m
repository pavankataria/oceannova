//
//  OXSearchTableViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 28/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSearchTableViewController.h"
#import "OXPlaceSectionHeaderTableViewCell.h"
#import "OXFilterScreenViewController.h"
@interface OXSearchTableViewController (){
}

@property (nonatomic, assign) CGFloat cellImageHeight;
@property (nonatomic, retain) OXFilterScreenViewController *searchFilterScreenViewController;

@end

@implementation OXSearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setupXIB];
}
-(void)setupXIB{
    [self setupTable];
    [self setupSearchBarView];
}
-(void)setupSearchBarView{
    self.searchBarView.delegate = self;
    [self.searchBarView setStoryboardName:@"SearchFilterScreenSB"];
//    UIStoryboard *main = [UIStoryboard storyboardWithName:@"SearchFilterScreenSB" bundle:nil];
//    self.searchFilterScreenViewController = [main instantiateInitialViewController];
}
#pragma mark - OXSearchBarDelegate methods
-(void)searchBarView:(OXSearchBarView *)searchBarView searchBarDidBeginEditing:(UITextField *)textField{
    NSLog(@"search bar text: %@", textField.text);

    
//    CATransition* transition = [CATransition animation];
//    
//    transition.duration = 0.3;
//    transition.type = kCATransitionFade;
//    
//    [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
//    [self.navigationController presentViewController:self.searchFilterScreenViewController animated:YES completion:nil];

    [self performSegueWithIdentifier:@"filterScreenSegueIdentifier" sender:self];
    
}

#pragma mark end -
-(void)setupTable{
    self.cellImageHeight = CGRectGetWidth(self.tableView.frame);
    NSLog(@"cell image height %f", self.cellImageHeight);
//    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 10;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kOXSearchTableViewControllerTableViewCellIdentifierImageCell forIndexPath:indexPath];
        // Configure the cell...
        return cell;
    }
    else {
        static NSString *emptyCellIdentifier = @"lol";
        UITableViewCell *emptyCell = [tableView dequeueReusableCellWithIdentifier:emptyCellIdentifier];
        if (emptyCell == nil) {
            emptyCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:emptyCellIdentifier];
        }
        return emptyCell;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return self.cellImageHeight;
    }
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString *CellIdentifier = @"SectionHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    [headerView.contentView setBackgroundColor:[UIColor colorWithWhite:1 alpha:kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha]];
    
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return kOXSearchTableViewControllerTableViewSectionHeaderHeight;
}

//-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ScrollView methods



@end
