//
//  OXReportTableViewController.m
//  OceanX
//
//  Created by Systango on 1/28/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

NSInteger const kotherReportTypeCellIndex = 2;

#import "OXReportTableViewController.h"
#import "OXReportDataModel.h"
#import "OXReportTextViewController.h"

@interface OXReportTableViewController () <ReportMessageDelegate>

@property (nonatomic, weak) IBOutlet UITableViewCell* spamReportTypeCell;
@property (nonatomic, weak) IBOutlet UITableViewCell* inappropriateReportTypeCell;
@property (nonatomic, weak) IBOutlet UITableViewCell* otherReportTypeCell;

@end

@implementation OXReportTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.spamReportTypeCell.textLabel.text = @"Feels like SPAM";
    self.inappropriateReportTypeCell.textLabel.text = @"Inappropriate or offensive";
    self.otherReportTypeCell.textLabel.text = @"Other...";
    [Flurry logEvent:kFlurryReportPlace];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
    if (indexPath.row != kotherReportTypeCellIndex) {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView cellForRowAtIndexPath:indexPath].textLabel.highlightedTextColor = [PKUIAppearanceController tealColor];
    } else {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}

#pragma mark - IBAction methods

- (IBAction)reportButtontapped:(id)sender {
    [self reportIssue];
}
- (IBAction)cancelButtonTapped:(id)sender {
    [self closeReportScreen];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kSegueIdentifierReportTextViewController])
    {
        OXReportTextViewController * destination = segue.destinationViewController;
        destination.delegate = self;
        destination.reportData = self.reportData;
    }
}

#pragma mark - Private methods

- (void)reportIssue {
    if ([self.spamReportTypeCell isSelected]) {
        self.reportData.reportType = ReportScreenTypeSpam;
    } else if ([self.inappropriateReportTypeCell isSelected]) {
        self.reportData.reportType = ReportScreenTypeInnapropriate;
    }
    [self reportIssueWithObject:self.reportData];
}

- (void)reportIssueWithObject:(OXReportDataModel *)reportData {
    [SVProgressHUD showWithStatus:@"Reporting" maskType:SVProgressHUDMaskTypeBlack];
    [OXRestAPICient reportIssueWithReportData:reportData
                                successBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         [SVProgressHUD showSuccessWithStatus:@"Reported" maskType:SVProgressHUDMaskTypeBlack];
         [self closeReportScreen];
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
         [SVProgressHUD showErrorWithStatus:@"Failed to report" maskType:SVProgressHUDMaskTypeBlack];
         NSLog(@"%@", error);
     }];
}

- (void)closeReportScreen {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Delegate methods

-(void)didEnteredMessageForReport:(OXReportDataModel *)reportData {
    self.reportData.reportType = ReportScreenTypeTextReport;
    [self reportIssueWithObject:reportData];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
