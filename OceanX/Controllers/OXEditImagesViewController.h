//
//  OXEditImagesViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 28/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKPreviewMediaViewController.h"
#import "OXImageStackItem.h"
@class OXImageStackItem;

@interface OXEditImagesViewController : PKPreviewMediaViewController

@property (nonatomic, retain) NSMutableArray/*<OXImagesStackItemProtocol>*/  *capturedImages ;
@end
