//
//  OXAddTagsBaseViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXAddTagsBaseViewController.h"


@interface OXAddTagsBaseViewController ()
@end


@implementation OXAddTagsBaseViewController
#pragma mark - Lazy Loading callers
-(NSMutableArray *)searchResultsArray{
    if( ! _searchTagsResultsArray){
        _searchTagsResultsArray = [[NSMutableArray alloc] init];
    }
    return _searchTagsResultsArray;
}

-(NSMutableArray *)storedTagsArray{
    if( ! _storedTagsArray){
        _storedTagsArray = [[NSMutableArray alloc] init];
    }
    return _storedTagsArray;
}
#pragma mark end -
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupAddTagTextField];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupAddTagTextField{
    self.addTagBackgroundView.backgroundColor = [PKUIAppearanceController darkBlueColor];
    [self.addTagTextField setTextColor:[UIColor whiteColor]];
    UIColor *color = [UIColor whiteColor];
    self.addTagTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kOXAddTagsTextFieldPlaceHolderText attributes:@{NSForegroundColorAttributeName:color}];
    self.addTagTextField.delegate = self;
    [self.addTagTextField addTarget:self action:@selector(addTagTextFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
}


-(void)addTagTextFieldTextChanged:(id)sender{
    if([self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
//        [self.searchTagsResultsArray removeAllObjects];
//        [self updateTableView];
//        return;
    }
    [OXRestAPICient getTagsAutoSuggestionWithString:[self addTagTextWithExcessCharactersRemoved]
                                   withSuccessBlock:^(AFHTTPRequestOperation *operation, NSArray *response) {
//                                       NSLog(@"success operation: %@\n\nresponse: %@", operation, response);
                                       
                                       [self handleAutoTagsArrayResponse:response];
                                       [self updateTableView];
                                   }
                                          failBlock:^(AFHTTPRequestOperation *operation, id response) {
                                              NSString *responseBody = operation.responseString;
                                              NSLog(@"failure operation: %@\n\n\n\nresponse: %@ \n\n\nresponseString %@", operation, response, responseBody);
                                              [self.searchTagsResultsArray removeAllObjects];
                                              [self updateTableView];
                                          } ];
}
-(NSString *)addTagTextWithExcessCharactersRemoved{
    
    NSString *searchString = self.addTagTextField.text;
    NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
    NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
    
    NSArray *parts = [searchString componentsSeparatedByCharactersInSet:whitespaces];
    NSArray *filteredArray = [parts filteredArrayUsingPredicate:noEmptyStrings];
    searchString = [filteredArray componentsJoinedByString:@" "];
    
//    NSLog(@".%@.", searchString);
    
    return searchString;
}

-(void)handleAutoTagsArrayResponse:(NSArray*)response{
    self.searchTagsResultsArray = [OXTagPlaceCountModel createAutoSuggestionArrayFromResponse:response filteringOutStoredTags:self.storedTagsArray];
    NSLog(@"self.searchTagsResultsArray: %@", self.searchTagsResultsArray);
    
    
//    OXTagPlaceCountModel *createNewTag = [[OXTagPlaceCountModel alloc] initWithTagName:[NSString stringWithFormat:@"%@", self.searchTagsTextField.text] andTotalPlacesCount:0 andCreateNewTag:YES];
//    [self.searchTagsArray insertObject:createNewTag atIndex:0];

}

-(BOOL)doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:(NSString*)needleTagString{
    for (OXTagPlaceCountModel *storedTag in self.storedTagsArray) {
        NSLog(@"comparing storedTag: %@ with needle: %@", storedTag, [needleTagString lowercaseString]);
        if([[storedTag.tagName lowercaseString] isEqualToString:[needleTagString lowercaseString]]){
            return YES;
        }
    }
    return NO;
}

-(void)updateTableView{
    NSLog(@"update table view called parent class");
    if([self.addTagTextField.text length] > 0){
        self.currentTableViewShowing = kAddTagViewControllerShowTableViewSearch;
    }
    else{
        self.currentTableViewShowing = kAddTagViewControllerShowTableViewNormal;
    }
    [self.tagsTableView reloadData];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
