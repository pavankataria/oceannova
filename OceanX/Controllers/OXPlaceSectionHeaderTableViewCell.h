//
//  OXPlaceSectionHeaderTableViewCell.h
//  OceanX
//
//  Created by Pavan Kataria on 30/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXPlaceSectionHeaderTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *poiNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *poiAddedByNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *poiDistanceLabel;

//-(void)setWithPlaceDictionaryTemporaryMethod:(NSDictionary*)dictionary;
-(void)setCellWithPlaceConformingObject:(id<OXSearchPOIForSectionViewOperationProtocol>)object;

@end
