//
//  OXGenderTableViewCell.m
//  OceanX
//
//  Created by Systango on 2/3/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXGenderTableViewCell.h"

@interface OXGenderTableViewCell()

@property (weak, nonatomic) IBOutlet UIButton *maleButton;
@property (weak, nonatomic) IBOutlet UIButton *femaleButton;

@end

@implementation OXGenderTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.selectedGender = male;
    [self.maleButton.layer setCornerRadius:10];
   [self.maleButton.layer setMasksToBounds:YES];
    
    [self.femaleButton.layer setCornerRadius:10];
    [self.femaleButton.layer setMasksToBounds:YES];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - IBActions

- (IBAction)maleButtonTapped:(id)sender
{
    self.selectedGender = (sender == self.maleButton) ? male : female;
    NSLog(@"selected gender: %lu", (unsigned long)self.selectedGender);
    self.maleButton.selected = (sender == self.maleButton);
    self.femaleButton.selected = !self.maleButton.isSelected;
}

-(void)setSelectedGender:(Gender)selectedGender{
    _selectedGender = selectedGender;
    NSLog(@"selected gender being set: %lu", (unsigned long)selectedGender);
}

@end
