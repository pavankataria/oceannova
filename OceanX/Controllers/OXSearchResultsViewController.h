//
//  OXSearchResultsViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 01/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXSearchResultsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) UIViewController <OXSearchContainerScreenViewControllerOperationProtocol> *parentVC;

@end
