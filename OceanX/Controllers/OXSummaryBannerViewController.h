//
//  OXSummaryBannerViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 08/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OXSummaryBannerViewController <NSObject>

@required
-(void)buttonOnePressed;
-(void)buttonTwoPressed;
-(void)presentingAnyData:(BOOL)isPresentingAnything;

@end
@interface OXSummaryBannerViewController : UIViewController


@property (nonatomic, weak) IBOutlet UIButton *firstButton;
@property (nonatomic, weak) IBOutlet UIButton *secondButton;
@property (nonatomic, weak) IBOutlet UILabel *firstLabel;

@property (nonatomic, weak) IBOutlet UIView *circularIconView;


@property (nonatomic, retain) OXPOISearchModel *poiSearchModel;
@property (nonatomic, weak) id <OXSummaryBannerViewController> delegate;

//"\u2B24"
@end
