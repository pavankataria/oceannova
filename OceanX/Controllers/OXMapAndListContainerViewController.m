//
//  OXMapAndListContainerViewController.m
//  EmbeddedSwapping
//
//  Created by Michael Luton on 11/13/12.
//  Copyright (c) 2012 Sandmoose Software. All rights reserved.
//  Heavily inspired by http://orderoo.wordpress.com/2012/02/23/container-view-controllers-in-the-storyboard/
//

#import "OXMapAndListContainerViewController.h"
#import "EmptySegue.h"

#define SegueIdentifierMap @"embedMap"
#define SegueIdentifierList @"embedList"


@interface OXMapAndListContainerViewController ()

@property (strong, nonatomic) NSString *currentSegueIdentifier;
@property (assign, nonatomic) BOOL transitionInProgress;
@property (nonatomic, retain) UIViewController <OXSearchContainerScreenViewControllerOperationProtocol, OXSearchContainerScreenViewControllerDelegate>*parentVC;
@property (strong, nonatomic) EmptySegue *mapSegue;
@property (strong, nonatomic) OXPOISearchModel *object;



@end

@implementation OXMapAndListContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.transitionInProgress = NO;
    self.currentSegueIdentifier = SegueIdentifierList;
    
    UIStoryboard * SearchByMapStoryboard = [UIStoryboard storyboardWithName:@"SearchByMapStoryboard" bundle:nil];
    self.mapViewController = [SearchByMapStoryboard instantiateInitialViewController];
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Instead of creating new VCs on each seque we want to hang on to existing
    // instances if we have it. Remove the second condition of the following
    // two if statements to get new VC instances instead.
    if ([segue.identifier isEqualToString:SegueIdentifierMap]) {
        //        self.mapViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:SegueIdentifierList]) {
        self.listViewController = segue.destinationViewController;
        
    }
    
    // If we're going to the first view controller.
    if ([segue.identifier isEqualToString:SegueIdentifierList]) {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0) {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.listViewController];
            
            [self.listViewController performSerchWithObject:self.object];
            
            
        }
        else {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:self.listViewController];
            UIView* destView = self.listViewController.view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    // By definition the second view controller will always be swapped with the
    // first one.
    else if ([segue.identifier isEqualToString:SegueIdentifierMap]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.mapViewController];
        
        [self.mapViewController performSerchWithObject:self.object];
        
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    
    [self retrieveSearchObject];
    
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    [self transitionFromViewController:fromViewController
                      toViewController:toViewController duration:0
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil completion:^(BOOL finished) {
                                [fromViewController removeFromParentViewController];
                                [toViewController didMoveToParentViewController:self];
                                self.transitionInProgress = NO;
                            }];
}

- (void)swapViewControllers
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if (self.transitionInProgress) {
        return;
    }
    
    self.transitionInProgress = YES;
    self.currentSegueIdentifier = ([self.currentSegueIdentifier isEqualToString:SegueIdentifierMap]) ? SegueIdentifierList : SegueIdentifierMap;
    
    if (([self.currentSegueIdentifier isEqualToString:SegueIdentifierMap]) && self.mapViewController) {
        [self swapFromViewController:self.listViewController toViewController:self.mapViewController];
        return;
    }
    
    if (([self.currentSegueIdentifier isEqualToString:SegueIdentifierList]) && self.listViewController) {
        [self swapFromViewController:self.mapViewController toViewController:self.listViewController];
        return;
    }
    
    
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}


-(void)retrieveSearchObject
{
    
    if (!self.object) {
        
        self.object = self.listViewController.poiSearchObject;
    }
    
}

- (void)swapToMapViewController{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if (self.transitionInProgress) {
        return;
    }
    self.transitionInProgress = YES;
    
    
    
    self.currentSegueIdentifier = SegueIdentifierMap;
    if(!self.mapSegue){
        self.mapSegue = [[EmptySegue alloc] initWithIdentifier:self.currentSegueIdentifier source:self destination:self.listViewController];
    }
    [self prepareForSegue:self.mapSegue sender:nil];
}


- (void)swapToListViewController{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if (self.transitionInProgress) {
        return;
    }
    
    
    self.currentSegueIdentifier = SegueIdentifierList;
    self.transitionInProgress = YES;
    [self performSegueWithIdentifier:SegueIdentifierList sender:nil];
    
}

-(void)performSerchWithObject:(id)object {
    
    NSLog(@"perform search in container with object params: %@", [object params]);
    
    self.object = object;
    
    if ([self.currentSegueIdentifier isEqualToString:SegueIdentifierMap]) {
        
        [self.mapViewController performSerchWithObject:self.object];
        
    } else {
        
        [self.listViewController performSerchWithObject:self.object];
    }
}

-(void)setParentViewControllerForChildViewControllers:(UIViewController<OXSearchContainerScreenViewControllerOperationProtocol, OXSearchContainerScreenViewControllerDelegate>*)parentViewControllerObject{
    NSLog(@"setting parent view controller now");
    self.parentVC = parentViewControllerObject;
}
@end
