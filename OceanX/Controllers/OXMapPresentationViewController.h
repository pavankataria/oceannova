//
//  OXMapPresentationViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 24/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXDetailPOIProtocol.h"

@interface OXMapPresentationViewController : UIViewController<MKMapViewDelegate>
@property (nonatomic, retain) MKMapView *mapView;
@property (nonatomic, retain) OXPOIModel *poiObject;
@property (nonatomic, weak) id <OXDetailPOIProtocol> delegate;

-(void)enableMapViewInteraction:(BOOL)enable;
-(void)showMapViewLabel:(BOOL)shouldShow;
-(void)mapDidExpand;
-(void)mapDidContract;
-(CGFloat)heightOfMapView;
-(CGRect)frameOfOriginalMapFrame;
@end
