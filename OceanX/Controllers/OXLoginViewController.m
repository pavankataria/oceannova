//
//  OXLoginViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 22/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXLoginViewController.h"
#import "AppDelegate.h"

static const CGFloat topLayoutWithKeyboard = 66;
static const CGFloat topLayoutWithoutKeyboard = 160;
static const CGFloat minimumPasswordLength = 1;

@interface OXLoginViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UILabel *loginErrorLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginbackgroundViewTopLayoutConstraint;

@end

@implementation OXLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    if ([OXLoginManager isLoggedIn]) {
        
//        [[OCNMetadata sharedInstance] refresh];
//        [self performSegueWithIdentifier:@"LoginSuccessful" sender:nil];
        NSLog(@"LOGIN SUCCESSFUL");
        
//        [OXHTTPClient getMetaDataWithCompletionBlock]
        [[OXCookieManager sharedManager] assignedSavedSessionCookies];

        [self showMainScreen];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Register notification when keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
//    [self.navigationController setNavigationBarHidden:NO animated:NO];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)closeButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)attemptLogin:(id)sender{
     self.loginErrorLabel.hidden = YES;
    NSLog(@"attempt login method called");
    [[OXLoginManager sharedManager] performLoginWithUserName:self.usernameTextfield.text andPassword:self.passwordTextfield.text successBlock:^(AFHTTPRequestOperation *operation, id response) {
        
        [[AppDelegate getAppDelegateReference] showMainScreen];
        
        [Flurry logEvent:kFlurryManualLogin];        
        
    }
                                                   failBlock:^(AFHTTPRequestOperation *operation, id error) {
        if ([error isEqualToString:@""]) {
            NSLog(@"Sorry, we are experiencing\ntechnical difficulties. Here’s a cat to cheer up\ncat   --->         (^_^)__*");
        }else{
//<<<<<<< HEAD
            self.loginErrorLabel.hidden = NO;
            self.loginErrorLabel.text = error;
//            NSLog(@"Problem : %@", errorMessage);
//=======
            NSLog(@"Problem : %@", error);
            NSLog(@"operation: %@", operation.responseString);
//>>>>>>> a lot more commits
            
        }
    }];
}


-(void)showMainScreen{
    [[AppDelegate getAppDelegateReference] showMainScreen];
}

#pragma mark - Textfield Delegate method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.usernameTextfield) {
        [self.usernameTextfield resignFirstResponder];
        [self.passwordTextfield becomeFirstResponder];
    }
    else if (textField == self.passwordTextfield) {
        [self performLogin];
    }
    
    
    return YES;
}
#pragma mark KeyBoard Show/Hide Delegate

- (void)keyboardWillShow:(NSNotification *)note
{
    self.loginbackgroundViewTopLayoutConstraint.constant +=topLayoutWithKeyboard ;
    
}
- (void)keyboardWillHide:(NSNotification *)note
{
    self.loginbackgroundViewTopLayoutConstraint.constant = 16;
}

#pragma mark - Private method

- (BOOL)validate
{
    return ([self.usernameTextfield.text length] && [self.passwordTextfield.text length] > minimumPasswordLength);
}

- (void)performLogin {
    [self.passwordTextfield resignFirstResponder];
    // execute any additional code
    if ([self validate]) {
        [self attemptLogin:nil];
    }
}

#pragma mark - IBAction method
- (IBAction)loginButtonTapped:(id)sender {
    [self performLogin];
}

@end
