//
//  OXMapPresentationViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 24/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXMapPresentationViewController.h"
#import "OXPKAnnotationView.h"
#import "OXFlurryProtocol.h"
@interface OXMapPresentationViewController ()<OXFlurryProtocol>{
    UILabel *mainAddressLabel;
}


@property (nonatomic, retain) UILabel *addressLabel;
@property (nonatomic, assign) CGFloat labelPadding;
@property (nonatomic, assign) CGFloat labelHeight;
@property (nonatomic, strong) UIView *labelBackgroundView;
@property (nonatomic, assign) BOOL isExpanded;
@property (nonatomic, assign) CGRect originalMapViewFrame;


@property (nonatomic, retain) MKPointAnnotation *businessLocationAnnotationView;

@end

@implementation OXMapPresentationViewController
#pragma mark - Lazily load - Methods
-(MKPointAnnotation *)businessLocationAnnotationView{
    if(!_businessLocationAnnotationView){
        _businessLocationAnnotationView = [MKPointAnnotation new];
    }
    return _businessLocationAnnotationView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.labelBackgroundView = [[UIView alloc] init];
    [self.labelBackgroundView setUserInteractionEnabled:NO];
    self.labelPadding = 5;
    self.labelHeight = 30;
    
    
    self.mapView = [[MKMapView alloc] initWithFrame:self.view.frame];
    
    CGRect mapRect = self.mapView.frame = self.view.bounds;
    mapRect.size.height = 100;
    self.mapView.frame = mapRect;
    self.mapView.delegate = self;
    
    self.isExpanded = FALSE;
    
    [self.view addSubview:self.mapView];
    [self.mapView addSubview:self.labelBackgroundView];
    
    NSLog(@"mapView size: %@", NSStringFromCGRect(self.view.frame));
    [self enableMapViewInteraction:NO];
}


-(void)setGradientViewGradientBlackSideUp:(BOOL)moveBlackSideUp;{
    CGFloat viewHeight = (self.addressLabel.frame.size.height + self.labelPadding) * 2;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    if (self.isExpanded) {
        self.labelBackgroundView.frame = CGRectMake(0, self.addressLabel.frame.origin.y - self.labelPadding, self.view.frame.size.width, viewHeight);
        
    }
    else{
        self.labelBackgroundView.frame = CGRectMake(0, self.view.frame.size.height - viewHeight, self.view.frame.size.width, viewHeight);
        
    }
    self.labelBackgroundView.layer.sublayers = nil;
    gradient.frame = self.labelBackgroundView.bounds;
    NSLog(@"bounds %f", self.labelBackgroundView.bounds.origin.y);
    UIColor *blackColour = [UIColor colorWithWhite:0.0 alpha:0.7];
    if (moveBlackSideUp) {
        gradient.colors = [NSArray arrayWithObjects: (id)[blackColour CGColor], (id)[[UIColor clearColor] CGColor], nil];
        gradient.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.5],[NSNumber numberWithFloat:1.0], nil];
    }else{
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[blackColour CGColor], nil];
        gradient.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],[NSNumber numberWithFloat:0.5], nil];
    }
//    [self.labelBackgroundView.layer insertSublayer:gradient atIndex:0];
}


-(void)mapDidExpand
{
    if (self.isExpanded == FALSE) {
        self.isExpanded = TRUE;
        [mainAddressLabel setHidden:YES];
        self.addressLabel.frame = CGRectMake(self.labelPadding, self.labelPadding, self.mapView.frame.size.width - 2*self.labelPadding, self.labelHeight);
        [self.addressLabel setHidden:NO];

        [self.addressLabel sizeToFit];
        //[self setGradientViewGradientBlackSideUp:TRUE];
        [self setBlueBarOnTop:TRUE];
        [self zoomMapViewToFitAnnotations:self.mapView animated:NO];

    }
}


-(void)mapDidContract{
    if (self.isExpanded == TRUE) {
        self.isExpanded = FALSE;
        [mainAddressLabel setHidden:NO];
        [self setupAddressLabel];
        [self setGradientViewGradientBlackSideUp:FALSE];
        [self setBlueBarOnTop:FALSE];
        [self.addressLabel setHidden:YES];

        [self.mapView showAnnotations:@[self.businessLocationAnnotationView] animated:NO];
    }
}


-(void)setBlueBarOnTop:(BOOL)isVisible{
    if(isVisible){
        CGFloat viewHeight = self.addressLabel.frame.size.height + self.labelPadding * 2;
        self.labelBackgroundView.layer.sublayers = nil;
        self.labelBackgroundView.backgroundColor = [PKUIAppearanceController darkBlueColor];
        self.labelBackgroundView.frame = CGRectMake(0, self.addressLabel.frame.origin.y - self.labelPadding, self.view.frame.size.width, viewHeight);
    }
    else{
        self.labelBackgroundView.backgroundColor = nil;
    }
}

-(CGFloat)heightOfMapView{
    return self.mapView.frame.size.height;
}
-(CGRect)frameOfOriginalMapFrame{
    if(CGRectIsEmpty(self.originalMapViewFrame)){
        return CGRectZero;
    }
    else{
        return self.originalMapViewFrame;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Other methods - Methods
-(void)setPoiObject:(OXPOIModel *)poiObject{
    _poiObject = poiObject;
    if(!mainAddressLabel){
        mainAddressLabel = [[UILabel alloc] init];
    }
    
    [mainAddressLabel setText:self.poiObject.poiAddress];
    mainAddressLabel.textColor = rgb(90, 90, 90);
    mainAddressLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
    [mainAddressLabel setTextAlignment:NSTextAlignmentLeft];
    [mainAddressLabel sizeToFit];
    
    CGRect rect = mainAddressLabel.frame;
    rect.origin.x = 20;
    rect.origin.y = 20;
    mainAddressLabel.frame = rect;
    
    NSLog(@"contactUsLabel.frame %@", NSStringFromCGRect(mainAddressLabel.frame));
    [self.view addSubview:mainAddressLabel];

    
    [self.view layoutIfNeeded];
    CGRect viewFrame = self.view.frame;
    self.mapView.frame = self.view.bounds;
    
    CGRect mapViewFrame = self.mapView.frame;
    mapViewFrame.size.width -= 20*2;
    mapViewFrame.origin.x = 20;

    
    if([self.poiObject.socialContact allKeys].count || self.poiObject.openingHours.count){
        mapViewFrame.origin.y = 50;
        viewFrame.size.height = CGRectGetMaxY(mapViewFrame);
    }
    else{
         viewFrame.size.height = kDetailPOIVCScrollViewViewControllerSpaceDivider + CGRectGetMaxY(mapViewFrame);
        mapViewFrame.origin.y = 50;
    }

    self.mapView.frame = mapViewFrame;
    self.view.frame = viewFrame;
    self.originalMapViewFrame = self.mapView.frame;

    [self.view setNeedsDisplay];
    
//    [self.delegate updateDetailPOIScrollViewContentSize];
    [self.mapView setShowsUserLocation:YES];
    
    self.businessLocationAnnotationView.coordinate = self.poiObject.coordinates.coordinate;
    self.businessLocationAnnotationView.title = self.poiObject.poiName;
    [self.mapView addAnnotation:self.businessLocationAnnotationView];
    [self.mapView showAnnotations:@[self.businessLocationAnnotationView] animated:NO];
    
    [self setupAddressLabel];
    [self setGradientViewGradientBlackSideUp:FALSE];
}
-(void)setupAddressLabel{
    if(!self.addressLabel){
        self.addressLabel = [[UILabel alloc] init];
        [self.mapView addSubview: self.addressLabel];
        
        [self.mapView insertSubview:self.addressLabel aboveSubview:self.labelBackgroundView];
    }
    self.addressLabel.frame = CGRectMake(self.labelPadding, self.mapView.frame.size.width - self.labelHeight, self.mapView.frame.size.width - 2*self.labelPadding, self.labelHeight);
    self.addressLabel.backgroundColor = [UIColor clearColor];
    self.addressLabel.textColor = [UIColor whiteColor];
    self.addressLabel.textAlignment = NSTextAlignmentCenter;
    self.addressLabel.adjustsFontSizeToFitWidth = YES;
    self.addressLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18.0];
    [self.addressLabel setText:self.poiObject.poiAddress];
    self.addressLabel.numberOfLines = 0;
    [self.addressLabel sizeToFit];
    [self.addressLabel setTextAlignment:NSTextAlignmentLeft];
    
    CGRect temp = self.addressLabel.frame;
    temp.origin.y = 0;//CGRectGetMaxY(self.mapView.frame)-CGRectGetHeight(self.addressLabel.frame)-self.labelPadding;
    self.addressLabel.frame = temp;
    [self.addressLabel setHidden:YES];
}

-(MKAnnotationView*) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{

    MKAnnotationView * view;
    if ([annotation isKindOfClass:[MKUserLocation class]]){
        view = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"userLocationPin"];
        [PKQuickMethods mapSetPinForUserLocationAnnotationView:view];

    }
    else{
        view = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Location"];
        [PKQuickMethods mapSetPinPointUnhighlightedImageAndOffsetForMKAnnotationView:view];
    }
    return view;
}

-(void)showMapViewLabel:(BOOL)shouldShow{
    if(shouldShow){
        [self.addressLabel setAlpha:1];
    }
    else{
        [self.addressLabel setAlpha:0];
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)enableMapViewInteraction:(BOOL)enable{
    self.mapView.zoomEnabled = enable;
    self.mapView.scrollEnabled = enable;
    [Flurry logEvent:kFlurryPlaceMapView withParameters:[self getFlurryParameters]];
    //    self.mapView.userInteractionEnabled = enable;
}


/*
 Taken from here: http://stackoverflow.com/questions/1336370/positioning-mkmapview-to-show-multiple-annotations-at-once
  Rushed for time - seconds before app store upload
 */
#define MINIMUM_ZOOM_ARC 0.014 //approximately 1 miles (1 degree of arc ~= 69 miles)
#define ANNOTATION_REGION_PAD_FACTOR 1.15
#define MAX_DEGREES_ARC 360
//size the mapView region to fit its annotations
- (void)zoomMapViewToFitAnnotations:(MKMapView *)mapView animated:(BOOL)animated
{
    NSArray *annotations = mapView.annotations;
    int count = [mapView.annotations count];
    if ( count == 0) { return; } //bail if no annotations
    
    //convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
    //can't use NSArray with MKMapPoint because MKMapPoint is not an id
    MKMapPoint points[count]; //C array of MKMapPoint struct
    for( int i=0; i<count; i++ ) //load points C array by converting coordinates to points
    {
        CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)[annotations objectAtIndex:i] coordinate];
        points[i] = MKMapPointForCoordinate(coordinate);
    }
    //create MKMapRect from array of MKMapPoint
    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
    //convert MKCoordinateRegion from MKMapRect
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
    
    //add padding so pins aren't scrunched on the edges
    region.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
    region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
    //but padding can't be bigger than the world
    if( region.span.latitudeDelta > MAX_DEGREES_ARC ) { region.span.latitudeDelta  = MAX_DEGREES_ARC; }
    if( region.span.longitudeDelta > MAX_DEGREES_ARC ){ region.span.longitudeDelta = MAX_DEGREES_ARC; }
    
    //and don't zoom in stupid-close on small samples
    if( region.span.latitudeDelta  < MINIMUM_ZOOM_ARC ) { region.span.latitudeDelta  = MINIMUM_ZOOM_ARC; }
    if( region.span.longitudeDelta < MINIMUM_ZOOM_ARC ) { region.span.longitudeDelta = MINIMUM_ZOOM_ARC; }
    //and if there is a sample of 1 we want the max zoom-in instead of max zoom-out
    if( count == 1 )
    {
        region.span.latitudeDelta = MINIMUM_ZOOM_ARC;
        region.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    }
    [mapView setRegion:region animated:animated];
}

#pragma mark - OXFlurryProtocol delegate methods

- (NSDictionary *)getFlurryParameters
{
    NSDictionary *params =
    [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld", (long)self.poiObject.poiID],
     kBasePOIPoiIdKey, // Parameter Name
     nil];
    return params;
}

@end