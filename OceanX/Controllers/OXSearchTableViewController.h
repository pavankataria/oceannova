//
//  OXSearchTableViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 28/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXSearchBarView.h"

@interface OXSearchTableViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, OXSearchBarDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet OXSearchBarView *searchBarView;
@end
