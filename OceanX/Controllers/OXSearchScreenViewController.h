//
//  OXSearchScreenViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXFilterTableViewController.h"
@interface OXSearchScreenViewController : UIViewController <UITextFieldDelegate, OXFilterTableViewControllerDelegate, OXSearchContainerScreenViewControllerOperationProtocol, OXSearchContainerScreenViewControllerDelegate>
@property (nonatomic, retain) NSMutableArray *searchResults;
@property (nonatomic, retain) IBOutlet UIView *searchBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintForSummaryBanner;
@end
