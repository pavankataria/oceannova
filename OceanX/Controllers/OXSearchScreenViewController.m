//
//  OXSearchScreenViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSearchScreenViewController.h"
#import "OXMultipleSearchViewController.h"
#import "OXSummaryBannerViewController.h"
#import "OXMapAndListContainerViewController.h"


@interface OXSearchScreenViewController ()<OXSummaryBannerViewController>

@property (nonatomic, retain) OXPOISearchModel *poiSearchObject;
@property (nonatomic, assign, getter=isLoading) BOOL loading;

@property (nonatomic, assign) CGFloat cellImageHeight;
@property (nonatomic, retain) OXSummaryBannerViewController *summaryBannerVC;
@property (nonatomic, assign) OXMultipleSearchViewControllerSenderType filterScreenSenderType;
@property (strong, nonatomic) IBOutlet UIButton *mapAndListButton;
@property (nonatomic, strong) OXMapAndListContainerViewController *mapAndListContainerViewController;
@property (nonatomic, weak) IBOutlet UITextField *searchBarTextField;

@end

@implementation OXSearchScreenViewController

#pragma mark - Lazy loading
-(OXPOISearchModel*)poiSearchObject{
    if(!_poiSearchObject){
        _poiSearchObject = [[OXPOISearchModel alloc] init];
    }
    return _poiSearchObject;
}

#pragma mark - UITextField delegate methods
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self performSegueWithIdentifier:@"OXMultipleSearchViewControllerSegueIdentifier" sender:self];
    self.filterScreenSenderType = OXMultipleSearhViewControllerSenderTypeNormal;
    return NO;
}

#pragma mark - UIViewController life cycle - Methods
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!self.isLoading && self.searchResults.count == 0){
        [self performSearch];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    
    
    [self setupSearchBarTextField];
    self.loading = YES;
    [self performSearch];
    [Flurry logEvent:kFlurrySearch];
}
-(void)setupSearchBarTextField{
    [self.searchBarView setBackgroundColor:[UIColor whiteColor]];

    [self.searchBarTextField.layer setCornerRadius:self.searchBarTextField.frame.size.height/2];
    [self.searchBarTextField.layer setBorderWidth:0];
    [self.searchBarTextField setBackgroundColor:[UIColor colorWithRed:0.89 green:0.89 blue:0.89 alpha:0.4]];
    
    [self.searchBarTextField setLeftViewMode:UITextFieldViewModeAlways];

    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchBarTextFieldIcon"]];
    UIView * leftImageView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, self.searchBarTextField.frame.size.height)];
    [leftImageView addSubview:imageView];
    CGRect rect = imageView.frame;
    rect.origin.x = leftImageView.frame.size.width/2-rect.size.width/2;
    rect.origin.y = leftImageView.frame.size.height/2-rect.size.height/2;
    imageView.frame = rect;
    self.searchBarTextField.leftView = leftImageView;
    CGFloat fakeNavigationBarBottomLineViewHeight = 1;
    
    UIView *fakeNavigationBarBottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.searchBarView.frame.size.height-fakeNavigationBarBottomLineViewHeight, CGRectGetWidth(self.searchBarView.frame), fakeNavigationBarBottomLineViewHeight)];
    [fakeNavigationBarBottomLineView setBackgroundColor:[UIColor colorWithRed:0.89 green:0.89 blue:0.89 alpha:1]];
    [self.searchBarView addSubview:fakeNavigationBarBottomLineView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - Private methods

-(void)performSearch{
    //    if(!self.isLoading){
//    [self.summaryBannerVC setPoiSearchModel:self.poiSearchObject];
    [OXPOISearchByListModel searchByListWithPOISearchObject:self.poiSearchObject
                                           withSuccessBlock:^(AFHTTPRequestOperation *operation, NSArray *response) {
                                               NSLog(@"response: %@", response);
                                               self.searchResults = [response mutableCopy];
                                               self.loading = NO;
                                           } failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                               NSLog(@"failed");
                                               self.loading = NO;
                                           }];
    //    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.destinationViewController isMemberOfClass:[UINavigationController class]]){
        if([[((UINavigationController*)segue.destinationViewController).viewControllers firstObject] isMemberOfClass:[OXMultipleSearchViewController class]]){
            OXMultipleSearchViewController *destinationVC = [((UINavigationController*)segue.destinationViewController).viewControllers firstObject];
            NSLog(@"preparing to segue %@", segue.destinationViewController);
            
            destinationVC.poiSearchObject = self.poiSearchObject;
            destinationVC.poiSearchObject = self.poiSearchObject;
            destinationVC.delegate = self;
            
            if(self.filterScreenSenderType == OXMultipleSearhViewControllerSenderTypeNormal){
                
            }
            else if(self.filterScreenSenderType == OXMultipleSearhViewControllerSenderTypeFirstButton){
                
            }
            else if(self.filterScreenSenderType == OXMultipleSearhViewControllerSenderTypeSecondButton){
                destinationVC.filterScreenSenderType = OXMultipleSearhViewControllerSenderTypeSecondButton;
            }
            
            return;
        }
    }
    if([segue.identifier isEqualToString:kOXSummaryBannerViewControllerSegueIdentifier]){
        self.summaryBannerVC = segue.destinationViewController;
        self.summaryBannerVC.delegate = self;
    }
    
    else if([segue.identifier isEqualToString:@"showDetailViewSegue"]){
        
    }
    else if([segue.identifier isEqualToString:@"embedMapAndListContainer"]){
        self.mapAndListContainerViewController = segue.destinationViewController;
        
        //<<<<<<< HEAD
        [self.mapAndListContainerViewController setParentViewControllerForChildViewControllers:self];
        //=======
        //        UITableViewCell <OXSearchTableViewCellOperationProtocol>*cell = [self.tableView dequeueReusableCellWithIdentifier:  kTableViewCellIdentifierSearchResultsLoadingCell forIndexPath:indexPath];
        //        [cell startAnimatingActivityIndicatorView];
        ////        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
        //        return cell;
        //    }
        //    else{
        //        if(indexPath.row == OXInstagramStyleCellTypePicture){
        //            OXPlaceDisplayImageTableViewCell *imageDisplayCell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsImageDisplayCell forIndexPath:indexPath];
        //            if (imageDisplayCell == nil){
        //                [NSException raise:@"imageDisplayCell == nil.." format:@"No cells with matching imageDisplayCell CellIdentifier loaded from your storyboard"];
        //            }
        //
        //            OXPOISearchByListModel *currentSearchListObject = self.searchResults[indexPath.section];
        //
        //            [imageDisplayCell setupCellWithObject:currentSearchListObject];
        ////            imageDisplayCell.separatorInset = UIEdgeInsetsMake(0.f, imageDisplayCell.bounds.size.width, 0.f, 0.f);
        //            [self preDownloadNextImagesAfterIndexPath:indexPath numberOfItems:2];
        //            return imageDisplayCell;
        //        }
        //        else if(indexPath.row == OXInstagramStyleCellTypeTagBanner){
        //            OXTagBannerTableViewCell * tagBannerCell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierTagBanner];
        //            OXPOISearchByListModel *currentSearchListObject = self.searchResults[indexPath.section];
        //            tagBannerCell.tagBannerView.tagsArray = currentSearchListObject.tags;
        ////            tagBannerCell.separatorInset = UIEdgeInsetsMake(0.f, tagBannerCell.bounds.size.width, 0.f, 0.f);
        //            [tagBannerCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        //            return tagBannerCell;
        //        }
        //        else if(indexPath.row == OXInstagramStyleCellTypeLikeAndDislikeBar){
        //            OXLikeDislikeBarUITableViewCell *likeDislikeBar = [tableView dequeueReusableCellWithIdentifier:kOXTableViewCellReuseLikeDislikeBarCellIdentifier];
        //            [likeDislikeBar setSelectionStyle:UITableViewCellSelectionStyleNone];
        //
        //            if (likeDislikeBar == nil){
        //                [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
        //            }
        //            OXPOISearchByListModel *currentPOI = self.searchResults[indexPath.section];
        //
        //                [likeDislikeBar.likeButton setSelected:currentPOI.userLiked];
        //                [likeDislikeBar.dislikeButton setSelected:currentPOI.userDisLiked];
        //
        //
        //            [likeDislikeBar.likeButton addTarget:self action:@selector(likeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        //            [likeDislikeBar.dislikeButton addTarget:self action:@selector(dislikeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        //            likeDislikeBar.poiCaption.text = currentPOI.caption;
        ////            [headerView.contentView setBackgroundColor:[UIColor colorWithRed:0.5 green:1.0 blue:0.7 alpha:kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha]];
        //            return likeDislikeBar;
        //        }
        //    }
        //    return nil;
        //}
        //
        //
        //
        //
        //-(void)likeButtonTapped:(id)sender{
        //    NSLog(@"likeButtonTapped");
        //    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        //    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        //    __block OXPOISearchByListModel *currentPOI = self.searchResults[indexPath.section];
        //
        //    if(((UIButton*)sender).isSelected){
        //        [OXRestAPICient likePOIWithID:currentPOI.poiId
        //                         successBlock:^(AFHTTPRequestOperation *operation, id response) {
        //                             NSLog(@"%@", response);
        //                             currentPOI.userLiked = !currentPOI.userLiked;
        //                             currentPOI.userDisLiked = !currentPOI.userLiked;
        //                         }
        //                            failBlock:^(AFHTTPRequestOperation *operation, id error) {
        //                                NSLog(@"%@", error);
        //
        //                            }];
        //    }
        //    else{
        //        [OXRestAPICient neutralPOIWithID:currentPOI.poiId
        //                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
        //                                 NSLog(@"%@", response);
        //                                currentPOI.userLiked = false;
        //                                currentPOI.userDisLiked = false;
        //                            }
        //                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
        //                                   NSLog(@"%@", error);
        //
        //                               }];
        //>>>>>>> thumbnailCommit
    }
}


#pragma mark end -
-(void)performSearchWithPOISearchObject:(OXPOISearchModel*)object{
    NSLog(@"performing search object params: %@", [object params]);
//    [self.summaryBannerVC setPoiSearchModel:object];
    [self.mapAndListContainerViewController performSerchWithObject:object];
}

#pragma mark - OXSummaryDelegate - Methods

-(void)buttonOnePressed{
    self.filterScreenSenderType = OXMultipleSearhViewControllerSenderTypeFirstButton;
    [self performSegueWithIdentifier:@"OXMultipleSearchViewControllerSegueIdentifier" sender:self];
}
-(void)buttonTwoPressed{
    self.filterScreenSenderType = OXMultipleSearhViewControllerSenderTypeSecondButton;
    [self performSegueWithIdentifier:@"OXMultipleSearchViewControllerSegueIdentifier" sender:self];
}

-(void)presentingAnyData:(BOOL)isPresentingAnything{
    if(isPresentingAnything){
        self.heightConstraintForSummaryBanner.constant = 0;
    }
    else{

        self.heightConstraintForSummaryBanner.constant = 0;
    }
}

#pragma mark - IBAction - Methods
- (IBAction)mapAndListButtonTapped:(id)sender {
    self.mapAndListButton.selected = !self.mapAndListButton.selected;
    if(self.mapAndListButton.selected){
        self.mapAndListButton.tintColor = rgb(110, 110, 100);
    }
    
    if (self.mapAndListButton.selected){
        [self.mapAndListContainerViewController swapToMapViewController];
        [Flurry logEvent:kFlurrySearchMapView];
    }
    else{
        [self.mapAndListContainerViewController swapToListViewController];
    }
}

@end
