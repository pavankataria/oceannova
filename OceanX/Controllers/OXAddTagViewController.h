//
//  OXAddTagViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 20/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXPOISearchModelDelegate.h"
#import "OXAddTagsBaseViewController.h"

@interface OXAddTagViewController : OXAddTagsBaseViewController <UITableViewDelegate, UITableViewDataSource,OXPOISearchModelDelegate>

@property (nonatomic, weak) IBOutlet UIButton *addTagFromCacheButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *finishButtonItem;

@property (nonatomic, retain) OXHCAddPoiModel *addPoiObject;

-(IBAction)addTagFromCache:(id)sender;
-(IBAction)finishButtonTapped:(id)sender;
@end
