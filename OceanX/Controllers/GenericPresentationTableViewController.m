//
//  GenericPresentationTableViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 17/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "GenericPresentationTableViewController.h"
#import "OXFriendTableViewCell.h"
#import "OXTagTableViewCell.h"

#import "OXTagPlaceCountModel.h"



@interface GenericPresentationTableViewController ()

@end

@implementation GenericPresentationTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return kOXGenericPresentationTableViewRowHeight;
//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.genericArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id genericObject = [self.genericArray objectAtIndex:indexPath.row];
    NSString *cellIdentifier = [genericObject cellIdentifier];
    UITableViewCell <GenericTableViewCellConfiguration> *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIButton *button = [cell addFriendButton];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
     [cell updateForModelObject:genericObject];
    return cell;
}
@end
