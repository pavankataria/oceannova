//
//  OXSearchTagsViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXAddTagsBaseViewController.h"
#import "OXSearchDelegate.h"

@interface OXSearchTagsViewController : OXAddTagsBaseViewController<UITableViewDataSource, UITableViewDelegate, OXPOISearchModelDelegate>


@property (nonatomic, weak) OXPOISearchModel *poiSearchObject;
@property (nonatomic, weak) id<OXSearchDelegate> delegate;
@end
