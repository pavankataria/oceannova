//
//  OXBusinessDetailsTableViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 18/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXBusinessDetailsTableViewController.h"
#import "OXBusinessInfoTableViewCell.h"
#import "OXExtraInfoTableViewCell.h"
#import "OXHoursTableViewCell.h"
#import "OXBusinessHoursModel.h"
#import "OXAmendHoursTableViewController.h"
#import "OXImageStackItem.h"
#import "OXAddTagViewController.h"

@interface OXBusinessDetailsTableViewController (){
    NSUInteger indexPathOffSetBecauseOfChooseOpeningHoursCell;
}
@property (nonatomic, retain) NSMutableArray *pretifiedBusinessOpeningHoursArray;
@end

@implementation OXBusinessDetailsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    indexPathOffSetBecauseOfChooseOpeningHoursCell = 1;
    //Debugging
    if(self.businessImages.count == 0){
        self.businessImages = [[NSMutableArray alloc] initWithObjects:[UIImage imageNamed:@"testImage"],[UIImage imageNamed:@"testImage2"], nil];
    }
    
//    NSLog(@"LOLOL");
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        NSLog(@"setting images in background thread");
        [self.addPoiObject setPictures:self.businessImages];
        
//        NSLog(@"self.businessimages: %@", self.businessImages);
    });
    
    [self registerForKeyboardNotifications];
    [self setupNavigtionBar];
//    NSLog(@"poi object: %@", [self.addPoiObject params]);
    [self.tableView reloadData];
    
    
    [self updateViewsBasedOnPOIObject];
}
-(void)updateViewsBasedOnPOIObject{
    OXBusinessInfoTableViewCell *cell = (OXBusinessInfoTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:kBusinessInfoTableSectionBusinessInfo]];
    cell.businessNameTextField.text = self.addPoiObject.poiName;

    
    for(int i = 0; i < 5; i++){
        OXExtraInfoTableViewCell *tempCell = (OXExtraInfoTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:kBusinessInfoTableSectionDetailsInfo]];
        switch (i) {
            case kExtraInfoTableViewCellIndexPathRowAddress:
                tempCell.infoTextField.text = self.addPoiObject.address;
                break;
            case kExtraInfoTableViewCellIndexPathRowPhone:
                tempCell.infoTextField.text = self.addPoiObject.telephone;
                break;
            case kExtraInfoTableViewCellIndexPathRowWebsite:
                tempCell.infoTextField.text = self.addPoiObject.website;
                break;
            case kExtraInfoTableViewCellIndexPathRowTwitter:
                tempCell.infoTextField.text = self.addPoiObject.twitter;
                break;
            default:
                break;
        }
    }
    if(self.addPoiObject.businessHours.count > 0){
        self.businessOpeningHours = [self.addPoiObject.businessHours mutableCopy];
    }
    [self updateTableViewForBusinessHours];
}
-(void)viewWillAppear:(BOOL)animated{
    [self updateUI];
}
-(void)updateUI{
    [self.nextBarButtonItem setEnabled:[self.addPoiObject isPoiValidAtBusinessDetailsPage]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case kBusinessInfoTableSectionBusinessInfo:
            return 1;
            break;
        case kBusinessInfoTableSectionDetailsInfo:
            return 4;
            break;
        case kBusinessInfoTableSectionHoursInfo:
            return indexPathOffSetBecauseOfChooseOpeningHoursCell + self.pretifiedBusinessOpeningHoursArray.count;
            
            break;
        default:
            return 1;
            break;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"DID SELECT: %@", indexPath);
    if(indexPath.section == kBusinessInfoTableSectionHoursInfo){
        [self performSegueWithIdentifier:kSegueIdentifierAmendHoursViewController sender:self];
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        OXBusinessInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceBusinessInfo forIndexPath:indexPath];
        
        cell.businessImageView.image = [(OXImageStackItem*)[self.businessImages firstObject] originalImage];
        cell.indexPath = indexPath;
        cell.delegate = self;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    else if(indexPath.section == 1){
        OXExtraInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceExtraInfo forIndexPath:indexPath];
        cell.delegate = self;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return [cell getPreparedCellWithIndexPath:indexPath];
        
    }
    else if(indexPath.section == kBusinessInfoTableSectionHoursInfo){
        if(indexPath.row == 0){
            OXHoursTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceHoursInfo forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            return cell;
        }
        else if(self.pretifiedBusinessOpeningHoursArray.count > 0){
            OXExtraInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceExtraInfo forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell updateCellForPretifyOpeningHour:self.pretifiedBusinessOpeningHoursArray[indexPath.row - indexPathOffSetBecauseOfChooseOpeningHoursCell]];
            cell.delegate = self;
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            return cell;
        }
        else{
            return nil;
        }
    }
    else{
        return nil;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == kBusinessInfoTableSectionBusinessInfo){
        if(indexPath.row == 0){
            return 88;
        }
    }
    return 44;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section == kBusinessInfoTableSectionBusinessInfo){
        return kBusinessInfoTableTitleHeaderBusinessInfo;
    }
    else if(section == kBusinessInfoTableSectionDetailsInfo){
        return kBusinessInfoTableTitleHeaderDetailsInfo;
        
    }
    else if(section == kBusinessInfoTableSectionHoursInfo){
        return kBusinessInfoTableTitleHeaderHoursInfo;
    }
    return @"";
}


#pragma mark - lol

-(void)ammendHoursDidSaveBusinessHours:(NSArray*)businessHours{
    self.businessOpeningHours = [businessHours mutableCopy];
    [self.addPoiObject setBusinessHours:self.businessOpeningHours];

    for(OXBusinessHoursModel * loool in self.businessOpeningHours){
        NSLog(@"saved hour LOL: %@", [loool hoursRepresentation]);
    }
    [self updateTableViewForBusinessHours];
}
-(void)updateTableViewForBusinessHours{
    self.pretifiedBusinessOpeningHoursArray = [PKQuickMethods prettifyBusinessOpeningHours:self.addPoiObject.businessHours];
    NSIndexSet *indexSetToUpdate = [[NSIndexSet alloc] initWithIndex:kBusinessInfoTableSectionHoursInfo];
    [self.tableView reloadSections:indexSetToUpdate withRowAnimation:UITableViewRowAnimationNone];

}

#pragma mark - OXBusinessDetailCellDelegate
-(void)textUpdatedForBusinessDetailCell:(UITableViewCell <OXBusinessDetailCellConfiguration>*)cell{
    NSDictionary *currentDictionary = [cell getRowValueDictionary];
            NSLog(@"currentDictionary: %@", currentDictionary);
    NSString *text = [currentDictionary objectForKey:kBusinessDetailCellConfigurationGetValuesDictionaryText];
    NSIndexPath *indexPath = [currentDictionary objectForKey:kBusinessDetailCellConfigurationGetValuesDictionaryIndexPath];
    
    if(indexPath.section == kBusinessInfoTableSectionBusinessInfo){
        if(indexPath.row == 0){
            [self.addPoiObject setPoiName:text];
            [self updateUI];
        }
    }
    else if(indexPath.section == kBusinessInfoTableSectionDetailsInfo){

        switch (indexPath.row) {
            case kExtraInfoTableViewCellIndexPathRowAddress:
                [self.addPoiObject setAddress:text];
                break;
            case kExtraInfoTableViewCellIndexPathRowPhone:
                [self.addPoiObject setTelephone:text];
                break;
            case kExtraInfoTableViewCellIndexPathRowWebsite:
                [self.addPoiObject setWebsite:text];
                break;
            case kExtraInfoTableViewCellIndexPathRowTwitter:
                [self.addPoiObject setTwitter:text];
                break;
            default:
                break;
        }
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:kSegueIdentifierAmendHoursViewController]){
        if( ! self.addPoiObject.businessHours){
            self.businessOpeningHours = [OXBusinessHoursModel defaultMondayToFriday9to5WithoutSiestas];
        }
        OXAmendHoursTableViewController *dVC;
        if([segue.destinationViewController isMemberOfClass:[UINavigationController class]]){
            dVC = [[segue.destinationViewController childViewControllers] firstObject];
        }
        else{
            dVC = segue.destinationViewController;
        }
        dVC.businessHours = [[NSMutableArray alloc] initWithArray:self.businessOpeningHours];
        dVC.delegate = self;
    }
    if([segue.identifier isEqualToString:kSegueIdentifierAddTagsViewController]){
        OXAddTagViewController *dVC = segue.destinationViewController;
        dVC.addPoiObject = self.addPoiObject;
//        NSLog(@"add poi object: %@", [self.addPoiObject params]);
    }
}


#pragma mark - Private methods

- (void)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

-(void)setupNavigtionBar {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);

}

#pragma mark - Keyboard notification observer methods
- (void) keyboardWillBeHidden:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self.tableView setContentInset:contentInsets];
    [self.tableView setScrollIndicatorInsets:contentInsets];
    [self.tableView setContentOffset:CGPointMake(0,self.tableView.contentOffset.y) animated:YES];
}

- (void) keyboardWasShown:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    [self.tableView setContentInset:contentInsets];
    [self.tableView setScrollIndicatorInsets:contentInsets];
    [self.tableView setContentOffset:CGPointMake(0,self.tableView.contentOffset.y) animated:YES];
}

@end
