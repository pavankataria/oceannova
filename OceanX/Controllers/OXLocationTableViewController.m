//
//  OXLocationTableViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXLocationTableViewController.h"

@interface OXLocationTableViewController ()
@property (nonatomic, retain) NSMutableArray *uniqueLocations;

@end


@implementation OXLocationTableViewController
#pragma mark - Lazy Loading 
-(NSMutableArray *)uniqueLocations{
    if(!_uniqueLocations){
        _uniqueLocations = [[NSMutableArray alloc] init];
    }
    return _uniqueLocations;
}
#pragma mark end -
#pragma mark - UISearchBarDelegate
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self performLocationSearchWithText:searchText];
}
#pragma mark end -

-(void)performLocationSearchWithText:(NSString*)searchText{
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = searchText;
    
    MKCoordinateRegion currentRegion = [[OXLocationManager sharedTracker] currentMapRegion];
    request.region = currentRegion;
    
    MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
    [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        [self processLocalSearchResponse:response];
    }];
}
-(void)processLocalSearchResponse:(MKLocalSearchResponse*)response{
    self.uniqueLocations = nil;
    for(MKMapItem *item in response.mapItems){
        OXCoordinateModel *coordinate = [[OXCoordinateModel alloc] init];
        [coordinate setCoordinate:item.placemark.coordinate];
        [coordinate setPlaceMarkTitleAndSubtitle:[NSString stringWithFormat:@"%@"/*@"%@, %@"*/, /*item.placemark.name,*/ item.placemark.title]];
        [self.uniqueLocations addObject:coordinate];
    }
    self.uniqueLocations = [self.uniqueLocations valueForKeyPath:@"@distinctUnionOfObjects.self"];
    [self.tableView reloadData];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchBar.delegate = self;
    [self.searchBar setSearchBarStyle:UISearchBarStyleProminent];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.searchBar becomeFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.uniqueLocations.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"locationCellTableViewCellIdentifier" forIndexPath:indexPath];
    cell.textLabel.text = [self.uniqueLocations[indexPath.row] placeMarkTitleAndSubtitle];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.searchPOIModel.coordinate = self.uniqueLocations[indexPath.row];
    [self.searchPOIModel setWithCoordinateModel:self.uniqueLocations[indexPath.row]];
    if(self.delegate){
        NSLog(@"delegate is not empty");
    }
    else{
        NSLog(@"DELEGATE IS TOTALLY EMPTY");
    }
    [self.delegate OXPOISearchModelUpdatePOISearchObject:self.searchPOIModel];
    [self.navigationController popViewControllerAnimated:NO];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelViewController:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
