//
//  OXSearchContainerScreenViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 01/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSearchContainerScreenViewController.h"
#import "OXSearchResultsViewController.h"
#import "OXFilterScreenViewController.h"
#import "ContainerViewController.h"

@interface OXSearchContainerScreenViewController (){
    CGFloat searchBarViewHeight;
    NSCharacterSet *blockedCharacters;


}
@property (nonatomic, weak) UINavigationController *embeddedStack;
@property (nonatomic, retain) UIViewController *currentViewController;
@property (nonatomic, assign, getter=isResultsViewControllerShowing) BOOL resultsViewControllerShowing;
@property (nonatomic, retain) ContainerViewController *containerViewController;
@property (nonatomic, retain) OXPOISearchModel *poiSearchModel;

@end

@implementation OXSearchContainerScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    searchBarViewHeight = self.searchBarHeightConstraint.constant;//self.searchBarView.frame.size.height;
    NSLog(@"search bar height: %f", searchBarViewHeight);
    
//    self.view.backgroundColor = [PKUIAppearanceController darkBlueColor];
    [self.searchBarView setBackgroundColor:[PKUIAppearanceController darkBlueColor]];//[UIColor greenColor]];
    self.searchBarView.delegate = self;
    self.searchBarView.dataSource = self;
    [self.searchBarView reloadViews];
    NSMutableCharacterSet *allowedCharacters = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
    [allowedCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@"_ "]];
    blockedCharacters = [allowedCharacters invertedSet];

}
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"view will appear method called");
    [self returnNavigationAndStatusBarBackToNormalWithAnimation:NO];
    NSLog(@"search bar view frame :%@", NSStringFromCGRect(self.searchBarView.frame));
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)cancel:(id)sender{
    [self.containerViewController swapToFirstViewController];
    [self returnNavigationAndStatusBarBackToNormalWithAnimation:NO];
}
-(void)returnNavigationAndStatusBarBackToNormalWithAnimation:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self searchBarViewAdjustForStatusBar:YES withAnimation:animated];
    [self.searchBarView.searchBarTextField resignFirstResponder];

}
#pragma mark - OXSearchBar View Datasource methods
-(UIColor *)searchBarViewBackgroundColorForSearchBarTextField{
    
    return [UIColor whiteColor];
}

-(UITextBorderStyle)searchBarViewTextFieldStyleForSearchBarTextField{
    return UITextBorderStyleRoundedRect;
}

-(NSString *)searchBarViewPlaceHolderString{
    return kOXSearchTextFieldPlaceHolderText;
}
#pragma mark end -
#pragma mark - OXSearchBar View delegate methods
-(void)searchBarViewAdjustForStatusBar:(BOOL)shouldAdjustForStatusBar withAnimation:(BOOL)animate{
    
    if(animate){
        [self.view layoutIfNeeded];
    }
    else{
        [self.view layoutIfNeeded]; // Called on parent vie
    }

    if(shouldAdjustForStatusBar){
        self.searchBarHeightConstraint.constant = searchBarViewHeight + 20;
        self.verticalSpaceTopLayoutGuide.constant = -20;

    }
    else{
        self.searchBarHeightConstraint.constant = searchBarViewHeight;
        self.verticalSpaceTopLayoutGuide.constant = 0;
    }
    if(animate){
        [UIView animateWithDuration:UINavigationControllerHideShowBarDuration
                         animations:^{
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
    else{
        [self.view layoutIfNeeded]; // Called on parent view
    }

}
-(void)searchBarView:(OXSearchBarView *)searchBarView searchBarDidBeginEditing:(UITextField *)textField{
    [self.containerViewController swapToSecondViewController];
    [self searchBarViewAdjustForStatusBar:NO withAnimation:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];

//    NSLog(@"search bar delegate method called");
//    [self.containerViewController swapViewControllers];

}

-(void)searchBarTextFieldShouldReturn:(UITextField *)textField{
    [self performSearchAndSaveInParent];
    if([self.searchBarView.searchBarTextField.text length] == 0){
        self.poiSearchModel.textSearchString = @"";
    }
    self.searchBarView.searchBarTextField.text = @"";
    [self.searchBarView.searchBarTextField resignFirstResponder];
}
#pragma mark end -



// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"segue identifier: %@", segue.identifier);
    if([segue.identifier isEqualToString:@"embedContainer"]){
        self.containerViewController = segue.destinationViewController;
        
        [self.containerViewController setParentViewControllerForChildViewControllers:self];
    }
}

#pragma mark - OXSearchContainerScreenViewControllerDelegate
-(NSMutableArray*)OXSearchContainerSharedSearchResultsArray{
    if(!_sharedSearchResultsArray){
        _sharedSearchResultsArray = [[NSMutableArray alloc] init];
    }
    return _sharedSearchResultsArray;
}
-(UITextField *)grabSearchTextField{
    return self.searchBarView.searchBarTextField;
}
#pragma mark end
-(OXPOISearchModel *)poiSearchModel{
    if(!_poiSearchModel){
        _poiSearchModel = [[OXPOISearchModel alloc] init];
    }
    return _poiSearchModel;
}
-(void)performSearchWithSuccessBlock:(SuccessCompletionBlock)successblock andFailureBlock:(FailureCompletionBlock)failureBlock{
    NSLog(@"calling perform search");
//#warning todo: to replace the search by searing with any place instead of current location;
    
    [OXRestAPICient searchByListWithPOISearchObject:self.poiSearchModel
                                   withSuccessBlock:^(AFHTTPRequestOperation *operation, NSArray *response) {
                                       [self setupSearchResultsArrayWithResponse:response];
                                       if(successblock){
                                           successblock(operation, response);
                                       }
                                   } failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                       [self.sharedSearchResultsArray removeAllObjects];
                                       if(failureBlock){
                                           failureBlock(operation, error);
                                       }
                                   }];
}


-(void)performSearchAndSaveInParent{
    [self.containerViewController swapToFirstViewController];
    [self performSearchWithSuccessBlock:nil andFailureBlock:nil];
}

-(void)setupSearchResultsArrayWithResponse:(NSArray*)array{
    [self.sharedSearchResultsArray removeAllObjects];
    [self.sharedSearchResultsArray setArray:[OXPOISearchByListModel setupSearchResultsArrayWithResponse:array]];
    

}
-(OXPOISearchModel *)getSharedPOISearchModel{
    return self.poiSearchModel;
}

#pragma mark - UITextField delegate

-(void)filterScreenTextFieldDidChange:(UITextField *)textField{
    [self.containerViewController.secondViewController textFieldDidChangeWithText:textField.text];
}

-(BOOL)filterScreenTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSLog(@"OMGOMG: %@", string);
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    return ([string rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);
}

#pragma mark end


@end
