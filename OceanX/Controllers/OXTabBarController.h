//
//  OXTabBarController.h
//  OceanX
//
//  Created by Pavan Kataria on 24/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXTabBarController : UITabBarController<UITabBarControllerDelegate, UIAlertViewDelegate>
@property(nonatomic, weak) IBOutlet UIButton *centerButton;
@property(nonatomic, assign) BOOL tabBarHidden;

@end
