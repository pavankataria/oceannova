//
//  OXBasicContentViewController.m
//  OceanX
//
//  Created by Systango on 2/16/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXBasicContentViewController.h"

@interface OXBasicContentViewController ()

@end

@implementation OXBasicContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -IBAction Method

- (IBAction)startNowButtonTapped:(id)sender {

    [self dismissViewControllerAnimated: YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kTutorialViewDidFinishNowDoesFBRecommendationsNeedToBeDisplayedNotification object:self];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
