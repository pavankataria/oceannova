//
//  OXGenderTableViewCell.h
//  OceanX
//
//  Created by Systango on 2/3/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXGenderTableViewCell : UITableViewCell
@property (assign, nonatomic) Gender selectedGender;
@end
