//
//  OXMapElementModel.m
//  OceanX
//
//  Created by Systango on 2/2/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

NSString * const kPOIModelTagsKey = @"tags";
NSString * const kPOIModelAddressKey = @"address";
NSString * const kPOISearchUserLikeOrDislikeKey2 = @"poiPreference";

NSString * const kPOIModelAddedByKey = @"addedBy";
NSString * const kPOIModelSocialContact = @"socialContact";
NSString * const kPOIModelOpeningHours = @"openingHours";

NSString * const kServerResponseTelephone = @"telephone";
NSString * const kServerResponseTwitter = @"twitter";
NSString * const kServerResponseWebsite = @"website";
NSString * const kServerResponseFacebook = @"facebook";

#import "OXPOIModel.h"
#import "OXProcessedPicture.h"
@implementation OXPOIModel

-(instancetype)initWithDictionary:(NSDictionary*) dictionaryValue{
    self = [super initWithDictionary:dictionaryValue];
    if (self)
    {
        NSLog(@"PAVAN PAVAN: %@", dictionaryValue);
        _tags = [OXTagModel getTagsFromDictionary:[dictionaryValue getValueForKey:kPOIModelTagsKey]];
        _addedBy = [dictionaryValue getValueForKey:kPOIModelAddedByKey];
        if([[dictionaryValue objectForKey:kPOISearchUserLikeOrDislikeKey2] isKindOfClass:[NSNumber class]]){
            
        }
        else{
//#warning todo: create an enum for LIKE, DISLIKE and NEUTRAL for strings
            NSString *stringLikeOrDislike = [dictionaryValue objectForKey:kPOISearchUserLikeOrDislikeKey2];
            if([stringLikeOrDislike isEqualToString:@"LIKE"]){
                self.userLiked = TRUE;
                self.userDisliked = FALSE;
                self.userConfused = FALSE;

            }
            else if([stringLikeOrDislike isEqualToString:@"DISLIKE"]){
                self.userLiked = FALSE;
                self.userDisliked = TRUE;
                self.userConfused = FALSE;
            }
            else if([stringLikeOrDislike isEqualToString:@"CONFUSED"]){
                self.userLiked = FALSE;
                self.userDisliked = FALSE;
                self.userConfused = TRUE;

            }

            if([stringLikeOrDislike isEqualToString:@"NEUTRAL"]){
                self.userLiked = FALSE;
                self.userDisliked = FALSE;
                self.userConfused = FALSE;
            }
            
        }
        
        
        _poiAddress = [dictionaryValue getValueForKey:kPOIModelAddressKey];
        _openingHours = [OXBusinessHoursModel businessHoursFromServerBusinessHoursDictionary:[dictionaryValue getValueForKey:kPOIModelOpeningHours]];
        
        
        
        self.socialContact = [dictionaryValue getValueForKey:kPOIModelSocialContact];
        NSMutableDictionary *temporarySocialContactDictionary = [self.socialContact mutableCopy];
        for (NSString *key in temporarySocialContactDictionary.allKeys) {
            if([key isEqualToString:kServerResponseTwitter]){
                [temporarySocialContactDictionary setObject: [temporarySocialContactDictionary objectForKey: key] forKey: kExtraInfoTableViewCellIndexPathRowLabelNameTwitter];
                [temporarySocialContactDictionary removeObjectForKey:key];
            }
            else if([key isEqualToString:kServerResponseTelephone]){
                [temporarySocialContactDictionary setObject: [temporarySocialContactDictionary objectForKey: key] forKey: kExtraInfoTableViewCellIndexPathRowLabelNamePhone];
                [temporarySocialContactDictionary removeObjectForKey:key];
            }
            else if([key isEqualToString:kServerResponseWebsite]){
                [temporarySocialContactDictionary setObject: [temporarySocialContactDictionary objectForKey: key] forKey: kExtraInfoTableViewCellIndexPathRowLabelNameWebsite];
                [temporarySocialContactDictionary removeObjectForKey:key];
            }
            else if([key isEqualToString:kServerResponseFacebook]){
                [temporarySocialContactDictionary setObject: [temporarySocialContactDictionary objectForKey: key] forKey: kExtraInfoTableViewCellIndexPathRowLabelNameFacebook];
                [temporarySocialContactDictionary removeObjectForKey:key];
            }
        }
        self.socialContact = temporarySocialContactDictionary;
        
        if([dictionaryValue objectForKey:kBasePOIPicturesKey]){
            if([[dictionaryValue objectForKey:kBasePOIPicturesKey] count] > 0){
//                                NSLog(@"kBasePOIPicturesKey: %@", [dictionaryValue objectForKey:kBasePOIPicturesKey]);

                self.pictures = [NSArray arrayWithObject:[self getPictureFromData:dictionaryValue]];
                
                self.picturesProperlyProcessed = [OXProcessedPicture getPicturesArrayFromJSONResponse:[dictionaryValue objectForKey:kBasePOIPicturesKey]];//[NSArray arrayWithObject:[self getPicturesFromPicturesJSON:dictionaryValue objectForKey:kBasePOIPicturesKey]];

                NSLog(@"PICTURES: %@", self.picturesProperlyProcessed);
            }
            
        }
        self.coordinates = [[OXCoordinateModel alloc] initWithDictionary:[dictionaryValue getValueForKey:kBasePOICoordinatesKey]];
        self.poiID = [[dictionaryValue getValueForKey:kBasePOIPoiIdKey] integerValue];
        self.poiName = [dictionaryValue getValueForKey:kPOIModelPoiNameKey];
        
        
        self.caption = [dictionaryValue objectForKey:@"caption"];
    }
    return self;
}

#pragma mark Private Methods

@end
