//
//  PKBusinessInformationPresentationTableViewController.m
//  
//
//  Created by Pavan Kataria on 17/02/2015.
//
//

#import "PKBusinessInformationPresentationTableViewController.h"
#import "OXBusinessInfoPresentationTableViewCell.h"
#import "OXHoursPresentationTableViewCell.h"
#import "OXFlurryProtocol.h"

@interface PKBusinessInformationPresentationTableViewController ()<OXFlurryProtocol>
@property (nonatomic, retain) NSMutableArray *pretifiedBusinessOpeningHoursArray;
@property (nonatomic,retain) NSMutableArray *businessOpeningHours;

@end

@implementation PKBusinessInformationPresentationTableViewController
#pragma mark - lazy loading - Methods

-(NSMutableArray *)businessOpeningHours{
    if(!_businessOpeningHours){
        _businessOpeningHours = [[NSMutableArray alloc] init];
    }
    return _businessOpeningHours;
}

#pragma mark - view controller life cycle - Methods


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setScrollEnabled:NO];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self registerNibs];
    [self setupTableViews];
    


}
-(void)registerNibs{
    [self.tableView registerNib:[UINib nibWithNibName:@"OXBusinessInfoPresentationTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellReuseIdentifierBusinessInfo];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OXHoursPresentationTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellReuseIdentifierHoursPresentation];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}
-(void)setupTableViews{
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ammendHoursDidSaveBusinessHours:(NSArray*)businessHours{
    NSLog(@"business hours:%@ ", businessHours);
    if(businessHours.count > 0){
        self.pretifiedBusinessOpeningHoursArray = [PKQuickMethods prettifyBusinessOpeningHours:businessHours];
//#warning todo: no need to reload the whole table view - this causes a crash due to array inconsistencies.
        //        NSIndexSet *indexSetToUpdate = [[NSIndexSet alloc] initWithIndex:kDetailPOIVCBusinessInfoTableSectionHoursInfo];
        //        [self.tableView reloadSections:indexSetToUpdate withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView reloadData];
    }
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == kDetailPOIVCBusinessInfoTableSectionDetailsInfo){
//        OXBusinessInfoPresentationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceExtraInfo forIndexPath:indexPath];
//        return cell;
        
        OXBusinessInfoPresentationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierBusinessInfo];
        
        NSString *stringKey = self.poiObject.socialContact.allKeys[indexPath.row];
        NSString *objectValue = [self.poiObject.socialContact objectForKey:stringKey];
        cell.headerlabel.text = stringKey;
        
        
        
        cell.infoLabel.text = objectValue;
        NSRange r = [objectValue rangeOfString:objectValue];
        if([cell.headerlabel.text isEqualToString:kExtraInfoTableViewCellIndexPathRowLabelNameTwitter]){
            [cell.infoLabel addLinkToPhoneNumber:[NSString stringWithFormat:@"%@", objectValue] withRange:r];
            cell.infoLabel.delegate = self;
            cell.infoLabel.tag = kExtraInfoTableViewCellIndexPathRowLabelTagTwitter;
        }
        else if([cell.headerlabel.text isEqualToString:kExtraInfoTableViewCellIndexPathRowLabelNamePhone]){
            [cell.infoLabel addLinkToPhoneNumber:[NSString stringWithFormat:@"%@", objectValue] withRange:r];
            cell.infoLabel.delegate = self;
            cell.infoLabel.tag = kExtraInfoTableViewCellIndexPathRowLabelTagPhone;
        }
        else if([cell.headerlabel.text isEqualToString:kExtraInfoTableViewCellIndexPathRowLabelNameWebsite]){
            [cell.infoLabel addLinkToURL:[NSURL URLWithString:objectValue] withRange:r];
            cell.infoLabel.delegate = self;
            cell.infoLabel.tag = kExtraInfoTableViewCellIndexPathRowLabelTagWebsite;
        }
        
        return cell;

//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//        return [cell getPreparedCellWithIndexPath:indexPath];
//        
    }
    else if(indexPath.section == kDetailPOIVCBusinessInfoTableSectionHoursInfo){
        OXHoursPresentationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifierHoursPresentation forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell updateCellForPretifyOpeningHour:self.pretifiedBusinessOpeningHoursArray[indexPath.row]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    return nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case kDetailPOIVCBusinessInfoTableSectionDetailsInfo:
            return 0;
            break;
        case kDetailPOIVCBusinessInfoTableSectionHoursInfo:
            return self.pretifiedBusinessOpeningHoursArray.count;
            break;
        default:
            return 1;
            break;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.section == 0){
//        OXBusinessInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceBusinessInfo forIndexPath:indexPath];
//        
//        cell.businessImageView.image = [(OXImageStackItem*)[self.businessImages firstObject] originalImage];
//        cell.indexPath = indexPath;
//        cell.delegate = self;
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//        return cell;
//    }
//    else if(indexPath.section == 1){
//        OXExtraInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceExtraInfo forIndexPath:indexPath];
//        
//        cell.delegate = self;
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//        return [cell getPreparedCellWithIndexPath:indexPath];
//        
//    }
//    else if(indexPath.section == 2){
//        if(indexPath.row == 0){
//            OXHoursTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceHoursInfo forIndexPath:indexPath];
//            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//            return cell;
//        }
//        else if(self.pretifiedBusinessOpeningHoursArray.count > 0){
//            OXExtraInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceExtraInfo forIndexPath:indexPath];
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            [cell updateCellForPretifyOpeningHour:self.pretifiedBusinessOpeningHoursArray[indexPath.row - indexPathOffSetBecauseOfChooseOpeningHoursCell]];
//            cell.delegate = self;
//            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//            return cell;
//        }
//        else{
//            return nil;
//        }
//    }
//    else{
//        return nil;
//    }
//}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(indexPath.section == kBusinessInfoTableSectionBusinessInfo){
//        if(indexPath.row == 0){
//            return 88;
//        }
//    }
    return 44;
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    if(section == kBusinessInfoTableSectionBusinessInfo){
//        return kBusinessInfoTableTitleHeaderBusinessInfo;
//    }
//    else if(section == kBusinessInfoTableSectionDetailsInfo){
//        return kBusinessInfoTableTitleHeaderDetailsInfo;
//        
//    }
//    else if(section == kBusinessInfoTableSectionHoursInfo){
//        return kBusinessInfoTableTitleHeaderHoursInfo;
//    }
//    return @"";
//}
//



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setPoiObject:(OXPOIModel *)poiObject{
    _poiObject = poiObject;

    if(poiObject.socialContact){
        NSMutableDictionary *socialContactDictionary = [[NSMutableDictionary alloc] init];
        for(NSString *key in self.poiObject.socialContact.allKeys){
            
            if( ! [[self.poiObject.socialContact objectForKey:key] isKindOfClass:[NSNull class]]){
                NSString *objectValue = [self.poiObject.socialContact objectForKey:key];
                
                NSLog(@"SOCIAL CONTACT STRING: .%@.", objectValue);
                if([objectValue length] > 0){
                    [socialContactDictionary setObject:objectValue forKey:key];
                }
            }
        }
        self.poiObject.socialContact = socialContactDictionary;
    }

    
//    NSLog(@"BUSINESS INFORMATION PRESENTATION TABLE VIEW CONTROLLER poi object set");
//    for(OXBusinessHoursModel *currentHour in poiObject.openingHours){
//        NSLog(@"weekday: %@ time: %@", [currentHour daysRepresentation], [currentHour hoursRepresentation]);
//    }

    [self ammendHoursDidSaveBusinessHours:self.poiObject.openingHours];
    
    [self.tableView layoutIfNeeded];
    
    CGSize tableViewSize = self.tableView.contentSize;
    CGRect viewFrame = self.view.frame;
    viewFrame.size = tableViewSize;
//    viewFrame.size.height += kDetailPOIVCScrollViewViewControllerSpaceDivider;
    self.view.frame = viewFrame;
    
//    [self.delegate updateDetailPOIScrollViewContentSize];
}
#pragma mark - TTTAttributedLabel - Methods
- (void)attributedLabel:(TTTAttributedLabel *)label
didSelectLinkWithPhoneNumber:(NSString *)phoneNumber{
    
    if(label.tag == kExtraInfoTableViewCellIndexPathRowLabelTagTwitter){
        NSString *twitterUsername = [phoneNumber stringByReplacingOccurrencesOfString:@"@" withString:@""];
        
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]]){
            
            NSString *twitterLink = [NSString stringWithFormat:@"twitter://user?screen_name=%@", twitterUsername] ;
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterLink]];
        }
        else{
            NSString *twitterLink = [NSString stringWithFormat:@"https://www.twitter.com/%@", twitterUsername];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: twitterLink]];
        }
        
        [Flurry logEvent:kFlurryPlaceTwitterHandle withParameters:[self getFlurryParameters]];
    }
    else if(label.tag == kExtraInfoTableViewCellIndexPathRowLabelTagPhone){
        NSLog(@"attributed label%@", label);
        NSLog(@"phone number: %@", phoneNumber);
        NSString *strippedPhoneNumber;
        NSMutableCharacterSet *allowedCharacters = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
        [allowedCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@"+"]];
        NSCharacterSet *doNotWant = [allowedCharacters invertedSet];

        strippedPhoneNumber = [[phoneNumber componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
        NSLog(@"stripped: %@", strippedPhoneNumber); // => foobarbazfoo
        NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", strippedPhoneNumber]];
        NSLog(@"phone url: %@", phoneURL);
        if([[UIApplication sharedApplication] canOpenURL:phoneURL]){
            NSLog(@"phone url can open");
            [[UIApplication sharedApplication] openURL:phoneURL];
        }
        else{
        }
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label
  didSelectLinkWithURL:(NSURL *)url{
    if([[UIApplication sharedApplication] canOpenURL:url]){
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - OXFlurryProtocol delegate methods

- (NSDictionary *)getFlurryParameters
{
    NSDictionary *params =
    [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld", (long)self.poiObject.poiID],
kBasePOIPoiIdKey, // Parameter Name
     nil];
    
    return params;
}

@end
