//
//  OXAmendHoursTableViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 18/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OXAmendHoursDelegate <NSObject>
@required
-(void)ammendHoursDidSaveBusinessHours:(NSArray*)businessHours;

@end

@interface OXAmendHoursTableViewController : UITableViewController<UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, retain) NSMutableArray *businessHours;
@property (nonatomic, weak) id <OXAmendHoursDelegate> delegate;


@end
