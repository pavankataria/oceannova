//
//  OXReportTextViewController.h
//  OceanX
//
//  Created by Systango on 16/02/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXViewController.h"
#import "OXReportDataModel.h"


@protocol ReportMessageDelegate
-(void)didEnteredMessageForReport:(OXReportDataModel *)reportData;
@end

@interface OXReportTextViewController : OXViewController
@property (nonatomic, strong) OXReportDataModel *reportData;
@property (nonatomic, weak) id <ReportMessageDelegate> delegate;

@end
