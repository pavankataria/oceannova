//
//  OXFriendsTableViewController.m
//  Ocean
//
//  Created by Systango on 01/02/2015.
//  Copyright (c) 2015 Ocean Technologies. All rights reserved.
//

#define REQUEST_SECTION 0
#define FRIENDS_SECTION 1

#import "OXFriendsTableViewController.h"
#import "OXFriendTableViewCell.h"
#import "OXFriendRequestTableViewCell.h"

@interface OXFriendsTableViewController ()

@property (nonatomic,strong) OXFriendModel *selectedFriend;

@end

@implementation OXFriendsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.friends = [NSMutableArray array];
    self.searchResults = [NSMutableArray array];
    self.friendRequests = [NSMutableArray array];
    self.friendRequestsSent = [NSMutableArray array];
    self.searchBar.delegate = self;
    [self loadContent];
    [self setupNavigtionBar];
    [Flurry logEvent:kFlurryFriendView];
}


#pragma mark -  AlertView delegate method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self unfollowFriend:self.selectedFriend];
    }
}

#pragma mark - Private methods

- (void)showAlertViewOnUnfriend:(OXFriendModel *) friend
{
    UIAlertView * closeAlertView = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:kUnfriendConfirmationAlertTitle, friend.name] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes" ,nil];
    
    [closeAlertView show];
}

-(BOOL) isFriendsWith:(OXFriendModel *) friend{
    for (OXFriendModel * tempFriend in self.friends) {
        if (tempFriend.personId == friend.personId) {
            return YES;
        }
    }
    return NO;
}

-(FriendStatus) friendStatusWith:(OXFriendModel *) friend
{
    for (OXFriendModel * tempFriend in self.friends) {
        if (tempFriend.personId == friend.personId) {
            return StatusUnFriend;
        }
    }
    
    for (OXNotificationModel * sentNotification in self.friendRequestsSent) {
        if (sentNotification.recieverId == friend.personId) {
            return StatusPending;
        }
    }
    return StatusAddFriend;
}

-(void) loadRequests{
    
    [OXRestAPICient getFriendRequestsWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         self.friendRequests = [OXNotificationModel arrayOfNotificationsFromJSONDictionary:response];
         [self.tableView reloadData];
         [self.refreshControl endRefreshing];
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
         [self.refreshControl endRefreshing];
     }];
}

-(void) loadSentRequests{
    
    [OXRestAPICient getSentFriendRequestsWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         self.friendRequestsSent = [OXNotificationModel arrayOfNotificationsFromJSONDictionary:response];
         [self.tableView reloadData];
         [self.refreshControl endRefreshing];
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
         [self.refreshControl endRefreshing];
     }];
}

-(void) loadFriends {
    [OXRestAPICient getFriendsListWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         self.friends = [OXFriendModel getFriendsArrayFromJSONResponse:response];
         [self.tableView reloadData];
         [self.refreshControl endRefreshing];
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
         [self.refreshControl endRefreshing];
     }];
}

-(void) loadContent {
    [self.refreshControl beginRefreshing];
    [self loadRequests];
    [self loadFriends];
    [self loadSentRequests];
    
}

-(void) searchWithSearchTerm:(NSString*) searchTerm{
    
    [self.refreshControl beginRefreshing];
    
    self.hasSearchResults = NO;
    
    [OXRestAPICient searchUsersWithSearchTerm:searchTerm withSuccessBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         self.searchResults = [OXFriendModel getFriendsArrayFromJSONResponse:response];
         self.hasSearchResults = YES;
         [self.tableView reloadData];
         [self.refreshControl endRefreshing];
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
         [self.refreshControl endRefreshing];
     }];
}

-(void) userDidTapAccept:(OXNotificationModel *)notification{
    
    [self.refreshControl beginRefreshing];
    
    [OXRestAPICient acceptFriendRequestForNotification:notification.notificationId withSuccessBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         [self loadContent];
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
         [self.refreshControl endRefreshing];
     }];
    
}

-(void) userDidTapReject:(OXNotificationModel *)notification
{
    [self.refreshControl beginRefreshing];
    
    [OXRestAPICient denyFriendRequestForNotification:notification.notificationId withSuccessBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         [self loadContent];
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
         [self.refreshControl endRefreshing];
     }];
}

-(void) unfollowFriend:(OXFriendModel *) friend{
    
    [OXRestAPICient unfriendUserWithID:friend.personId withSuccessBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         [self.friends removeObject:friend];
         [self.tableView reloadData];
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
     }];
}

-(void) followFriend:(OXFriendModel *) friend{
    [OXRestAPICient addFriendWithID:friend.personId withSuccessBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         [self.friendRequestsSent addObject:[[OXNotificationModel alloc] initWithReceiverId:friend.personId recieverName:friend.username]];
         [self.tableView reloadData];
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
     }];
}

-(void) userTappedFollowForFriendCell:(OCNFriendTableViewCell *)cell atIndexPath:(NSIndexPath *)path{
    OXFriendModel * friend;
    if (self.searchResults.count && self.hasSearchResults) {
        friend = self.searchResults[path.row];
    }else if(path.section == FRIENDS_SECTION){
        friend = self.friends[path.row];
    }else if(path.section == REQUEST_SECTION){
        friend = self.friendRequests[path.row];
    }
    
    if ((self.searchResults.count && self.hasSearchResults) || !path.section == REQUEST_SECTION){
        
        if([self isFriendsWith:friend]) {
            self.selectedFriend = friend;
            [self showAlertViewOnUnfriend:friend];
        }else{
            [self followFriend:friend];
        }
    }
}

-(void)setupNavigtionBar
{

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);

}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - UISearchBarDelegate

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([searchText isEqualToString:@""]) {
        
        self.searchResults = [NSMutableArray array];
        self.hasSearchResults = NO;
        [self.tableView reloadData];
        
    }else{
        
        [self searchWithSearchTerm:searchText];
        
    }
    
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar resignFirstResponder];
    [self searchWithSearchTerm:searchBar.text];
    
}

#pragma mark - IBAction methods

-(IBAction)refresh:(id)sender{
    [self loadContent];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.searchResults.count && self.hasSearchResults) {
        return 1;
    }else{
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (self.searchResults.count && self.hasSearchResults) {
        return self.searchResults.count;
    }else if(section == REQUEST_SECTION){
        return self.friendRequests.count;
    }else if(section == FRIENDS_SECTION){
        return self.friends.count;
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searchResults.count && self.hasSearchResults)
    {
        return [self searchTableCell:tableView indexPath:indexPath];
    }
    else if(indexPath.section == REQUEST_SECTION){
        OXFriendRequestTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellFriendRequestIdentifier forIndexPath: indexPath];
        [cell updateWithNotification:self.friendRequests[indexPath.row] forIndexPath:indexPath withDelegate:self];
        return cell;
    }
    else if(indexPath.section == FRIENDS_SECTION){
        OXFriendTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellFriendIdentifier forIndexPath:indexPath];
        [cell updateWithFriend:self.friends[indexPath.row] withDelegate:self atIndexPath:indexPath friendStatus:[self friendStatusWith:self.friends[indexPath.row]]];
        return cell;
    }
    
    return nil;
}

-(UITableViewCell *)searchTableCell:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    OXFriendModel *friend = self.searchResults[indexPath.row];
    NSUInteger friendRequestIndex = [self.friendRequests indexOfObject:[[OXNotificationModel alloc] initWithSenderId:friend.personId senderName:friend.username]];
    
    if(friendRequestIndex == NSNotFound)
    {
        OXFriendTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellFriendIdentifier forIndexPath:indexPath];
        [cell updateWithFriend:friend withDelegate:self atIndexPath:indexPath friendStatus:[self friendStatusWith:friend]];
        return cell;
    }
    else
    {
        OXNotificationModel *notification = [self.friendRequests objectAtIndex:friendRequestIndex];
        OXFriendRequestTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellFriendRequestIdentifier forIndexPath: indexPath];
        [cell updateWithNotification:notification forIndexPath:indexPath withDelegate:self];
        return cell;
    }
}

//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//}


@end

