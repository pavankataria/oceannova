//
//  OXTutorialManagerPageViewController.m
//  OceanX
//
//  Created by Systango on 2/13/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXTutorialManagerPageViewController.h"

@interface OXTutorialManagerPageViewController ()<UIPageViewControllerDataSource>
{
    NSMutableArray *viewControllers;
}

@end

@implementation OXTutorialManagerPageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataSource = self;
    
    viewControllers = [[NSMutableArray alloc] init];
    [viewControllers addObject:kSettingTutorialFirstScreen];
    [viewControllers addObject:kSettingTutorialSecondScreen];
    [viewControllers addObject:kSettingTutorialEighthScreen];
//    [viewControllers addObject:kSettingTutorialThirdScreen];
    [viewControllers addObject:kSettingTutorialSeventhScreen];
    [viewControllers addObject:kSettingTutorialNinthScreen];
    [viewControllers addObject:kSettingTutorialTenthScreen];
    [viewControllers addObject:kSettingTutorialEleventhScreen];
    [viewControllers addObject:kSettingTutorialTwelfthScreen];
    
    [self setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:viewControllers[0]]]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:YES completion:nil];
    
    [self setupNavigtionBar];
    
    [Flurry logEvent:kFlurryTutorial];
}
-(void)setupNavigtionBar{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cross"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
}

- (void)backButtonTapped:(id)sender{
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kTutorialViewDidFinishNowDoesFBRecommendationsNeedToBeDisplayedNotification object:self];

    }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PageView Controller Data Source Method

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger vcIndex = [viewControllers indexOfObject:viewController.restorationIdentifier];
    vcIndex++;
    
    return [self viewControllerAtIndex:vcIndex];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger vcIndex = [viewControllers indexOfObject:viewController.restorationIdentifier];
    vcIndex--;
    
    return [self viewControllerAtIndex:vcIndex];
    
}

#pragma mark - IBAction Method

- (IBAction)cancelButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private Method

- (UIViewController*)viewControllerAtIndex:(NSUInteger)index
{
    if((viewControllers.count == 0) || (index >= viewControllers.count))
    {
        return nil;
    }
    else
    {
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:viewControllers[index]];
        return viewController;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}

@end
