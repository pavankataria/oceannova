//
//  OXQuestionViewController.m
//  OceanX
//
//  Created by Systango on 2/9/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXQuestionViewController.h"
#import "OXQuestionSaveViewController.h"
#import "OXQuestionModel.h"

@interface OXQuestionViewController ()

@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *nextFacetButton;
@property (weak, nonatomic) IBOutlet UILabel  *questionLabel;
@property (weak, nonatomic) IBOutlet UIButton *stronglyDisagreeButton;
@property (weak, nonatomic) IBOutlet UIButton *disagreeButton;
@property (weak, nonatomic) IBOutlet UIButton *neitherButton;
@property (weak, nonatomic) IBOutlet UIButton *agreeButton;
@property (weak, nonatomic) IBOutlet UIButton *stronglyAgreeButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (strong, nonatomic) OXFacetModel *facet;
@property (strong, nonatomic) OXQuestionModel *question;

@end

@implementation OXQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.currentQuestionIndex == 1)
    {
        self.backButton.hidden = YES;
    }
    if (self.missingFacets.count)
    {
        self.facet = self.missingFacets[self.currentFacetIndex - 1];
        
        if(self.facet.questions.count > (self.currentQuestionIndex - 1))
        {
            self.question = self.facet.questions[self.currentQuestionIndex - 1];
            NSLog(@"QUESTION: %@", self.question);
            self.questionLabel.text = self.question.questionText;
        }
    }
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [Flurry logEvent:kFlurryProfileQuestionView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Methods

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)facetAnswerButtonTapped:(id)sender
{
    [self facetAnswer:sender];
}

- (IBAction)closeButtonTapped:(id)sender
{
    [self showAlertViewOnCloseButton];
}

#pragma mark Private Methods

- (void)showNextFacetButton
{
    [UIView animateWithDuration:1 animations:^{
        
        self.nextButton.hidden = (self.currentQuestionIndex == kOXQuestionTotalQuestionPerFacet)? YES :NO;
        self.nextFacetButton.hidden = !self.nextButton.hidden;
    }];
}

- (void)showAlertViewOnCloseButton
{
    UIAlertView * closeAlertView = [[UIAlertView alloc] initWithTitle:kQuestionCloseConfirmationAlertTitle message:kQuestionCloseConfirmationAlertMessage delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes" ,nil];
    
    [closeAlertView show];
}

- (void)setUnselectButtonBackgroundColor: (UIColor *)color
{
    self.stronglyDisagreeButton.backgroundColor = color;
    self.neitherButton.backgroundColor = color;
    self.disagreeButton.backgroundColor = color;
    self.agreeButton.backgroundColor = color;
    self.stronglyAgreeButton.backgroundColor = color;
}

- (void)facetAnswer:(UIButton *)sender
{
    [self setUnselectButtonBackgroundColor:[UIColor whiteColor]];
    
    
    
    CGFloat darkRed = 0.0f;
    CGFloat darkGreen = 0.61f;
    CGFloat darkBlue = 0.68f;
    sender.backgroundColor = [UIColor colorWithRed:darkRed green:darkGreen blue:darkBlue alpha:1];
    
    // Can directly assign button.tag in question.answer
    
    switch (sender.tag)
    {
        case kQuestionAnswerStronglyDisagree:
            self.question.answer = kQuestionAnswerStronglyDisagree;
            break;
            
        case kQuestionAnswerDisagree:
            self.question.answer = kQuestionAnswerDisagree;
            break;
            
        case kQuestionAnswerNeither:
            self.question.answer = kQuestionAnswerNeither;
            break;
            
        case kQuestionAnswerAgree:
            self.question.answer = kQuestionAnswerAgree;
            break;
            
        case kQuestionAnswerStronglyAgree:
            self.question.answer = kQuestionAnswerStronglyAgree;
            break;
            
        default:
            break;
    }
    
    [self showNextFacetButton];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueIdentifierQuestionSaveViewController])
    {
        OXQuestionSaveViewController * destination = segue.destinationViewController;
        destination.missingFacets = self.missingFacets;
        destination.currentFacetIndex = self.currentFacetIndex;
    }
    
    else if([segue.identifier isEqualToString:kSegueIdentifierQuestionViewController])
    {
        OXQuestionViewController * destination = segue.destinationViewController;
        destination.missingFacets = self.missingFacets;
        destination.currentFacetIndex = self.currentFacetIndex;
        destination.currentQuestionIndex = self.currentQuestionIndex + 1;
    }
}

#pragma mark -  AlertView delegate method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];

    }
}

@end
