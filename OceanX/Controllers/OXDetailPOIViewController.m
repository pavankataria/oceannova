//
//  OXDetailPOIViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 17/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXDetailPOIViewController.h"
#import "PKThumbnailSliderView.h"
#import "PKPreviewSliderView.h"
#import "PKLikeDislikeShareBarView.h"
#import "OXTagsPresentationTableViewController.h"
#import "OXContactUsPresentationViewController.h"
#import "RDActivityViewController.h"
#import "OXAddTagPresentationViewController.h"
#import "OXReportTableViewController.h"
#import "OXMapPresentationViewController.h"
#import "OXCameraCaptureScreenViewController.h"
#import "MBGalleryViewController.h"
#import "OXProcessedPicture.h"
#import "PKEditMediaThumbnailView.h"
#import "OXLoadingAnimation.h"
#import "OXCaptionPresentationViewController.h"
#import "OXFlurryProtocol.h"
#import "PKBusinessInformationPresentationTableViewController.h"

#import "OXReportButtonPresentationViewController.h"
const CGFloat mapViewHeight = 100;

@interface OXDetailPOIViewController ()<UIActionSheetDelegate, OXFlurryProtocol>
@property (nonatomic, retain) PKThumbnailSliderView *thumbnailSliderView;
@property (nonatomic, retain) PKPreviewSliderView *previewSliderView;
@property (nonatomic, retain) PKLikeDislikeShareBarView *likeDislikeShareBarView;
@property (nonatomic, retain) OXTagsPresentationTableViewController *tagsPresentationView;
@property (nonatomic, retain) OXContactUsPresentationViewController *contactUsVC;

@property (nonatomic, retain) PKBusinessInformationPresentationTableViewController *businessInfoVC;

@property (nonatomic, retain) OXMapPresentationViewController *mapVC;
@property (nonatomic, strong) UIButton *expandMapButton;
@property (nonatomic, assign) BOOL alreadySetupViews;
@property (nonatomic, retain) OXLoadingAnimation *loadingAnimation;

@property (nonatomic, retain) OXCaptionPresentationViewController *captionPresentationVC;
@property (nonatomic, retain) OXReportButtonPresentationViewController *reportPresentationVC;
@property (nonatomic, retain) NSMutableArray *dividerArray;
@end

@implementation OXDetailPOIViewController
#pragma mark - lazily load - Methods

-(NSMutableArray *)dividerArray{
    if(!_dividerArray){
        _dividerArray = [[NSMutableArray alloc] init];
    }
    return _dividerArray;
}
-(void)viewWillDisappear:(BOOL)animated{
    [self contractMapView];
    [self.navigationController.navigationBar setTitleTextAttributes:
    [NSDictionary dictionaryWithObjectsAndKeys:
     [UIFont fontWithName:@"HelveticaNeue" size:25],
     NSFontAttributeName, [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0], NSForegroundColorAttributeName, nil]];
    [super viewWillDisappear:animated];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}
-(UIView*)aNewDividerView{
    UIView *dividerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.detailPageScrollView.frame), 20)];
    [dividerView setBackgroundColor:[UIColor blueColor]];
    CGFloat margin = 20;
    CGFloat heightOfDividerView = 1;
    
    UIView *dividerViewLine = [[UIView alloc] initWithFrame:CGRectMake(margin, dividerView.frame.size.height/2-heightOfDividerView/2, CGRectGetWidth(dividerView.frame)-(2*margin), heightOfDividerView)];
    [dividerViewLine setBackgroundColor:[UIColor colorWithRed:0.89 green:0.89 blue:0.89 alpha:1]];
    [dividerView addSubview:dividerViewLine];
    
    return dividerView;
}
-(OXReportButtonPresentationViewController *)reportPresentationVC{
    if(!_reportPresentationVC){
        _reportPresentationVC = [[OXReportButtonPresentationViewController alloc] initWithNibName:@"OXReportButtonPresentationViewController" bundle:nil];
        _reportPresentationVC.delegate = self;
        
        CGRect frame = _reportPresentationVC.view.frame;
        frame = CGRectMake(0, 0, CGRectGetWidth(self.detailPageScrollView.frame), 70);
        _reportPresentationVC.view.frame = frame;
    }
    return _reportPresentationVC;
}
-(OXMapPresentationViewController*)mapVC{
    if(!_mapVC){
        _mapVC = [[OXMapPresentationViewController alloc] init];
        //        Change the frame size of viewcontroller.view.frame, and then add to subview. [viewcontrollerparent.view addSubview:viewcontroller.view]
        CGRect frame = _mapVC.view.frame;
        frame = CGRectMake(0, 0, CGRectGetWidth(self.detailPageScrollView.frame), 200);
        _mapVC.view.frame = frame;
//        _mapVC.delegate = self;
    }
    return _mapVC;
}

-(PKBusinessInformationPresentationTableViewController *)businessInfoVC{
    if (!_businessInfoVC) {
        _businessInfoVC = [[PKBusinessInformationPresentationTableViewController alloc] initWithStyle:UITableViewStylePlain];
//        _businessInfoVC.delegate = self;
    }
    return _businessInfoVC;
}
-(OXContactUsPresentationViewController *)contactUsVC{
    if (!_contactUsVC) {
        _contactUsVC = [[OXContactUsPresentationViewController alloc] init];
        CGRect frame = _mapVC.view.frame;
        if(IS_IPHONE_4_OR_LESS){
            frame = CGRectMake(0, 0, CGRectGetWidth(self.detailPageScrollView.frame), 70);
        }
        else{
            frame = CGRectMake(0, 0, CGRectGetWidth(self.detailPageScrollView.frame), 100);
        }
        _contactUsVC.view.frame = frame;
//        _contactUsVC.delegate = self;
    }
    return _contactUsVC;
}
-(OXTagsPresentationTableViewController *)tagsPresentationView{
    if(!_tagsPresentationView){
        _tagsPresentationView = [[OXTagsPresentationTableViewController alloc] initWithStyle:UITableViewStylePlain];
        _tagsPresentationView.delegate = self;
    }
    return _tagsPresentationView;
}
-(PKThumbnailSliderView *)thumbnailSliderView{
    if(!_thumbnailSliderView){
        
        if(IS_IPHONE_4_OR_LESS){
            _thumbnailSliderView = [[PKThumbnailSliderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.detailPageScrollView.frame), 100)];
        }
        else{
            _thumbnailSliderView = [[PKThumbnailSliderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.detailPageScrollView.frame), 140)];
        }
        //        _thumbnailSliderView.images = self.poiObject.pictures;
        _thumbnailSliderView.delegate = self.previewSliderView;
        _thumbnailSliderView.parentDelegate = self;
    }
    return _thumbnailSliderView;
}

-(PKPreviewSliderView *)previewSliderView{
    if(!_previewSliderView){
        _previewSliderView = [[PKPreviewSliderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.detailPageScrollView.frame), CGRectGetWidth(self.detailPageScrollView.frame))];
        _previewSliderView.delegate = self.thumbnailSliderView;
    }
    return _previewSliderView;
}
-(PKLikeDislikeShareBarView *)likeDislikeShareBarView{
    if(!_likeDislikeShareBarView){
        _likeDislikeShareBarView = [[PKLikeDislikeShareBarView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.detailPageScrollView.frame), 60)];
        _likeDislikeShareBarView.delegate = self;
        
        
        [_likeDislikeShareBarView.likeButton addTarget:self action:@selector(likeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_likeDislikeShareBarView.dislikeButton addTarget:self action:@selector(dislikeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeDislikeShareBarView;
}

-(OXCaptionPresentationViewController*)captionPresentationVC{
    if(!_captionPresentationVC){
        _captionPresentationVC = [[OXCaptionPresentationViewController alloc] init];
        CGRect frame = _mapVC.view.frame;
        frame = CGRectMake(0, 0, CGRectGetWidth(self.detailPageScrollView.frame), 0);
        _captionPresentationVC.view.frame = frame;
//        _captionPresentationVC.delegate = self;

    }
    return _captionPresentationVC;
}

#pragma mark - UIViewController life cycle - Methods
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _alreadySetupViews = FALSE;
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Nabila" size:25],
      NSFontAttributeName, [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0], NSForegroundColorAttributeName, nil]];

    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigtionBar];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
//    self.loadingAnimation.frame = self.view.bounds;
//    [self.loadingAnimation centerAnimation];
    
//    [self.loadingAnimation showAnimation];
    
    [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Loading %@", self.parentPOIObject.poiName] maskType:SVProgressHUDMaskTypeNone];

    
    [OXRestAPICient getFullPOIDetailsWithPOIID:self.poiID
                                  successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                      self.fullPOIObject = [[OXPOIModel alloc] initWithDictionary:response];
                                      
                                      [self updateViews];
                                      NSLog(@"done loading everything");
                                      
//                                      [self.loadingAnimation hideAnimation];
                                      [SVProgressHUD dismiss];
                                      
                                      [Flurry logEvent:kFlurryPlaceDetail  withParameters:[self getFlurryParameters]];
                                      
                                  }
                                     failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                         NSLog(@"failed operation: %@", operation);
                                         NSLog(@"error: %@", error);
//                                         [self.loadingAnimation hideAnimation];
                                         [SVProgressHUD dismiss];
                                         
                                     }];
    
}

-(OXLoadingAnimation *)loadingAnimation{
    if(!_loadingAnimation){
        _loadingAnimation = [[OXLoadingAnimation alloc] init];
        [_loadingAnimation beginAnimation];
        [self.view addSubview:_loadingAnimation];
    }
    return _loadingAnimation;
}
-(void)updateViews{
    [self setupViews];
    NSLog(@"self.fullpoiobject.pictures.coutn: %lu", (unsigned long)self.fullPOIObject.pictures.count);
    self.thumbnailSliderView.images = self.fullPOIObject.picturesProperlyProcessed;
    self.captionPresentationVC.poiObject = self.fullPOIObject;
    
    self.likeDislikeShareBarView.poiObject = self.fullPOIObject;
    //    [self.likeDislikeShareBarView setBackgroundColor:[UIColor redColor]];
    
    self.tagsPresentationView.poiObject = self.fullPOIObject;
    //    [self.tagsPresentationView.view setBackgroundColor:[UIColor greenColor]];
    
    if(self.fullPOIObject.openingHours){
        self.businessInfoVC.poiObject = self.fullPOIObject;
    }
    if(self.fullPOIObject.socialContact){
        self.contactUsVC.poiObject = self.fullPOIObject;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Nabila" size:25],
      NSFontAttributeName, [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0], NSForegroundColorAttributeName, nil]];
    self.title = self.fullPOIObject.poiName;
    self.mapVC.poiObject = self.fullPOIObject;
    self.reportPresentationVC.poiObject = self.fullPOIObject;
    
    [self updateDetailPOIScrollViewContentSize];
}


-(void)showCloseMapButton{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"cross"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] style:UIBarButtonItemStylePlain target:self action:@selector(contractMapView)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    
}

-(void)hideCloseMapButton{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
}

-(void)expandMapView{
    self.expandMapButton.hidden = YES;
    
    
    [self.mapVC mapDidExpand];
    
    CGRect frame = CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    self.mapVC.mapView.frame = frame;
    
    frame = CGRectMake(0, self.detailPageScrollView.contentOffset.y,[[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-self.navigationController.navigationBar.frame.size.height-self.tabBarController.tabBar.frame.size.height);

    self.mapVC.view.frame = frame;

    NSLog(@"mapVC.MapView: %@", NSStringFromCGRect(self.mapVC.mapView.frame));
    NSLog(@"mapVC: %@", NSStringFromCGRect(self.mapVC.view.frame));
    
    [UIView commitAnimations];
    [self.mapVC enableMapViewInteraction:YES];
    [self showCloseMapButton];
}

-(void)contractMapView{
    self.expandMapButton.hidden = NO;
    [self hideCloseMapButton];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    self.mapVC.mapView.frame = [self.mapVC frameOfOriginalMapFrame];
    self.mapVC.view.frame = CGRectMake(0, CGRectGetMaxY(self.tagsPresentationView.view.frame), self.view.frame.size.width, [self.mapVC heightOfMapView]);
    [self.mapVC mapDidContract];
    [UIView commitAnimations];
    //self.expandMapButton.layer.hidden = NO;
    [self.mapVC enableMapViewInteraction:NO];
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if(_alreadySetupViews == FALSE){
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)setupViews{
    
    [[self.detailPageScrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _alreadySetupViews = YES;
    //    CGRect frame;
    //View 1 - thumbnail slider
    [self.detailPageScrollView addSubview:self.thumbnailSliderView];
    
    ////    View 2 - preview slider
    //    frame = self.previewSliderView.frame;
    //    frame.origin.y = CGRectGetMaxY(self.thumbnailSliderView.frame);
    //    self.previewSliderView.frame = frame;
    //    [self.detailPageScrollView addSubview:self.previewSliderView];
    //
    
    //View 3 - like, dislike, and share bar
    //    frame = self.likeDislikeShareBarView.frame;
    //    frame.origin.y = CGRectGetMaxY(self.thumbnailSliderView.frame);
    [self.detailPageScrollView addSubview:self.likeDislikeShareBarView];
    //    self.likeDislikeShareBarView.frame = frame;
    
    
    [self.detailPageScrollView addSubview:self.captionPresentationVC.view];
    
    //View 4 - tags presentation view
    //    frame = self.tagsPresentationView.view.frame;
    //    frame.origin.y = CGRectGetMaxY(self.likeDislikeShareBarView.frame);
    [self.detailPageScrollView addSubview:self.tagsPresentationView.view];
    //    self.tagsPresentationView.view.frame = frame;
    
    
    
    
//    if(self.fullPOIObject.openingHours){
//        NSLog(@"DOES HAVE OPENING HOURS");
        [self.detailPageScrollView addSubview:self.businessInfoVC.view];
//    }
//    if(self.fullPOIObject.socialContact){
        [self.detailPageScrollView addSubview:self.contactUsVC.view];
//    }
//    else{
//        NSLog(@"DOESNT HAVE OPENING HOURS");
//        NSLog(@"opening hours: %@", self.fullPOIObject.openingHours);
//        NSLog(@"social contact: %@r", self.fullPOIObject.socialContact);
//    }
    
    //View 5 - maps page
    
    [self.detailPageScrollView addSubview:self.mapVC.view];
    
    //View 6 - button for expanding map
    
    self.expandMapButton = [[UIButton alloc] init];
    [self.expandMapButton addTarget:self action:@selector(expandMapView) forControlEvents:UIControlEventTouchUpInside];
    [self.detailPageScrollView addSubview:self.expandMapButton];
    
    
    
    [self.detailPageScrollView insertSubview:self.reportPresentationVC.view belowSubview:self.mapVC.view];
    //View 7 - add gradient to button
    
    /* UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 50.0f)];
     CAGradientLayer *gradient = [CAGradientLayer layer];
     gradient.frame = view.bounds;
     gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor], (id)[[UIColor blackColor] CGColor], nil];
     [view.layer insertSublayer:gradient atIndex:0];*/
    
}
//
//-(void)setpoiObject:(OXPOISearchByListModel *)poiObject{
//    _poiObject = poiObject;
//    self.thumbnailSliderView.images = _poiObject;
//}
//

#pragma mark - OXDetailPOIProtocol delegate - Methods
-(void)displayAddTagsViewControllerWithControllerType:(OXTagsPresentationTableViewComingFromVCType)comingFromVCType{
    UIStoryboard * reportingScreenStoryBoard = [UIStoryboard storyboardWithName:@"AddTagsScreenSB" bundle:nil];
    OXAddTagPresentationViewController *reportScreen = [reportingScreenStoryBoard instantiateInitialViewController];
    
    reportScreen.comingFromVC = OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags;
    reportScreen.poiObject = self.fullPOIObject;
    
    [Flurry logEvent:kFlurryMorePlaceTags withParameters:[self getFlurryParameters]];
    
    [self.navigationController pushViewController:reportScreen animated:YES];
}
-(void)displayAddTagsViewController{
    UIStoryboard * reportingScreenStoryBoard = [UIStoryboard storyboardWithName:@"AddTagsScreenSB" bundle:nil];
    OXAddTagPresentationViewController *reportScreen = [reportingScreenStoryBoard instantiateInitialViewController];
    
    reportScreen.comingFromVC = OXTagsPresentationTableViewComingFromVCTypeDetailPOIVC;
    reportScreen.poiObject = self.fullPOIObject;
    //        OXReportDataModel *reportData = [[OXReportDataModel alloc] initWithId:((OXBasePOIModel *)[self.mapElements lastObject]).poiID andSource:ReportTypePOI];
    //        reportScreen.reportData = reportData;
    
    //    reportScreen.storedTagsArray = self.fullPOIObject.tags;
    
    [Flurry logEvent:kFlurryPlaceTags withParameters:[self getFlurryParameters]];
    
    [self.navigationController pushViewController:reportScreen animated:YES];
    
}
-(void)updateDetailPOIScrollViewContentSize{
    [self layoutSubviewsInScrollView];
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.detailPageScrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.detailPageScrollView.contentSize = contentRect.size;
}
-(void)layoutSubviewsInScrollView{
    CGRect frame;
    frame = self.thumbnailSliderView.frame;
    self.thumbnailSliderView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    
    UIView *viewDivider = [PKQuickMethods aNewDividerViewBelowFrame:self.thumbnailSliderView.frame];
    [self.detailPageScrollView addSubview:viewDivider];
    
    frame = self.likeDislikeShareBarView.frame;
    self.likeDislikeShareBarView.frame = CGRectMake(0, CGRectGetMaxY(viewDivider.frame), frame.size.width, frame.size.height);
    
    viewDivider = [PKQuickMethods aNewDividerViewBelowFrame:self.likeDislikeShareBarView.frame];
    [self.detailPageScrollView addSubview:viewDivider];

    
    frame = self.captionPresentationVC.view.frame;
    self.captionPresentationVC.view.frame = CGRectMake(0, CGRectGetMaxY(viewDivider.frame), frame.size.width, frame.size.height);
    
    viewDivider = [PKQuickMethods aNewDividerViewBelowFrame:self.captionPresentationVC.view.frame];
    [self.detailPageScrollView addSubview:viewDivider];

    frame = self.tagsPresentationView.view.frame;
    self.tagsPresentationView.view.frame = CGRectMake(0, CGRectGetMaxY(viewDivider.frame), frame.size.width, frame.size.height);
    
    
    viewDivider = [PKQuickMethods aNewDividerViewBelowFrame:self.tagsPresentationView.view.frame];
    [self.detailPageScrollView addSubview:viewDivider];

    frame = self.mapVC.view.frame;
    self.mapVC.view.frame = CGRectMake(0, CGRectGetMaxY(viewDivider.frame), frame.size.width, frame.size.height);
    
    self.expandMapButton.frame = self.mapVC.view.frame;
    
    viewDivider = [PKQuickMethods aNewDividerViewBelowFrame:self.mapVC.view.frame];
    [self.detailPageScrollView addSubview:viewDivider];

    if(self.fullPOIObject.openingHours.count){
        frame = self.businessInfoVC.view.frame;
        self.businessInfoVC.view.frame = CGRectMake(0, CGRectGetMaxY(viewDivider.frame), frame.size.width, frame.size.height);
        viewDivider = [PKQuickMethods aNewDividerViewBelowFrame:self.businessInfoVC.view.frame];
        [self.detailPageScrollView addSubview:viewDivider];
        

    }
    if(self.fullPOIObject.socialContact){
        frame = self.contactUsVC.view.frame;
        self.contactUsVC.view.frame = CGRectMake(0, CGRectGetMaxY(viewDivider.frame), frame.size.width, frame.size.height);
        
        viewDivider = [PKQuickMethods aNewDividerViewBelowFrame:self.contactUsVC.view.frame];
        [self.detailPageScrollView addSubview:viewDivider];

    }

    
    self.detailPageScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.mapVC.view.frame), CGRectGetMaxY(frame));
    

    frame = self.reportPresentationVC.view.frame;
    self.reportPresentationVC.view.frame = CGRectMake(0, CGRectGetMaxY(viewDivider.frame), frame.size.width, frame.size.height);
}



#pragma mark - PKLikeDislikeShareBarViewDelegate - Methods
-(void)didPressLike{
    NSLog(@"like button pressed");
}
-(void)didPressDislike{
    NSLog(@"dislike button pressed");
}
-(void)didPressShare{
    NSLog(@"SHARE BUTTON PRESSED");
    [self showShareSheet];
}
-(void)showShareSheet{
//    NSString *string = @"Hi";
//    NSURL *URL = [NSURL URLWithString:@"http://www.pavankataria.com"];
    
    //    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[string, URL] applicationActivities:nil];
    //
    //    activityViewController.excludedActivityTypes = @[
    //                                                     UIActivityTypePrint,
    //                                                     UIActivityTypeCopyToPasteboard,
    //                                                     UIActivityTypeAssignToContact,
    //                                                     UIActivityTypeSaveToCameraRoll,
    //                                                     UIActivityTypeAddToReadingList,
    //                                                     UIActivityTypeAirDrop];
    //
    //    [self.navigationController presentViewController:activityViewController
    //                                            animated:YES
    //                                          completion:^{
    //                                              NSLog(@"completion");
    //                                          }];
    
    RDActivityViewController *viewController = [[RDActivityViewController alloc] initWithDelegate:self];
    viewController.excludedActivityTypes = @[
                                             UIActivityTypePrint,
                                             UIActivityTypeCopyToPasteboard,
                                             UIActivityTypeAssignToContact,
                                             UIActivityTypeSaveToCameraRoll,
                                             UIActivityTypeAddToReadingList,
                                             UIActivityTypeAirDrop];
    
    [self.navigationController presentViewController:viewController
                                            animated:YES
                                          completion:^{
                                              NSLog(@"completion");
                                          }];
    
}

//- (id)activityViewController:(UIActivityViewController *)activityViewController
//         itemForActivityType:(NSString *)activityType
//{
//    NSString *message = nil;
//    if ([activityType isEqualToString:UIActivityTypeMail]) {
//        message = @"text for email";
//    } else if ([activityType isEqualToString:UIActivityTypeMessage]) {
//        message = @"text for iMessage";
//    } else if ([activityType isEqualToString:UIActivityTypePostToFacebook]) {
//        message = @"text for facebook";
//    } else if ([activityType isEqualToString:UIActivityTypePostToTwitter]) {
//        message = @"text for twitter.";
//    } else {
//        message = @"a default text";
//    }
//    return message;
//}

/*
 NSString *firstLine = [NSString stringWithFormat:@"Check out %@ ", self.place.name];
 NSUInteger totalCharactersUsedSoFar = [firstLine length];
 
 NSArray *finalTagsUsed = [[NSMutableArray alloc] init];
 
 NSString *tagDescriptionParagraph = [self calculateAndRetrieveAppropriateNumberOfTagsToDisplayWithTags:self.place.tags
 withStaticBodyTextLength:totalCharactersUsedSoFar
 withMaxCharacterLength:kMaxFacebookSheetCharacterCount
 isFacebookMessage:YES
 pointerToFinalTagsUsed:&finalTagsUsed];
 //If there are tags available THEN append the "known for" string.
 
 
 if([tagDescriptionParagraph length] > 0){
 tagDescriptionParagraph = [NSString stringWithFormat:@"tagged as %@", tagDescriptionParagraph];
 
 
 for(OCNTag * currentTag in finalTagsUsed){
 NSLog(@"affirmedByUser: %ld tag count: %ld", (long)currentTag.affirmedByUser, (long)currentTag.tagCount);
 }
 NSString *affirmationsString = [NSString stringWithFormat:@" by %d Ocean users.\n%@", [[finalTagsUsed valueForKeyPath:@"@sum.tagCount"] intValue], kFacebookMessageSheetDownloadPhrase];
 
 tagDescriptionParagraph = [tagDescriptionParagraph stringByAppendingString:affirmationsString];
 }
 
 
 NSString *bodyText = [firstLine stringByAppendingString:tagDescriptionParagraph];
 
 */


#pragma mark - RDActivityControllerDelegate - Methods
- (NSArray *)activityViewController:(NSArray *)activityViewController itemsForActivityType:(NSString *)activityType{
    if ([activityType isEqualToString:UIActivityTypePostToFacebook] || [activityType isEqualToString:UIActivityTypeMail] || [activityType isEqualToString:UIActivityTypeMessage]) {
        return @[[self facebookMessage], [NSURL URLWithString:@"http://oceanapp.com"]];
    }
    else if([activityType isEqualToString:UIActivityTypePostToTwitter]){
        return @[[self twitterMessage]];
    }
    
    return nil;
}

-(NSString*)twitterMessage{
    NSString *firstLine = [NSString stringWithFormat:@"Check out %@ ", self.fullPOIObject.poiName];
    NSString *lastLine = [NSString stringWithFormat:@"(via @OceanAppLondon)"];
    NSUInteger totalCharactersUsedSoFar = [firstLine length] + [lastLine length];
    
    NSString *tagDescriptionParagraph = [self calculateAndRetrieveAppropriateNumberOfTagsToDisplayWithTags:[self.fullPOIObject.tags mutableCopy]
                                                                                  withStaticBodyTextLength:totalCharactersUsedSoFar
                                                                                    withMaxCharacterLength:kMaxTwitterSheetCharacterCount
                                                                                         isFacebookMessage:NO
                                                                                    pointerToFinalTagsUsed:nil];
    //If there are tags available THEN append the "known for" string.
    //            if([tagDescriptionParagraph length] > 0) tagDescriptionParagraph = [NSString stringWithFormat:@"known for %@", tagDescriptionParagraph];;
    NSString *bodyText = [[firstLine stringByAppendingString:tagDescriptionParagraph] stringByAppendingString:lastLine];
    return bodyText;
}
-(NSString*)facebookMessage{
    NSString *firstLine = [NSString stringWithFormat:@"Check out %@ ", self.fullPOIObject.poiName];
    NSUInteger totalCharactersUsedSoFar = [firstLine length];
    
    NSArray *finalTagsUsed = [[NSMutableArray alloc] init];
    
    NSLog(@"OK HERE");
    NSString *tagDescriptionParagraph = [self calculateAndRetrieveAppropriateNumberOfTagsToDisplayWithTags:[self.fullPOIObject.tags mutableCopy]
                                                                                  withStaticBodyTextLength:totalCharactersUsedSoFar
                                                                                    withMaxCharacterLength:kMaxFacebookSheetCharacterCount
                                                                                         isFacebookMessage:YES
                                                                                    pointerToFinalTagsUsed:&finalTagsUsed];
    
    
    if([tagDescriptionParagraph length] > 0){
        tagDescriptionParagraph = [NSString stringWithFormat:@"tagged as %@", tagDescriptionParagraph];
        
        
        for(OXTagModel * currentTag in finalTagsUsed){
            NSLog(@"affirmedByUser: %ld tag count: %ld", (long)currentTag.tagHasAffirmed, (long)currentTag.tagCount);
        }
        NSString *affirmationsString = [NSString stringWithFormat:@" by %d Ocean users.\n%@", [[finalTagsUsed valueForKeyPath:@"@sum.tagCount"] intValue], kFacebookMessageSheetDownloadPhrase];
        
        tagDescriptionParagraph = [tagDescriptionParagraph stringByAppendingString:affirmationsString];
    }
    
    NSString *bodyText = [firstLine stringByAppendingString:tagDescriptionParagraph];
    return bodyText;
}
-(NSString*)calculateAndRetrieveAppropriateNumberOfTagsToDisplayWithTags:(NSMutableArray*)tagsArray
                                                withStaticBodyTextLength:(NSUInteger)staticCharactersUsedSoFar
                                                  withMaxCharacterLength:(NSInteger)maxCharacterLength
                                                       isFacebookMessage:(BOOL)isFacebookMessage
                                                  pointerToFinalTagsUsed:(NSArray **)finalTagsUsed{
    NSMutableArray *processedTagsArray = [[NSMutableArray alloc] init];
    NSMutableArray *finalTagsToDisplay = [[NSMutableArray alloc] init];
    NSMutableArray *finalTagObjectsUsed = [[NSMutableArray alloc] init];
    BOOL thereAreTagsLeftToProcess = YES;
    
    if(tagsArray){
        NSLog(@"OK HERE 2");
        //#############################
        //## Step 1: prepare tags for curation
        //##########
        for (int i = 0; i < [tagsArray count]; i++) {
            OXTagModel *currentTag = [[OXTagModel alloc] init];
            currentTag.tagName = [tagsArray[i] tagName];
            currentTag.tagCount = [tagsArray[i] tagCount];
            
            NSString *currentTagName = currentTag.tagName;
            /*
             Add special characters such as quotes which will then create the final string
             to be processed and length is calculated from this final string
             */
            NSLog(@"NAME: %@", currentTag.tagName);
            //Capitalise each string
            NSMutableString *result = [currentTagName mutableCopy];
            NSString * finalStringNameForCurrentTag;
            if(!isFacebookMessage){
                
                [result enumerateSubstringsInRange:NSMakeRange(0, [result length])
                                           options:NSStringEnumerationByWords
                                        usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                            [result replaceCharactersInRange:NSMakeRange(substringRange.location, 1)
                                                                  withString:[[substring substringToIndex:1] uppercaseString]];
                                        }];
                //remove space occurences
                finalStringNameForCurrentTag = [result stringByReplacingOccurrencesOfString:@" " withString:@""];
            }
            else{
                finalStringNameForCurrentTag = result;
            }
            NSString *currentTagNameWithQuotes;
            if(isFacebookMessage){
                currentTagNameWithQuotes = [NSString stringWithFormat:@"[%@]", finalStringNameForCurrentTag];
            }
            else{
                currentTagNameWithQuotes = [NSString stringWithFormat:@"#%@", finalStringNameForCurrentTag];
            }
            currentTag.tagName = currentTagNameWithQuotes;
            NSLog(@"processed NAME: %@", currentTag.tagName);
            
            [processedTagsArray addObject:currentTag];
        }
        
        //#############################
        //## Step 2: determine which tags can be presented
        //##########
        //Calculate the remaining characters that we have left to work with.
        NSUInteger remainingCharacters = maxCharacterLength - staticCharactersUsedSoFar;
        
        while (thereAreTagsLeftToProcess) {
            /*
             Repeat this procedure until there are no more tags left to process
             Technical note: Must be in a while loop since we are removing objects on the fly.
             Shit will get crazy if we try and remove objects within an array and use that same array to determine the
             number of executions per loop, BIG NO NO, thats why a while loop is used,
             moral of the story: Make changes only if you know what is going on here.
             */
            for (int i = 0; i < [processedTagsArray count]; i++) {
                //                NSString *currentTagStringName = ((OCNTag*)(processedTagsArray[i])).name;
                OXTagModel *currentTag = processedTagsArray[i];
                NSString *currentTagStringName= currentTag.tagName;
                
                if(remainingCharacters >= currentTagStringName.length){
                    
                    //Add to a final array which are the tags that can fit in the tweet display
                    [finalTagsToDisplay addObject:currentTagStringName];
                    
                    [finalTagObjectsUsed addObject:processedTagsArray[i]];
                    //calculate "tag string length" + the "tag seperator character" and deduct the total value from the remaining count
                    remainingCharacters -= currentTagStringName.length + [@" " length]
                    ;
                    //                    NSLog(@"processed NAME: %@ remainingCharacter: %d", currentTagStringName, remainingCharacters);
                    
                    [processedTagsArray removeObject:currentTag]; //This tag has been used so remove it so it doesnt get processed again
                    break;
                }
                else{
                    /*
                     Stop processing arrays as soon as the remaining tags can no longer fit in a tweet based
                     on the remaining character count
                     */
                    thereAreTagsLeftToProcess = NO;
                }
                if(isFacebookMessage){
                    /*
                     To find the difference between the last tag separator and the common tag separtor character
                     since there normally is a length difference which needs to be taken into account to accurately know
                     the remaining character count
                     
                     DO THIS ONCE ONLY, this will deduct the character count by the difference discussed in the comment above.
                     
                     This is important because then no clipping will occur in the tweet message if the last tag seperator character is longer than the
                     common seperator character
                     */
                    if([finalTagsToDisplay count] == 2){
                        NSInteger differenceInLength = [kTagSeperatorCharacter length] < [kTagFinalSeperatorCharacter length] ? ([kTagFinalSeperatorCharacter length] - [kTagSeperatorCharacter length]) : 0;
                        remainingCharacters -= differenceInLength;
                    }
                }
            }
            if(finalTagsUsed){
                *finalTagsUsed = finalTagObjectsUsed;
            }
            //This ensures the while loop exits.
            if([processedTagsArray count] == 0){
                thereAreTagsLeftToProcess = NO;
            }
        }
    }
    NSMutableString *finalTagsMessageLine = [[NSMutableString alloc] initWithString:@""];
    for(int i = 0; i < [finalTagsToDisplay count]; i++){
        NSString *currentTagStringName = finalTagsToDisplay[i];
        if(isFacebookMessage){
            if(i < [finalTagsToDisplay count]-1){
                [finalTagsMessageLine appendFormat:@"%@%@", currentTagStringName, ([finalTagsToDisplay count] == 2) ? @"" : kTagSeperatorCharacter];
            }
            else{
                [finalTagsMessageLine appendFormat:@"%@%@", kTagFinalSeperatorCharacter, currentTagStringName];
            }
        }
        else{
            [finalTagsMessageLine appendFormat:@"%@ ", currentTagStringName];
        }
    }
    return finalTagsMessageLine;
}

#pragma mark - IBAction methods

- (IBAction)reportButtonTapped:(id)sender{
    [self reportAction];
}
-(void)reportAction{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Report", nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheetDelegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        UIStoryboard * reportingScreenStoryBoard = [UIStoryboard storyboardWithName:@"ReportingSB" bundle:nil ];
        OXReportTableViewController *reportScreen = [reportingScreenStoryBoard instantiateInitialViewController];
        OXReportDataModel *reportData = [[OXReportDataModel alloc] initWithId:self.fullPOIObject.poiID andSource:ReportTypePOI];
        reportScreen.reportData = reportData;
        [self.navigationController pushViewController:reportScreen animated:YES];
    }
}

#pragma mark - Private methods

-(void)setupNavigtionBar
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);

}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)didSelectCameraIcon{
    NSLog(@"detail camera icon did select");
    UIStoryboard *addPlaceStoryboard = [UIStoryboard storyboardWithName:@"AddPOISB" bundle:nil];
    
    if([[((UINavigationController*)[addPlaceStoryboard instantiateInitialViewController]).viewControllers firstObject] isMemberOfClass:[OXCameraCaptureScreenViewController class]]){
        NSLog(@"LOADING OX CAMERA CAPTURE SCREEN VIEW CONTROLLER");
        OXCameraCaptureScreenViewController *destinationVC = [((UINavigationController*)[addPlaceStoryboard instantiateInitialViewController]).viewControllers firstObject];
        
        
        UINavigationController *nav = [[UINavigationController alloc]
                                       initWithRootViewController:destinationVC];
        
        
        destinationVC.comingFromVC = kAddPlaceComingFromVCTypeDetailPOIPage;
        destinationVC.poiID = self.poiID;
        destinationVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentViewController:nav animated:YES completion:nil];
    }
}

-(void)didSelectThumbnail {
//#warning pavan  TODO: Just pass the array of images or pre download the images so that it would be easy to swipe on predownloaded images.
    MBGalleryViewController *cnt = [[MBGalleryViewController alloc] initWithNibName:@"MBGalleryViewController" bundle:nil];
    cnt.galleryImages = [[NSMutableArray alloc] init];
    for(PKEditMediaThumbnailView *pic in self.thumbnailSliderView.urlLoadedImageThumbnails){
        if(pic.previewThumbnail.image){
            [cnt.galleryImages addObject:pic.previewThumbnail.image];
        }
        
    }
    [self presentViewController:cnt animated:YES completion:nil];
}

-(void)likeButtonTapped:(id)sender{
    NSLog(@"likeButtonTapped");
    __block OXPOIModel *currentPOI = self.fullPOIObject;
    
    
    if(((UIButton*)sender).isSelected){
        [OXRestAPICient likePOIWithID:currentPOI.poiID
                         successBlock:^(AFHTTPRequestOperation *operation, id response) {
                             NSLog(@"%@", response);
                             currentPOI.userLiked = !currentPOI.userLiked;
                             currentPOI.userDisliked = !currentPOI.userLiked;
                             //                             self.parentLikeDislikeCell.likeButton.selected = currentPOI.userLiked;
                             //                             self.parentLikeDislikeCell.dislikeButton.selected = currentPOI.userDisliked;
                             self.parentPOIObject.userLiked = currentPOI.userLiked;
                             self.parentPOIObject.userDisLiked = currentPOI.userDisliked;
                         }
                            failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                NSLog(@"%@", error);
                                
                            }];
    }
    else{
        [OXRestAPICient neutralPOIWithID:currentPOI.poiID
                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                NSLog(@"%@", response);
                                currentPOI.userLiked = false;
                                currentPOI.userDisliked = false;
                                //                                self.parentLikeDislikeCell.likeButton.selected = currentPOI.userLiked;
                                //                                self.parentLikeDislikeCell.dislikeButton.selected = currentPOI.userDisliked;
                                
                                self.parentPOIObject.userLiked = currentPOI.userLiked;
                                self.parentPOIObject.userDisLiked = currentPOI.userDisliked;
                            }
                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                   NSLog(@"%@", error);
                                   
                               }];
    }
}
-(void)dislikeButtonTapped:(id)sender{
    NSLog(@"dislikeButtonTapped");
    __block OXPOIModel *currentPOI = self.fullPOIObject;
    
    if(((UIButton*)sender).isSelected){
        [OXRestAPICient dislikePOIWithID:currentPOI.poiID
                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                currentPOI.userDisliked = !currentPOI.userDisliked;
                                currentPOI.userLiked = !currentPOI.userDisliked;
                                //                                self.parentLikeDislikeCell.likeButton.selected = currentPOI.userLiked;
                                //                                self.parentLikeDislikeCell.dislikeButton.selected = currentPOI.userDisliked;
                                self.parentPOIObject.userLiked = currentPOI.userLiked;
                                self.parentPOIObject.userDisLiked = currentPOI.userDisliked;
                            }
                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                   NSLog(@"%@", error);
                                   
                               }];
    }
    else{
        NSLog(@"calling neutral api");
        [OXRestAPICient neutralPOIWithID:currentPOI.poiID
                            successBlock:^(AFHTTPRequestOperation *operation, id response) {
                                currentPOI.userLiked = false;
                                currentPOI.userDisliked = false;
                                //                                self.parentLikeDislikeCell.likeButton.selected = currentPOI.userLiked;
                                //                                self.parentLikeDislikeCell.dislikeButton.selected = currentPOI.userDisliked;
                                self.parentPOIObject.userLiked = currentPOI.userLiked;
                                self.parentPOIObject.userDisLiked = currentPOI.userDisliked;
                                
                                
                            }
                               failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                   NSLog(@"%@", error);
                                   
                               }];
    }
}

#pragma mark - OXFlurryProtocol delegate methods

- (NSDictionary *)getFlurryParameters
{
    NSDictionary *params =
    [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld", (long)self.poiID],
     kBasePOIPoiIdKey,
     nil];
    
    return params;
}
-(void)reportButtonPressed{
    [self reportAction];
}

@end