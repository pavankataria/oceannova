//
//  OXQuestionViewController.h
//  OceanX
//
//  Created by Systango on 2/9/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXQuestionViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *missingFacets;
@property (assign, nonatomic) NSInteger currentFacetIndex;
@property (assign, nonatomic) NSInteger currentQuestionIndex;

@end
