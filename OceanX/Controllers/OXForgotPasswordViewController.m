//
//  OXForgotPasswordViewController.m
//  OceanX
//
//  Created by Systango on 1/29/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXForgotPasswordViewController.h"

@interface OXForgotPasswordViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (weak, nonatomic) IBOutlet UILabel *forgotPasswordErrorLabel;

@end

@implementation OXForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Method

- (IBAction)forgotPasswordButtonTapped:(id)sender
{
    [self requestPasswordReset];
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    [self dismiss];
}

#pragma mark - Textfield Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self requestPasswordReset];
    return YES;
}

#pragma mark - Private method

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)requestPasswordReset
{
    self.forgotPasswordErrorLabel.hidden = YES;
    
    if ([self validate])
    {
        [self.loading startAnimating];
        
        OXUserModel* user = [[OXUserModel alloc] initWithUsername:self.usernameTextField.text];
        
        [OXRestAPICient requestPasswordResetForUser:user successBlock:^(AFHTTPRequestOperation *operation, id response) {
            
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [self.loading stopAnimating];
                               
                               [self dismiss];
                               
                               [[StatusHUDSBViewController sharedManager] displaySuccessStatusWithTitle:kForgotPasswordEmailSentTitle message:kForgotPasswordSuccessMessage];
                               
                               [Flurry logEvent:kFlurryForgotPassword];
                           });
            
        } failBlock:^(AFHTTPRequestOperation *operation, id error) {
            
            NSLog(@"Problem : %@", error);
            
           
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               NSDictionary * dictionary = (NSDictionary*) error;
                               
                               if ([dictionary isKindOfClass:[NSDictionary class]]) {
                                   self.forgotPasswordErrorLabel.text = [dictionary getValueForKey:@"message"];
                               }
                               [self.loading stopAnimating];
                               
                               self.forgotPasswordErrorLabel.hidden = NO;
                           });
            
        }];
    }
}

- (BOOL)validate
{
    NSString *username = [self.usernameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
     if( ([username length] < kValidateUsernameMinimumLength) || ([username length] > kValidateUsernameMaximumLength) )
    {
        self.forgotPasswordErrorLabel.hidden = NO;
        self.forgotPasswordErrorLabel.text = kInvalidUsernameMessage;
        return NO;
    }

    return YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
