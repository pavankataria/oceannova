//
//  OXTagOpinionViewController.m
//  OceanX
//
//  Created by Systango on 2/16/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXTagOpinionViewController.h"
#import "OXTagsPresentationTableViewController.h"
#import "OXAddTagPresentationViewController.h"

@interface OXTagOpinionViewController () <OXDetailPOIProtocol>

@property (strong, nonatomic) IBOutlet UIView *tagsContainerView;
@property (nonatomic, retain) OXTagsPresentationTableViewController *tagsPresentationView;

@end

@implementation OXTagOpinionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tagsPresentationView.view.frame = CGRectMake(0, 0, self.tagsContainerView.frame.size.width, self.tagsContainerView.frame.size.height);
    [self.tagsContainerView addSubview:self.tagsPresentationView.view];

    self.tagsPresentationView.poiObject = [self getPOIObject];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(OXTagsPresentationTableViewController *)tagsPresentationView{
    if(!_tagsPresentationView){
        _tagsPresentationView = [[OXTagsPresentationTableViewController alloc] initWithStyle:UITableViewStylePlain];
         _tagsPresentationView.comingFromVC = OXTagsPresentationTableViewComingFromVCTypeTutorial;
        _tagsPresentationView.delegate = self;
    }
    return _tagsPresentationView;
}

#pragma mark - OXDetailPOIProtocol delegate - Methods
-(void)updateDetailPOIScrollViewContentSize{
}
-(void)displayAddTagsViewControllerWithControllerType:(OXTagsPresentationTableViewComingFromVCType)comingFromVCType{
    UIStoryboard * reportingScreenStoryBoard = [UIStoryboard storyboardWithName:@"AddTagsScreenSB" bundle:nil];
    OXAddTagPresentationViewController *reportScreen = [reportingScreenStoryBoard instantiateInitialViewController];
    
    reportScreen.comingFromVC = OXTagsPresentationTableViewComingFromVCTypeDetailPOIVCWithShowMoreTags;
    reportScreen.poiObject = self.tagsPresentationView.poiObject;
    [self.navigationController pushViewController:reportScreen animated:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - Private Methods

- (OXPOIModel *)getPOIObject {
    
    OXTagModel *tagOne = [OXTagModel new];
    tagOne.name = @"pizza";
    tagOne.tagCount = 22;
    OXTagModel *tagTwo = [OXTagModel new];
    tagTwo.name = @"good service";
    tagTwo.tagCount = 15;
    OXTagModel *tagThree = [OXTagModel new];
    tagThree.name = @"italian food";
    tagThree.tagCount = 10;
//    OXTagModel *tagFour = [OXTagModel new];
//    tagFour.name = @"mozzarella";
//    tagFour.tagCount = 9;
//    OXTagModel *tagFive = [OXTagModel new];
//    tagFive.name = @"cozy";
//    tagFive.tagCount = 2;
    
    OXPOIModel *poi = [OXPOIModel new];
    poi.tags = [NSArray arrayWithObjects:tagOne, tagTwo, tagThree, nil];
    return poi;
}

@end
