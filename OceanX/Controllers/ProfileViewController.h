//
//  ProfileViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 17/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController

-(void)showFriendsScreen;
@end
