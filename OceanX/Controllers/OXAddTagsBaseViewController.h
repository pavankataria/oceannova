//
//  OXAddTagsBaseViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXTagTableViewCell.h"
#import "OXAddTagDisplayTableViewCell.h"
#import "OXViewController.h"

typedef NS_ENUM(NSUInteger, kAddTagViewControllerShowTableView) {
    kAddTagViewControllerShowTableViewNormal,
    kAddTagViewControllerShowTableViewSearch,
};

@interface OXAddTagsBaseViewController : OXViewController<UITextFieldDelegate>
@property (nonatomic, weak) IBOutlet UITextField *addTagTextField;
@property (nonatomic, weak) IBOutlet UIView *addTagBackgroundView;
@property (nonatomic, weak) IBOutlet UITableView *tagsTableView;
@property (nonatomic, retain) NSMutableArray *searchTagsResultsArray;
@property (nonatomic, retain) NSMutableArray *storedTagsArray;
@property (nonatomic, assign) kAddTagViewControllerShowTableView currentTableViewShowing;
@property (nonatomic, retain) NSCharacterSet *blockedCharacters;

-(BOOL)doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:(NSString*)needleTagString;
-(NSString *)addTagTextWithExcessCharactersRemoved;
@end
