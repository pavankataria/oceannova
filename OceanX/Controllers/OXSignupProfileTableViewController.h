//
//  OXSignupProfileTableViewController.h
//  OceanX
//
//  Created by Systango on 1/30/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXTableViewController.h"

typedef NS_ENUM(NSUInteger, kCreateNewUserControllerShowTableView) {
    kErrorLabelCellTableView,
    kEmailCellTableView,
    kUserNameCellTableView,
    kPasswordCellTableView,
    kGenderCellTableView,
    kYearOfBornCellTableView,
    kTermsAndConditionsCellTableView,
    kContinueCellTableView
};

@interface OXSignupProfileTableViewController : OXTableViewController

@end
