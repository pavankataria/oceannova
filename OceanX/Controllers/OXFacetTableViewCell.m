//
//  OXFacetTableViewCell.m
//  OceanX
//
//  Created by Systango on 2/5/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXFacetTableViewCell.h"

@interface OXFacetTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *facetNameLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *facetScoreBarFillProgressView;
@property (weak, nonatomic) IBOutlet UILabel *facetScorePercentageLabel;
@property (assign, nonatomic) CGFloat percentage;
@property (strong, nonatomic) IBOutlet UIView *progressBarBGView;
@property (strong, nonatomic) IBOutlet UIView *progressView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *progressViewWidthConstraint;

@end

@implementation OXFacetTableViewCell

- (void)awakeFromNib{
    [super awakeFromNib];
//    self.progressView.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void) setupWithFacet: (OXFacetModel*) facet
{
    if (facet.facetScore != FACET_NULL_VALUE) {
        
        self.facetNameLabel.text = facet.facetText;
        self.descriptionLabel.text = facet.facetDescription;
        
    }else{
        
        self.facetNameLabel.text = @"Undiscovered";
        self.descriptionLabel.text = facet.facetDescription;
    }
    [self setPercentScore:facet.facetScore];
}

-(void)setPercentScore:(double)facetScore
{
    NSLog(@"percent: %f", facetScore);
    
    if(facetScore <= 0){
        self.percentage = 0;
        self.facetScorePercentageLabel.hidden = YES;
        self.facetScoreBarFillProgressView.progress = self.percentage;
        
    }else{
        
        //    NSAssert1(percent < 1, @"percentage value must not be given a value greater than 1.0", percent);
        if(facetScore > 0 && facetScore <= 1){
            self.percentage = facetScore;
        }
        else{
            self.percentage = ((facetScore + FACET_SCORE_POSITIVE_ADJUSTMENT)/FACET_SCORE_RANGE);
        }
        self.facetScorePercentageLabel.hidden = NO;
        self.facetScorePercentageLabel.text = [NSString stringWithFormat:@"%ld%%", (long) (self.percentage * 100)];
        self.facetScoreBarFillProgressView.progress = self.percentage;
        
    }
    NSLog(@"percentage: %f", self.percentage);
    [self refreshBar];
}

-(void)refreshBar{
    [self layoutIfNeeded]; // Ensures that all pending layout operations have been completed
    self.progressViewWidthConstraint.constant = self.progressBarBGView.frame.size.width * self.percentage;

    if(self.percentage > 0){
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut animations:^{
                                [self.progressBarBGView layoutIfNeeded]; // Called on parent view
                            }
                         completion:^(BOOL finished) {
                             
                         }];
    }
    NSLog(@"progressViewWidthConstraint: %f self.percentage: %f", self.progressViewWidthConstraint.constant, self.percentage);
}

- (void) layoutSubviews
{
    NSLog(@"layout subviews for bar self.percentage: %f", self.percentage);
    [super layoutSubviews];
    [self performSelector:@selector(refreshBar) withObject:self afterDelay:0.0];
}
@end
