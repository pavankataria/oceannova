//
//  OXFilterScreenViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 31/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXFilterScreenViewController.h"
#import "OXTagTableViewCell.h"

typedef NS_ENUM(NSUInteger, OXFilterScreenViewControllerShowTableView) {
    OXFilterScreenViewControllerShowTableViewTags = 0,
    OXFilterScreenViewControllerShowTableViewSearch,
    OXFilterScreenViewControllerShowTableViewLocation
};

@interface OXFilterScreenViewController (){
    NSCharacterSet *blockedCharacters;
    
}
@property (nonatomic, retain) NSMutableArray *searchTagsResultsArray;
@property (nonatomic, retain) NSMutableArray *storedTagsArray;
@property (nonatomic, assign) OXFilterScreenViewControllerShowTableView currentTableViewShowing;
@property (nonatomic, assign, getter=isSearchingByText) BOOL searchingByText;
@property (nonatomic, retain) NSMutableArray *uniqueLocations;

@end

@implementation OXFilterScreenViewController
-(NSMutableArray *)searchResultsArray{
    if( ! _searchTagsResultsArray){
        _searchTagsResultsArray = [[NSMutableArray alloc] init];
    }
    return _searchTagsResultsArray;
}

-(NSMutableArray *)storedTagsArray{
    if( ! _storedTagsArray){
        _storedTagsArray = [[NSMutableArray alloc] init];
    }
    return _storedTagsArray;
}
-(NSMutableArray *)uniqueLocations{
    if( !_uniqueLocations){
        _uniqueLocations = [[NSMutableArray alloc] init];
    }
    return _uniqueLocations;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentTableViewShowing = OXFilterScreenViewControllerShowTableViewTags;
    // Do any additional setup after loading the view.
    self.addressSearchBar.delegate = self;
    self.addressSearchBar.dataSource = self;
    [self.addressSearchBar reloadViews];
    [self registerForKeyboardNotifications];
    self.searchingByText = false;
    [self setupTable];
}

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void) keyboardWillBeHidden:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self.tableView setContentInset:contentInsets];
    [self.tableView setScrollIndicatorInsets:contentInsets];
    [self.tableView setContentOffset:CGPointMake(0,self.tableView.contentOffset.y) animated:YES];
}

- (void) keyboardWasShown:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height-50, 0.0);
    [self.tableView setContentInset:contentInsets];
    [self.tableView setScrollIndicatorInsets:contentInsets];
    [self.tableView setContentOffset:CGPointMake(0,self.tableView.contentOffset.y) animated:YES];
}

-(void)setupTable{
    [self.tableView registerNib:[UINib nibWithNibName:@"OXTagTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewCellTagIdentifier];
    
    NSMutableCharacterSet *allowedCharacters = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
    [allowedCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@" "]];
    blockedCharacters = [allowedCharacters invertedSet];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - OXSearchBar delegate methods
-(void)searchBarView:(OXSearchBarView *)searchBarView searchBarDidBeginEditing:(UITextField *)textField{
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied){
        NSLog(@"LOCATION");
        self.currentTableViewShowing = OXFilterScreenViewControllerShowTableViewLocation;
        OXCoordinateModel *currentObject = [[OXCoordinateModel alloc] init];
        currentObject.coordinate = [[OXLocationManager sharedTracker] currentLocation];
        currentObject.placeMarkTitleAndSubtitle = @"Current Location";
        [self.uniqueLocations addObject:currentObject];
        [self.tableView reloadData];
    }
    else{
        [[StatusHUDSBViewController sharedManager] displayErrorStatusWithTitle:@"Turn On Location Services" message:@"Open Settings\nTap Privacy\nTap Location Services\nSelect 'While Using the App'"];
    }
}
#pragma end -
#pragma mark - OXSearchBar View Datasource methods
-(UIColor *)searchBarViewBackgroundColorForSearchBarTextField{
    return [UIColor whiteColor];
}

-(UITextBorderStyle)searchBarViewTextFieldStyleForSearchBarTextField{
    return UITextBorderStyleRoundedRect;
}

-(void)searchBarTextFieldShouldReturn:(UITextField *)textField{
    if(self.storedTagsArray.count > 0){
        [[self.parentVC getSharedPOISearchModel] setTags:self.storedTagsArray];
    }
    [self performSearch];
}
#pragma mark end -

-(void)performSearch{
    [self.parentVC performSearchAndSaveInParent];
}
#pragma mark - UITextField delegate methods

-(NSString *)addTagTextWithExcessCharactersRemoved{
    NSString *searchString = [self.parentVC grabSearchTextField].text;
    NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
    NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
    
    NSArray *parts = [searchString componentsSeparatedByCharactersInSet:whitespaces];
    NSArray *filteredArray = [parts filteredArrayUsingPredicate:noEmptyStrings];
    searchString = [filteredArray componentsJoinedByString:@" "];
    
    NSLog(@".%@.", searchString);
    return searchString;
}

-(void)textFieldDidChangeWithText:(NSString *)text{
    if([self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
        [self.searchTagsResultsArray removeAllObjects];
        [self updateTableView];
        return;
    }
    
    [OXRestAPICient getTagsAutoSuggestionWithString:text
                                   withSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
                                       //                                       NSLog(@"GET AUTO TAGS SUCCESS: %@", response);
                                       [self handleAutoTagsArrayResponse:response];
                                       [self updateTableView];
                                   }
                                          failBlock:^(AFHTTPRequestOperation *operation, id error) {
                                              NSString *responseBody = operation.responseString;
                                              NSLog(@"failure operation: %@\n\n\n\nresponse: %@ \n\n\nresponseString %@", operation, error, responseBody);
                                              [self.searchTagsResultsArray removeAllObjects];
                                              [self updateTableView];
                                              
                                          }];
}


-(void)handleAutoTagsArrayResponse:(NSArray*)response{
    self.searchTagsResultsArray = [OXTagPlaceCountModel createAutoSuggestionArrayFromResponse:response filteringOutStoredTags:self.storedTagsArray];
}
-(void)updateTableView{
    if([[self.parentVC grabSearchTextField].text length] > 0){
        //        NSLog(@"GREATER THAN 0");
        self.currentTableViewShowing = OXFilterScreenViewControllerShowTableViewSearch;
    }
    else{
        self.currentTableViewShowing = OXFilterScreenViewControllerShowTableViewTags;
    }
    //    [self.addPoiObject setTags:self.storedTagsArray];
    [self.tableView reloadData];
}



#pragma mark - UITableView delegate and datasource methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!self.isSearchingByText){
        if(self.currentTableViewShowing == OXFilterScreenViewControllerShowTableViewSearch){
            if(self.storedTagsArray.count == 0){
                if(indexPath.row == 0){
                    self.searchingByText = YES;
                    
                    //                OXTagPlaceCountModel *newTag = [[OXTagPlaceCountModel alloc] initWithTagName:[self addTagTextWithExcessCharactersRemoved] andTotalPlacesCount:0];
                    //                [self.storedTagsArray addObject:newTag];
                    [self.parentVC getSharedPOISearchModel].textSearchString = [self addTagTextWithExcessCharactersRemoved];
                    [[self.parentVC grabSearchTextField] resignFirstResponder];
                    [[self addressSearchBar] resignFirstResponder];
                    [self.parentVC performSearchAndSaveInParent];
                }
                else{
                    self.searchingByText = NO;
                    [self.storedTagsArray addObject:self.searchTagsResultsArray[indexPath.row-1]];
                    NSLog(@"stored tags: %@", ((OXTagPlaceCountModel*)([self.storedTagsArray lastObject])).tagName);
                    [self.searchTagsResultsArray removeAllObjects];
                }
            }
            else{
                self.searchingByText = NO;
                [self.storedTagsArray addObject:self.searchTagsResultsArray[indexPath.row]];
                NSLog(@"stored tags: %@", ((OXTagPlaceCountModel*)([self.storedTagsArray lastObject])).tagName);
                [self.searchTagsResultsArray removeAllObjects];
            }
            [self.parentVC grabSearchTextField].text = @"";
            [self updateTableView];
        }
        else if(self.currentTableViewShowing == OXFilterScreenViewControllerShowTableViewTags){
            
        }
        else if (self.currentTableViewShowing == OXFilterScreenViewControllerShowTableViewLocation){
            OXCoordinateModel *currentCoordinate = [self.uniqueLocations objectAtIndex:indexPath.row];
            NSLog(@"did select: %@", currentCoordinate.placeMarkTitleAndSubtitle);
            [[self.parentVC getSharedPOISearchModel] setWithCoordinateModel:currentCoordinate];
            [[self.parentVC grabSearchTextField] becomeFirstResponder];
            self.addressSearchBar.searchBarTextField.text = @"ahahaha";//currentCoordinate.placeMarkTitleAndSubtitle;	
        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.currentTableViewShowing == OXFilterScreenViewControllerShowTableViewTags){
        if(self.storedTagsArray.count == 0){
            return 1;
        }
        else{
            return self.storedTagsArray.count;
        }
    }
    else if(self.currentTableViewShowing == OXFilterScreenViewControllerShowTableViewSearch){
        if(self.storedTagsArray.count == 0){
            NSUInteger totalNumberOfRows = 1;
            if(!self.isSearchingByText){
                totalNumberOfRows += self.searchTagsResultsArray.count;
            }
            return totalNumberOfRows;
        }
        else{
            return self.searchTagsResultsArray.count;
        }
    }
    else if(self.currentTableViewShowing == OXFilterScreenViewControllerShowTableViewLocation){
        return self.uniqueLocations.count;
    }
    return 0;
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        OXTagTableViewCell *cell = (OXTagTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewCellTagIdentifier];
        
        cell.tagNameLabel.textColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.userInteractionEnabled = YES;
        
        
        if(self.currentTableViewShowing == OXFilterScreenViewControllerShowTableViewTags){
            if(indexPath.row == self.storedTagsArray.count){
                [cell setSearchTableCellForUIMessageCellForIndexPath:indexPath];
            }
            else{
                NSLog(@"indexPath row: %lu", (long)indexPath.row );
                OXTagPlaceCountModel *tag = self.storedTagsArray[indexPath.row];
                cell.tagNameLabel.text = tag.tagName;
                cell.tagPlacesCountLabel.text = tag.totalPlacesCountString;
            }
        }
        else if(self.currentTableViewShowing == OXFilterScreenViewControllerShowTableViewSearch){
            if(self.storedTagsArray.count == 0){
                if(indexPath.row == 0){
                    if(![self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
                        cell.tagNameLabel.text = [NSString stringWithFormat:@"Search by text: %@", [self addTagTextWithExcessCharactersRemoved]];
                        cell.tagPlacesCountLabel.text = @"";
                    }
                }
                else{
                    static NSUInteger indexRowOffset = 1;
                    cell.tagNameLabel.text = [self.searchTagsResultsArray[indexPath.row-indexRowOffset] tagName];
                    cell.tagPlacesCountLabel.text = [self.searchTagsResultsArray[indexPath.row-indexRowOffset] totalPlacesCountString];
                }
            }
            else{
                cell.tagNameLabel.text = [self.searchTagsResultsArray[indexPath.row] tagName];
                cell.tagPlacesCountLabel.text = [self.searchTagsResultsArray[indexPath.row] totalPlacesCountString];
            }
        }
        else if(self.currentTableViewShowing == OXFilterScreenViewControllerShowTableViewLocation){
            cell.tagNameLabel.text = [self.uniqueLocations[indexPath.row] placeMarkTitleAndSubtitle];
            cell.tagPlacesCountLabel.text = @"";
        }
        return cell;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kOXAddTagsTableViewRowHeight;
}

-(BOOL)doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:(NSString*)needleTagString{
    for (OXTagPlaceCountModel *storedTag in self.storedTagsArray) {
        NSLog(@"comparing storedTag: %@ with needle: %@", storedTag, [needleTagString lowercaseString]);
        
        if([[storedTag.tagName lowercaseString] isEqualToString:[needleTagString lowercaseString]]){
            return YES;
        }
    }
    return NO;
}
-(NSString *)searchBarViewPlaceHolderString{
    return kOXSearchTextFieldAddressPlaceHolderText;
}

-(BOOL)filterScreenTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSLog(@"location update: %@", string);
    BOOL canContinue = ([string rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);
    if (canContinue) {
        
        //        [self performLocationSearchWithText:];
    }
    return canContinue;
}
-(void)filterScreenTextFieldDidChange:(UITextField *)textField{
    NSLog(@"textfield location did change %@", textField.text);
    [self performLocationSearchWithText:textField.text];
}
-(void)performLocationSearchWithText:(NSString*)searchText{
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = searchText;
    MKCoordinateRegion currentRegion = [[OXLocationManager sharedTracker] currentMapRegion];
    request.region = currentRegion;
    MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
    [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        [self processLocalSearchResponse:response];
    }];
}
-(void)processLocalSearchResponse:(MKLocalSearchResponse*)response{
    self.uniqueLocations = nil;
    for(MKMapItem *item in response.mapItems){
        OXCoordinateModel *coordinate = [[OXCoordinateModel alloc] init];
        [coordinate setCoordinate:item.placemark.coordinate];
        [coordinate setPlaceMarkTitleAndSubtitle:[NSString stringWithFormat:@"%@"/*@"%@, %@"*/, /*item.placemark.name,*/ item.placemark.title]];
        [self.uniqueLocations addObject:coordinate];
    }
    //    NSLog(@"before filter, count: %lu", uniqueLocations.count);
    self.uniqueLocations = [self.uniqueLocations valueForKeyPath:@"@distinctUnionOfObjects.self"];
    //    NSLog(@"after filter, count: %lu", uniqueLocations.count);
    
    self.currentTableViewShowing = OXFilterScreenViewControllerShowTableViewLocation;
    [self.tableView reloadData];
}

@end
