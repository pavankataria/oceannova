//
//  OXSearchByMapViewController.m
//  OceanX
//
//  Created by Systango on 2/2/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSearchByMapViewController.h"
#import "OXDetailSearchByMapViewController.h"
#import "MapsListTableViewController.h"
#import "OXDetailPOIViewController.h"
#import "OXLoadingAnimation.h"

@interface OXSearchByMapViewController () <OXMapViewDelegate, OXPOIDetailDelegate, UIGestureRecognizerDelegate>{
    UIStoryboard * poiDetailSB;
}

@property (nonatomic,strong) IBOutlet OXMapView * mapView;
@property (nonatomic,strong) NSArray * mapElements;
@property (nonatomic,strong) OXBasePOIModel *mapElement;
@property (nonatomic,strong) MapsListTableViewController *mapsListVC;
@property (nonatomic,strong) OXPOISearchModel *currentPOISearch;

@property (nonatomic, assign, getter=isPerformingMapsCall)BOOL performingMapsCall;

@property (assign, nonatomic) BOOL didAppear; //to avoid multiple calls to regionChanged
@property (assign, nonatomic) BOOL regionChangedAtLeastOnce; //to avoid calling regionChanged on changing region on search call
@property (strong, nonatomic) OXLoadingAnimation *loadingAnimation;


@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end

@implementation OXSearchByMapViewController


-(void)viewDidDisappear:(BOOL)animated
{
    
    self.didAppear = FALSE;
    self.regionChangedAtLeastOnce = FALSE;
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerNibs];
    self.mapView.customDelegate = self;
    
    
    NSLog(@"view did load");
    
    
    //TODO: set London to current region, may be we need to assign current user region
    MKCoordinateRegion currentRegion = [[OXLocationManager sharedTracker] currentMapRegion];
    
    
    // assign default region on Map
    [self.mapView setRegion:currentRegion animated:YES];
    self.mapView.lastUpdatedMapRect = self.mapView.visibleMapRect;
    // perform API call to get OXMapElementModel array
    //[self performSearchByMap:currentRegion];
    
    
    self.mapsListVC = [[MapsListTableViewController alloc] init];
    self.mapsListVC.delegate = self;
    self.mapsListVC.view.hidden = YES;
    
    
    ////////////////for the sake of the animation the view needs to start at the bottom
    
    self.mapsListVC.view.frame = CGRectMake(self.mapsListVC.view.frame.origin.x, CGRectGetMaxY(self.view.frame), self.mapsListVC.view.frame.size.width , self.mapsListVC.view.frame.size.height);
    
    ////////////////
    
    [self.view addSubview:self.mapsListVC.view];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
//    self.loadingAnimation = [[OXLoadingAnimation alloc] init];
//    [self.loadingAnimation beginAnimation];
//    [self.view addSubview:self.loadingAnimation];
}


-(void)viewDidAppear:(BOOL)animated{
//    self.loadingAnimation.frame = self.mapView.frame;
//    [self.loadingAnimation centerAnimation];
    
    [super viewDidAppear:animated];
    NSLog(@"view did appear");
    if (!self.didAppear) {
        self.didAppear = TRUE;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - public methods
-(void)performSerchWithObject:(id)object {
    
    NSLog(@"performing search in map view controller");
//    [self.loadingAnimation showAnimation];
    
    
    self.currentPOISearch = object;
    //[self.mapView setCenterCoordinate:((OXPOISearchModel *)object).coordinate.coordinate animated:YES];
    [self performSearchByPOI:object];
}


#pragma mark - Private methods

-(void)performSearchByMap:(MKCoordinateRegion)mapRegion
{
    OCNPlaneModel * plane = [OCNPlaneModel planeModelFromMapRegion:mapRegion];
    self.currentPOISearch.planeRestModel = plane;
    
    [self performSearchByPOI:self.currentPOISearch];
}


- (void)performSearchByPOI:(OXPOISearchModel *)poiSearchModel {
    NSLog(@"poi search model in map view controller params: %@", [poiSearchModel params]);
    
    [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Loading places"] maskType:SVProgressHUDMaskTypeNone];

    if(!self.isPerformingMapsCall){
        self.performingMapsCall = YES;
        if (self.mapsListVC.view.hidden == NO) {
            [self animateHidingList];
        }
        
        //very ugly solution that may potentially cause problems. Works now
        
        if (!(poiSearchModel.coordinate.coordinate.latitude == 0 && poiSearchModel.coordinate.coordinate.longitude == 0)) {
            
            [self.mapView setCenterCoordinate:poiSearchModel.coordinate.coordinate animated:YES];
            
        }
        
        
        [OXRestAPICient searchByMapWithPOISearchObject:poiSearchModel withSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
            
//            [self.loadingAnimation hideAnimation];
            
            [SVProgressHUD dismiss];
            self.performingMapsCall = NO;
            NSLog(@"search by map call is a success with respone : %@",response);
            
            [self createMapElementsWithResponse:response];
        } failBlock:^(AFHTTPRequestOperation *operation, id error) {
            NSLog(@"search by map call failed with error : %@",error);
            
//            [self.loadingAnimation hideAnimation];
            [SVProgressHUD dismiss];
            self.performingMapsCall = NO;
        }];
    }
}




-(void)updateMapWithElements:(NSMutableArray*)mapElements{
    self.mapElements = mapElements;
    [self.mapView refreshWithPlaces:mapElements];
}

-(void)createMapElementsWithResponse:(id)response
{
    NSArray *responseArray = response;
    NSMutableArray * mapElements = [NSMutableArray array];
    for(NSDictionary *mapElement  in responseArray)
    {
        [mapElements addObject:[[OXBasePOIModel alloc] initWithDictionary:mapElement]];
    }
    [self updateMapWithElements:mapElements];
}

- (NSArray *)getPoibaseObjectArrayFromAnnotations:(NSArray *)annnotations {
    NSMutableArray *selectedMapElements = [NSMutableArray array];
    for (OXAnnotation *annotation in annnotations) {
        for (OXBasePOIModel *basePOI in self.mapElements) {
            if (annotation.annotationID == basePOI.poiID) {
                [selectedMapElements addObject:basePOI];
            }
        }
    }
    return selectedMapElements;
}

//TODO : pavan will set frame
- (void)resizeTableView {
    
    
    //Animation
    
    CGFloat animationDuration = 0.2;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    
    
    //resizing
    CGFloat height = MIN(self.view.frame.size.height/2, self.mapsListVC.tableView.contentSize.height);
    CGSize tableViewSize = self.mapsListVC.tableView.contentSize;
    tableViewSize.height = height;
    
    CGRect viewFrame = self.mapsListVC.view.frame;
    viewFrame.size = tableViewSize;
    self.mapsListVC.view.frame = viewFrame;
    [self.mapsListVC.view setFrame:CGRectMake(0, self.view.frame.size.height - viewFrame.size.height, viewFrame.size.width, viewFrame.size.height)];
    [self.mapsListVC.tableView setFrame:CGRectMake(0, 0, viewFrame.size.width, viewFrame.size.height)];
    
    
    //commit animation
    
    [UIView commitAnimations];
}

#pragma mark MapView Delegate Methods


-(void)regionWillChange{
    if (self.mapsListVC.view.hidden == NO) {
        [self animateHidingList];
    }
}

- (void)regionChanged{
    
    
    NSLog(@"region changed");
    
    if (!self.currentPOISearch) {
        self.currentPOISearch = [[OXPOISearchModel alloc] init];
        
    }
    
    
    if (self.didAppear) {
        
        if (!self.regionChangedAtLeastOnce) {
            
            self.regionChangedAtLeastOnce = TRUE;
            
        }else{
            
            NSLog(@"region changed finished loading and changed at least once");
            
            OCNPlaneModel * plane = [OCNPlaneModel planeModelFromMapRegion:self.mapView.region];
            self.currentPOISearch.planeRestModel = plane;
            
            
            [self performSearchByPOI:self.currentPOISearch];
        }
        
        
    }
    
    
}

- (void)deselectedAnnotation{
    //TODO: will check if required
}

- (void)selectedAnnotationId:(NSInteger) annotationId{
    NSLog(@"selected annotation Id: %lu", (long)annotationId);
    // deselect annotation
    [self.mapView deselectAllAnnotation];
    
    // show detail view
    NSUInteger mapElementIndex = [self.mapElements indexOfObject:[[OXBasePOIModel alloc] initWithId:annotationId]];
    
    if(mapElementIndex != NSNotFound)
    {
        self.mapElement = [self.mapElements objectAtIndex:mapElementIndex];
        //        [self performSegueWithIdentifier:kSegueIdentifierDetailSearchByMapViewController sender:self];
        self.mapsListVC.view.hidden = NO;
        self.mapsListVC.poiList = @[self.mapElement];
        [self resizeTableView];
    }
    
}

- (void)selectedCluster:(NSArray *)annotations {
    NSLog(@"selectedCluster annotations: %lu %@", (unsigned long)[annotations count], annotations);
    
    self.mapsListVC.view.hidden = NO;
    self.mapsListVC.poiList = [self getPoibaseObjectArrayFromAnnotations:annotations];
    [self resizeTableView];
    
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:kSegueIdentifierDetailSearchByMapViewController]){
        OXDetailSearchByMapViewController *mapElementDetailsViewController = segue.destinationViewController;
        mapElementDetailsViewController.mapElement = self.mapElement;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


//TODO : Remove table view on single click on map view
#pragma mark - UIGestureRecognizer Delegate Methods
/*- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
 if (![touch.view isKindOfClass:[MKAnnotationView class]] && ![touch.view isKindOfClass:[UITableViewCell class]]) {
 //        self.mapsListVC.view.hidden = YES;
 }
 return true;
 }*/



-(void)animateHidingList{
    CGFloat animationDuration = 0.2;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    self.mapsListVC.view.frame = CGRectMake(self.mapsListVC.view.frame.origin.x, CGRectGetMaxY(self.view.frame), self.mapsListVC.view.frame.size.width , self.mapsListVC.view.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:@selector(hideList) withObject:nil afterDelay:animationDuration];
}

-(void)hideList
{
    self.mapsListVC.view.hidden = YES;
}


#pragma mark - OXPOIDetailDelegate Method
-(void)didSelectedPoi:(OXBasePOIModel*)basePoi {
    self.mapElement = basePoi;
    //    [self performSegueWithIdentifier:kSegueIdentifierDetailSearchByMapViewController sender:self];
    //self.mapsListVC.view.hidden = YES;
    
    OXDetailPOIViewController *destinationVC = [poiDetailSB instantiateInitialViewController];
    destinationVC.poiID = basePoi.poiID;
    
    [self.navigationController pushViewController:destinationVC animated:YES];
}

-(void)registerNibs{
    
    poiDetailSB = [UIStoryboard storyboardWithName:@"OXPOIDetailSB" bundle:nil ];
    
}
@end


