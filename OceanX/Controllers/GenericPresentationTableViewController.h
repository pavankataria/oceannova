//
//  GenericPresentationTableViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 17/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol GenericTableViewCellConfiguration
@required
-(void)updateForModelObject:(id)anObject;

@optional
-(UIButton*)addFriendButton;
@end

@protocol GenericDataModelForCellConfiguration <NSObject>
-(NSString*)cellIdentifier;
-(BOOL)isFriendSelected;
@end


@interface GenericPresentationTableViewController : UITableViewController

@property (nonatomic, retain) NSArray *genericArray;
@end
