//
//  OXCameraCaptureScreenViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 26/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "OXCameraShutterButton.h"
#import "OXCameraThumbnailPreviewViewController.h"
#import "OXAddPOIViewControllerDelegate.h"

#import "OXThumbnailsViewController.h"
@interface OXCameraCaptureScreenViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, OXAddPOIViewControllerDelegate, OXThumbnailsViewControllerDelegate>

@property (nonatomic, retain) UIView *imagePreview;
//#warning todo - Change the class name of OXCameraShutterButton to something more generic without breaking code
@property (weak, nonatomic) IBOutlet OXCameraShutterButton *shutterButton;
@property (weak, nonatomic) IBOutlet OXCameraShutterButton *cancelButton;

@property (weak, nonatomic) IBOutlet UIButton *flashButton;
@property (weak, nonatomic) IBOutlet UIButton *reverseCameraButton;

@property (nonatomic, assign, getter = isCapturingImage) BOOL capturingImage;

@property (nonatomic, assign) kAddPlaceComingFromVCType comingFromVC;
@property (nonatomic, assign) NSUInteger poiID;


@property (nonatomic, retain) OXHCAddPoiModel *addPoiObject;



@end
