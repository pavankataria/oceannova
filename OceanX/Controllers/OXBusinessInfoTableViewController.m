//
//  OXBusinessInfoTableViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 12/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXBusinessInfoTableViewController.h"
#import "OXBusinessInfoTableViewCell.h"
#import "OXExtraInfoTableViewCell.h"
#import "OXHoursTableViewCell.h"

#import "OXImageStackItem.h"

#import "OXDaysSelectionTableViewController.h"



@interface OXBusinessInfoTableViewController ()
@end

@implementation OXBusinessInfoTableViewController
-(NSMutableArray *)businessOpeningHours{
    if(!_businessOpeningHours){
        _businessOpeningHours = [[NSMutableArray alloc] init];
    }
    return _businessOpeningHours;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Details";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    if(self.businessImages.count == 0){
        self.businessImages = [[NSMutableArray alloc] initWithObjects:[UIImage imageNamed:@"testImage"],[UIImage imageNamed:@"testImage2"], nil];
    }
    [self.tableView setEditing:YES];
    
    //to remove extra uitableviewseparator lines
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == kBusinessInfoTableSectionBusinessInfo){
        if(indexPath.row == 0){
            return 88;
        }
    }
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == kBusinessInfoTableSectionBusinessInfo){
        return kBusinessInfoTableTotalRowsBusinessInfo;
    }
    else if(section == kBusinessInfoTableSectionDetailsInfo){
        return kBusinessInfoTableTotalRowsExtraInfo;
    }
    else if(section == kBusinessInfoTableSectionHoursInfo){
        return self.businessOpeningHours.count + 1; //for extra field
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == kBusinessInfoTableSectionBusinessInfo){

        OXBusinessInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceBusinessInfo];
//        NSLog(@"section: %lu row: %lu", indexPath.section, indexPath.row);

        cell.businessImageView.image = [(OXImageStackItem*)[self.businessImages firstObject] originalImage];
        return cell;
    }

    else if(indexPath.section == kBusinessInfoTableSectionDetailsInfo){
        OXExtraInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceExtraInfo];
        
        return [cell getPreparedCellWithIndexPath:indexPath];
    }
    else if(indexPath.section == kBusinessInfoTableSectionHoursInfo){
        id cell;
        if([self.businessOpeningHours count] == 0){
            cell = (OXHoursTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceHoursInfo forIndexPath:indexPath];
        }
        else{
//            cell = (OXHoursTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierAddPlaceHoursInfo forIndexPath:indexPath];
//            [((OXHoursTableViewCell*)cell) updateCellWithObject:self.businessOpeningHours[indexPath.row]];
        }
        return cell;
    }
    NSLog(@"shoudnt be here: section: %lu row: %lu", (long)indexPath.section, (long)indexPath.row);
    return nil;
//    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.section == kBusinessInfoTableSectionDetailsInfo){
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        OXExtraInfoTableViewCell *cell = (OXExtraInfoTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        [cell.infoTextField becomeFirstResponder];
    }
    else if(indexPath.section == kBusinessInfoTableSectionHoursInfo){
        [self performSegueWithIdentifier:kSegueIdentifierDaysSelectionViewController sender:nil];
    }
//    else if(indexPath.section == kBusinessInfoTableSectionHoursInfo){
//        if([self.businessOpeningHours count] == indexPath.row){
//            NSLog(@"did select pressed");
//            [self tableView:self.tableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
//        }
//    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section == kBusinessInfoTableSectionBusinessInfo){
        return kBusinessInfoTableTitleHeaderBusinessInfo;
    }
    else if(section == kBusinessInfoTableSectionDetailsInfo){
        return kBusinessInfoTableTitleHeaderDetailsInfo;
        
    }
    else if(section == kBusinessInfoTableSectionHoursInfo){
        return kBusinessInfoTableTitleHeaderHoursInfo;
    }
    return @"";
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kBusinessInfoTableSectionDetailsInfo){
        return UITableViewCellEditingStyleInsert;
    }
//    else if(indexPath.section == kBusinessInfoTableSectionHoursInfo){
//        if(indexPath.row < [self.businessOpeningHours count]){
//            return UITableViewCellEditingStyleDelete;
//        }
//        else {
//            return UITableViewCellEditingStyleInsert;
//        }
//    }
    else{
        return UITableViewCellEditingStyleNone;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == kBusinessInfoTableSectionDetailsInfo){
        if (editingStyle == UITableViewCellEditingStyleInsert){
            OXExtraInfoTableViewCell *cell = (OXExtraInfoTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            [cell.infoTextField becomeFirstResponder];
        }
        else{
            //handle everything else...
        }
    }
//    else if(indexPath.section == kBusinessInfoTableSectionHoursInfo){
//        if (editingStyle == UITableViewCellEditingStyleInsert){
//            [self insertRowAtSectionHoursWithIndexPath:indexPath];
//        }
//        else{
//            //handle everything else...
//        }
//
//    }
}

-(void)insertRowAtSectionHoursWithIndexPath:(NSIndexPath*)indexPath{
    OXBusinessHoursModel *newBusinessHour = [[OXBusinessHoursModel alloc] init];
    [self.businessOpeningHours addObject:newBusinessHour];
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
}
- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == kBusinessInfoTableSectionDetailsInfo){
        return YES;
    }
    else{
        return NO;
    }
    
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

/*
 // In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSLog(@"loading: %@", segue.identifier);

    if([segue.identifier isEqualToString:kSegueIdentifierDaysSelectionViewController]){
        NSLog(@"sender: %@", sender);
        self.businessOpeningHours = [OXBusinessHoursModel defaultMondayToFriday9to5WithoutSiestas];
        OXDaysSelectionTableViewController *dVC = segue.destinationViewController;
        dVC.businessHours = self.businessOpeningHours;
    }
}
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"BUSINESS LOADING: %@", segue.identifier);
    if([segue.identifier isEqualToString:kSegueIdentifierDaysSelectionViewController]){
        self.businessOpeningHours = [OXBusinessHoursModel defaultMondayToFriday9to5WithoutSiestas];
        OXDaysSelectionTableViewController *dVC;
        if([segue.destinationViewController isMemberOfClass:[UINavigationController class]]){
            dVC = [[segue.destinationViewController childViewControllers] firstObject];
        }
        else{
            dVC = segue.destinationViewController;
        }
        dVC.businessHours = self.businessOpeningHours;
    }
}

//segueIdentifierDaysSelectionViewController

@end
