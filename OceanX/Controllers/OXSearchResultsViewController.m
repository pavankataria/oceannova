//
//  OXSearchResultsViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 01/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSearchResultsViewController.h"
#import "OXPlaceSectionHeaderTableViewCell.h"
#import "OXPlaceDisplayImageTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "OXProcessedPicture.h"
#import "OXPOISearchByListModel.h"
#import "UIImageView+WebCache.h"


@interface OXSearchResultsViewController ()
@property (nonatomic, assign) CGFloat cellImageHeight;
@property (nonatomic, assign, getter=isLoading) BOOL loading;
@end

@implementation OXSearchResultsViewController
-(NSMutableArray *)searchResultsArray{
    return [_parentVC OXSearchContainerSharedSearchResultsArray];
}
-(void)setLoading:(BOOL)loading{
    NSLog(@"set loading to: %@", loading == TRUE ? @"yes" : @"no");
    _loading = loading;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setupTable];
    [self performSearchFirstTime];
}
-(void)performSearch{
    [self.parentVC performSearchWithSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
        self.loading = FALSE;
        [self.tableView reloadData];
    } andFailureBlock:^(AFHTTPRequestOperation *operation, id error) {
        NSLog(@"search by list failed with error: %@", error);
        self.loading = FALSE;
        [self.tableView reloadData];

    }];
}
-(void)viewWillDisappear:(BOOL)animated{
    self.loading = FALSE;
    [[self searchResultsArray] removeAllObjects];
    [self.tableView reloadData];

}
-(void)performSearchFirstTime{
    self.loading = TRUE;
    [self.tableView reloadData];
    [self performSearch];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods
-(void)setupTable{
    self.cellImageHeight = CGRectGetWidth(self.tableView.bounds);
    NSLog(@"cell image height %f", self.cellImageHeight);
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

#pragma mark end -
#pragma mark - UITableView delegate and datasource methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    NSLog(@"number of rows in secion: %lu", section);
    if(self.isLoading){
        if(section == 0){
            return 1;
        }
        else{
            return 0;
        }
    }
    else{
        return 1;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(self.isLoading){
//        NSLog(@"number of sections in table view: 1");

        return 1;
    }
    else{
//        NSLog(@"number of sections in table view: %lu", self.searchResultsArray.count);

        return self.searchResultsArray.count;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isLoading){
        NSLog(@"loading activity indicator cell");

        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsLoadingCell forIndexPath:indexPath];
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
        return cell;
    }
    else{
        OXPlaceDisplayImageTableViewCell *imageDisplayCell = [self.tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierSearchResultsImageDisplayCell forIndexPath:indexPath];
        imageDisplayCell.placeImageView.image = nil;
        if (imageDisplayCell == nil){
            [NSException raise:@"imageDisplayCell == nil.." format:@"No cells with matching imageDisplayCell CellIdentifier loaded from your storyboard"];
        }

        OXPOISearchByListModel *currentSearchListObject = self.searchResultsArray[indexPath.section];
        
        
        if(currentSearchListObject.pictures.count > 0){
//            [NSException raise:@"current search list pictures is > 0" format:@"Please figure out whats happening here, since only the main image should be coming through and not the place's other images if there is more than 1"];
        }

        OXProcessedPicture *processedPicture = [currentSearchListObject.pictures firstObject];
        
        if(processedPicture.processedPictures.count > 0){
//            [NSException raise:@"processed pictures is > 0" format:@"Please write some code here to take the appropriate image as required from the processPictures array"];
        }
        NSURL *url = [NSURL URLWithString:processedPicture.pictureURL];
        [imageDisplayCell.placeImageView sd_setImageWithURL:url];
        
        return imageDisplayCell;
    }
    return nil;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(self.isLoading == FALSE){
        static NSString *CellIdentifier = @"SectionHeader";
        OXPlaceSectionHeaderTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (headerView == nil){
            [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
        }
        [headerView.contentView setBackgroundColor:[UIColor colorWithWhite:1 alpha:kOXSearchTableViewControllerTableViewSectionHeaderBackgroundWhiteAlpha]];
        [headerView setCellWithPlaceConformingObject:self.searchResultsArray[section]];
        return headerView;
    }
    
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(self.isLoading == FALSE){
        return kOXSearchTableViewControllerTableViewSectionHeaderHeight;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isLoading){
        return 44;
    }
    else{
        if(indexPath.row == 0){
            return self.cellImageHeight;
        }
        return 44;
    }
    return 10;
}

#pragma mark end -

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
