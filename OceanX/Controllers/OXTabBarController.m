//
//  OXTabBarController.m
//  OceanX
//
//  Created by Pavan Kataria on 24/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXTabBarController.h"
#import "OXCameraAddPlaceViewController.h"
#import "UIImage+imageWithColor.h"
#import "ProfileViewController.h"
#import "FBRecommendationViewController.h"
@interface OXTabBarController ()
@property (nonatomic, retain) NSArray *poisThatAreStored;
@property (nonatomic, retain) NSString *poisSourceStringEnum;

@end

@implementation OXTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self registerNotifications];
    self.delegate = self;
    
    [self.viewControllers makeObjectsPerformSelector:@selector(view)];
    [self setSelectedIndex:0];
    
    CGFloat smallestEdge = MIN(CGRectGetWidth([[UIScreen mainScreen] bounds]), CGRectGetHeight([[UIScreen mainScreen] bounds]));
    [self.tabBar setItemWidth:smallestEdge/self.tabBar.items.count];
    NSLog(@"screen width: %f, items count: %lu", CGRectGetWidth([[UIScreen mainScreen] bounds]), (unsigned long)self.tabBar.items.count);
    
    NSLog(@"item width: %f", CGRectGetWidth([[UIScreen mainScreen] bounds])/self.tabBar.items.count);
    
    
    [self addCenterButtonWithTarget:self action:@selector(addPlaceButtonPressed:)];
    
    
    
//    
//    UITabBarItem *maps = [[UITabBarItem alloc] initWithTitle:nil image:[[UIImage imageNamed:@"image"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] tag:0];
//    [self setTabBarItem:maps];
    [[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
}
-(void)registerNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(unselectAddPOIButton:)
                                                 name:kTabBarViewControllerAddPOIButtonSetToUnselectStateNotification object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showProfilePageWithFriendsScreenActiviated)
                                                 name:kTabBarViewControllerProfileViewShowFriendsNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showFBRecommendationPage:)
                                                 name:kNotificationPushNameFBRecommendation object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishedDisplayingTutorialViewDoQuickFBRecommendationShouldDisplayCheck:) name:kTutorialViewDidFinishNowDoesFBRecommendationsNeedToBeDisplayedNotification object:nil];

    
    

}
-(void)finishedDisplayingTutorialViewDoQuickFBRecommendationShouldDisplayCheck:(NSNotification*)notification{
    [OXLoginManager setReplayTutorialViewed];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"finished displaying tutorial");
    [self showFacebookRecommendations];
    
}
-(void)showFBRecommendationPage:(NSNotification*)notification{
    NSLog(@"SHOW FACEBOOK RECOMMENDATIONS: %@", [notification userInfo]);
    
//    NSLog(@"%@", [OXPOISearchByListModel setupSearchResultsArrayWithResponse:(NSArray*)[notification userInfo]]);
    NSDictionary *dictionary = [notification userInfo];
    
    NSArray *pois = [OXPOISearchByListModel setupSearchResultsArrayWithResponse:[dictionary objectForKey:@"pois"]];
    self.poisSourceStringEnum = [dictionary objectForKey:@"sourceType"];
    self.poisThatAreStored = pois;

    if(![OXLoginManager isProfileTutorialViewed]){
        [OXLoginManager setFBRecommendationNeedsDisplayingWithBOOL:YES];
    }
    else{
        [OXLoginManager setFBRecommendationNeedsDisplayingWithBOOL:NO];
        [self showFacebookRecommendations];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];

}

-(void)showFacebookRecommendations{
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"FBRecommendation" bundle:nil];
    UIViewController *viewController = [main instantiateInitialViewController];
    FBRecommendationViewController *fbRecommendationVC;
    NSLog(@"view controllers: %@", [((UINavigationController*)viewController) viewControllers]);
    
    if([viewController isKindOfClass:[UINavigationController class]]){
        fbRecommendationVC = [[((UINavigationController*)viewController) viewControllers] firstObject];
    }
    else if([viewController isKindOfClass:[FBRecommendationViewController class]]){
        fbRecommendationVC = [main instantiateInitialViewController];
    }
    if(self.poisThatAreStored.count){
        fbRecommendationVC.pois = self.poisThatAreStored;
        fbRecommendationVC.poisSourceStringEnum = self.poisSourceStringEnum;
        viewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentViewController:viewController animated:YES completion:nil];
        [self showFBRecommendationExplanation];

    }
}
-(void)showFBRecommendationExplanation{
    NSString * message = @"";
    NSString * subString = @" ";
    if([self.poisSourceStringEnum isEqualToString:@"GENERIC"]){
        message = @"Let's get started with Ocean";
        subString = @"Please rate some of our most popular places.";
    }
    else if([self.poisSourceStringEnum isEqualToString:@"FB_RECOMMENDATION"]){
        message = @"Let's get started with Ocean";
        subString = @"The following recommendations have been tailored uniquely to you, based on your Facebook information";
    }
    else{
        message = @"Let's get started with Ocean";
        subString = @"Please rate some of our most popular places.";
    }
    [[StatusHUDSBViewController sharedManager] displaySuccessStatusWithTitle:message message:subString];
}
-(void)showProfilePageWithFriendsScreenActiviated{
    [self setSelectedIndex:3];
    ProfileViewController *profileScreen = (ProfileViewController*)[self.tabBarController.viewControllers objectAtIndex:3];
    [profileScreen showFriendsScreen];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    NSLog(@"did select: %lu", (unsigned long)tabBarController.selectedIndex);
    
    
    //    if((NSUInteger)tabBarController.selectedIndex == 1){
    //        [self performSelector:@selector(goToTagsScreen)];//performSelector:@selector(doNotHighlight:) withObject:self.centerButton afterDelay:0];
    //    }
}

- (void)addCenterButtonWithTarget:(id)target action:(SEL)action{
    /*
     Add a custom button in the middle of the tabbar since there is no willSelectItem delegate method which would allow us to prevent a view controller being shown.
     We dont want the middle tab to show when selected which causes our previously selected view controller to disappear with a blank screen.
     What we want instead is for a view controller to modally display over our tabbarcontroller similarly seen on instagram.
     */
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
//    [button.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:18.0]];
    
    
    //    [button setBackgroundColor:[PKUIAppearanceController darkBlueColor]];
//    [button setBackgroundImage:[UIImage imageWithColor:[PKUIAppearanceController darkBlueColor]] forState:UIControlStateNormal];
    
    button.frame = CGRectMake(0.0, 0.0, self.tabBar.itemWidth, self.tabBar.frame.size.height);
//    [button setTitle:@"+ Place" forState:UIControlStateNormal];
//    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [button setImage:[UIImage imageNamed:@"tabbarcamera"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"tabbarcamera_active"] forState:UIControlStateSelected];
    [button setImage:[UIImage imageNamed:@"tabbarcamera_active"] forState:UIControlStateHighlighted];

    
    button.center = self.tabBar.center;
    
    CGRect newFrame = button.frame;
    newFrame.origin.x = self.tabBar.frame.size.width-self.tabBar.itemWidth*2;
    [button setFrame:newFrame];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    self.centerButton = button;
}


+ (CGRect)frameForTabInTabBar:(UITabBar*)tabBar withIndex:(NSUInteger)index
{
    NSMutableArray *tabBarItems = [NSMutableArray arrayWithCapacity:[tabBar.items count]];
    for (UIView *view in tabBar.subviews) {
        if ([view isKindOfClass:NSClassFromString(@"UITabBarButton")] && [view respondsToSelector:@selector(frame)]) {
            // check for the selector -frame to prevent crashes in the very unlikely case that in the future
            // objects thar don't implement -frame can be subViews of an UIView
            [tabBarItems addObject:view];
        }
    }
    if ([tabBarItems count] == 0) {
        // no tabBarItems means either no UITabBarButtons were in the subView, or none responded to -frame
        // return CGRectZero to indicate that we couldn't figure out the frame
        return CGRectZero;
    }
    
    // sort by origin.x of the frame because the items are not necessarily in the correct order
    [tabBarItems sortUsingComparator:^NSComparisonResult(UIView *view1, UIView *view2) {
        if (view1.frame.origin.x < view2.frame.origin.x) {
            return NSOrderedAscending;
        }
        if (view1.frame.origin.x > view2.frame.origin.x) {
            return NSOrderedDescending;
        }
        NSAssert(NO, @"%@ and %@ share the same origin.x. This should never happen and indicates a substantial change in the framework that renders this method useless.", view1, view2);
        return NSOrderedSame;
    }];
    
    CGRect frame = CGRectZero;
    if (index < [tabBarItems count]) {
        // viewController is in a regular tab
        UIView *tabView = tabBarItems[index];
        if ([tabView respondsToSelector:@selector(frame)]) {
            frame = tabView.frame;
        }
    }
    else {
        // our target viewController is inside the "more" tab
        UIView *tabView = [tabBarItems lastObject];
        if ([tabView respondsToSelector:@selector(frame)]) {
            frame = tabView.frame;
        }
    }
    return frame;
}


- (void)addPlaceButtonPressed:(UIButton*)sender{
    
    if([[OXLocationManager sharedTracker] isLocationsServicesAvailable]){
        [sender setSelected:YES];
        UIStoryboard *addPlaceStoryboard = [UIStoryboard storyboardWithName:@"AddPOISB" bundle:nil];
        UIViewController *addPlaceViewController = [addPlaceStoryboard instantiateInitialViewController];
        addPlaceViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentViewController:addPlaceViewController animated:YES completion:nil];
    }
    else{
        if ([CLLocationManager authorizationStatus] != (kCLAuthorizationStatusAuthorizedAlways|kCLAuthorizationStatusAuthorizedWhenInUse)) {
            
            BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
            NSURL *url;
            if (canOpenSettings) {
                url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            }
            //Check whether Settings page is openable (iOS 5.1 not allows Settings page to be opened via openURL:)
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Location access disabled" message:@"To add a new place to Ocean we'll need to use your location. Please enable Location Services in settings." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Settings", nil];
                [alert setDelegate:self];
                [alert show];
                
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You must enable location service" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                [alert show];
            }
        }
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:url];
}



- (BOOL)tabBarHidden {
    return self.centerButton.hidden && self.tabBar.hidden;
}

- (void)setTabBarHidden:(BOOL)tabBarHidden
{
    self.centerButton.hidden = tabBarHidden;
    self.tabBar.hidden = tabBarHidden;
}

-(NSUInteger)supportedInterfaceOrientations{
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskPortrait;
    }
    return UIInterfaceOrientationMaskAllButUpsideDown;
}


-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UINavigationController *)navigationController{
    //    if([[navigationController viewControllers] count] == 0){
    //        return NO;
    //    }
    return YES;
}

#pragma mark - notification - Methods

-(void)unselectAddPOIButton:(NSNotification*)notification{
    NSLog(@"unselect add poi button notification received: %@", notification);
    [self.centerButton setSelected:NO];
}




@end
