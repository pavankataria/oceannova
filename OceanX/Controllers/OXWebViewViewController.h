//
//  OXWebViewViewController.h
//  OceanX
//
//  Created by Systango on 2/12/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXWebViewViewController : UIViewController
@property (strong, nonatomic) NSString * URL;
@end
