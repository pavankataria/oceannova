//
//  OXPlaceSectionHeaderTableViewCell.m
//  OceanX
//
//  Created by Pavan Kataria on 30/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXPlaceSectionHeaderTableViewCell.h"

@implementation OXPlaceSectionHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCellWithPlaceConformingObject:(id<OXSearchPOIForSectionViewOperationProtocol>)object{
    self.poiNameLabel.text = [object OXSearchPOIForSectionViewOperationPoiName];
    self.poiAddedByNameLabel.text = [NSString stringWithFormat:@"By %@", [object OXSearchPOIForSectionViewOperationPoiAddedBy]];
    self.poiDistanceLabel.text = [self getDistanceWithCoordinate:[object OXSearchPOIForSectionViewOperationPoiCoordinate]];
    
//    NSLog(@"trying to add image for: %@", self.poiNameLabel.text);
}

-(NSString*)getDistanceWithCoordinate:(CLLocationCoordinate2D)placeCoordinate{
    CLLocationCoordinate2D userLocationCoordinate = [[OXLocationManager sharedTracker] currentLocation];
    CLLocation * placelocation = [[CLLocation alloc] initWithLatitude:placeCoordinate.latitude longitude:placeCoordinate.longitude];
    
    CLLocation * currentLocation = [[CLLocation alloc] initWithLatitude:userLocationCoordinate.latitude longitude:userLocationCoordinate.longitude];
    
    NSNumber * distance =  [NSNumber numberWithDouble:[placelocation distanceFromLocation:currentLocation]];

    
    if([distance integerValue] < 100){
        return [NSString stringWithFormat:@"Less than 100 metres away"];
    }
    else if ([distance integerValue] < 1000) {
        return [NSString stringWithFormat:@"%ld metres away", (long) (50 * floor((distance.floatValue/50)+0.5))];
    }
    else{
        return [NSString stringWithFormat:@"%.01fkm away",  distance.floatValue/1000];
    }
}
@end
