//
//  OXDaysSelectionTableViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 15/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXBusinessHoursModel.h"

@interface OXDaysSelectionTableViewController : UITableViewController
@property (nonatomic, retain) NSMutableArray *businessHours;

@end
