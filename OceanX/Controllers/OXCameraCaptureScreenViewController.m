//
//  OXCameraCaptureScreenViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 26/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXCameraCaptureScreenViewController.h"
#import "OXImageStackItem.h"
#import "OXEditImagesViewController.h"
#import "UIImage+SPImageFunctions.h"
#import "OXBusinessDetailsTableViewController.h"

@interface OXCameraCaptureScreenViewController (){
    OXCameraThumbnailPreviewViewController *thumbnailPreviewController;
    
}
@property (nonatomic, assign) BOOL shouldDisplayFrontCamera;
@property (nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic, assign) NSUInteger shutterCounter;
@property (nonatomic, retain) NSMutableArray *capturedImages;
@property (nonatomic, assign, getter=isTorchOn) BOOL torchOn;
@property (nonatomic, retain) AVCaptureSession *captureSession;
@property (nonatomic, assign) CGRect cropFrame;
@property (nonatomic, assign) CGFloat smallestEdgeLength;

@end

@implementation OXCameraCaptureScreenViewController
#pragma mark - Lazy loading variables
-(AVCaptureStillImageOutput *)stillImageOutput{
    if(!_stillImageOutput){
        _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
        [_stillImageOutput setOutputSettings:outputSettings];
    }
    return _stillImageOutput;
}
-(NSMutableArray *)capturedImages{
    if(!_capturedImages){
        _capturedImages = [[NSMutableArray alloc] init];
    }
    return _capturedImages;
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
    if(self.comingFromVC == kAddPlaceComingFromVCTypeDetailPOIPage){
        self.title = @"Add a picture";
    }
    else{
        self.title = kCameraCaptureScreenViewControllerNavigationBarTitle;
    }
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"HelveticaNeue" size:25],
      NSFontAttributeName, [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0], NSForegroundColorAttributeName, nil]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.addPoiObject = [[OXHCAddPoiModel alloc] init];
    [self initialiseCameraScene];
}

#pragma mark - init methods

-(void)initialiseCameraScene{
    
    [self initialiseCameraOverlayScreen];
    [self initializeCamera];
}
-(void)initialiseCameraOverlayScreen{
    
    CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    CGFloat navigationBarHeight = self.navigationController.navigationBar.frame.size.height;
    CGFloat height = CGRectGetHeight(self.view.frame)-navigationBarHeight-statusBarHeight;
    CGFloat minimumBottomBorder = 150;
    //        CGFloat minimumTopBorder = 100;
    
    self.smallestEdgeLength = fmin(CGRectGetWidth(self.view.bounds), height);
    
    CGFloat yPositionForCameraOverlay = (height-self.smallestEdgeLength-minimumBottomBorder);
    NSLog(@"yposition for camera overlay: %f", yPositionForCameraOverlay);
    CGFloat getBestYPosition = (yPositionForCameraOverlay > 0) ? yPositionForCameraOverlay : 20;
    
    
    
    self.cropFrame = CGRectMake(0, getBestYPosition, self.smallestEdgeLength, self.smallestEdgeLength);
    self.imagePreview = [[UIView alloc] initWithFrame:self.cropFrame];
    
    [self.view insertSubview:self.imagePreview atIndex:0];
    self.capturingImage = FALSE;
    self.shutterCounter = 0;
    [self.shutterButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:16.0]];
    [self.shutterButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    [self.cancelButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:16.0]];
    [self.cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.cancelButton setHidden:YES];
    
    
    [self.flashButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:16.0]];
    [self.flashButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    UIImage *cameraReverseIcon = [[UIImage imageNamed:@"cameraReverseIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    cameraReverseIcon = [cameraReverseIcon imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.reverseCameraButton setImage:cameraReverseIcon forState:UIControlStateNormal];
    [self.reverseCameraButton setTitle:@"" forState:UIControlStateNormal];
    self.reverseCameraButton.tintColor = [UIColor whiteColor];
    
    self.torchOn = NO;
    [self updateCameraScreenLabels];
    
    
}


//AVCaptureSession to show live video feed in view
- (void) initializeCamera {
    _captureSession = [[AVCaptureSession alloc] init];
    _captureSession.sessionPreset = AVCaptureSessionPresetPhoto;
    /*
     CGRect f = imagePickerController.view.bounds;
     f.size.height -= imagePickerController.navigationBar.bounds.size.height;
     CGFloat barHeight = (f.size.height - f.size.width) / 2;
     UIGraphicsBeginImageContext(f.size);
     [[UIColor colorWithWhite:0 alpha:.5] set];
     UIRectFillUsingBlendMode(CGRectMake(0, 0, f.size.width, barHeight), kCGBlendModeNormal);
     UIRectFillUsingBlendMode(CGRectMake(0, f.size.height - barHeight, f.size.width, barHeight), kCGBlendModeNormal);
     UIImage *overlayImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     
     UIImageView *overlayIV = [[UIImageView alloc] initWithFrame:f];
     overlayIV.image = overlayImage;
     
     
     */
    CGFloat height = CGRectGetHeight(self.view.bounds);
    CGFloat smallestEdgeLength = fmin(CGRectGetWidth(self.view.bounds), height);
    
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    captureVideoPreviewLayer.frame = CGRectMake(0, 0, smallestEdgeLength, smallestEdgeLength);
    [self.imagePreview.layer addSublayer:captureVideoPreviewLayer];
    
    UIView *view = [self imagePreview];
    CALayer *viewLayer = [view layer];
    [viewLayer setMasksToBounds:YES];
    
    //    CGRect bounds = [view bounds];
    
    
    
    //    [captureVideoPreviewLayer setFrame:];
    
    NSArray *devices = [AVCaptureDevice devices];
    AVCaptureDevice *frontCamera;
    AVCaptureDevice *backCamera;
    
    for (AVCaptureDevice *device in devices) {
        
        //        NSLog(@"Device name: %@", [device localizedName]);
        
        if ([device hasMediaType:AVMediaTypeVideo]) {
            
            if ([device position] == AVCaptureDevicePositionBack) {
                //                NSLog(@"Device position : back");
                backCamera = device;
            }
            else {
                //                NSLog(@"Device position : front");
                frontCamera = device;
            }
        }
    }
    
    if (!self.shouldDisplayFrontCamera) {
        NSError *error = nil;
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
        if (!input) {
            NSLog(@"ERROR: trying to open camera: %@", error);
        }
        [_captureSession addInput:input];
    }
    if (self.shouldDisplayFrontCamera) {
        NSError *error = nil;
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
        if (!input) {
            NSLog(@"ERROR: trying to open camera: %@", error);
        }
        [_captureSession addInput:input];
    }
    [_captureSession addOutput:self.stillImageOutput];
    [_captureSession startRunning];
}



- (IBAction)flashButtonDidPress:(UIButton*)button {
    //doing this rather than checking iOS or device versions. much quicker and easier.
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasFlash]){
            [device lockForConfiguration:nil];
            if (self.isTorchOn) {
                [device setFlashMode:AVCaptureFlashModeOff];
                self.torchOn = NO;
            } else {
                [device setFlashMode:AVCaptureFlashModeOn];
                self.torchOn = YES;
            }
            [device unlockForConfiguration];
        }
        else{
            self.torchOn = NO;
        }
    }
    [self updateCameraScreenLabels];
}
- (IBAction)cameraReverseButtonDidPress:(UIButton*)button {
    
    if(_captureSession && !self.isCapturingImage){
        //Indicate that some changes will be made to the session
        [_captureSession beginConfiguration];
        
        //Remove existing input
        AVCaptureInput* currentCameraInput = [_captureSession.inputs objectAtIndex:0];
        [_captureSession removeInput:currentCameraInput];
        
        AVCaptureDevice *newCamera = [self getReverseCaptureDeviceUnitWithCurrentCameraInput:(AVCaptureDeviceInput*)currentCameraInput];
        
        
        //Add input to session
        AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:newCamera error:nil];
        [_captureSession addInput:newVideoInput];
        
        //Commit all the configuration changes at once
        [_captureSession commitConfiguration];
    }
}
-(AVCaptureDevice*)getReverseCaptureDeviceUnitWithCurrentCameraInput:(AVCaptureDeviceInput*)currentCameraInput{
    //Get new input
    AVCaptureDevice *newCamera = nil;
    if(currentCameraInput.device.position == AVCaptureDevicePositionBack){
        newCamera = [self photoCameraWithPosition:AVCaptureDevicePositionFront];
        [self updatecameraScreenLabelsWithCameraFacingBack:YES];
    }
    else{
        newCamera = [self photoCameraWithPosition:AVCaptureDevicePositionBack];
        [self updatecameraScreenLabelsWithCameraFacingBack:NO];
    }
    return newCamera;
}

// Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
- (AVCaptureDevice *)photoCameraWithPosition:(AVCaptureDevicePosition)position{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices){
        if ([device position] == position){
            return device;
        }
    }
    return nil;
}


- (IBAction)shootButtonDidPress:(UIButton*)sender {
    [self checkIfMorePicturesAreAllowedToBeTaken:^(BOOL isAllowedToTakeAnotherPicture) {
        self.capturingImage = FALSE;
        if(isAllowedToTakeAnotherPicture){
            [self captureImage];
        }
        else{
            [self performSegueWithIdentifier:kSegueIdentifierBusinessInfoViewController sender:self];
        }
    }];
}
-(void)captureImage{
    if( ! self.isCapturingImage){
        self.capturingImage = TRUE;
        AVCaptureConnection *videoConnection = nil;
        for (AVCaptureConnection *connection in self.stillImageOutput.connections) {
            
            for (AVCaptureInputPort *port in [connection inputPorts]) {
                
                if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                    videoConnection = connection;
                    break;
                }
            }
            
            if (videoConnection) {
                break;
            }
        }
        
        NSLog(@"about to request a capture from: %@", self.stillImageOutput);
        [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
            
            if (imageSampleBuffer != NULL) {
                [self processImageWithImageSampleBuffer:imageSampleBuffer];
            }
            else{
                self.capturingImage = FALSE;
            }
        }];
        
    }
}

- (void) processImageWithImageSampleBuffer:(CMSampleBufferRef )imageSampleBuffer { //process captured image, crop, resize and rotate
    NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [UIImage imageWithData:imageData];
        OXImageStackItem *currentImageStackItem = [[OXImageStackItem alloc] initWithImage:image withSideLength:self.smallestEdgeLength];
        [self.capturedImages addObject:currentImageStackItem];
        
        
        UIImageWriteToSavedPhotosAlbum(currentImageStackItem.originalImage, nil, nil, nil);
        
//#warning todo - might have to create a datasource method for thumbnail preview controller so that this screen can be loaded again with the parent supplying the array of imageStackItems with ease.
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [thumbnailPreviewController addToThumbnailStackWithImage:currentImageStackItem];
            NSLog(@"processed image");
            self.shutterCounter++;
            [self updateCameraScreenLabels];
            [self checkIfMorePicturesAreAllowedToBeTaken:^(BOOL isAllowedToTakeAnotherPicture) {
                self.capturingImage = FALSE;
                if( ! isAllowedToTakeAnotherPicture){
                    
                    if(self.comingFromVC == kAddPlaceComingFromVCTypeDetailPOIPage){
                        [SVProgressHUD showInfoWithStatus:@"You can only take 5 images at a time" maskType:SVProgressHUDMaskTypeBlack];
                    }
                    else{
                        [self performSegueWithIdentifier:kSegueIdentifierBusinessInfoViewController sender:self];
                    }
                }
            }];
        });
    });
}

-(void)checkIfMorePicturesAreAllowedToBeTaken:(void(^)(BOOL isAllowedToTakeAnotherPicture))completionBlock{
    if(self.shutterCounter >= kAddPlaceCameraCaptureMaxPhotosAllowed){
        completionBlock(FALSE);
    }
    else{
        completionBlock(TRUE);
    }
}
-(void)updateCameraScreenLabels{
    NSString * shutterCounterLabelText;
    if(self.shutterCounter > 0){
        
        if(self.comingFromVC == kAddPlaceComingFromVCTypeDetailPOIPage){
            shutterCounterLabelText = [NSString stringWithFormat:@"%lu", (unsigned long)self.shutterCounter];
            [self.cancelButton setTitle:@"Add" forState:UIControlStateNormal];
            [self.cancelButton setHidden:NO];
            
        }
        else{
            shutterCounterLabelText = [NSString stringWithFormat:@"%lu", (unsigned long)self.shutterCounter];
            [self.cancelButton setTitle:kAddPlaceCameraCaptureNavigationButtonTitle forState:UIControlStateNormal];
            [self.cancelButton setHidden:NO];
        }
    }
    else{
        [self.cancelButton setHidden:YES];
        
        shutterCounterLabelText = @"";
        [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    [self.shutterButton setTitle:shutterCounterLabelText forState:UIControlStateNormal];
    
    
    if(self.isTorchOn){
        [self.flashButton setTitle:@"Flash on" forState:UIControlStateNormal];
    }
    else{
        [self.flashButton setTitle:@"Flash off" forState:UIControlStateNormal];
    }
}

-(void)updatecameraScreenLabelsWithCameraFacingBack:(BOOL)cameraFacingBack{
    if(cameraFacingBack){
        [self.flashButton setHidden:YES];
    }
    else{
        [self.flashButton setHidden:NO];
        
    }
}

-(void)setCapturingImage:(BOOL)capturingImage{
    //keep this method clean.
    _capturingImage = capturingImage;
    if(capturingImage){
        [self cameraOverlayActiveState];
    }
    else{
        [self cameraOverlayInactiveState];
    }
}
-(void)cameraOverlayActiveState{
    [self.shutterButton setProcessingPicture:YES];
    //more stuff can be inserted here
}
-(void)cameraOverlayInactiveState{
    [self.shutterButton setProcessingPicture:NO];
}
//more stuff can be inserted here

- (IBAction)cancelButtonDidPress:(UIButton*)sender {
    if([[sender titleLabel].text isEqualToString:kAddPlaceCameraCaptureNavigationButtonTitle]){
        [self performSegueWithIdentifier:kSegueIdentifierBusinessInfoViewController sender:self];
    }
    else if([[sender titleLabel].text isEqualToString:@"Add"]){
        NSLog(@"add image to server code here");
        
        
        [self processExtraImagesToPOI];
    }
    else{
        NSLog(@"cancel button pressed");
        [[NSNotificationCenter defaultCenter] postNotificationName:kTabBarViewControllerAddPOIButtonSetToUnselectStateNotification object:self];
        
        [self.navigationController.tabBarController setSelectedIndex:0];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(void)processExtraImagesToPOI{
    
    [SVProgressHUD showWithStatus:@"Adding extra images to place"];
    [OXRestAPICient addImagesToPOI:self.capturedImages withPOIID:self.poiID successBlock:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"SUCCESS FOR ADDING EXTRA IMAGES TO POI");
        
        //        [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
        [SVProgressHUD showSuccessWithStatus:@"Successfully added images to place"];
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        NSLog(@"FAILED TO ADD EXTRA IMAGES TO POI: %@", error);
        [SVProgressHUD showErrorWithStatus:@"An error occurred while uploading your images. Please try again later."];
    }];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:kSegueIdentifierCameraThumbnailPreview]){
        NSLog(@"loading thumbnail preview container");
        thumbnailPreviewController = (OXCameraThumbnailPreviewViewController *)segue.destinationViewController;
        [thumbnailPreviewController.view.layer setCornerRadius:10];
        thumbnailPreviewController.delegate = self;
    }
    else if([segue.identifier isEqualToString:kSegueIdentifierBusinessInfoViewController]){
        OXBusinessDetailsTableViewController * businessVC = segue.destinationViewController;
        businessVC.businessImages = self.capturedImages;
        businessVC.addPoiObject = self.addPoiObject;
    }
    
    else if([segue.identifier isEqualToString:kSegueIdentifierEditImagesViewController]){
        NSLog(@"loading edit images view controller");
        OXThumbnailsViewController *editImagesViewController = (OXThumbnailsViewController*)segue.destinationViewController;
        
        if(self.comingFromVC == kAddPlaceComingFromVCTypeDetailPOIPage){
            NSLog(@"COMING FROM DETAIL POI PAGE");
            editImagesViewController.comingFromVC = kAddPlaceComingFromVCTypeDetailPOIPage;
        }
        
        NSLog(@"loading editimages with captured image count: %lu",(unsigned long)self.capturedImages.count);
        [editImagesViewController setCapturedImages:self.capturedImages];
        editImagesViewController.delegate = self;
        editImagesViewController.addPoiObject = self.addPoiObject;
        
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addPOIShowEditImagesWithViewController:(NSArray*)array{
    NSLog(@"array LOL count: %lu", (unsigned long)array.count);
    
    
    if(self.comingFromVC == kAddPlaceComingFromVCTypeDetailPOIPage){
        if(self.capturedImages.count > 0){
            NSLog(@"preparing edit images view controller from detail poi page");
            [self performSegueWithIdentifier:kSegueIdentifierEditImagesViewController sender:self];
        }
        
    }
    else{
        
        if(self.capturedImages.count > 0){
            
            [self performSegueWithIdentifier:kSegueIdentifierEditImagesViewController sender:self];
        }
    }
}


#pragma mark - OXThumbnailsViewControllerDelegate - Methods
-(void)processPicturesAndSendToServer{
    if(self.comingFromVC == kAddPlaceComingFromVCTypeDetailPOIPage){
        [self processExtraImagesToPOI];
    }
}
-(void)updateViewAfterUpdateCapturedImagesArray:(NSMutableArray*)capturedImages{
    self.capturedImages = [[NSMutableArray alloc] initWithArray:capturedImages];
    self.shutterCounter = self.capturedImages.count;
    
    [self updateCameraScreenLabels];
    [thumbnailPreviewController updateThumbnailStackWithCapturedImages:capturedImages];
}
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (BOOL)shouldAutorotate {
    return NO;
}
@end