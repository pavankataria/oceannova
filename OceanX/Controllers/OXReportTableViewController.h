//
//  OXReportTableViewController.h
//  OceanX
//
//  Created by Systango on 1/28/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol OXReportProtocol
-(NSDictionary* )reportingParams;
@end

@interface OXReportTableViewController : UITableViewController
@property (nonatomic, strong) OXReportDataModel* reportData;
@end
