//
//  OXLoginViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 22/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXViewController.h"

@interface OXLoginViewController : OXViewController
@end
