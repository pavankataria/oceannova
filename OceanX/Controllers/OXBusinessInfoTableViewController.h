//
//  OXBusinessInfoTableViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 12/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "OXBusinessHoursModel.h"


@interface OXBusinessInfoTableViewController : UITableViewController
@property (nonatomic, retain) NSMutableArray *businessOpeningHours;

@property (nonatomic, retain) NSMutableArray *businessImages;
@end
