//
//  OXFriendsTableViewController.h
//  Ocean
//
//  Created by Systango on 01/02/2015.
//  Copyright (c) 2015 Ocean Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXFriendTableViewCell.h"
#import "OXFriendRequestTableViewCell.h"
#import "OXTableViewController.h"

@interface OXFriendsTableViewController : OXTableViewController<UISearchBarDelegate, FriendCellDelegate, FriendRequestCellDelegate>

@property (nonatomic,strong) NSMutableArray * friends;
@property (nonatomic,strong) NSMutableArray * searchResults;
@property (nonatomic,strong) IBOutlet UISearchBar * searchBar;
@property (nonatomic,strong) NSMutableArray * friendRequests;
@property (nonatomic,strong) NSMutableArray * friendRequestsSent;
@property (nonatomic) BOOL hasSearchResults;
@end