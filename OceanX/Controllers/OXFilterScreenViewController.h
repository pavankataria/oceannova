//
//  OXFilterScreenViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 31/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXSearchBarView.h"
@interface OXFilterScreenViewController : UIViewController<OXSearchBarDelegate, OXSearchBarDataSource, OXSearchContainerScreenViewControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) IBOutlet OXSearchBarView *addressSearchBar;

@property (nonatomic, weak) id <OXSearchContainerScreenViewControllerOperationProtocol> parentVC;
//@property (nonatomic, weak) id <OXSearchContainerScreenViewControllerDelegate> delegate;
@property (nonatomic, retain) IBOutlet UITableView *tableView;


@end
