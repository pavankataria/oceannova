//
//  ContainerViewController.h
//  EmbeddedSwapping
//
//  Created by Michael Luton on 11/13/12.
//  Copyright (c) 2012 Sandmoose Software. All rights reserved.
//

#import "OXMapAndListContainerViewController.h"
#import "OXSearchByMapViewController.h"
#import "OXSearchListViewController.h"


@interface OXMapAndListContainerViewController : UIViewController{
    
}
@property (strong, nonatomic) OXSearchByMapViewController *mapViewController;
@property (strong, nonatomic) OXSearchListViewController *listViewController;

-(void)swapViewControllers;
-(void)swapToListViewController;
-(void)swapToMapViewController;

-(void)performSerchWithObject:(id)object;

-(void)setParentViewControllerForChildViewControllers:(UIViewController<OXSearchContainerScreenViewControllerOperationProtocol>*)parentViewControllerObject;
@end
