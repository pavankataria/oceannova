//
//  OXDetailSearchByMapViewController.h
//  OceanX
//
//  Created by Systango on 2/2/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXDetailSearchByMapViewController : UIViewController
@property (strong, nonatomic) OXBasePOIModel *mapElement;
@end
