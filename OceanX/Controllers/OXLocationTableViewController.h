//
//  OXLocationTableViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXFilterTableViewController.h"

#import "OXPOISearchModelDelegate.h"
@interface OXLocationTableViewController : UITableViewController<UISearchBarDelegate,OXPOISearchModelDelegate>
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, retain) OXPOISearchModel *searchPOIModel;
@property (nonatomic, weak) id <OXPOISearchModelDelegate> delegate;
- (IBAction)cancelViewController:(id)sender;
@end
