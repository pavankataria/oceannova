//
//  OXReportTextViewController.m
//  OceanX
//
//  Created by Systango on 16/02/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXReportTextViewController.h"
#import "OXReportDataModel.h"

@interface OXReportTextViewController () <UITextViewDelegate> {
}
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;

@end

@implementation OXReportTextViewController
-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    NSLog(@"view will layoutsubviews");
    NSLog(@"self.view.frame report: %@", NSStringFromCGRect(self.view.frame));
    self.messageTextView.frame = self.view.frame;
    NSLog(@"self.messageTextView.frame report: %@", NSStringFromCGRect(self.messageTextView.frame));
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"What's wrong?";
//    [self.view setBackgroundColor:[PKUIAppearanceController grayColor]];
    [self setupNavigtionBar];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.messageTextView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction methods
- (IBAction)reportButtonTapped:(id)sender {
     self.reportData.reportText = [self.messageTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    [self.messageTextView resignFirstResponder];
    [self reportIssue];
}

- (IBAction)cancelButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private methods
- (void)reportIssue {
    [self.view endEditing:YES];
    [self.delegate didEnteredMessageForReport:self.reportData];
}

-(void)setupNavigtionBar{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);

}

- (void)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark - Text View Delegate methods
- (void)textViewDidChange:(UITextView *)textView {
    NSString *message = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (message.length > 0 && (!self.navigationItem.rightBarButtonItem.enabled)) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else if (message.length == 0 && self.navigationItem.rightBarButtonItem.enabled){
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

@end
