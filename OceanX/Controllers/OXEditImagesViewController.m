//
//  OXEditImagesViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 28/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OXEditImagesViewController.h"
#import "OXBusinessDetailsTableViewController.h"

@interface OXEditImagesViewController ()

@end

@implementation OXEditImagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.previewMediaTableThumbnailHeight = 300;
    [self reloadMediaView];
    
//    NSLog(@"preview scroll rect: {%@}", NSStringFromCGRect(self.previewScrollView.frame));
//    NSLog(@"preview scroll rect bounds: {%@}", NSStringFromCGSize(self.view.frame.size));
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - PKPreviewMediaViewController Data source methods

-(PKPreviewItemView*)previewItemViewForIndexPosition:(NSUInteger)position{
    PKPreviewItemView *currentPreviewItem = [[PKPreviewItemView alloc] init];
    currentPreviewItem.backgroundColor = position % 2 == 0 ? [UIColor darkGrayColor] : [UIColor grayColor];
    UIImage *image = [_capturedImages[position] originalImage];
    currentPreviewItem.previewThumbnail.image = image;
    return currentPreviewItem;

}
-(PKPreviewThumbnail *)previewThumbnailViewForIndexPosition:(NSUInteger)position{
    NSLog(@"position: %lu", (unsigned long)position);
    PKPreviewThumbnail *currentThumbnail = [[PKPreviewThumbnail alloc] init];
    currentThumbnail.backgroundColor = [UIColor blackColor];
    
    
    UIImage *image = [_capturedImages[position] thumbnailImage];
//    NSLog(@"image: %@ isMemberOfImage: %@", image, [image isMemberOfClass:[UIImage class]] == TRUE ? @"YES" : @"NO");
    
    
//NSLog(@"size of image: %@ and thumbnail: %@", NSStringFromCGSize([[_capturedImages[position] originalImage] size]),NSStringFromCGSize([image size]));
    
    currentThumbnail.previewThumbnail.image = image;
    return currentThumbnail;
}
-(NSUInteger)numberOfPreviewCellsToDisplay{
    return self.capturedImages.count;
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}

-(void)deleteThumbnailForIndexPosition:(NSUInteger)index{
    NSLog(@"delete thumbnail for index position: %lu", (unsigned long)index);
    [self.capturedImages removeObjectAtIndex:index];
    [self reloadMediaView];
}
@end
