//
//  OXDaysSelectionTableViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 15/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXDaysSelectionTableViewController.h"
#import "OXHoursTableViewCell.h"



@interface OXDaysSelectionTableViewController ()
@end

@implementation OXDaysSelectionTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Opening Hours";
    NSLog(@"view did load days selection tableview controller");
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - navigation
-(IBAction)changeTime{
    
//    NSArray *businessHourObjectsToChangeTimeFor = ;//
}
-(IBAction)cancel{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    
    return kDaysSelectionWeeksToShow;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return kDaysSelectionDaysOfTheWeek;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//        NSLog(@"cellForRowAtIndexPath: %lu", indexPath.row);
    OXHoursTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifierDaysSelection];
    
    [cell updateCellWithObject:self.businessHours[indexPath.row]];
    //In this method I simply grab the values of the object's attributes and assign them to the cell's two label's text property
    
    NSLog(@"is cell nill: %@", cell == nil ? @"nil" : @"not nil");
    NSLog(@"The cell class is a %@, which %s a subclass of UITableViewCell", NSStringFromClass([cell class]), [cell isKindOfClass:[UITableViewCell class]] ? "is" : "is not");
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kOXDaysSelectionTableViewRowHeight;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Select day(s) that you wish to ammend the opening time for and click change time.";
}



/*kTableViewCellIdentifierDaysSelection
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
