//
//  OXBusinessDetailsTableViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 18/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXAmendHoursTableViewController.h"
#import "OXTableViewController.h"

@interface OXBusinessDetailsTableViewController : OXTableViewController<OXAmendHoursDelegate, OXBusinessDetailCellDelegate>
@property (nonatomic,retain) NSMutableArray *businessOpeningHours;

@property (nonatomic, retain) NSMutableArray *businessImages;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *nextBarButtonItem;
@property (nonatomic, retain) OXHCAddPoiModel *addPoiObject;


@end
