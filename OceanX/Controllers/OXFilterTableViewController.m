//
//  OXFilterTableViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXFilterTableViewController.h"
#import "OXFilterTableViewCell.h"

#import "OXFilterTextTableViewCell.h"
@interface OXFilterTableViewController (){
    UITextField *weakLinkToTextSearchTextField;
}

@end

@implementation OXFilterTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    switch (indexPath.section) {
        case OXFilterTableSectionTypeLocation:
        case OXFilterTableSectionTypeTags:
        case OXFilterTableSectionTypeFriends:
            cell = (OXFilterTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"reuseFilterCell" forIndexPath:indexPath];
            [(OXFilterTableViewCell*)cell setCellWithSearchPOIObject:self.searchPOIObject andWithSection:indexPath.section];
            return cell;
            break;
        case OXFilterTableSectionTypeText:
            cell = (OXFilterTextTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"textReuseFilterCell" forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [(OXFilterTextTableViewCell *)cell setCellWithSearchPOIObject:self.searchPOIObject];
            
            weakLinkToTextSearchTextField = ((OXFilterTextTableViewCell *) cell).textSearchTextField;
            weakLinkToTextSearchTextField.delegate = self;
            [weakLinkToTextSearchTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

        default:
            break;
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    switch (indexPath.section) {
        case OXFilterTableSectionTypeLocation:
            [self performSegueWithIdentifier:@"locationViewControllerSegueIdentifier" sender:self];
            break;
            
        case OXFilterTableSectionTypeTags:
            [self performSegueWithIdentifier:@"tagsViewControllerSegueIdentifier" sender:self];
            break;
            
        case OXFilterTableSectionTypeFriends:
            
            break;
            
        case OXFilterTableSectionTypeText:
            break;
            
        default:
            break;
            
    }
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case OXFilterTableSectionTypeLocation:
            return @"Location";
            break;
            
        case OXFilterTableSectionTypeTags:
            return @"Tags";
            break;
            
        case OXFilterTableSectionTypeFriends:
            return @"Friends";
            break;
            
        case OXFilterTableSectionTypeText:
            return @"Text";
            break;
            
        default:
            return @"should never come here";
            break;
    }
}
-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    if(section == OXFilterTableSectionTypeText){
        return @"You can only either search by text or tags\nSearching by text will remove the tags from your current search.";
    }
    return @"";
}
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"segue: %@", segue.identifier);
    
    NSLog(@"segue destination view controller: %@", segue.destinationViewController);
    
    if([segue.destinationViewController conformsToProtocol:@protocol(OXPOISearchModelDelegate)]){
        NSLog(@"segue destination view controller: %@", segue.destinationViewController);
        id <OXPOISearchModelDelegate>destinationVC = segue.destinationViewController;
        [destinationVC setDelegate:self];
        [destinationVC setSearchPOIModel:self.searchPOIObject];
    }
}

-(void)OXPOISearchModelUpdatePOISearchObject:(OXPOISearchModel*)object{
    self.searchPOIObject = object;
    NSLog(@"self.searchpoimodel.coordinate: %@", self.searchPOIObject.coordinate);
    [self.tableView reloadData];
}


- (IBAction)searchAction:(id)sender {
    NSLog(@"filter table view controller: %@", [self.searchPOIObject params]);
    [self.delegate performSearchWithPOISearchObject:self.searchPOIObject];
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - OXSearchDelegate - Methods
-(void)refreshTableView{
    if([self.searchPOIObject.tags count] > 0){
        self.searchPOIObject.textSearchString = @"";
        weakLinkToTextSearchTextField.text = @"";
    }
    [self.tableView reloadData];
}

#pragma mark - UITextField Delegate methods - Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.searchPOIObject removeAllTagsFromSearch];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:OXFilterTableSectionTypeTags] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)textFieldDidChange:(UITextField*)textField{
    [self.searchPOIObject setTextSearchString:textField.text];
}

@end
