//
//  OXCameraAddPlaceViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 26/12/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface OXCameraAddPlaceViewController : UIViewController

@property(nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic, assign) NSUInteger shutterCounter;

@end
