//
//  OXPickerViewController.h
//  OceanX
//
//  Created by Systango on 1/30/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OXPickerViewDelegate <NSObject>

- (void)didSelectYear:(NSString *)year;

@end

@interface OXPickerViewController : UIViewController

@property(nonatomic, assign)id <OXPickerViewDelegate> delegate;

@end
