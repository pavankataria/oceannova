//
//  OXQuestionSaveViewController.m
//  OceanX
//
//  Created by Systango on 2/9/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXQuestionSaveViewController.h"
#import "OXQuestionViewController.h"

@interface OXQuestionSaveViewController ()

@property (weak, nonatomic) IBOutlet UILabel *questionsCompleteTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionsCompleteMessageLabel;
@property (weak, nonatomic) IBOutlet UIButton *saveAndExitButton;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivityIndicatorView;

@end

@implementation OXQuestionSaveViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.currentFacetIndex == self.missingFacets.count)
    {
        self.questionsCompleteTitleLabel.text = kQuestionSaveFacetsCompleteTitle;
        self.questionsCompleteMessageLabel.text = kQuestionSaveFacetsCompleteMessage;
        self.continueButton.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)hideViewElements:(BOOL)shouldHide
{
    self.questionsCompleteTitleLabel.hidden = shouldHide;
    self.questionsCompleteMessageLabel.hidden = shouldHide;
    self.saveAndExitButton.hidden = shouldHide;
    self.continueButton.hidden = shouldHide;
    self.backButton.hidden = shouldHide;
    [self.continueButton.layer setCornerRadius:10];
    [self.continueButton.layer setMasksToBounds:YES];
}

# pragma mark - IBAction Methods

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveAndExitButtonTapped:(id)sender
{
    [self hideViewElements:YES];
    [self.loadingActivityIndicatorView startAnimating];
    
    OXFacetModel * facet = self.missingFacets[self.currentFacetIndex - 1];
    
    [OXRestAPICient addAnswersForQuestions:facet.questions successBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             [[UIApplication sharedApplication] setStatusBarHidden:NO];
             [self.navigationController dismissViewControllerAnimated:YES completion:nil];
         });
         
     } failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             [self hideViewElements:NO];
             [self.loadingActivityIndicatorView stopAnimating];
         });
         
     }];
}

- (IBAction)continueButtonTapped:(id)sender
{
    [self hideViewElements:YES];
    [self.loadingActivityIndicatorView startAnimating];
    
    OXFacetModel *facet = self.missingFacets[self.currentFacetIndex - 1];
    
    [OXRestAPICient addAnswersForQuestions:facet.questions successBlock:^(AFHTTPRequestOperation *operation, id response)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             NSArray *viewControllers = [NSArray arrayWithObject:self.navigationController.viewControllers.lastObject];
             self.navigationController.viewControllers = viewControllers;
             
             [self performSegueWithIdentifier:kSegueIdentifierQuestionViewController sender:nil];
             
         });
         
     }  failBlock:^(AFHTTPRequestOperation *operation, id error)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             [self hideViewElements:NO];
             [self.loadingActivityIndicatorView startAnimating];
             
         });
     }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:kSegueIdentifierQuestionViewController])
    {
        OXQuestionViewController * destination = segue.destinationViewController;
        destination.missingFacets = self.missingFacets;
        destination.currentFacetIndex = self.currentFacetIndex + 1;
        destination.currentQuestionIndex = 1;
    }
}

@end
