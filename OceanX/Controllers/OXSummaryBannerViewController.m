//
//  OXSummaryBannerViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 08/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSummaryBannerViewController.h"

@interface OXSummaryBannerViewController ()

@end

@implementation OXSummaryBannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.view.backgroundColor = [PKUIAppearanceController darkBlueColor];
    self.circularIconView.layer.cornerRadius = CGRectGetWidth(self.circularIconView.frame)/2;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapFirstLabel)];
    [self.firstLabel addGestureRecognizer:tap];
    [self.firstLabel setUserInteractionEnabled:YES];
    [self updateButtonLabels];
    [self updateButtons];
    
}
-(void)didTapFirstLabel{
    NSLog(@"LOL did tap");
    [self.delegate buttonOnePressed];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)updateButtons{
    [self.firstButton addTarget:self action:@selector(firstButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.secondButton addTarget:self action:@selector(secondButton:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setPoiSearchModel:(OXPOISearchModel *)poiSearchModel{
    _poiSearchModel = poiSearchModel;
    [self updateButtonLabels];
}

-(void)updateButtonLabels{
    NSString *string;
    BOOL presentingData = FALSE;
    if(self.poiSearchModel.textSearchString.length == 0){
        if(self.poiSearchModel.tags.count > 0){
            string = [NSString stringWithFormat:@"Tags: %lu", (unsigned long)self.poiSearchModel.tags.count];
            presentingData = TRUE;
        }
        else{
            string = @"Search by tags?";
        }
    }
    else{
        string = [NSString stringWithFormat:@"Name search: %@", self.poiSearchModel.textSearchString];
        presentingData = TRUE;
    }
    
    //http://stackoverflow.com/questions/6538196/uibutton-text-truncated
    //WE DONT WANT TO USE THIS:
    [self.firstLabel setText:string];
//    self.firstButton.titleLabel.text = string; // instead we want this
    

    NSString *friendsJoining;
    if(self.poiSearchModel.friendsIds.count > 0){
        friendsJoining = [NSString stringWithFormat:@"Friends joining: %lu", (unsigned long)self.poiSearchModel.friendsIds.count];
        presentingData = TRUE;
    }
    else{
        friendsJoining = [NSString stringWithFormat:@"Search with friends"];
    }
    [self.secondButton setTitle:friendsJoining forState:UIControlStateNormal];
    
    
    if(presentingData){
        [self.view setHidden:NO];
    }
    else{
        [self.view setHidden:YES];
    }
    [self.delegate presentingAnyData:presentingData];

}
#pragma mark - OXSummary delegate - Methods
-(void)firstButton:(id)sender{
    [self.delegate buttonOnePressed];
}
-(void)secondButton:(id)sender{
    [self.delegate buttonTwoPressed];
}


@end
