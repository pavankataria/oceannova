//
//  OXTagsTableViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 20/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OXTagsTableViewController : UITableViewController
@property (nonatomic, weak) IBOutlet UITextField *addTagTextField;
@property (nonatomic, weak) IBOutlet UIButton *addTagFromCacheButton;
@property (nonatomic, weak) IBOutlet UIView *addTagBackgroundView;
//-(IBAction)addTagFromCache:(id)sender;

//@property (nonatomic, retain) NSMutableDictionary *tags;
// TODO : there should be an NsMututableArray instead of NSMutableDictornary
@property (nonatomic, retain) NSMutableArray *tags;

@end
