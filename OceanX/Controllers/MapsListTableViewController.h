//
//  MapsListTableViewController.h
//  OceanX
//
//  Created by admin on 13/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapListTableViewCell.h"

@protocol OXPOIDetailDelegate <NSObject>
@required
-(void)didSelectedPoi:(OXBasePOIModel*)basePoi;

@end

@interface MapsListTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MapListCellProtocol>

@property (nonatomic, strong) NSArray *poiList;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, weak) id <OXPOIDetailDelegate> delegate;

@end
