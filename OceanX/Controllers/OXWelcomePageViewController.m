//
//  OXWelcomePageViewController.m
//  OceanX
//
//  Created by Systango on 2/13/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXWelcomePageViewController.h"

@interface OXWelcomePageViewController ()
@property (weak, nonatomic) IBOutlet UILabel *usernameWelcomeLabel;

@end

@implementation OXWelcomePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kSavedUserNameKey];

    self.usernameWelcomeLabel.text = [NSString stringWithFormat:@"Hi %@", [userName stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                                                                            withString:[[userName substringToIndex:1] capitalizedString]]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
