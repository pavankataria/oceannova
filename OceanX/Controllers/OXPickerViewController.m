//
//  OXPickerViewController.m
//  OceanX
//
//  Created by Systango on 1/30/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXPickerViewController.h"

@interface OXPickerViewController ()<UIPickerViewDataSource ,UIPickerViewDelegate>

@property (strong ,nonatomic) NSMutableArray *years;
@property (weak ,nonatomic) IBOutlet UIPickerView *yearPickerView;
@property (strong, nonatomic) NSString* selectedYear;

@end

@implementation OXPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.selectedYear = [NSString stringWithFormat:@"%lu", (unsigned long)kInitialYearOfBornToShow];
   
    [self yearArray];
    
    [self.yearPickerView selectRow:90 inComponent:0 animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Method

- (IBAction)doneButtontapped:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(didSelectYear:)]) {
        [self.delegate didSelectYear:self.selectedYear];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - Picker View Delegate

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView*)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.years count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.years objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    self.selectedYear = [self.years objectAtIndex:row];
}

#pragma mark - Private Method

- (void)yearArray
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy"];
    
    int currentYear  = [[formatter stringFromDate:[NSDate date]] intValue];
    
    //Create Years Array from 1960 to This year
    self.years = [[NSMutableArray alloc] init];
    
    for (NSUInteger initialYear = kInitialYearOfBornToShow; initialYear <= currentYear; initialYear++) {
        
        [self.years addObject:[NSString stringWithFormat:@"%lu",(unsigned long)initialYear]];
    }
}

@end
