//
//  OXAddTagViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 20/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXAddTagViewController.h"
#import "OXTagTableViewCell.h"
#import "OXAddTagDisplayTableViewCell.h"
#import "OXTagPlaceCountModel.h"
#import "OXTagTableViewCell.h"



@interface OXAddTagViewController ()
@end

@implementation OXAddTagViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self registerForKeyboardNotifications];
    [self setupTable];
    [self setupNavigtionBar];
    
    NSMutableCharacterSet *allowedCharacters = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
    [allowedCharacters formUnionWithCharacterSet:[NSMutableCharacterSet characterSetWithCharactersInString:@"_ "]];
    self.blockedCharacters = [allowedCharacters invertedSet];
}
-(void)setupTable{
    self.tagsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tagsTableView setEditing:YES];
    self.tagsTableView.allowsSelectionDuringEditing = YES;
    
    [self.tagsTableView registerNib:[UINib nibWithNibName:@"OXTagTableViewCell" bundle:nil] forCellReuseIdentifier:@"normalTagSearchCell"];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self.addTagTextField becomeFirstResponder];
    [self updateUI];
}
-(void)updateUI{
    if(self.storedTagsArray.count < kOXAddTagsMinimumTagsRequirement){
        [self.finishButtonItem setEnabled:NO];
    }
    else{
        [self.finishButtonItem setEnabled:YES];
    }
}
- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void) keyboardWillBeHidden:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self.tagsTableView setContentInset:contentInsets];
    [self.tagsTableView setScrollIndicatorInsets:contentInsets];
    
    [self.tagsTableView setContentOffset:CGPointMake(0,self.tagsTableView.contentOffset.y) animated:YES];
    
}

- (void) keyboardWasShown:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    [self.tagsTableView setContentInset:contentInsets];
    [self.tagsTableView setScrollIndicatorInsets:contentInsets];
    [self.tagsTableView setContentOffset:CGPointMake(0,self.tagsTableView.contentOffset.y) animated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.addTagTextField resignFirstResponder];
    
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
        if(self.searchTagsResultsArray.count == 0){
            return 1;
        }
        else{
            return self.searchTagsResultsArray.count;
        }
    }
    else if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
        static NSUInteger kAddTagGuidanceDisplayCellOffset = 1;
        return self.storedTagsArray.count + kAddTagGuidanceDisplayCellOffset;
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        OXTagTableViewCell *cell = (OXTagTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"normalTagSearchCell"];
        cell.tagNameLabel.textColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.userInteractionEnabled = YES;
        if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
            NSLog(@"reached here table view search");
            if([self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
                
//                cell.tagNameLabel.text = [NSString stringWithFormat:@"Already created: %@", [self addTagTextWithExcessCharactersRemoved]];
//                cell.tagNameLabel.textColor = [UIColor lightGrayColor];
//                cell.selectionStyle = UITableViewCellSelectionStyleNone;
//                cell.userInteractionEnabled = NO;
//                return cell;
            }
            if(self.searchTagsResultsArray.count == 0){
                if(![self doesStoredTagsArrayContainUncapitalisedComparisonWithStringTag:[self addTagTextWithExcessCharactersRemoved]]){
                    cell.tagNameLabel.text = [NSString stringWithFormat:@"Create tag: %@", [self addTagTextWithExcessCharactersRemoved]];
                    cell.tagPlacesCountLabel.text = @"";
                }
            }
            else{
                cell.tagNameLabel.text = [self.searchTagsResultsArray[indexPath.row] tagName];
                cell.tagPlacesCountLabel.text = [self.searchTagsResultsArray[indexPath.row] totalPlacesCountString];
            }
        }
        else if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
            if(indexPath.row == self.storedTagsArray.count){
                [cell setTableCellForUIMessageCellForIndexPath:indexPath];
            }
            else{
                OXTagPlaceCountModel *tag = self.storedTagsArray[indexPath.row];
                cell.tagNameLabel.text = tag.tagName;
                cell.tagPlacesCountLabel.text = tag.totalPlacesCountString;
            }
        }
        return cell;
    }
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kOXAddTagsTableViewRowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
        if(self.searchTagsResultsArray.count == 0){
            OXTagPlaceCountModel *newTag = [[OXTagPlaceCountModel alloc] initWithTagName:[self addTagTextWithExcessCharactersRemoved] andTotalPlacesCount:0];
            [self.storedTagsArray addObject:newTag];
        }
        else{
            [self.storedTagsArray addObject:self.searchTagsResultsArray[indexPath.row]];
            [self.searchTagsResultsArray removeAllObjects];
        }
        self.addTagTextField.text = @"";
        [self updateTableView];
        
    }
    else if(self.currentTableViewShowing ==kAddTagViewControllerShowTableViewNormal){
        
    }
}
#pragma mark - editing delegate methods
//
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.section == 0){
//        return NO;
//    }
//    else{
//        return YES;
//    }
//}

#pragma mark - UITextField delegate methods
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)characters{
    //    NSLog(@"textfield.length %lu\nrange.length %lu\nrange.location: %lu", [textField.text length], range.length, range.location);
    if (range.location == 0 && [characters isEqualToString:@" "]) {
        return NO;
    }
    
    return ([characters rangeOfCharacterFromSet:self.blockedCharacters].location == NSNotFound);
}


-(void)updateTableView{
    if([self.addTagTextField.text length] > 0){
        self.currentTableViewShowing = kAddTagViewControllerShowTableViewSearch;
    }
    else{
        self.currentTableViewShowing = kAddTagViewControllerShowTableViewNormal;
    }
    [self.addPoiObject setTags:self.storedTagsArray];
    [self updateUI];
    [self.tagsTableView reloadData];
}
-(IBAction)finishButtonTapped:(id)sender{
//    NSLog(@"add poi params to be sent to the server", [self.addPoiObject params]);
    [self processPOIToServer];
}
-(void)processPOIToServer{
    [self.view endEditing:YES];
    
    [SVProgressHUD showWithStatus:@"Adding place" maskType:SVProgressHUDMaskTypeBlack];
    
    [OXRestAPICient postAddPoiWithAddPoiModel:self.addPoiObject
                             withSuccessBlock:^(AFHTTPRequestOperation *operation, id response) {
        
                                 [SVProgressHUD showSuccessWithStatus:@"Successfully Added!" maskType:SVProgressHUDMaskTypeBlack];

                                 [[NSNotificationCenter defaultCenter] postNotificationName:kTabBarViewControllerAddPOIButtonSetToUnselectStateNotification object:self];
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                 
                                 
    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        NSLog(@"failed operation: %@", operation);
        NSLog(@"error: %@", error);
        
        [SVProgressHUD showErrorWithStatus:@"Failed to add place. Please try again!" maskType:SVProgressHUDMaskTypeBlack];
        
    }];
}



#pragma mark - UITable View Editing options

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewSearch){
        return NO;
    }
    else if(self.currentTableViewShowing == kAddTagViewControllerShowTableViewNormal){
        
        if(indexPath.row == self.storedTagsArray.count){
            return NO;
        }
        else{
            return YES;
        }
    }
    return NO;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        [self.storedTagsArray removeObjectAtIndex:indexPath.row];
        
        [self.tagsTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tagsTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.storedTagsArray.count inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self updateUI];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark - Private methods

-(void)setupNavigtionBar
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);

}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
