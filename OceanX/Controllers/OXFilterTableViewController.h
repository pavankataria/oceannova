//
//  OXFilterTableViewController.h
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OXPOISearchModelDelegate.h"
#import "OXSearchDelegate.h"
#import "OXFilterTableViewControllerDelegate.h"

@interface OXFilterTableViewController : UITableViewController<OXPOISearchModelDelegate, OXSearchDelegate, UITextFieldDelegate, UIAlertViewDelegate>
@property (nonatomic, retain) OXPOISearchModel *searchPOIObject;

@property (nonatomic, weak) id<OXFilterTableViewControllerDelegate> delegate;

- (IBAction)searchAction:(id)sender;

@end
