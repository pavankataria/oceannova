//
//  OXTagShowPlaceViewController.m
//  OceanX
//
//  Created by Systango on 2/16/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXTagShowPlaceViewController.h"
#import "OXTagsPresentationTableViewController.h"


@interface OXTagShowPlaceViewController () <OXDetailPOIProtocol>
@property (strong, nonatomic) IBOutlet UIView *tagView;
@property (nonatomic, retain) OXTagsPresentationTableViewController *tagsPresentationView;

@end
@implementation OXTagShowPlaceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tagsPresentationView.view.frame = CGRectMake(0, 0, self.tagView.frame.size.width, self.tagView.frame.size.height);
    [self.tagView addSubview:self.tagsPresentationView.view];
 
    self.tagsPresentationView.poiObject = [self getPOIObject];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(OXTagsPresentationTableViewController *)tagsPresentationView{
    if(!_tagsPresentationView){
        _tagsPresentationView = [[OXTagsPresentationTableViewController alloc] initWithStyle:UITableViewStylePlain];
        _tagsPresentationView.comingFromVC = OXTagsPresentationTableViewComingFromVCTypeTutorial;
        _tagsPresentationView.delegate = self;
    }
    return _tagsPresentationView;
}

#pragma mark - OXDetailPOIProtocol delegate - Methods
-(void)updateDetailPOIScrollViewContentSize{
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - Private Methods

- (OXPOIModel *)getPOIObject {
    OXTagModel *tagOne = [OXTagModel new];
    tagOne.name = @"pizza";
    tagOne.tagCount = 22;
    OXPOIModel *poi = [OXPOIModel new];
    poi.tags = [NSArray arrayWithObjects:tagOne, nil];
    return poi;
}

@end
