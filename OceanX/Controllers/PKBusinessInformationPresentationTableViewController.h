//
//  PKBusinessInformationPresentationTableViewController.h
//  
//
//  Created by Pavan Kataria on 17/02/2015.
//
//

#import <UIKit/UIKit.h>
#import "OXDetailPOIProtocol.h"
#import "TTTAttributedLabel.h"
@interface PKBusinessInformationPresentationTableViewController : UITableViewController<TTTAttributedLabelDelegate>
@property (nonatomic, retain) OXPOIModel *poiObject;
@property (nonatomic, weak) id <OXDetailPOIProtocol> delegate;
@end
