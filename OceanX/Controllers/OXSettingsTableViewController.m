//
//  OXSettingsTableViewController.m
//  OceanX
//
//  Created by Systango on 2/12/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXSettingsTableViewController.h"
#import "OXWebViewViewController.h"
@interface OXSettingsTableViewController ()

@end

@implementation OXSettingsTableViewController

- (void)viewDidLoad
{    
    [super viewDidLoad];
    [self setupNavigtionBar];
    [Flurry logEvent:kFlurrySettingScreen];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Delegate Method

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"row: %ld section: %ld", (long)indexPath.row, (long)indexPath.section);
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    switch (indexPath.section)
    {
        case kSettingsTableSectionLogout:
            [self performLogout];
            break;
            
        case kSettingsTableSectionSocialNetworking:
            
            switch (indexPath.row)
            
        {
            case kSettingsSocialNetworkingTableViewCellFacebook:
                
                
                
                if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://profile/711828442175087"]]){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/711828442175087"]];
                }
                else{
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/OceanApp"]];
                }

                
                break;
                
            case kSettingsSocialNetworkingTableViewCellTwitter:
                
                if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]]){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=OceanAppLondon"]];
                }
                else{
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.twitter.com/OceanAppLondon"]];
                }

                
                break;
                
            default:
                break;
                
        }
          case kSettingsTableSectionSupport:
            if (indexPath.row == 3)
            {
                //TODO: Handle open mail
                NSString *  URLEMail  = @"mailto:support@oceanapp.com?subject=Ocean App Support Message";
                NSString *url = [URLEMail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
            }
        default:
            break;
    }
}

#pragma mark - Private Method

-(void)performLogout
{
    [[OXLoginManager sharedManager] performLogoutSegue];
}

-(void)setupNavigtionBar
{
    self.navigationController.navigationBar.topItem.title = kProfileViewControllerNavigationTitle;

    self.title = kSettingsViewControllerNavigationTitle;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[PKQuickMethods fadedLeftArrowImage] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    self.navigationItem.leftBarButtonItem.tintColor = rgb(110, 110, 110);
    self.navigationItem.rightBarButtonItem.tintColor = rgb(110, 110, 110);

}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    UINavigationController * navigationController =  (UINavigationController*)[segue destinationViewController];
    OXWebViewViewController * destination = (OXWebViewViewController*) navigationController.viewControllers[0];
    
    if ([segue.identifier isEqualToString:kSegueIdentifierPrivacyWebviewViewController])
    {
        destination.URL = @"http://oceanapp.com/privacy/";
    }
    else if([segue.identifier isEqualToString:kSegueIdentifierAboutWebviewViewController])
    {
        destination.URL = @"http://oceanapp.com/about/";
    }
    else if([segue.identifier isEqualToString:kSegueIdentifierTermsAndConditionWebviewViewController])
    {
        destination.URL = @"http://oceanapp.com/eula/";
    }
    
}

- (void)backButtonTapped:(id)sender
{
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    transition.subtype = kCATransitionFromRight; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
}
@end
