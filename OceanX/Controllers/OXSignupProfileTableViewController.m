//
//  OXSignupProfileTableViewController.m
//  OceanX
//
//  Created by Systango on 1/30/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//
// FYI - background colour set programatically in the view. Cell background set in storyboard

#import "OXSignupProfileTableViewController.h"
#import "OXGenderTableViewCell.h"
#import "OXGenderSelectionTableViewCell.h"
#import "OXPickerViewController.h"
#import "AppDelegate.h"
#import "OXWebViewViewController.h"

static const CGFloat kMinimumPasswordLength = 5;
static const CGFloat kYearOfBornLabelTag = 1;


@interface OXSignupProfileTableViewController () <UITextFieldDelegate, OXPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITableViewCell *yearOfBornCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *createNewUserCell;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthYearField;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (strong, nonatomic) IBOutlet UIButton *checkmarkButton;
@property (strong, nonatomic) OXPickerViewController *pickerViewController;
@property (strong, nonatomic) OXUserModel *user;
@end

@implementation OXSignupProfileTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.user = [[OXUserModel alloc]init];
    self.view.backgroundColor = [PKUIAppearanceController darkBlueColor];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self.emailTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kSegueIdentifierPickerViewController]) {
        self.pickerViewController = segue.destinationViewController;
        self.pickerViewController.delegate = self;
    }
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    if(indexPath.row == kContinueCellTableView){
        [self performSignup];
    }
}

#pragma mark - Textfield Delegate Method

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ( self.emailTextField ||  self.userNameTextField || self.passwordTextField)
    {
        NSCharacterSet* numbers = [NSCharacterSet
                                   characterSetWithCharactersInString:@" "];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([numbers characterIsMember:c]) {
                return NO;
            }
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.emailTextField) {
        [self.emailTextField resignFirstResponder];
        [self.userNameTextField becomeFirstResponder];
    }
    else if (textField == self.userNameTextField)
    {
        [self.userNameTextField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
        [self scrollTableView];
    }
    else if (textField == self.passwordTextField)
    {
        [self.passwordTextField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - Private Method

- (void)scrollTableView
{
    [self.tableView scrollRectToVisible:self.createNewUserCell.frame animated:YES];
}

- (BOOL)performValidation
{
    self.errorLabel.hidden = YES;
    NSString *username = [self.userNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *password = [self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *email = [self.emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (![PKQuickMethods validateEmailWithString:email])
    {
        self.errorLabel.hidden = NO;
        [SVProgressHUD showErrorWithStatus:@"Email Invalid. Please enter a valid email address."];
        return NO;
    }
    else if( ([username length] < kValidateUsernameMinimumLength) || ([username length] > kValidateUsernameMaximumLength) )
    {
        self.errorLabel.hidden = NO;
        self.errorLabel.text = @"Invalid username.Please enter Username. ";
        return NO;
    }
    else if ([password length] < kMinimumPasswordLength)
    {
        self.errorLabel.hidden = NO;
        self.errorLabel.text = @"Password too short. Password should be at least 5 characters long.";
        return NO;
    }
    else if(self.user.yearBorn == 0)
    {
        self.errorLabel.hidden = NO;
        self.errorLabel.text = kCreateNewUserInvalidBirthYearMessage;
        return NO;
    }
    else if(!self.checkmarkButton.selected){
        self.errorLabel.hidden = NO;
        self.errorLabel.text = @"Please select Terms & Conditions.";
        return NO;
    }
    return YES;
}

- (void)setValuesInUser{
    self.user.email = self.emailTextField.text;
    self.user.username = self.userNameTextField.text;
    self.user.password = self.passwordTextField.text;
    
    OXGenderTableViewCell *genderCell = (OXGenderTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:kGenderCellTableView inSection:0]];
    
    self.user.gender = genderCell.selectedGender;
    
    NSLog(@"selected gender (OXGendeTableViewCell from view controller) %lu",(unsigned long)genderCell.selectedGender);
    
}

- (void)performSignup{
    if ([self performValidation]){
        [self setValuesInUser];
        
        self.errorLabel.hidden = YES;
        
        [SVProgressHUD showWithStatus:@"Perfoming signup" maskType:SVProgressHUDMaskTypeBlack];
        
        
        [OXRestAPICient signUpWithUser:self.user successBlock:^(AFHTTPRequestOperation *operation, id response) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD showSuccessWithStatus:@"Success" maskType:SVProgressHUDMaskTypeBlack];
                
                [self showMainScreen];
                
                [[StatusHUDSBViewController sharedManager] displaySuccessStatusWithTitle:kCreateNewUserSuccessTitle message:kCreateNewUserSuccessMessage];
                
            });
            
        } failBlock:^(AFHTTPRequestOperation *operation, id error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD showErrorWithStatus:@"Signup failed" maskType:SVProgressHUDMaskTypeBlack];
                
                self.errorLabel.hidden = NO;
                
                NSDictionary * dictionary = (NSDictionary*) error;
                if ([dictionary isKindOfClass:[NSDictionary class]]) {
                    self.errorLabel.text = [dictionary getValueForKey:@"message"];
                }
                NSLog(@"Problem : %@", error);
                
            });
            
        }];
    }
}

-(void)showMainScreen{
    // TODO : Should we need to get assign saved cookies
    [[AppDelegate getAppDelegateReference] showMainScreen];
}

-(void)showWebView:(NSString*)urlString
{
    UIStoryboard * settingScreenStoryBoard = [UIStoryboard storyboardWithName:@"SettingsScreenSB" bundle:nil];
    UINavigationController *navigationController = [settingScreenStoryBoard instantiateViewControllerWithIdentifier:@"WebView"];
    OXWebViewViewController *webViewViewController = [[navigationController childViewControllers] lastObject];
    webViewViewController.URL = urlString;
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - IBAction methods

- (IBAction)closeButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)checkMarkButtonTapped:(id)sender {
    self.checkmarkButton.selected = !self.checkmarkButton.selected;
}

- (IBAction)termsAndConditionsButtonTapped:(id)sender {
    [self showWebView:@"http://oceanapp.com/eula/"];
}

- (IBAction)privacyPolicyButtonTapped:(id)sender {
     [self showWebView:@"http://oceanapp.com/privacy/"];
}

#pragma mark - PickerView Delegate Method

- (void)didSelectYear:(NSString *)year
{
    /*UILabel *yearOfBornLabel = (UILabel *)[self.yearOfBornCell viewWithTag:kYearOfBornLabelTag];
     yearOfBornLabel.text = year;
     yearOfBornLabel.textAlignment = NSTextAlignmentCenter;*/
    
    self.user.yearBorn =  [year integerValue];
    
    self.birthYearField.text = year;
    
    [self.tableView endEditing:YES];
}


@end
