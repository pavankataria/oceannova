//
//  OXMapElementModel.h
//  OceanX
//
//  Created by Systango on 2/2/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXBasePOIModel.h"

@interface OXPOIModel : OXBasePOIModel

@property (nonatomic, strong) NSArray * openingHours;
@property (nonatomic, strong) NSDictionary * socialContact;
//@property (nonatomic, assign) NSNumber * userLiked;

//TODO: converting likeOrDislike into bool value and assuming API response paramerer " likeOrDislike "'s 1 corresponds to userLiked = TRUE
@property (nonatomic, assign) BOOL userLiked;
@property (nonatomic, assign) BOOL userDisliked;

@property (nonatomic, assign) BOOL userConfused;

@property (nonatomic, strong) NSString * poiAddress;
@property (nonatomic, strong) NSArray * tags;
@property (nonatomic, strong) NSString * addedBy;

@property (nonatomic, retain) NSArray *picturesProperlyProcessed;
@property (nonatomic, retain) UIImage *mainImage;
@property (nonatomic, retain) NSString *caption;

-(instancetype)initWithDictionary:(NSDictionary*) dictionaryValue;

@end
