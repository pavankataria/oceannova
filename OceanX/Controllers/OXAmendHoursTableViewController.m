//
//  OXAmendHoursTableViewController.m
//  OceanX
//
//  Created by Pavan Kataria on 18/01/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXAmendHoursTableViewController.h"
#import "OXHoursTableViewCell.h"

@interface OXAmendHoursTableViewController ()
@property (nonatomic, retain) UITextField *hiddenTextFieldForPicker;
@property (nonatomic, retain) UIPickerView *timePicker;

@property (strong, nonatomic) NSArray *hourValues;
@property (strong, nonatomic) NSArray *minuteValues;
@property (strong, nonatomic) NSArray *meridiemValues;
@property (nonatomic, assign, getter=isWeekModified) BOOL weekModified;

@end

@implementation OXAmendHoursTableViewController

#pragma mark - Lazy Loading
-(NSArray *)hourValues{
    if(!_hourValues){
        NSMutableArray *hours = [[NSMutableArray alloc] init];
        for(int i = 0; i < 24; i++){
            [hours addObject:[NSNumber numberWithInt:i]];
        }
        _hourValues = hours;
    }
    return _hourValues;
}
-(NSArray *)minuteValues{
    if(!_minuteValues){
        NSMutableArray *minutes = [[NSMutableArray alloc] init];
        for(int i = 0; i < 60; i += 5){
            [minutes addObject:[NSNumber numberWithInt:i]];
        }
        _minuteValues = minutes;
    }
    return _minuteValues;
}
-(NSArray *)meridiemValues{
    if(!_meridiemValues){
        _meridiemValues = @[@"am", @"pm"];
    }
    return _meridiemValues;
}
-(UIPickerView *)timePicker{
    if(!_timePicker){
        
        _timePicker = [[UIPickerView alloc] init];
        _timePicker.delegate = self;
    }
    return _timePicker;
}

-(UITextField *)hiddenTextFieldForPicker{
    if(!_hiddenTextFieldForPicker){
        _hiddenTextFieldForPicker = [[UITextField alloc] init];
        _hiddenTextFieldForPicker.hidden = YES;
        [self.view addSubview:_hiddenTextFieldForPicker];
        
//        _hiddenTextFieldForPicker.inputAccessoryView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        _hiddenTextFieldForPicker.inputView = self.timePicker;
        
        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:(CGRect){{0,-44},{CGRectGetWidth(_timePicker.frame), 44}}];
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(didSelectDoneOnPickerView:)];
        
        UIBarButtonItem *barButtonClosed = [[UIBarButtonItem alloc] initWithTitle:@"Set to 'Closed'"
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(didSelectBusinessHoursClosedOnPickerView:)];
        UIBarButtonItem *flexibleSpaceBarButton = [[UIBarButtonItem alloc]
                                                   initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                   target:nil
                                                   action:nil];
            
        toolBar.items = [[NSArray alloc] initWithObjects:barButtonClosed, flexibleSpaceBarButton, barButtonDone,nil];
        [toolBar setBackgroundColor:[_timePicker backgroundColor]];
        _hiddenTextFieldForPicker.inputAccessoryView = toolBar;
    }
    return _hiddenTextFieldForPicker;
}

-(void)didSelectBusinessHoursClosedOnPickerView:(id)sender{
    NSIndexPath *selectedIndex = [self.tableView indexPathForSelectedRow];
//    NSLog(@"selected index: %lu", selectedIndex.row);
    [self.businessHours[selectedIndex.row] setBusinessOpen:NO];
    // Add them in an index path array
    NSArray* indexArray = [NSArray arrayWithObjects:selectedIndex, nil];
    // Launch reload for the two index path
    [self.tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
    [self setPickerHidden:YES];
}
-(void)didSelectDoneOnPickerView:(id)sender{
    [self setPickerHidden:YES];
}

#pragma mark - UIPickerView delegate and datasource methods

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSIndexPath *selectedIndexPath = self.tableView.indexPathForSelectedRow;
    if(!self.isWeekModified){
        if(selectedIndexPath.row > 0){
            self.weekModified = YES;
        }
    }
    OXBusinessHoursModel *currentObject = self.businessHours[selectedIndexPath.row];
    if(!currentObject){
        return;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[OXBusinessHoursModel getFormatForTimeWith24HourSystem:NO]];
    NSCalendar *calendar = [[NSCalendar alloc]  initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    NSDateComponents *openingComponents = [[NSDateComponents alloc] init];
    NSDateComponents *closingComponents = [[NSDateComponents alloc] init];
    [openingComponents setCalendar:calendar];
    [closingComponents setCalendar:calendar];


    NSUInteger openingHour = [self.timePicker selectedRowInComponent:kAmendHoursPickerViewWithoutMeridiemOpeningHour];
    NSUInteger openingMinute = [self.timePicker selectedRowInComponent:kAmendHoursPickerViewWithoutMeridiemOpeningMinute];
    NSUInteger closingHour = [self.timePicker selectedRowInComponent:kAmendHoursPickerViewWithoutMeridiemClosingHour];
    NSUInteger closingMinute = [self.timePicker selectedRowInComponent:kAmendHoursPickerViewWithoutMeridiemClosingMinute];

    [openingComponents setHour:[self.hourValues[openingHour] integerValue]];
    [openingComponents setMinute:[self.minuteValues[openingMinute] integerValue]];
    [closingComponents setHour:[self.hourValues[closingHour] integerValue]];
    [closingComponents setMinute:[self.minuteValues[closingMinute] integerValue]];
    
    currentObject.startHour = [calendar dateFromComponents:openingComponents];
    currentObject.endHour = [calendar dateFromComponents:closingComponents];
    if(!self.isWeekModified){
        [self setWithDaysArray:@[@1,@2,@3,@4] toSameAsMondayObject:currentObject andMondayIndexPath:selectedIndexPath/*willalwaysbemondaySinceisWeekModifiedCanOnlyBeTrueWhenanotherDayHasBeenSelectedWhichWontAllowThisIfBloxkTobeExecutedUnlessMondayIsSelected*/];
    }
    
    NSArray *indexPathsToReload = @[selectedIndexPath];
    [self.tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView selectRowAtIndexPath:selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

-(void)setWithDaysArray:(NSArray*)daysArray toSameAsMondayObject:(OXBusinessHoursModel*)mondayObject andMondayIndexPath:(NSIndexPath*)mondayIndexPath{
    NSMutableArray *indexPathsToReload = [[NSMutableArray alloc] init];
    
    for(NSNumber *day in daysArray){
        OXBusinessHoursModel *currentObject = self.businessHours[[day integerValue]];
        currentObject.startHour = mondayObject.startHour;
        currentObject.endHour = mondayObject.endHour;
        [indexPathsToReload addObject:[NSIndexPath indexPathForRow:[day integerValue] inSection:mondayIndexPath.section]];
    }
    
    [self.tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationFade];
    
}
-(void)setPickerHidden:(BOOL)shouldHide {
    if(shouldHide){
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
        [self.hiddenTextFieldForPicker resignFirstResponder];
    }
    else{
        [self.hiddenTextFieldForPicker becomeFirstResponder];
    }
}
-(void)updatePickerPositionWithIndexPath:(NSIndexPath*)indexPath animated:(BOOL)shouldValuesAnimate{
    [self setTimePickerWithBusinessHourObject:self.businessHours[indexPath.row] animated:shouldValuesAnimate];
    
}
-(void)setTimePickerWithBusinessHourObject:(OXBusinessHoursModel*)businessHourObject animated:(BOOL)shouldValuesAnimate{
    if([businessHourObject isBusinessOpen]){
        NSLog(@"business is open");
        [self setPickerOpeningTimeWithDate:businessHourObject.startHour animated:shouldValuesAnimate];
        [self setPickerClosingTimeWithDate:businessHourObject.endHour animated:shouldValuesAnimate];
    }
    else{
        NSLog(@"business is closed");
        [self.timePicker selectRow:0 inComponent:kAmendHoursPickerViewWithoutMeridiemOpeningHour animated:shouldValuesAnimate];
        [self.timePicker selectRow:0 inComponent:kAmendHoursPickerViewWithoutMeridiemOpeningMinute animated:shouldValuesAnimate];
        [self.timePicker selectRow:0 inComponent:kAmendHoursPickerViewWithoutMeridiemClosingHour animated:shouldValuesAnimate];
        [self.timePicker selectRow:0 inComponent:kAmendHoursPickerViewWithoutMeridiemClosingMinute animated:shouldValuesAnimate];
    }
}
-(void)setPickerOpeningTimeWithDate:(NSDate*)openingTimeDate animated:(BOOL)shouldValuesAnimate{
    [self.timePicker selectRow:[OXBusinessHoursModel getHourPositionForDate:openingTimeDate use24HourSystem:YES] inComponent:kAmendHoursPickerViewWithoutMeridiemOpeningHour animated:shouldValuesAnimate];
    [self.timePicker selectRow:[OXBusinessHoursModel getMinutePositionForDate:openingTimeDate use24HourSystem:YES] inComponent:kAmendHoursPickerViewWithoutMeridiemOpeningMinute animated:shouldValuesAnimate];
}
-(void)setPickerClosingTimeWithDate:(NSDate*)closingTimeDate animated:(BOOL)shouldValuesAnimate{
    [self.timePicker selectRow:[OXBusinessHoursModel getHourPositionForDate:closingTimeDate use24HourSystem:YES] inComponent:kAmendHoursPickerViewWithoutMeridiemClosingHour animated:shouldValuesAnimate];
    [self.timePicker selectRow:[OXBusinessHoursModel getMinutePositionForDate:closingTimeDate use24HourSystem:YES] inComponent:kAmendHoursPickerViewWithoutMeridiemClosingMinute animated:shouldValuesAnimate];
}
-(void)datePickerValueChanged:(UIDatePicker*)datePicker{
    
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return kAmendHoursPickerViewWithoutMeridiemTotalCount;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component == kAmendHoursPickerViewWithoutMeridiemOpeningHour || component == kAmendHoursPickerViewWithoutMeridiemClosingHour){
        return self.hourValues.count;
    }
    else if(component == kAmendHoursPickerViewWithoutMeridiemOpeningMinute || component == kAmendHoursPickerViewWithoutMeridiemClosingMinute){
        return self.minuteValues.count;
    }
    else{
        return 1;
    }
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *titleForRow;
    NSNumber *number;
    if(component == kAmendHoursPickerViewWithoutMeridiemOpeningHour || component == kAmendHoursPickerViewWithoutMeridiemClosingHour){
        number = self.hourValues[(row % [self.hourValues count])];
        titleForRow = [NSString stringWithFormat:@"%02d", number.intValue];
    }
    else if(component == kAmendHoursPickerViewWithoutMeridiemOpeningMinute || component == kAmendHoursPickerViewWithoutMeridiemClosingMinute){
        number = self.minuteValues[(row % [self.minuteValues count])];
        titleForRow = [NSString stringWithFormat:@"%02d", number.intValue];
    }
    else{
        titleForRow = kAmendHoursPickerViewSeparator;
    }
    return titleForRow;
}

#pragma mark - rest of the code
- (void)viewDidLoad {
    [super viewDidLoad];
    self.weekModified = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if(section == kAmendHoursTableSectionSaveChanges){
//        return 1;
//    }
//    else if(section == kAmendHoursTableSectionOpeningHours){
        return self.businessHours.count;
//    }
//    else{
//        return 0;
//    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.section == kAmendHoursTableSectionSaveChanges){
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"risavechanges"];
//        return cell;
//    }
//    else if(indexPath.section == kAmendHoursTableSectionOpeningHours){
        OXHoursTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"riammendhours"];
        [cell updateCellWithObject:self.businessHours[indexPath.row]];
        
        return cell;
//    }
//    else{
//        return nil;
//    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}
-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.tableView indexPathForSelectedRow] == indexPath){
        [self setPickerHidden:YES];
        return nil;
    }
    else{
        if(![self.hiddenTextFieldForPicker isFirstResponder]){
            [self setPickerHidden:NO];
            //Dont show rolling animation when picker isnt shown, gives the impression that its not ready
            [self updatePickerPositionWithIndexPath:indexPath animated:NO];
        }
        else{
            //But, do animate changes between time when the picker is already showing to showing cool transitioning effect
            [self updatePickerPositionWithIndexPath:indexPath animated:YES];

        }
        return indexPath;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kOXDaysSelectionTableViewRowHeight;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    if([tableView isEditing]){
//    if(section == kAmendHoursTableSectionSaveChanges){
//        return @"Once you are happy with the opening hours, tap below to save changes";
//    }
//    else if(section == kAmendHoursTableSectionOpeningHours){
//        return @"Select day(s) that you wish to ammend the opening time for and tap on `change time`.";
//    }
//    else{
//        return nil;
//    }
        return @"Update the hours as necessary";
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(IBAction)save:(id)sender{
    [self setPickerHidden:YES];
    [self.delegate performSelector:@selector(ammendHoursDidSaveBusinessHours:) withObject:self.businessHours];
    [self.view endEditing:YES];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
