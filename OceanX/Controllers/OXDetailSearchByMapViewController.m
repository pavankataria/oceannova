//
//  OXDetailSearchByMapViewController.m
//  OceanX
//
//  Created by Systango on 2/2/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

NSString * const kQuickInfoTagNameKey = @"tagName";
#import "OXDetailSearchByMapViewController.h"

@interface OXDetailSearchByMapViewController ()
@property (weak, nonatomic) IBOutlet UILabel *placeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *coordinateXLabel;
@property (weak, nonatomic) IBOutlet UILabel *coordinateYLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagsLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeStateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *mainPictureImageView;
@property (strong, nonatomic) OXPOIModel *poiModel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * loading;

@end

@implementation OXDetailSearchByMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getQuickInfo];
    // set data from |OXMapElement| model
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)setPOIDetials
{
    self.placeNameLabel.text = self.poiModel.poiName;
    self.coordinateXLabel.text = [NSString stringWithFormat:@"%f", self.poiModel.coordinates.coordinate.longitude];
    self.coordinateYLabel.text =  [NSString stringWithFormat:@"%f",self.poiModel.coordinates.coordinate.latitude];
    self.scoreLabel.text = [NSString stringWithFormat:@"%@", self.poiModel.poiScore];
    self.addressLabel.text =  self.poiModel.poiAddress;
    self.likeStateLabel.text = [NSString stringWithFormat:@"%d", self.poiModel.userLiked];
    self.tagsLabel.text = [self getTagsLabelText];
    
    // TODO: Need to show image with proper dimension.
    self.mainPictureImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[self.poiModel.pictures lastObject]]];
}

-(void)getQuickInfo{
    [self.loading startAnimating];
    [OXRestAPICient getQuickPoiInfoWithBasePOIObject:self.mapElement successBlock:^(AFHTTPRequestOperation *operation, id response) {
        [self.loading stopAnimating];
        NSLog(@"Get quick info call is a success with respone : %@",response);
        [self createPOIModelWithResponseObject:response];

    } failBlock:^(AFHTTPRequestOperation *operation, id error) {
        [self.loading stopAnimating];
        NSLog(@"Get quick info call failed with error : %@",error);
    }];
}

- (void)createPOIModelWithResponseObject:(id )response{
    if ([response isKindOfClass:[NSDictionary class]])
    {
        self.poiModel = [[OXPOIModel alloc] initWithDictionary:response];
        [self setPOIDetials];
    }
}

-(void)dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSString *)getTagsLabelText
{
    if (self.poiModel.tags.count) {
        NSArray *tagNames = [self.poiModel.tags valueForKey:@"name"];
        return [tagNames componentsJoinedByString:@", "];
    }
    return nil;
}

#pragma mark - IBAction

- (IBAction)okButtonTapped:(id)sender {
    [self dismissView];
}


@end
