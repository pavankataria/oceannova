//
//  MapsListTableViewController.m
//  OceanX
//
//  Created by admin on 13/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "MapsListTableViewController.h"
#import "MapListTableViewCell.h"
#import "OXBasePOIModel.h"
#import "UIImageView+WebCache.h"
#import "OXDetailPOIViewController.h"

@interface MapsListTableViewController (){
    
    UIStoryboard * poiDetailSB;
    
}


@property (strong, nonatomic) NSMutableArray *detailedPOIList;

@end

@implementation MapsListTableViewController

-(void)registerNibs{
    
    poiDetailSB = [UIStoryboard storyboardWithName:@"OXPOIDetailSB" bundle:nil ];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNibs];
    // init table view
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    
    // must set delegate & dataSource, otherwise the the table will be empty and not responsive
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.detailedPOIList = [[NSMutableArray alloc] init];
    
    
    
    //    self.tableView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    //        self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //        self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [self.view addSubview:self.tableView];
}

- (void)setPoiList:(NSArray *)poiList {
    
    [self.tableView setContentOffset:CGPointZero animated:NO];
    
    _poiList = poiList;
    [self.tableView reloadData];
    [self.detailedPOIList removeAllObjects];
    [self.tableView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return self.poiList.count;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"did select row at index path: %lu", (long)indexPath.row);
    
    
    OXBasePOIModel *selectedPOI = [self.poiList objectAtIndex:indexPath.row];
    
    NSLog(@"self.poiList: %@", self.poiList);
    //    OXDetailPOIViewController *destinationVC = [poiDetailSB instantiateInitialViewController];
    //    destinationVC.poiSearchObject = selectedPOI;
    
    
    
    if([self.delegate respondsToSelector:@selector(didSelectedPoi:)]){
        [self.delegate didSelectedPoi:[self.poiList objectAtIndex:indexPath.row]];
    }
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"poiCell";
    
    
    
    MapListTableViewCell *cell = (MapListTableViewCell *)[theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[MapListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    OXBasePOIModel *basePOI = [self.poiList objectAtIndex:indexPath.row];
    
    
    [self emptyCell:cell]; // to avoid the persistence of old data
    
    
    [cell.placeImageView sd_setImageWithURL:basePOI.thumbnailUrl];
    BOOL foundPOI = FALSE;
    NSLog(@"poi list count %lu", (unsigned long)self.detailedPOIList.count);
    //look through the list of detailed poi to check if we have the data
    for (OXPOIModel *poiModel in self.detailedPOIList) {
        if (poiModel.poiID == basePOI.poiID){
            OXPOIModel *detailedPOI = poiModel;
            [self setUpCell:cell withModel:detailedPOI];
            foundPOI = TRUE;
            break;
        }
    }
    //if we don't have the data, make a request
    if (!foundPOI) {
        //Server call for details
        [OXRestAPICient getQuickPoiInfoWithBasePOIObject:basePOI successBlock:^(AFHTTPRequestOperation *operation, id response) {
            NSLog(@"Get quick info call is a success with respone : %@",response);
            
            if ([response isKindOfClass:[NSDictionary class]])
            {
                OXPOIModel *detailedPOI = [[OXPOIModel alloc] initWithDictionary:response];
                
                [self.detailedPOIList addObject:detailedPOI];
                [self setUpCell:cell withModel:detailedPOI];
                
                
            }
            
        } failBlock:^(AFHTTPRequestOperation *operation, id error) {
            NSLog(@"Get quick info call failed with error : %@",error);
        }];
    }
    
    
    
    return cell;
}

-(void)emptyCell:(MapListTableViewCell *)cell{
    
    [cell setPlaceName:@""];
    [cell setAddedBy:@""];
    [cell setTagBannerTags:@[]];
    [cell setPlaceImage:nil];
    [cell setPlaceScoreImage:nil];
    
}


-(void)setUpCell:(MapListTableViewCell *)cell withModel:(OXPOIModel *)detailedPOI{
    
    [cell setPlaceName:detailedPOI.poiName];
    [cell setAddedBy:[NSString stringWithFormat:@"added by %@", detailedPOI.addedBy]];
    [cell setTagBannerTags:detailedPOI.tags];
    
    CGFloat poiScore = detailedPOI.poiScore.floatValue;
    [cell setPlaceScoreImage:[self getImageForScore:poiScore]];
    
}

-(UIImage*)getImageForScore:(CGFloat)poiScore
{
    
    UIImage *poiScoreImage;
    
    if(poiScore < 0.2){
        poiScoreImage = [UIImage imageNamed:@"veryUnamused"];
    }
    else if(poiScore < 0.4){
        poiScoreImage = [UIImage imageNamed:@"unamused"];
    }
    else if(poiScore < 0.6){
        poiScoreImage = [UIImage imageNamed:@"neutral"];
    }
    else if(poiScore < 0.8){
        poiScoreImage = [UIImage imageNamed:@"happy"];
    }
    else {
        poiScoreImage = [UIImage imageNamed:@"veryhappy"];
    }
    
    return poiScoreImage;
}


#pragma mark - UITableViewDelegate
/*- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
 NSLog(@"selected %ld row", (long)indexPath.row);
 if([self.delegate respondsToSelector:@selector(didSelectedPoi:)])
 [self.delegate didSelectedPoi:[self.poiList objectAtIndex:indexPath.row]];
 }*/

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100;
}


@end