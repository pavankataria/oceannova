//
//  OXPOISearchModelDelegate.h
//  OceanX
//
//  Created by Pavan Kataria on 06/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//
#import "OXPOISearchModel.h"

@protocol OXPOISearchModelDelegate <NSObject>
@optional
-(void)OXPOISearchModelUpdatePOISearchObject:(OXPOISearchModel*)object;
-(void)setDelegate:(id<OXPOISearchModelDelegate>)delegate;
-(void)setSearchPOIModel:(OXPOISearchModel*)object;
@end
