//
//  OXSearchPOIForSectionViewOperationProtocol.h
//  OceanX
//
//  Created by Pavan Kataria on 04/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OXSearchPOIForSectionViewOperationProtocol <NSObject>
@required
-(NSString*)OXSearchPOIForSectionViewOperationPoiName;
-(NSString*)OXSearchPOIForSectionViewOperationPoiAddedBy;
-(CLLocationCoordinate2D)OXSearchPOIForSectionViewOperationPoiCoordinate;
-(UIImage*)OXSearchPOIForSectionViewOperationPoiScoreImage;
@end


