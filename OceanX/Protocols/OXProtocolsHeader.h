//
//  OXProtocolsHeader.h
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import "OXTagModelOperationProtocol.h"
#import "OXPOISearchModelDelegate.h"

#import "OXSearchTableViewCellOperationProtocol.h"