//
//  OXFlurryProtocol.h
//  Ocean
//
//  Created by Vinita Rathi on 3/25/15.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

@protocol OXFlurryProtocol <NSObject>
@optional
-(NSDictionary *)getFlurryParameters;
-(NSDictionary *)getFlurryParametersWithEventType:(NSUInteger)eventType;
@end
