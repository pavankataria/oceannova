//
//  OXSearchContainerScreenViewControllerOperationProtocol.h
//  OceanX
//
//  Created by Pavan Kataria on 05/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OXPOISearchByListModel.h"
@protocol OXSearchContainerScreenViewControllerOperationProtocol <NSObject>

@optional
-(NSMutableArray*)OXSearchContainerSharedSearchResultsArray;
-(void)performSearchAndSaveInParent;
-(void)performSearchWithSuccessBlock:(SuccessCompletionBlock)successblock andFailureBlock:(FailureCompletionBlock)failureBlock;
-(UITextField*)grabSearchTextField;
-(OXPOISearchByListModel*)OXSearchContainerSharedSearchByListObject;

-(OXPOISearchModel*)getSharedPOISearchModel;
@end


@protocol OXSearchContainerScreenViewControllerDelegate <NSObject>

@required

-(void)textFieldDidChangeWithText:(NSString*)text;
@end