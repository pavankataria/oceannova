//
//  OXDetailPOIProtocol.h
//  OceanX
//
//  Created by Pavan Kataria on 19/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

@protocol OXDetailPOIProtocol <NSObject>

@required
-(void)updateDetailPOIScrollViewContentSize;
@optional
-(void)displayAddTagsViewController;
-(void)displayAddTagsViewControllerWithControllerType:(OXTagsPresentationTableViewComingFromVCType)comingFromVCType;

-(void)reportButtonPressed;
@end