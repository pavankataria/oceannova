//
//  SignupPageTableViewProtocol.h
//  Ocean
//
//  Created by Pavan Kataria on 12/03/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SignupPageTableViewProtocol <NSObject>

@end

@protocol SignupPageTableViewConfiguration <NSObject>
-(NSString*)propertyName;
@end

