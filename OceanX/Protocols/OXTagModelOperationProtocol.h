//
//  OXTagModelOperationProtocol.h
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OXTagModelOperationProtocol <NSObject>
@required
-(NSString*)OXTagModelOperationTagNameString;
@optional
-(NSString*)OXTagModelOperationPlaceCountString;
-(BOOL)OXTagModelOperationShouldCreateNewTag;
@end


@protocol OXTagsProtocolConfiguration <NSObject>
@required
- (NSString*)tagName;
@optional
-(NSUInteger)tagCount;
-(NSUInteger)tagId;
-(NSUInteger)tagHasAffirmed;
@end