//
//  OXSearchTableViewCellOperationProtocol.h
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

@protocol OXSearchTableViewCellOperationProtocol <NSObject>
@optional
-(void)startAnimatingActivityIndicatorView;
-(void)setCellWithObjectFollowingSearchModelProtocol:(id<OXSearchModelOperationProtocol>)object;
@end
