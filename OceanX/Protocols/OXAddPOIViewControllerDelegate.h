//
//  OXAddPOIViewControllerDelegate.h
//  OceanX
//
//  Created by Pavan Kataria on 24/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//


@protocol OXAddPOIViewControllerDelegate <NSObject>

-(void)addPOIShowEditImagesWithViewController:(NSArray*)array;

@end
