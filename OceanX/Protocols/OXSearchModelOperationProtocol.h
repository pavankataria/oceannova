//
//  OXSearchModelOperationProtocol.h
//  OceanX
//
//  Created by Pavan Kataria on 07/02/2015.
//  Copyright (c) 2015 Pavan Kataria. All rights reserved.
//

@protocol OXSearchModelOperationProtocol <NSObject>
@required
-(NSString*)searchResultTitle;

@end